#include "MultiKalmanFilter.h"
#include "HungarianAlg.h"

CMultiKalmanFilter::CMultiKalmanFilter()
{
	m_dist_thres = 40.0;

	m_nextID = 0;

	m_typeMotionModel = "ConstantVelocity";

	if (m_typeMotionModel == "ConstantVelocity")
		m_dimStateVariable = 4;
	else
		m_dimStateVariable = 6;

	m_dimStateMeasure = 2;
}

CMultiKalmanFilter::~CMultiKalmanFilter()
{
}

void CMultiKalmanFilter::DrawResult(Mat &imgDisplay)
{
	int minVisibleCount = 5;

	for (int i = 0; i < (int)m_vecTracks.size(); i++)
	{
		if (m_vecTracks[i].cntTotalVisible <= (unsigned)minVisibleCount)
			continue;

		Point2f cent;
		cent.x = m_vecTracks[i].objKF.statePost.at<float>(0);
		cent.y = m_vecTracks[i].objKF.statePost.at<float>(m_dimStateVariable / 2);
		Rect_<float> rect = m_vecTracks[i].rectBB;
		rect.x = cent.x - rect.width / 2.f;
		rect.y = cent.y - rect.height / 2.f;

		rectangle(imgDisplay, rect, CV_RGB(255, 255, 0));

		char szTemp[100];
		sprintf(szTemp, "ID:%d", m_vecTracks[i].nID);
		if (m_vecTracks[i].cntConsecutiveInvisible > 0)
			sprintf(szTemp, "%s Predict", szTemp);

		putText(imgDisplay, szTemp, cent, CV_FONT_HERSHEY_PLAIN, 1, CV_RGB(255, 255, 0), 1);
	}
}

void CMultiKalmanFilter::Track(const vector<Rect_<int> > &rectDetectBB)
{
	PredictNewLocationOfTracks();

	detectionToTrackAssignment(rectDetectBB);

	updateAssignedTracks(rectDetectBB);

	updateUnassignedTracks();

	deleteLostTracks();

	createNewTracks(rectDetectBB);
}

void CMultiKalmanFilter::createNewTracks(const vector<Rect_<int> > &rectDetectBB)
{
	for (int i = 0; i < (int)m_vecUnassignedDetections.size(); i++)
	{
		int j = m_vecUnassignedDetections[i];

		STrackInfo ti;

		ti.rectBB = rectDetectBB[j];

		ti.ptCentroid = Point2f(ti.rectBB.x + ti.rectBB.width / 2.f, ti.rectBB.y + ti.rectBB.height / 2.f);

		configureKalmanFilter(ti.objKF, m_typeMotionModel, ti.ptCentroid);

		ti.cntAge = 1;
		ti.cntTotalVisible = 1;
		ti.cntConsecutiveInvisible = 0;
		ti.nID = m_nextID++;

		m_vecTracks.push_back(ti);
	}
}

void CMultiKalmanFilter::configureKalmanFilter(KalmanFilter &KF, string strModelType, Point2f centroid)
{
	int nSubDim = m_dimStateVariable / 2;

	Mat tempA = Mat::eye(nSubDim, nSubDim, CV_32FC1);
	for (int i = 0; i < nSubDim - 1; i++) tempA.at<float>(i, i + 1) = 1;

	Mat tempH = Mat::zeros(1, nSubDim, CV_32FC1);
	tempH.at<float>(0, 0) = 1;

	float Q[] = { 100, 25, 1 };
	Mat tempQ(m_dimStateVariable, 1, CV_32FC1);
	for (int i = 0; i < m_dimStateVariable; i++)
		tempQ.at<float>(i) = Q[i % nSubDim];

	float R = 200;
	Mat tempR(2, 1, CV_32FC1, R);

	float cov[] = { 200, 50, 10 };
	Mat tempCov(m_dimStateVariable, 1, CV_32FC1);
	for (int i = 0; i < m_dimStateVariable; i++)
		tempCov.at<float>(i) = cov[i % nSubDim];

	// KF
	KF.init(m_dimStateVariable, m_dimStateMeasure);

	tempA.copyTo(KF.transitionMatrix(Range(0, nSubDim), Range(0, nSubDim)));
	tempA.copyTo(KF.transitionMatrix(Range(nSubDim, m_dimStateVariable), Range(nSubDim, m_dimStateVariable)));

	tempH.copyTo(KF.measurementMatrix(Range(0, 1), Range(0, nSubDim)));
	tempH.copyTo(KF.measurementMatrix(Range(1, 2), Range(nSubDim, m_dimStateVariable)));

	KF.processNoiseCov = Mat::diag(tempQ);
	KF.measurementNoiseCov = Mat::diag(tempR);

	KF.errorCovPost = Mat::diag(tempCov);

	KF.statePost.at<float>(0) = centroid.x;
	KF.statePost.at<float>(m_dimStateVariable / 2) = centroid.y;
}

void CMultiKalmanFilter::PredictNewLocationOfTracks()
{
	for (auto track : m_vecTracks)
	{
		Rect_<float> &r = track.rectBB;

		track.objKF.predict();

		track.ptCentroid.x = track.objKF.statePre.at<float>(0);
		track.ptCentroid.y = track.objKF.statePre.at<float>(m_dimStateVariable / 2);

		r.x = track.ptCentroid.x - r.width / 2.f;
		r.y = track.ptCentroid.y - r.height / 2.f;
	}
}

void CMultiKalmanFilter::detectionToTrackAssignment(const vector<Rect_<int> > &rectDetectBB)
{
	int N = (int)m_vecTracks.size();
	int M = (int)rectDetectBB.size();

	vector<vector<double> > Cost(N, vector<double>(M));

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++)
		{
			Point2f cent(rectDetectBB[j].x + rectDetectBB[j].width / 2.f, rectDetectBB[j].y + rectDetectBB[j].height / 2.f);
			Point2f diff = (m_vecTracks[i].ptCentroid - cent);
			double dist = norm(diff);
			Cost[i][j] = dist;
		}
	}

	vector<int> assignment;
	if (N != 0)
	{
		AssignmentProblemSolver APS;
		APS.Solve(Cost, assignment, AssignmentProblemSolver::optimal);
	}
	
	m_vecAssignments.clear();
	m_vecUnassignedTracks.clear();
	m_vecUnassignedDetections.clear();

	for (int i = 0; i < (int)assignment.size(); i++)
	{
		if (assignment[i] != -1)
		{
			if (Cost[i][assignment[i]] <= m_dist_thres)
				m_vecAssignments.push_back(pair<int, int>(i, assignment[i]));
			else
				m_vecUnassignedTracks.push_back(i);
		}
		else
			m_vecUnassignedTracks.push_back(i);
	}

	for (int i = 0; i < (int)rectDetectBB.size(); i++)
	{
		auto it = find(assignment.begin(), assignment.end(), i);
		if (it == assignment.end())
			m_vecUnassignedDetections.push_back(i);
	}
}

void CMultiKalmanFilter::updateAssignedTracks(const vector<Rect_<int> > &rectDetectBB)
{
	for (int i = 0; i < (int)m_vecAssignments.size(); i++)
	{
		int idxT = m_vecAssignments[i].first;
		int idxD = m_vecAssignments[i].second;

		Mat meas = Mat::zeros(2, 1, CV_32FC1);
		meas.at<float>(0) = rectDetectBB[idxD].x + rectDetectBB[idxD].width / 2.f;
		meas.at<float>(1) = rectDetectBB[idxD].y + rectDetectBB[idxD].height / 2.f;

		m_vecTracks[idxT].objKF.correct(meas);

		m_vecTracks[idxT].rectBB = rectDetectBB[idxD];

		m_vecTracks[idxT].cntAge++;
		m_vecTracks[idxT].cntTotalVisible++;
		m_vecTracks[idxT].cntConsecutiveInvisible = 0;
	}
}

void CMultiKalmanFilter::updateUnassignedTracks()
{
	for (int i = 0; i < (int)m_vecUnassignedTracks.size(); i++)
	{
		int idx = m_vecUnassignedTracks[i];

		m_vecTracks[idx].cntAge++;
		m_vecTracks[idx].cntConsecutiveInvisible++;
	}
}

void CMultiKalmanFilter::deleteLostTracks()
{
	if ((int)m_vecTracks.size() == 0)
		return;

	unsigned int invisibleForTooLong = 3;
	unsigned int ageThreshold = 8;

	for (int i = 0; i < (int)m_vecTracks.size(); i++)
	{
		float fVisibility = (float)m_vecTracks[i].cntTotalVisible / (float)m_vecTracks[i].cntAge;

		if ((m_vecTracks[i].cntAge < ageThreshold && fVisibility < 0.6f) || m_vecTracks[i].cntConsecutiveInvisible >= invisibleForTooLong)
		{
			m_vecTracks.erase(m_vecTracks.begin() + i);
			i--;
		}
	}
}