#pragma once

#include "DBSequence.h"

class CVideoPlayer
{
public:
	CVideoPlayer(void);
	virtual ~CVideoPlayer(void);

	enum EDBtype { DB_UNDEFINED = -1, DB_IMAGE = 0, DB_VIDEO = 1 };

	bool LoadDBFile(EDBtype eImageOrVideo, string strPathOrFileName);
	bool UnloadDBFile();

	bool ReadCurrFrame(Mat& img);
	virtual bool GoToSpecificFrame(int nSpecFrame);
	bool GoToNextFrame();
	bool GoToPrevFrame();

	inline Size2i GetFrameSize() const { return m_objDBSeq->GetFrameSize(); }// temp
	inline int GetFrameChannel() const { return m_objDBSeq->GetFrameChannel(); }// temp

	bool operator++(int);
	bool operator--(int);
	bool operator>>(Mat& img);

protected:
	EDBtype m_eDBmode;
	int m_nTotalFrame;
	Size2i m_sizeFrame;
	int m_nCurrFrame;
	string m_strPathAbsolute;
	CDBSequence *m_objDBSeq;
};

