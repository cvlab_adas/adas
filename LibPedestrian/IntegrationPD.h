#pragma once

#include "../include/DefStruct.h"
#include "PedestrianACF2.h"

using namespace cv;

class CIntegrationPD
{
public:
	CIntegrationPD();
	CIntegrationPD(bool &flgReady);
	~CIntegrationPD();

	void RunOnce(Mat &imgInput,vector<Object_t> &vecobjPed);

private:
	CPedestrianACF2 m_objPD;
	double boxoverlap(Rect_<double> a, Rect_<double> b, int32_t criterion = -1);
};

