#include "PedestrianCascade.h"


CPedestrianCascade::CPedestrianCascade()
{
}

CPedestrianCascade::~CPedestrianCascade()
{
}

bool CPedestrianCascade::LoadClassifier(string strClassifierFile)
{
	if (!m_objClassifier.load(strClassifierFile))
		return false;

	return true;
}

void CPedestrianCascade::Detect(Mat& imgSrc)
{
	m_vecrectResultBB.clear();

	bool bEmptyROI = false;
	if (m_vecrectROIs.empty())
		bEmptyROI = true;

	for (int i = 0; i < (int)m_vecrectROIs.size() || bEmptyROI; i++)
	{
		Mat imgROI;
		if (bEmptyROI)
			imgROI = imgSrc;
		else
			imgROI = imgSrc(m_vecrectROIs[i]);

		vector<Rect_<int> > vecrectDetected;

		//m_objClassifier.detectMultiScale(imgROI, vecrectDetected, 1.1, 3, 0, Size(24, 48), Size(96, 192));
		m_objClassifier.detectMultiScale(imgROI, vecrectDetected);

		for (int j = 0; j < (int)vecrectDetected.size(); j++)
		{
			vecrectDetected[j].x += bEmptyROI ? 0 : m_vecrectROIs[i].x;
			vecrectDetected[j].y += bEmptyROI ? 0 : m_vecrectROIs[i].y;

			m_vecrectResultBB.push_back(vecrectDetected[j]);
		}

		bEmptyROI = false;
	}
}
