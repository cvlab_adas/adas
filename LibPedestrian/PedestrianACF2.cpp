#include "PedestrianACF2.h"
#include <algorithm>
#include <functional>
#include <typeinfo>

#include <chrono> //★★★★★


CPedestrianACF2::CPedestrianACF2() : m_fWidth(0.f), m_fHeight(0.f)
{
	m_nChns = 10; // number of L,U,V,Hist,Orient1,2,3,4,5,6
	m_fLambda[0] = 0;			m_fLambda[1] = 0;
	m_fLambda[2] = 0;			m_fLambda[3] = 0.11047f;
	m_fLambda[4] = 0.10826f;	m_fLambda[5] = 0.10826f;
	m_fLambda[6] = 0.10826f;	m_fLambda[7] = 0.10826f;
	m_fLambda[8] = 0.10826f;	m_fLambda[9] = 0.10826f;

	m_nRadius = 5;
	m_nDownSampling = 1;
	m_fNormConst = 0.005f;

	m_nBinSize = 2;
	m_nOrients = 6;
	m_nSoftBin = 1;
	m_bFullOrient = false;

	//for detection
	m_fDetectSizeW = 20.5f;
	m_fDetectSizeH = 50.f;
	m_nPadW = 0;// 8;
	m_nPadH = 0;// 6;
	m_nModelPadH = 64;
	m_nModelPadW = 32;

	m_nTreeNodes = 63;
	m_nTrees = 4096;
	m_pThrs = new float[m_nTreeNodes*m_nTrees];
	m_pHs = new float[m_nTreeNodes*m_nTrees];
	m_pFids = new unsigned int[m_nTreeNodes*m_nTrees];
	m_pChild = new unsigned int[m_nTreeNodes*m_nTrees];
}

CPedestrianACF2::~CPedestrianACF2()
{
	//thread 해제 ★★★★★
	if (m_flgThread)
	{
		m_flgThread = false;
		m_threadDetection.join();
	}

	delete[] m_pThrs;
	delete[] m_pHs;
	delete[] m_pFids;
	delete[] m_pChild;
}

bool CPedestrianACF2::LoadClassifier(string strClassifierFile)
{
	FILE *fp = fopen(strClassifierFile.c_str(), "rt");
	if (fp == NULL) return false;

	for (int i = 0; i < m_nTreeNodes*m_nTrees; i++)
		fscanf(fp, "%f", m_pThrs + i);
	for (int i = 0; i < m_nTreeNodes*m_nTrees; i++)
		fscanf(fp, "%f", m_pHs + i);
	for (int i = 0; i < m_nTreeNodes*m_nTrees; i++)
		fscanf(fp, "%d", m_pFids + i);
	for (int i = 0; i < m_nTreeNodes*m_nTrees; i++)
		fscanf(fp, "%d", m_pChild + i);

	fclose(fp);

	return true;
}

void CPedestrianACF2::Detect(Mat& imgSrc)
{
	m_mtxDetectedBB.lock();
	m_vecrectResultBB.clear();
	m_mtxDetectedBB.unlock();

	bool bEmptyROI = false;
	if (m_vecrectROIs.empty())
		bEmptyROI = true;

	Rect_<int> rectPad(0, 0, imgSrc.cols, imgSrc.rows);
	int nRawPad = 4 - rectPad.height % 4;//16 - rectPad.height % 16;
	int nColPad = 0;// 16 - rectPad.width % 16;
	Mat imgInputPad = Mat::zeros(imgSrc.rows + nRawPad, imgSrc.cols + nColPad, CV_8UC3);
	imgSrc.copyTo(imgInputPad(rectPad));

	vector<pair<float, Rect_<float> > > vecShiftBB;

	for (int i = 0; i < (int)m_vecrectROIs.size() || bEmptyROI; i++)
	{
		Mat imgROI;
		if (bEmptyROI)
			imgROI = imgInputPad;
		else
			imgROI = imgInputPad(m_vecrectROIs[i]);

		if ((float)imgROI.cols < m_fDetectSizeW || (float)imgROI.rows < m_fDetectSizeH)
			continue;

		Mat imgROIt;
		transpose(imgROI, imgROIt);
		cvtColor(imgROIt, imgROIt, CV_BGR2RGB);

		vector<Mat> vecimgChnFtrs;
		BuildPyramid(imgROIt, vecimgChnFtrs);

		//detection
		vector<pair<float, Rect_<float> > > vecRawBB;
		for (int j = 0; j < (int)vecimgChnFtrs.size(); j++)
		{
			vecRawBB.clear();
			AcfDetect(vecimgChnFtrs[j], vecRawBB);

			float shiftH = ((float)m_nModelPadH - m_fDetectSizeH) / 2.f - (float)m_nPadW;
			float shiftW = ((float)m_nModelPadW - m_fDetectSizeW) / 2.f - (float)m_nPadH;

			for (int k = 0; k < (int)vecRawBB.size(); k++)
			{
				vecRawBB[k].second.x = (vecRawBB[k].second.x + shiftW) / m_vecfScalesHW[j].second;
				vecRawBB[k].second.y = (vecRawBB[k].second.y + shiftH) / m_vecfScalesHW[j].first;
				vecRawBB[k].second.width = m_fDetectSizeW / m_vecfScales[j];
				vecRawBB[k].second.height = m_fDetectSizeH / m_vecfScales[j];

				vecRawBB[k].second.x += bEmptyROI ? 0 : m_vecrectROIs[i].x;
				vecRawBB[k].second.y += bEmptyROI ? 0 : m_vecrectROIs[i].y;

				vecShiftBB.push_back(vecRawBB[k]);
			}
		}

		bEmptyROI = false;
	}

	m_mtxDetectedBB.lock();
	m_vecrectResultBB = bbNMS(vecShiftBB);
	m_mtxDetectedBB.unlock();
}

void CPedestrianACF2::BuildPyramid(Mat &imgRGB, vector<Mat> &vecimgChnFtrs)
{
	// step 1 : RGB to LUV
	Mat imgLUV = Mat::zeros(imgRGB.rows*imgRGB.channels(), imgRGB.cols, CV_32FC1);
	float* pImgLUV = (float*)imgLUV.data;
	ConvertRGB2LUV(imgRGB, pImgLUV); // 3이 아닐 때를 고려하지 않음. 즉, gray 일 때 어떻게 되는지 확인이 필요함

	// step 2 : get pyramid scales
	if ((int)m_fWidth != imgRGB.rows || (int)m_fHeight != imgRGB.cols)
	{
		m_fWidth = (float)imgRGB.rows;
		m_fHeight = (float)imgRGB.cols;
		GetPyramidScales();
	}

	vecimgChnFtrs.resize(m_nScales);

	// step 3 : channel computation along the real scales
	int nWidthOrg = imgRGB.rows;
	int nHeightOrg = imgRGB.cols;
	vector<int> vecRef;
	for (int i = 0; i < m_nScales; i += m_nApprox) // "isR" part
	{
		float s = m_vecfScales[i];
		int nDownHeight = (int)(round(m_fHeight * s / m_fShrink)*m_fShrink);
		int nDownWidth = (int)(round(m_fWidth* s / m_fShrink)*m_fShrink);

		float *pResizedLUV;
		bool bNew = false;

		if ((int)m_fWidth == nDownWidth && (int)m_fHeight == nDownHeight)
			pResizedLUV = pImgLUV;
		else
		{
			pResizedLUV = new float[nDownWidth*nDownHeight*imgRGB.channels()]; bNew = true;
			ResampleImage(pImgLUV, pResizedLUV, nHeightOrg, nDownHeight, nWidthOrg, nDownWidth, imgRGB.channels(), 1.f);

			if (s == .5f && (m_nApprox > 0 || m_fPerOct == 1.f))
			{
				Mat imgTemp(nDownWidth*imgRGB.channels(), nDownHeight, CV_32FC1, pResizedLUV);

				nWidthOrg = nDownWidth;
				nHeightOrg = nDownHeight;

				imgLUV = imgTemp.clone();
				pImgLUV = (float*)imgLUV.data;
			}
		}

		Mat imgResizedLUV(nDownWidth*imgRGB.channels(), nDownHeight, CV_32FC1, pResizedLUV);

		ComputeChannelFeature(imgResizedLUV, vecimgChnFtrs, nDownHeight, nDownWidth, imgRGB.channels(), i);

		if (bNew) delete[] pResizedLUV;

		if (i + m_nApprox < m_nScales) // for step 4.
		{
			int j = (int)((i + i + m_nApprox) / 2);
			vecRef.push_back(j);
		}
	}

	// step 4 : channel computation along the approx. scales
	vecRef.push_back(m_nScales - 1);
	for (int i = 0, jcnt = 0; i < m_nScales; i++) // "isA" part
	{
		if (i % m_nApprox == 0)
			continue;

		if (i <= vecRef[jcnt])
		{
			int iR = jcnt*m_nApprox;
			int hR = vecimgChnFtrs[iR].cols;
			int wR = vecimgChnFtrs[iR].rows / m_nChns;

			int h1 = (int)round(m_fHeight*m_vecfScales[i] / m_fShrink);
			int w1 = (int)round(m_fWidth*m_vecfScales[i] / m_fShrink);

			vecimgChnFtrs[i] = Mat::zeros(w1*m_nChns, h1, CV_32FC1);

			for (int j = 0; j < m_nChns; j++)
			{
				float ratio_ = pow(m_vecfScales[i] / m_vecfScales[iR], -m_fLambda[j]);

				Mat imgApprox = vecimgChnFtrs[i](Rect_<int>(0, w1*j, h1, w1));
				float *pImgApprox = (float*)imgApprox.data;

				Mat imgChnFtrs = vecimgChnFtrs[iR](Rect_<int>(0, wR*j, hR, wR));
				float *pImgChnFtrs = (float*)imgChnFtrs.data;

				ResampleImage(pImgChnFtrs, pImgApprox, hR, h1, wR, w1, 1, ratio_);
			}
		}
		else
		{
			jcnt++;
			i--;
		}
	}

	// step 5 : smooth
	SmoothChannel(vecimgChnFtrs);
}

void CPedestrianACF2::AcfDetect(Mat &imgChnFtrs, vector<pair<float, Rect_<float> > >& vecRetVal)
{
	const int stride = 4;
	const int shrink = (int)m_fShrink;
	const float cascThr = -1.f;

	int width = imgChnFtrs.rows / m_nChns;
	int height = imgChnFtrs.cols;
	const int height1 = (int)ceil(float(height*shrink - m_nModelPadH + 1) / stride);
	const int width1 = (int)ceil(float(width*shrink - m_nModelPadW + 1) / stride);

	// construct cids array
	int nFtrs = m_nModelPadH / shrink * m_nModelPadW / shrink * m_nChns;

	uint32 *cids = new uint32[nFtrs];
	int m = 0;

	for (int z = 0; z < m_nChns; z++)
		for (int c = 0; c < m_nModelPadW / shrink; c++)
			for (int r = 0; r < m_nModelPadH / shrink; r++)
				cids[m++] = z*width*height + c*height + r;

	// apply classifier to each patch
	vector<int> rs, cs;
	vector<float> hs1;

	float *chns = (float*)imgChnFtrs.data;

	for (int c = 0; c < width1; c++)
	{
		for (int r = 0; r < height1; r++)
		{
			float h = 0;
			float* chns1 = chns + (r*stride / shrink) + (c*stride / shrink)*height;

			// general case (variable tree depth)
			for (int t = 0; t < m_nTrees; t++)
			{
				uint32 offset = t*m_nTreeNodes;
				uint32 k = offset;
				uint32 k0 = k;
				while (m_pChild[k])
				{
					float ftr = chns1[cids[m_pFids[k]]];
					k = (ftr < m_pThrs[k]) ? 1 : 0;
					k0 = k = m_pChild[k0] - k + offset;
				}
				h += m_pHs[k];
				if (h <= cascThr) // rejecting
					break;
			}
			if (h > cascThr)
			{
				cs.push_back(c);
				rs.push_back(r);
				hs1.push_back(h);
			}
		}
	}
	delete[] cids;
	m = cs.size();

	pair<float, Rect_<float> > pair1;
	for (int i = 0; i < m; i++)
	{
		pair1.first = hs1[i];
		pair1.second = Rect_<float>((float)(cs[i] * stride), (float)(rs[i] * stride), (float)m_nModelPadW, (float)m_nModelPadH);
		vecRetVal.push_back(pair1);
	}
}

// step 1
void CPedestrianACF2::ConvertRGB2LUV(Mat &imgRGB, float* pImgLUV)
{
	vector<Mat> vecImgRGB;
	split(imgRGB, vecImgRGB);

	int h = imgRGB.cols;
	int w = imgRGB.rows;
	int d = imgRGB.channels();
	int n = h*w;

	Mat imgRGBwide = Mat::zeros(w*d, h, CV_8UC1);
	for (int i = 0; i < (int)vecImgRGB.size(); i++)
		vecImgRGB[i].copyTo(imgRGBwide(Rect_<int>(0, w*i, h, w)));

	unsigned char *pImgRGBwide = (unsigned char*)imgRGBwide.data;

#if USE_SSE_HR
	bool useSSE = n % 4 == 0;
	if (useSSE)
		for (int i = 0; i < d / 3; i++)
			rgb2luv_sse(pImgRGBwide + i*n * 3, (float*)(pImgLUV + i*n * 3), n, 1.f / 255);
	else
		for (int i = 0; i < d / 3; i++)
			rgb2luv(pImgRGBwide + i*n * 3, pImgLUV + i*n * 3, n, 1.f / 255);
#else
	for (int i = 0; i < d / 3; i++)
		rgb2luv(pImgRGBwide + i*n * 3, pImgLUV + i*n * 3, n, 1.f / 255);
#endif
}

// Constants for rgb2luv conversion and lookup table for y-> l conversion
float* CPedestrianACF2::rgb2luv_setup(float z, float *mr, float *mg, float *mb,
	float &minu, float &minv, float &un, float &vn)
{
	// set constants for conversion
	const float y0 = (float)((6.0 / 29)*(6.0 / 29)*(6.0 / 29));
	const float a = (float)((29.0 / 3)*(29.0 / 3)*(29.0 / 3));
	un = (float) 0.197833; vn = (float) 0.468331;
	mr[0] = (float) 0.430574*z; mr[1] = (float) 0.222015*z; mr[2] = (float) 0.020183*z;
	mg[0] = (float) 0.341550*z; mg[1] = (float) 0.706655*z; mg[2] = (float) 0.129553*z;
	mb[0] = (float) 0.178325*z; mb[1] = (float) 0.071330*z; mb[2] = (float) 0.939180*z;
	float maxi = (float) 1.0 / 270; minu = -88 * maxi; minv = -134 * maxi;
	// build (padded) lookup table for y->l conversion assuming y in [0,1]
	static float lTable[1064]; static bool lInit = false;
	if (lInit) return lTable; float y, l;
	for (int i = 0; i < 1025; i++) {
		y = (float)(i / 1024.0);
		l = y > y0 ? 116 * (float)pow((double)y, 1.0 / 3.0) - 16 : y*a;
		lTable[i] = l*maxi;
	}
	for (int i = 1025; i < 1064; i++) lTable[i] = lTable[i - 1];
	lInit = true; return lTable;
}

// Convert from rgb to luv
void CPedestrianACF2::rgb2luv(unsigned char *I, float *J, int n, float nrm) {
	float minu, minv, un, vn, mr[3], mg[3], mb[3];
	float *lTable = rgb2luv_setup(nrm, mr, mg, mb, minu, minv, un, vn);
	float *L = J, *U = L + n, *V = U + n; unsigned char *R = I, *G = R + n, *B = G + n;
	for (int i = 0; i < n; i++) {
		float r, g, b, x, y, z, l;
		r = (float)*R++; g = (float)*G++; b = (float)*B++;
		x = mr[0] * r + mg[0] * g + mb[0] * b;
		y = mr[1] * r + mg[1] * g + mb[1] * b;
		z = mr[2] * r + mg[2] * g + mb[2] * b;
		l = lTable[(int)(y * 1024)];
		*(L++) = l; z = 1 / (x + 15 * y + 3 * z + (float)1e-35);
		*(U++) = l * (13 * 4 * x*z - 13 * un) - minu;
		*(V++) = l * (13 * 9 * y*z - 13 * vn) - minv;
	}
}

// Convert from rgb to luv using sse
void CPedestrianACF2::rgb2luv_sse(unsigned char *I, float *J, int n, float nrm) {
	const int k = 256; float R[k], G[k], B[k];
	if ((size_t(R) & 15 || size_t(G) & 15 || size_t(B) & 15 || size_t(I) & 15 || size_t(J) & 15)
		|| n % 4>0) {
		rgb2luv(I, J, n, nrm); return;
	}
	int i = 0, i1, n1; float minu, minv, un, vn, mr[3], mg[3], mb[3];
	float *lTable = rgb2luv_setup(nrm, mr, mg, mb, minu, minv, un, vn);
	while (i < n) {
		n1 = i + k; if (n1 > n) n1 = n; float *J1 = J + i; float *R1, *G1, *B1;
		// convert to floats (and load input into cache)
		if (typeid(unsigned char) != typeid(float)) {
			R1 = R; G1 = G; B1 = B; unsigned char *Ri = I + i, *Gi = Ri + n, *Bi = Gi + n;
			for (i1 = 0; i1 < (n1 - i); i1++) {
				R1[i1] = (float)*Ri++; G1[i1] = (float)*Gi++; B1[i1] = (float)*Bi++;
			}
		}
		else { R1 = ((float*)I) + i; G1 = R1 + n; B1 = G1 + n; }
		// compute RGB -> XYZ
		for (int j = 0; j < 3; j++) {
			__m128 _mr, _mg, _mb, *_J = (__m128*) (J1 + j*n);
			__m128 *_R = (__m128*) R1, *_G = (__m128*) G1, *_B = (__m128*) B1;
			_mr = SET(mr[j]); _mg = SET(mg[j]); _mb = SET(mb[j]);
			for (i1 = i; i1 < n1; i1 += 4) *(_J++) = ADD(ADD(MUL(*(_R++), _mr),
				MUL(*(_G++), _mg)), MUL(*(_B++), _mb));
		}
	{ // compute XZY -> LUV (without doing L lookup/normalization)
		__m128 _c15, _c3, _cEps, _c52, _c117, _c1024, _cun, _cvn;
		_c15 = SET(15.0f); _c3 = SET(3.0f); _cEps = SET(1e-35f);
		_c52 = SET(52.0f); _c117 = SET(117.0f), _c1024 = SET(1024.0f);
		_cun = SET(13 * un); _cvn = SET(13 * vn);
		__m128 *_X, *_Y, *_Z, _x, _y, _z;
		_X = (__m128*) J1; _Y = (__m128*) (J1 + n); _Z = (__m128*) (J1 + 2 * n);
		for (i1 = i; i1 < n1; i1 += 4) {
			_x = *_X; _y = *_Y; _z = *_Z;
			_z = RCP(ADD(_x, ADD(_cEps, ADD(MUL(_c15, _y), MUL(_c3, _z)))));
			*(_X++) = MUL(_c1024, _y);
			*(_Y++) = SUB(MUL(MUL(_c52, _x), _z), _cun);
			*(_Z++) = SUB(MUL(MUL(_c117, _y), _z), _cvn);
		}
	}
	{ // perform lookup for L and finalize computation of U and V
		for (i1 = i; i1 < n1; i1++) J[i1] = lTable[(int)J[i1]];
		__m128 *_L, *_U, *_V, _l, _cminu, _cminv;
		_L = (__m128*) J1; _U = (__m128*) (J1 + n); _V = (__m128*) (J1 + 2 * n);
		_cminu = SET(minu); _cminv = SET(minv);
		for (i1 = i; i1 < n1; i1 += 4) {
			_l = *(_L++);
			*_U = SUB(MUL(_l, *_U), _cminu); _U++;
			*_V = SUB(MUL(_l, *_V), _cminv); _V++;
		}
	}
	i = n1;
	}
}

// step 2
void CPedestrianACF2::GetPyramidScales()
{
	m_fPerOct = 8.f;
	m_fShrink = 2.f;
	m_nApprox = (int)m_fPerOct;

	// ksizeSmallestPyramid 만큼까지 작아지려면 몇 개의 스케일이 필요한가?
	m_nScales = (int)floor(m_fPerOct*(log2(min(m_fHeight / m_fDetectSizeH,
		m_fWidth / m_fDetectSizeW))) + 1);

	m_vecfScales.clear();
	for (int i = 0; i < m_nScales; i++)
	{
		// 2^0, 2^(-1/8), 2^(-2/8), ... 하면서 계속 스케일을 작게 만듬.
		float dScale = pow(2.0f, (float)-i / m_fPerOct);

		float s0 = (float)(round(m_fHeight*dScale / m_fShrink)*m_fShrink - 0.25*m_fShrink) / m_fHeight;
		float s1 = (float)(round(m_fHeight*dScale / m_fShrink)*m_fShrink + 0.25*m_fShrink) / m_fHeight;
		float ss[101];
		float es0[101];
		float es1[101];
		float maxEs0Es1[101];
		int minIdx = -1;
		float minVal = 9999;
		for (int j = 0; j <= 100; j++)
		{
			ss[j] = (float)j*0.01f*(s1 - s0) + s0;
			es0[j] = m_fHeight * ss[j];
			es0[j] = abs(es0[j] - round(es0[j] / m_fShrink)*m_fShrink);
			es1[j] = m_fWidth * ss[j];
			es1[j] = abs(es1[j] - round(es1[j] / m_fShrink)*m_fShrink);
			maxEs0Es1[j] = (es0[j] > es1[j]) ? es0[j] : es1[j];
			if (maxEs0Es1[j] <= minVal)
			{
				minVal = maxEs0Es1[j];
				minIdx = j;
			}
		}
		m_vecfScales.push_back(ss[minIdx]);
	}

	m_vecfScalesHW.clear();
	for (int i = 0; i < m_nScales - 1; i++)
	{
		if (abs(m_vecfScales[i] - m_vecfScales[i + 1]) < 0.00001f)
		{
			m_vecfScales.erase(m_vecfScales.begin() + i);
			i--;
			continue;
		}
		pair<float, float> scaleHW;
		scaleHW.first = round(m_fHeight*m_vecfScales[i] / m_fShrink)*m_fShrink / m_fHeight;
		scaleHW.second = round(m_fWidth*m_vecfScales[i] / m_fShrink)*m_fShrink / m_fWidth;
		m_vecfScalesHW.push_back(scaleHW);
	}
}

// step 3-1
void CPedestrianACF2::ResampleImage(float *A, float *B, int ha, int hb, int wa, int wb, int d, float r)
{
#if USE_SSE_HR
	int hn, wn, x, x1, y, z, xa, xb, ya;
	float *A0, *A1, *A2, *A3, *B0, wt, wt1;

	float *C = (float*)alMalloc((ha + 4)*sizeof(float), 16);
	for (y = ha; y < ha + 4; y++)
		C[y] = 0;

	bool sse = !(size_t(A) & 15) && !(size_t(B) & 15);

	// get coefficients for resampling along w and h
	int *xas, *xbs, *yas, *ybs;
	float *xwts, *ywts;
	int xbd[2], ybd[2];

	CalcResampleCoef(wa, wb, wn, xas, xbs, xwts, xbd, 0);
	CalcResampleCoef(ha, hb, hn, yas, ybs, ywts, ybd, 4);

	if (wa == 2 * wb)
		r /= 2;
	if (wa == 3 * wb)
		r /= 3;
	if (wa == 4 * wb)
		r /= 4;

	r /= float(1 + 1e-6);
	for (y = 0; y < hn; y++)
		ywts[y] *= r;

	// resample each channel in turn
	for (z = 0; z < d; z++)
	{
		for (x = 0; x < wb; x++)
		{
			if (x == 0)
				x1 = 0;
			xa = xas[x1];
			xb = xbs[x1];
			wt = xwts[x1];
			wt1 = 1 - wt;
			y = 0;

			A0 = A + z*ha*wa + xa*ha;
			A1 = A0 + ha, A2 = A1 + ha, A3 = A2 + ha;
			B0 = B + z*hb*wb + xb*hb;

			// variables for SSE (simple casts to float)
			float *Af0, *Af1, *Af2, *Af3, *Bf0, *Cf, *ywtsf, wtf, wt1f;

			Af0 = (float*)A0;
			Af1 = (float*)A1;
			Af2 = (float*)A2;
			Af3 = (float*)A3;

			Bf0 = (float*)B0;
			Cf = (float*)C;
			ywtsf = (float*)ywts;
			wtf = (float)wt;
			wt1f = (float)wt1;

			// resample along x direction (A -> C)
#define FORs(X) if(sse) for(; y<ha-4; y+=4) STR(Cf[y],X);
#define FORr(X) for(; y<ha; y++) C[y] = X;

			if (wa == 2 * wb)
			{
				FORs(ADD(LDu(Af0[y]), LDu(Af1[y])));
				FORr(A0[y] + A1[y]);
				x1 += 2;
			}
			else if (wa == 3 * wb)
			{
				FORs(ADD(LDu(Af0[y]), LDu(Af1[y]), LDu(Af2[y])));
				FORr(A0[y] + A1[y] + A2[y]);
				x1 += 3;
			}
			else if (wa == 4 * wb)
			{
				FORs(ADD(LDu(Af0[y]), LDu(Af1[y]), LDu(Af2[y]), LDu(Af3[y])));
				FORr(A0[y] + A1[y] + A2[y] + A3[y]);
				x1 += 4;
			}
			else if (wa > wb)
			{
				int m = 1;
				while (x1 + m < wn && xb == xbs[x1 + m])
					m++;

				float wtsf[4];
				for (int x0 = 0; x0 < (m < 4 ? m : 4); x0++)
					wtsf[x0] = float(xwts[x1 + x0]);

#define U(x) MUL( LDu(*(Af ## x + y)), SET(wtsf[x]) )
#define V(x) *(A ## x + y) * xwts[x1+x]

				if (m == 1) { FORs(U(0));                     FORr(V(0)); }
				if (m == 2) { FORs(ADD(U(0), U(1)));           FORr(V(0) + V(1)); }
				if (m == 3) { FORs(ADD(U(0), U(1), U(2)));      FORr(V(0) + V(1) + V(2)); }
				if (m >= 4) { FORs(ADD(U(0), U(1), U(2), U(3))); FORr(V(0) + V(1) + V(2) + V(3)); }
#undef U
#undef V
				for (int x0 = 4; x0 < m; x0++)
				{
					A1 = A0 + x0*ha;
					wt1 = xwts[x1 + x0];
					Af1 = (float*)A1;
					wt1f = float(wt1);
					y = 0;

					FORs(ADD(LD(Cf[y]), MUL(LDu(Af1[y]), SET(wt1f))));
					FORr(C[y] + A1[y] * wt1);
				}
				x1 += m;
			}
			else
			{
				bool xBd = x < xbd[0] || x >= wb - xbd[1];
				x1++;

				if (xBd) memcpy(C, A0, ha*sizeof(float));
				if (!xBd) FORs(ADD(MUL(LDu(Af0[y]), SET(wtf)), MUL(LDu(Af1[y]), SET(wt1f))));
				if (!xBd) FORr(A0[y] * wt + A1[y] * wt1);
			}
#undef FORs
#undef FORr
			// resample along y direction (B -> C)
			if (ha == hb * 2)
			{
				float r2 = r / 2;
				int k = ((~((size_t)B0) + 1) & 15) / 4;
				y = 0;

				for (; y < k; y++)
					B0[y] = (C[2 * y] + C[2 * y + 1])*r2;

				if (sse)
					for (; y < hb - 4; y += 4)
						STR(Bf0[y], MUL((float)r2, _mm_shuffle_ps(ADD(
						LDu(Cf[2 * y]), LDu(Cf[2 * y + 1])), ADD(LDu(Cf[2 * y + 4]), LDu(Cf[2 * y + 5])), 136)));

				for (; y < hb; y++)
					B0[y] = (C[2 * y] + C[2 * y + 1])*r2;
			}
			else if (ha == hb * 3)
			{
				for (y = 0; y < hb; y++)
					B0[y] = (C[3 * y] + C[3 * y + 1] + C[3 * y + 2])*(r / 3);
			}
			else if (ha == hb * 4)
			{
				for (y = 0; y < hb; y++)
					B0[y] = (C[4 * y] + C[4 * y + 1] + C[4 * y + 2] + C[4 * y + 3])*(r / 4);
			}
			else if (ha > hb)
			{
				y = 0;
				//if( sse && ybd[0]<=4 ) for(; y<hb; y++) // Requires SSE4
				//  STR1(Bf0[y],_mm_dp_ps(LDu(Cf[yas[y*4]]),LDu(ywtsf[y*4]),0xF1));
#define U(o) C[ya+o]*ywts[y*4+o]
				if (ybd[0] == 2) for (; y < hb; y++) { ya = yas[y * 4]; B0[y] = U(0) + U(1); }
				if (ybd[0] == 3) for (; y < hb; y++) { ya = yas[y * 4]; B0[y] = U(0) + U(1) + U(2); }
				if (ybd[0] == 4) for (; y < hb; y++) { ya = yas[y * 4]; B0[y] = U(0) + U(1) + U(2) + U(3); }
				if (ybd[0] > 4)  for (; y < hn; y++) { B0[ybs[y]] += C[yas[y]] * ywts[y]; }
#undef U
			}
			else
			{
				for (y = 0; y < ybd[0]; y++)
					B0[y] = C[yas[y]] * ywts[y];
				for (; y < hb - ybd[1]; y++)
					B0[y] = C[yas[y]] * ywts[y] + C[yas[y] + 1] * (r - ywts[y]);
				for (; y < hb; y++)
					B0[y] = C[yas[y]] * ywts[y];
			}
		}
	}
	alFree(xas); alFree(xbs); alFree(xwts); alFree(C);
	alFree(yas); alFree(ybs); alFree(ywts);

#else // USE_SSE_HR

	printf("nothing to do...\n");

#endif // USE_SSE_HR
}

// compute interpolation values for single column for resapling
// step 3-1(1)
void CPedestrianACF2::CalcResampleCoef(int ha, int hb, int &n, int *&yas, int *&ybs, float *&wts, int bd[2], int pad)
{
	const float s = float(hb) / float(ha), sInv = 1 / s; float wt, wt0 = float(1e-3)*s;
	bool ds = ha > hb; int nMax; bd[0] = bd[1] = 0;
	if (ds) { n = 0; nMax = ha + (pad > 2 ? pad : 2)*hb; }
	else { n = nMax = hb; }
	// initialize memory
	wts = (float*)alMalloc(nMax*sizeof(float), 16);
	yas = (int*)alMalloc(nMax*sizeof(int), 16);
	ybs = (int*)alMalloc(nMax*sizeof(int), 16);
	if (ds) for (int yb = 0; yb < hb; yb++) {
		// create coefficients for downsampling
		float ya0f = yb*sInv, ya1f = ya0f + sInv, W = 0;
		int ya0 = int(ceil(ya0f)), ya1 = int(ya1f), n1 = 0;
		for (int ya = ya0 - 1; ya < ya1 + 1; ya++) {
			wt = s; if (ya == ya0 - 1) wt = (ya0 - ya0f)*s; else if (ya == ya1) wt = (ya1f - ya1)*s;
			if (wt > wt0 && ya >= 0) { ybs[n] = yb; yas[n] = ya; wts[n] = wt; n++; n1++; W += wt; }
		}
		if (W > 1) for (int i = 0; i < n1; i++) wts[n - n1 + i] /= W;
		if (n1 > bd[0]) bd[0] = n1;
		while (n1 < pad) { ybs[n] = yb; yas[n] = yas[n - 1]; wts[n] = 0; n++; n1++; }
	}
	else for (int yb = 0; yb < hb; yb++) {
		// create coefficients for upsampling
		float yaf = (float(.5) + yb)*sInv - float(.5); int ya = (int)floor(yaf);
		wt = 1; if (ya >= 0 && ya < ha - 1) wt = 1 - (yaf - ya);
		if (ya < 0) { ya = 0; bd[0]++; } if (ya >= ha - 1) { ya = ha - 1; bd[1]++; }
		ybs[yb] = yb; yas[yb] = ya; wts[yb] = wt;
	}
}

// step 3-2
void CPedestrianACF2::ComputeChannelFeature(Mat &imgLUV, vector<Mat> &vecimgChnFtrs, int h, int w, int d, int idx)
{
	int nShrink = (int)m_fShrink;

	// step 0 : crop image
	int cr1 = h%nShrink;
	int cr2 = w%nShrink;
	if (cr1 || cr2)
	{
		h -= cr1;
		w -= cr2;
		Mat imgCrop[3];
		for (int i = 0; i < d; i++)
			imgCrop[i] = imgLUV(Rect_<int>(0, (w + cr2)*i, h, w)).clone();
		imgLUV = Mat::zeros(w * d, h, CV_32FC1);
		for (int i = 0; i < d; i++)
			imgCrop[i].copyTo(imgLUV(Range(w*i, w*(i + 1)), Range(0, h)));
	}
	h /= nShrink;
	w /= nShrink;

	Mat imgChnFtrs = Mat::zeros(w*m_nChns, h, CV_32FC1);
	int posStart = 0;

	// step 1 : compute color channels
	AddChannel(imgLUV, imgChnFtrs, h, w, d, posStart);

	// step 2 : compute gradient magnitude channel
	Mat imgMag, imgOrient;
	CalculateGradMag(imgLUV, imgMag, imgOrient, d);
	NormalizeGradMag(imgMag);
	AddChannel(imgMag, imgChnFtrs, h, w, 1, posStart);

	// step 3 : compute gradient histogram channel
	Mat imgHist;
	CalculateGradHist(imgMag, imgOrient, imgHist);
	AddChannel(imgHist, imgChnFtrs, h, w, m_nOrients, posStart);

	vecimgChnFtrs[idx] = imgChnFtrs.clone();
}

void CPedestrianACF2::AddChannel(Mat &imgChn, Mat &imgChnFtrs, int h, int w, int d, int &posStart)
{
	int h1 = imgChn.cols;
	int w1 = imgChn.rows / d;

	float *pImgChn = (float*)imgChn.data;
	float *pImgChnFtrs = (float*)imgChnFtrs.data;

	if (h1 != h || w1 != w)
		ResampleImage(pImgChn, pImgChnFtrs + posStart, h1, h, w1, w, d, 1.f);
	else
		memcpy(pImgChnFtrs + posStart, pImgChn, sizeof(float)*h*w*d);

	posStart += h*w*d;
}

void CPedestrianACF2::CalculateGradMag(Mat& imgSrc, Mat& imgDstMag, Mat& imgDstOrient, int d)
{
	int h = imgSrc.cols;
	int w = imgSrc.rows / d;

	imgDstMag = Mat::zeros(w, h, CV_32FC1);
	imgDstOrient = Mat::zeros(w, h, CV_32FC1);

#if USE_SSE_HR
	float *pImgSrc = (float*)imgSrc.data;
	float *pImgDstMag = (float*)imgDstMag.data;
	float *pImgDstOrient = (float*)imgDstOrient.data;

	gradMag(pImgSrc, pImgDstMag, pImgDstOrient, h, w, d, false);
#else
#endif
}

void CPedestrianACF2::NormalizeGradMag(Mat& imgSrcDst)
{
	int h = imgSrcDst.cols;
	int w = imgSrcDst.rows;

	Mat imgSmooth = Mat::zeros(w, h, CV_32FC1);
	ConvTriangle(imgSrcDst, imgSmooth);

#if USE_SSE_HR
	float *pImgSrcDst = (float*)imgSrcDst.data;
	float *pImgSmooth = (float*)imgSmooth.data;

	gradMagNorm(pImgSrcDst, pImgSmooth, h, w, m_fNormConst);

#else
#endif
}

void CPedestrianACF2::CalculateGradHist(Mat& imgSrcMag, Mat& imgSrcOrient, Mat& imgDstHist)
{
	int h = imgSrcMag.cols;
	int w = imgSrcMag.rows;

	int h2 = h / m_nBinSize;
	int w2 = w / m_nBinSize;

	imgDstHist = Mat::zeros(w2*m_nOrients, h2, CV_32FC1);

	float *pImgSrcMag = (float *)imgSrcMag.data;
	float *pImgSrcOrient = (float *)imgSrcOrient.data;
	float *pImgDstHist = (float *)imgDstHist.data;

	gradHist(pImgSrcMag, pImgSrcOrient, pImgDstHist, h, w, m_nBinSize, m_nOrients, m_nSoftBin, m_bFullOrient);
}

void CPedestrianACF2::SmoothChannel(vector<Mat> &vecimgChnFtrs)
{
	for (int i = 0; i < (int)vecimgChnFtrs.size(); i++)
	{
		int hR = vecimgChnFtrs[i].cols;
		int wR = vecimgChnFtrs[i].rows / m_nChns;
		for (int j = 0; j < m_nChns; j++)
		{
			Mat imgChnFtrs = vecimgChnFtrs[i](Rect_<int>(0, wR*j, hR, wR));
			float *pImgChnFtrs = (float*)imgChnFtrs.data;
			float *pImgChnFtrs2 = new float[hR*wR];

			float r = 1.f;
			convTri1(pImgChnFtrs, pImgChnFtrs2, hR, wR, 1, 12.f / r / (r + 2.f) - 2.f, 1);
			memcpy(pImgChnFtrs, pImgChnFtrs2, sizeof(float)*hR*wR);

			delete[] pImgChnFtrs2;
		}
	}
}

void CPedestrianACF2::ConvTriangle(Mat& imgSrc, Mat& imgDst)
{
	int d = imgSrc.channels();
	int h = imgSrc.cols;
	int w = imgSrc.rows / d;

#if USE_SSE_HR
	float *pImgSrc = (float*)imgSrc.data;
	float *pImgDst = (float*)imgDst.data;

	convTri(pImgSrc, pImgDst, h, w, d, m_nRadius, m_nDownSampling);

#else

	Mat temp1;
	transpose(imgSrc, temp1);

	float fKernel[11 * 11] = {
		1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1,
		2, 4, 6, 8, 10, 12, 10, 8, 6, 4, 2,
		3, 6, 9, 12, 15, 18, 15, 12, 9, 6, 3,
		4, 8, 12, 16, 20, 24, 20, 16, 12, 8, 4,
		5, 10, 15, 20, 25, 30, 25, 20, 15, 10, 5,
		6, 12, 18, 24, 30, 36, 30, 24, 18, 12, 6,
		5, 10, 15, 20, 25, 30, 25, 20, 15, 10, 5,
		4, 8, 12, 16, 20, 24, 20, 16, 12, 8, 4,
		3, 6, 9, 12, 15, 18, 15, 12, 9, 6, 3,
		2, 4, 6, 8, 10, 12, 10, 8, 6, 4, 2,
		1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1
	};
	Mat matKernel(11, 11, CV_32FC1, fKernel);
	matKernel *= 1 / 1296.f;

	Mat imgSrc1 = temp1.clone();
	Mat imgFilt;
	filter2D(imgSrc1, imgFilt, -1, matKernel);

	transpose(imgFilt, imgDst);

#endif
}

// convolve one column of I by a [1 p 1] filter (uses SSE)
void CPedestrianACF2::convTri1Y(float *I, float *O, int h, float p, int s) {
#define C4(m,o) ADD(ADD(LDu(I[m*j-1+o]),MUL(p,LDu(I[m*j+o]))),LDu(I[m*j+1+o]))
	int j = 0, k = ((~((size_t)O) + 1) & 15) / 4, h2 = (h - 1) / 2;
	if (s == 2) {
		for (; j < k; j++) O[j] = I[2 * j] + p*I[2 * j + 1] + I[2 * j + 2];
		for (; j < h2 - 4; j += 4) STR(O[j], _mm_shuffle_ps(C4(2, 1), C4(2, 5), 136));
		for (; j < h2; j++) O[j] = I[2 * j] + p*I[2 * j + 1] + I[2 * j + 2];
		if (h % 2 == 0) O[j] = I[2 * j] + (1 + p)*I[2 * j + 1];
	}
	else {
		O[j] = (1 + p)*I[j] + I[j + 1]; j++; if (k == 0) k = (h <= 4) ? h - 1 : 4;
		for (; j < k; j++) O[j] = I[j - 1] + p*I[j] + I[j + 1];
		for (; j < h - 4; j += 4) STR(O[j], C4(1, 0));
		for (; j < h - 1; j++) O[j] = I[j - 1] + p*I[j] + I[j + 1];
		O[j] = I[j - 1] + (1 + p)*I[j];
	}
#undef C4
}

// convolve I by a [1 p 1] filter (uses SSE)
void CPedestrianACF2::convTri1(float *I, float *O, int h, int w, int d, float p, int s) {
	const float nrm = 1.0f / ((p + 2)*(p + 2)); int i, j, h0 = h - (h % 4);
	float *Il, *Im, *Ir, *T = (float*)alMalloc(h*sizeof(float), 16);
	for (int d0 = 0; d0 < d; d0++) for (i = s / 2; i < w; i += s) {
		Il = Im = Ir = I + i*h + d0*h*w; if (i > 0) Il -= h; if (i < w - 1) Ir += h;
		for (j = 0; j < h0; j += 4)
			STR(T[j], MUL(nrm, ADD(ADD(LDu(Il[j]), MUL(p, LDu(Im[j]))), LDu(Ir[j]))));
		for (j = h0; j < h; j++) T[j] = nrm*(Il[j] + p*Im[j] + Ir[j]);
		convTri1Y(T, O, h, p, s); O += h / s;
	}
	alFree(T);
}

// compute gradient magnitude and orientation at each location (uses sse)
void CPedestrianACF2::gradMag(float *I, float *M, float *O, int h, int w, int d, bool full)
{
	int x, y, y1, c, h4, s;
	float *Gx, *Gy, *M2;
	__m128 *_Gx, *_Gy, *_M2, _m;

	float *acost = acosTable();
	float acMult = 10000.0f;

	// allocate memory for storing one column of output (padded so h4%4==0)
	h4 = (h % 4 == 0) ? h : h - (h % 4) + 4;
	s = d*h4*sizeof(float);

	M2 = (float*)alMalloc(s, 16);
	Gx = (float*)alMalloc(s, 16);
	Gy = (float*)alMalloc(s, 16);

	_M2 = (__m128*) M2;
	_Gx = (__m128*) Gx;
	_Gy = (__m128*) Gy;

	// compute gradient magnitude and orientation for each column
	for (x = 0; x < w; x++)
	{
		// compute gradients (Gx, Gy) with maximum squared magnitude (M2)
		for (c = 0; c < d; c++)
		{
			grad1(I + x*h + c*w*h, Gx + c*h4, Gy + c*h4, h, w, x);

			for (y = 0; y < h4 / 4; y++)
			{
				y1 = h4 / 4 * c + y;

				_M2[y1] = ADD(MUL(_Gx[y1], _Gx[y1]), MUL(_Gy[y1], _Gy[y1]));

				if (c == 0)
					continue;

				_m = CMPGT(_M2[y1], _M2[y]);

				_M2[y] = OR(AND(_m, _M2[y1]), ANDNOT(_m, _M2[y]));
				_Gx[y] = OR(AND(_m, _Gx[y1]), ANDNOT(_m, _Gx[y]));
				_Gy[y] = OR(AND(_m, _Gy[y1]), ANDNOT(_m, _Gy[y]));
			}
		}
		// compute gradient mangitude (M) and normalize Gx
		for (y = 0; y < h4 / 4; y++)
		{
#if !defined(__ARM_NEON_FP)
			_m = MIN_(RCPSQRT(_M2[y]), SET(1e10f));
#else
			_m = MIN_(RCP(SQRT_(_M2[y])), SET(1e10f));
#endif

			_M2[y] = RCP(_m);

			if (O)
				_Gx[y] = MUL(MUL(_Gx[y], _m), SET(acMult));
			if (O)
				_Gx[y] = XOR(_Gx[y], AND(_Gy[y], SET(-0.f)));
		}

		memcpy(M + x*h, M2, h*sizeof(float));

		// compute and store gradient orientation (O) via table lookup
		if (O != 0)
			for (y = 0; y < h; y++)
				O[x*h + y] = acost[(int)Gx[y]];

		if (O != 0 && full)
		{
			y1 = ((~size_t(O + x*h) + 1) & 15) / 4;

			y = 0;

			for (; y < y1; y++)
				O[y + x*h] += (Gy[y] < 0)*((float)CV_PI);

			for (; y < h - 4; y += 4)
				STRu(O[y + x*h], ADD(LDu(O[y + x*h]), AND(CMPLT(LDu(Gy[y]), SET(0.f)), SET((float)CV_PI))));

			for (; y < h; y++)
				O[y + x*h] += (Gy[y] < 0)*((float)CV_PI);
		}
	}
	alFree(Gx); alFree(Gy); alFree(M2);
}

// compute x and y gradients for just one column (uses sse)
void CPedestrianACF2::grad1(float *I, float *Gx, float *Gy, int h, int w, int x)
{
	int y, y1;
	float *Ip, *In, r;
	__m128 *_Ip, *_In, *_G, _r;

	// compute column of Gx
	Ip = I - h;
	In = I + h;
	r = .5f;

	if (x == 0)
	{
		r = 1;
		Ip += h;
	}
	else if (x == w - 1)
	{
		r = 1;
		In -= h;
	}

	if (h < 4 || h % 4>0 || (size_t(I) & 15) || (size_t(Gx) & 15))
	{
		for (y = 0; y < h; y++)
			*Gx++ = (*In++ - *Ip++)*r;
	}
	else
	{
		_G = (__m128*) Gx;
		_Ip = (__m128*) Ip;
		_In = (__m128*) In;
		_r = SET(r);

		for (y = 0; y < h; y += 4)
			*_G++ = MUL(SUB(*_In++, *_Ip++), _r);
	}

	// compute column of Gy
#define GRADY(r) *Gy++=(*In++-*Ip++)*r;

	Ip = I;
	In = Ip + 1;

	// GRADY(1); Ip--; for(y=1; y<h-1; y++) GRADY(.5f); In--; GRADY(1);
	y1 = ((~((size_t)Gy) + 1) & 15) / 4;
	if (y1 == 0)
		y1 = 4;
	if (y1 > h - 1)
		y1 = h - 1;

	GRADY(1);
	Ip--;

	for (y = 1; y < y1; y++)
		GRADY(.5f);

	_r = SET(.5f);
	_G = (__m128*) Gy;

	for (; y + 4 < h - 1; y += 4, Ip += 4, In += 4, Gy += 4)
		*_G++ = MUL(SUB(LDu(*In), LDu(*Ip)), _r);

	for (; y < h - 1; y++)
		GRADY(.5f);

	In--;
	GRADY(1);

#undef GRADY
}

// normalize gradient magnitude at each location (uses sse)
void CPedestrianACF2::gradMagNorm(float *M, float *S, int h, int w, float norm)
{
	__m128 *_M;
	__m128 *_S;
	__m128 _norm;

	int i = 0;
	int n = h*w;
	int n4 = n / 4;

	_S = (__m128*) S;
	_M = (__m128*) M;
	_norm = SET(norm);

	bool sse = !(size_t(M) & 15) && !(size_t(S) & 15);

	if (sse)
		for (; i < n4; i++)
		{
			*_M = MUL(*_M, RCP(ADD(*_S++, _norm)));
			_M++;
		}

	if (sse)
		i *= 4;

	for (; i < n; i++)
		M[i] /= (S[i] + norm);
}

// build lookup table a[] s.t. a[x*n]~=acos(x) for x in [-1,1]
float* CPedestrianACF2::acosTable() {
	const int n = 10000, b = 10;
	int i;
	static float a[n * 2 + b * 2];
	static bool init = false;

	float *a1 = a + n + b;
	if (init)
		return a1;

	for (i = -n - b; i < -n; i++)
		a1[i] = (float)CV_PI;

	for (i = -n; i < n; i++)
		a1[i] = float(acos(i / float(n)));

	for (i = n; i < n + b; i++)
		a1[i] = 0;

	for (i = -n - b; i<n / 10; i++)
		if (a1[i] >(float)CV_PI - 1e-6f)
			a1[i] = (float)CV_PI - 1e-6f;

	init = true;
	return a1;
}

// convolve one column of I by a 2rx1 triangle filter
void CPedestrianACF2::convTriY(float *I, float *O, int h, int r, int s) {
	r++; float t, u; int j, r0 = r - 1, r1 = r + 1, r2 = 2 * h - r, h0 = r + 1, h1 = h - r + 1, h2 = h;
	u = t = I[0]; for (j = 1; j < r; j++) u += t += I[j]; u = 2 * u - t; t = 0;
	if (s == 1) {
		O[0] = u; j = 1;
		for (; j < h0; j++) O[j] = u += t += I[r - j] + I[r0 + j] - 2 * I[j - 1];
		for (; j < h1; j++) O[j] = u += t += I[j - r1] + I[r0 + j] - 2 * I[j - 1];
		for (; j < h2; j++) O[j] = u += t += I[j - r1] + I[r2 - j] - 2 * I[j - 1];
	}
	else {
		int k = (s - 1) / 2; h2 = (h / s)*s; if (h0 > h2) h0 = h2; if (h1 > h2) h1 = h2;
		if (++k == s) { k = 0; *O++ = u; } j = 1;
		for (; j < h0; j++) { u += t += I[r - j] + I[r0 + j] - 2 * I[j - 1]; if (++k == s){ k = 0; *O++ = u; } }
		for (; j < h1; j++) { u += t += I[j - r1] + I[r0 + j] - 2 * I[j - 1]; if (++k == s){ k = 0; *O++ = u; } }
		for (; j < h2; j++) { u += t += I[j - r1] + I[r2 - j] - 2 * I[j - 1]; if (++k == s){ k = 0; *O++ = u; } }
	}
}

// convolve I by a 2rx1 triangle filter (uses SSE)
void CPedestrianACF2::convTri(float *I, float *O, int h, int w, int d, int r, int s)
{
	r++;
	float nrm = 1.0f / (r*r*r*r);
	int i, j, k = (s - 1) / 2, h0, h1, w0;

	if (h % 4 == 0)
		h0 = h1 = h;
	else
	{
		h0 = h - (h % 4);
		h1 = h0 + 4;
	}
	w0 = (w / s)*s;

	float* T = (float*)alMalloc(2 * h1*sizeof(float), 16);
	float* U = T + h1;

	while (d-- > 0)
	{
		// initialize T and U
		for (j = 0; j < h0; j += 4)
			STR(U[j], STR(T[j], LDu(I[j])));

		for (i = 1; i < r; i++)
			for (j = 0; j < h0; j += 4)
				INC(U[j], INC(T[j], LDu(I[j + i*h])));

		for (j = 0; j < h0; j += 4)
			STR(U[j], MUL(nrm, (SUB(MUL(2, LD(U[j])), LD(T[j])))));

		for (j = 0; j < h0; j += 4)
			STR(T[j], 0);

		for (j = h0; j < h; j++)
			U[j] = T[j] = I[j];

		for (i = 1; i < r; i++)
			for (j = h0; j < h; j++)
				U[j] += T[j] += I[j + i*h];

		for (j = h0; j < h; j++)
		{
			U[j] = nrm * (2 * U[j] - T[j]); T[j] = 0;
		}

		// prepare and convolve each column in turn
		k++;
		if (k == s)
		{
			k = 0;
			convTriY(U, O, h, r - 1, s);
			O += h / s;
		}
		for (i = 1; i < w0; i++)
		{
			float *Il = I + (i - 1 - r)*h;

			if (i <= r)
				Il = I + (r - i)*h;

			float *Im = I + (i - 1)*h;
			float *Ir = I + (i - 1 + r)*h;

			if (i > w - r)
				Ir = I + (2 * w - r - i)*h;

			for (j = 0; j < h0; j += 4)
			{
				INC(T[j], ADD(LDu(Il[j]), LDu(Ir[j]), MUL(-2, LDu(Im[j]))));
				INC(U[j], MUL(nrm, LD(T[j])));
			}

			for (j = h0; j < h; j++)
				U[j] += nrm*(T[j] += Il[j] + Ir[j] - 2 * Im[j]);

			k++;
			if (k == s)
			{
				k = 0;
				convTriY(U, O, h, r - 1, s);
				O += h / s;
			}
		}
		I += w*h;
	}
	alFree(T);
}

// compute nOrients gradient histograms per bin x bin block of pixels
void CPedestrianACF2::gradHist(float *M, float *O, float *H, int h, int w,
	int bin, int nOrients, int softBin, bool full)
{
	const int hb = h / bin;
	const int wb = w / bin;
	const int h0 = hb*bin;
	const int w0 = wb*bin;
	const int nb = wb*hb;
	const float s = (float)bin;
	const float sInv = 1 / s;
	const float sInv2 = 1 / s / s;

	float *H0, *H1, *M0, *M1;
	int x, y;
	int *O0, *O1;
	float xb, init;

#if USE_SSE_HR

	O0 = (int*)alMalloc(h*sizeof(int), 16); M0 = (float*)alMalloc(h*sizeof(float), 16);
	O1 = (int*)alMalloc(h*sizeof(int), 16); M1 = (float*)alMalloc(h*sizeof(float), 16);
	// main loop
	for (x = 0; x < w0; x++) {
		// compute target orientation bins for entire column - very fast
		gradQuantize(O + x*h, M + x*h, O0, O1, M0, M1, nb, h0, sInv2, nOrients, full, softBin >= 0);

		if (softBin < 0 && softBin % 2 == 0) {
			// no interpolation w.r.t. either orienation or spatial bin
			H1 = H + (x / bin)*hb;
#define GH H1[O0[y]]+=M0[y]; y++;
			if (bin == 1)      for (y = 0; y < h0;) { GH; H1++; }
			else if (bin == 2) for (y = 0; y < h0;) { GH; GH; H1++; }
			else if (bin == 3) for (y = 0; y < h0;) { GH; GH; GH; H1++; }
			else if (bin == 4) for (y = 0; y < h0;) { GH; GH; GH; GH; H1++; }
			else for (y = 0; y < h0;) { for (int y1 = 0; y1 < bin; y1++) { GH; } H1++; }
#undef GH

		}
		else if (softBin % 2 == 0 || bin == 1) {
			// interpolate w.r.t. orientation only, not spatial bin
			H1 = H + (x / bin)*hb;
#define GH H1[O0[y]]+=M0[y]; H1[O1[y]]+=M1[y]; y++;
			if (bin == 1)      for (y = 0; y < h0;) { GH; H1++; }
			else if (bin == 2) for (y = 0; y < h0;) { GH; GH; H1++; }
			else if (bin == 3) for (y = 0; y < h0;) { GH; GH; GH; H1++; }
			else if (bin == 4) for (y = 0; y < h0;) { GH; GH; GH; GH; H1++; }
			else for (y = 0; y < h0;) { for (int y1 = 0; y1 < bin; y1++) { GH; } H1++; }
#undef GH

		}
		else {
			// interpolate using trilinear interpolation
			float ms[4], xyd, yb, xd, yd; __m128 _m, _m0, _m1;
			bool hasLf, hasRt; int xb0, yb0;
			if (x == 0) { init = (0 + .5f)*sInv - 0.5f; xb = init; }
			hasLf = xb >= 0; xb0 = hasLf ? (int)xb : -1; hasRt = xb0 < wb - 1;
			xd = xb - xb0; xb += sInv; yb = init; y = 0;
			// macros for code conciseness
#define GHinit yd=yb-yb0; yb+=sInv; H0=H+xb0*hb+yb0; xyd=xd*yd; \
ms[0]=1-xd-yd+xyd; ms[1]=yd-xyd; ms[2]=xd-xyd; ms[3]=xyd;
#define GH(H,ma,mb) H1=H; STRu(*H1,ADD(LDu(*H1),MUL(ma,mb)));
			// leading rows, no top bin
			for (; y < bin / 2; y++) {
				yb0 = -1; GHinit;
				if (hasLf) { H0[O0[y] + 1] += ms[1] * M0[y]; H0[O1[y] + 1] += ms[1] * M1[y]; }
				if (hasRt) { H0[O0[y] + hb + 1] += ms[3] * M0[y]; H0[O1[y] + hb + 1] += ms[3] * M1[y]; }
			}
			// main rows, has top and bottom bins, use SSE for minor speedup
			if (softBin < 0) for (;; y++) {
				yb0 = (int)yb; if (yb0 >= hb - 1) break; GHinit; _m0 = SET(M0[y]);
				if (hasLf) { _m = SET(0, 0, ms[1], ms[0]); GH(H0 + O0[y], _m, _m0); }
				if (hasRt) { _m = SET(0, 0, ms[3], ms[2]); GH(H0 + O0[y] + hb, _m, _m0); }
			}
			else for (;; y++) {
				yb0 = (int)yb; if (yb0 >= hb - 1) break; GHinit;
				_m0 = SET(M0[y]); _m1 = SET(M1[y]);
				if (hasLf) {
					_m = SET(0, 0, ms[1], ms[0]);
					GH(H0 + O0[y], _m, _m0); GH(H0 + O1[y], _m, _m1);
				}
				if (hasRt) {
					_m = SET(0, 0, ms[3], ms[2]);
					GH(H0 + O0[y] + hb, _m, _m0); GH(H0 + O1[y] + hb, _m, _m1);
				}
			}
			// final rows, no bottom bin
			for (; y < h0; y++) {
				yb0 = (int)yb; GHinit;
				if (hasLf) { H0[O0[y]] += ms[0] * M0[y]; H0[O1[y]] += ms[0] * M1[y]; }
				if (hasRt) { H0[O0[y] + hb] += ms[2] * M0[y]; H0[O1[y] + hb] += ms[2] * M1[y]; }
			}
#undef GHinit
#undef GH
		}
	}
	alFree(O0); alFree(O1); alFree(M0); alFree(M1);
	// normalize boundary bins which only get 7/8 of weight of interior bins
	if (softBin % 2 != 0) for (int o = 0; o < nOrients; o++) {
		x = 0; for (y = 0; y < hb; y++) H[o*nb + x*hb + y] *= 8.f / 7.f;
		y = 0; for (x = 0; x < wb; x++) H[o*nb + x*hb + y] *= 8.f / 7.f;
		x = wb - 1; for (y = 0; y < hb; y++) H[o*nb + x*hb + y] *= 8.f / 7.f;
		y = hb - 1; for (x = 0; x < wb; x++) H[o*nb + x*hb + y] *= 8.f / 7.f;
	}

#else

	O0 = (int*)malloc(h*sizeof(int));
	M0 = (float*)malloc(h*sizeof(float));

	O1 = (int*)malloc(h*sizeof(int));
	M1 = (float*)malloc(h*sizeof(float));

	// main loop
	for (x = 0; x < w0; x++)
	{
		// compute target orientation bins for entire column - very fast
		gradQuantize(O + x*h, M + x*h, O0, O1, M0, M1, nb, h0, sInv2, nOrients, full, softBin >= 0);

		// interpolate using trilinear interpolation
		float ms[4], xyd, yb, xd, yd;
		bool hasLf, hasRt;
		int xb0, yb0;

		if (x == 0)
		{
			init = (0 + .5f)*sInv - 0.5f;
			xb = init;
		}

		hasLf = xb >= 0;
		xb0 = hasLf ? (int)xb : -1;
		hasRt = xb0 < wb - 1;

		xd = xb - xb0;
		xb += sInv;
		yb = init;
		y = 0;

		// leading rows, no top bin
		for (; y < bin / 2; y++)
		{
			yb0 = -1;
			/*GHinit;*/

			//
			yd = yb - yb0;
			yb += sInv;
			H0 = H + xb0*hb + yb0; // H에 -181 을 한것과 같음.
			xyd = xd*yd;

			ms[0] = 1 - xd - yd + xyd;
			ms[1] = yd - xyd;
			ms[2] = xd - xyd;
			ms[3] = xyd;
			//

			if (hasLf)
			{
				H0[O0[y] + 1] += ms[1] * M0[y];
				H0[O1[y] + 1] += ms[1] * M1[y];
			}
			if (hasRt)
			{
				H0[O0[y] + hb + 1] += ms[3] * M0[y]; // hb+1 을 하기 때문에 -181은 보상됨
				H0[O1[y] + hb + 1] += ms[3] * M1[y];
			}
		}
		// main rows, has top and bottom bins, use SSE for minor speedup
		for (;; y++)
		{
			yb0 = (int)yb; // yb0는 몇 번째 bin인가를 나타내는 인덱스
			if (yb0 >= hb - 1)
				break;
			/*GHinit;*/

			//
			yd = yb - yb0;
			yb += sInv;
			H0 = H + xb0*hb + yb0;
			xyd = xd*yd;

			ms[0] = 1 - xd - yd + xyd;
			ms[1] = yd - xyd;
			ms[2] = xd - xyd;
			ms[3] = xyd;
			//

			if (hasLf)
			{
				H0[O0[y]] += ms[0] * M0[y];
				H0[O1[y]] += ms[0] * M1[y];

				H0[O0[y] + 1] += ms[1] * M0[y];
				H0[O1[y] + 1] += ms[1] * M1[y];
			}
			if (hasRt)
			{
				H0[O0[y] + hb] += ms[2] * M0[y];
				H0[O1[y] + hb] += ms[2] * M1[y];

				H0[O0[y] + hb + 1] += ms[3] * M0[y];
				H0[O1[y] + hb + 1] += ms[3] * M1[y];
			}
		}
		// final rows, no bottom bin
		for (; y < h0; y++)
		{
			yb0 = (int)yb;
			/*GHinit;*/

			//
			yd = yb - yb0;
			yb += sInv;
			H0 = H + xb0*hb + yb0;
			xyd = xd*yd;

			ms[0] = 1 - xd - yd + xyd;
			ms[1] = yd - xyd;
			ms[2] = xd - xyd;
			ms[3] = xyd;
			//

			if (hasLf)
			{
				H0[O0[y]] += ms[0] * M0[y];
				H0[O1[y]] += ms[0] * M1[y];
			}
			if (hasRt)
			{
				H0[O0[y] + hb] += ms[2] * M0[y];
				H0[O1[y] + hb] += ms[2] * M1[y];
			}
		}
	}
	free(O0);
	free(O1);
	free(M0);
	free(M1);

	// normalize boundary bins which only get 7/8 of weight of interior bins
	if (softBin % 2 != 0)
		for (int o = 0; o < nOrients; o++)
		{
			x = 0;

			for (y = 0; y < hb; y++)
				H[o*nb + x*hb + y] *= 8.f / 7.f;

			y = 0;
			for (x = 0; x < wb; x++)
				H[o*nb + x*hb + y] *= 8.f / 7.f;

			x = wb - 1;
			for (y = 0; y < hb; y++)
				H[o*nb + x*hb + y] *= 8.f / 7.f;

			y = hb - 1;
			for (x = 0; x < wb; x++)
				H[o*nb + x*hb + y] *= 8.f / 7.f;
		}

#endif
}

// helper for gradHist, quantize O and M into O0, O1 and M0, M1 (uses sse)
void CPedestrianACF2::gradQuantize(float *O, float *M, int *O0, int *O1, float *M0, float *M1,
	int nb, int n, float norm, int nOrients, bool full, bool interpolate)
{
	// assumes all *OUTPUT* matrices are 4-byte aligned
	int i, o0, o1;
	float o, od, m;

	// define useful constants
	const float oMult = (float)nOrients / (full ? 2 * (float)CV_PI : (float)CV_PI);
	const int oMax = nOrients*nb;

#if USE_SSE_HR
	__m128i _o0, _o1, *_O0, *_O1; __m128 _o, _od, _m, *_M0, *_M1;

	const __m128 _norm = SET(norm), _oMult = SET(oMult), _nbf = SET((float)nb);
	const __m128i _oMax = SET(oMax), _nb = SET(nb);
	// perform the majority of the work with sse
	_O0 = (__m128i*) O0; _O1 = (__m128i*) O1; _M0 = (__m128*) M0; _M1 = (__m128*) M1;
	if (interpolate) for (i = 0; i <= n - 4; i += 4) {
		_o = MUL(LDu(O[i]), _oMult); _o0 = CVT(_o); _od = SUB(_o, CVT(_o0));
		_o0 = CVT(MUL(CVT(_o0), _nbf)); _o0 = AND(CMPGT(_oMax, _o0), _o0); *_O0++ = _o0;
		_o1 = ADD(_o0, _nb); _o1 = AND(CMPGT(_oMax, _o1), _o1); *_O1++ = _o1;
		_m = MUL(LDu(M[i]), _norm); *_M1 = MUL(_od, _m); *_M0++ = SUB(_m, *_M1); _M1++;
	}
	else for (i = 0; i <= n - 4; i += 4) {
		_o = MUL(LDu(O[i]), _oMult); _o0 = CVT(ADD(_o, SET(.5f)));
		_o0 = CVT(MUL(CVT(_o0), _nbf)); _o0 = AND(CMPGT(_oMax, _o0), _o0); *_O0++ = _o0;
		*_M0++ = MUL(LDu(M[i]), _norm); *_M1++ = SET(0.f); *_O1++ = SET(0);
	}
	// compute trailing locations without sse
	if (interpolate) for (; i < n; i++) {
		o = O[i] * oMult; o0 = (int)o; od = o - o0;
		o0 *= nb; if (o0 >= oMax) o0 = 0; O0[i] = o0;
		o1 = o0 + nb; if (o1 == oMax) o1 = 0; O1[i] = o1;
		m = M[i] * norm; M1[i] = od*m; M0[i] = m - M1[i];
	}
	else for (; i < n; i++) {
		o = O[i] * oMult; o0 = (int)(o + .5f);
		o0 *= nb; if (o0 >= oMax) o0 = 0; O0[i] = o0;
		M0[i] = M[i] * norm; M1[i] = 0; O1[i] = 0;
	}
#else

	// compute trailing locations without sse
	if (interpolate)
	{
		for (i = 0; i < n; i++)
		{
			o = O[i] * oMult; // pi로 나누고 nOrient를 곱함
			o0 = (int)o; // quantize 시켜서 해당 orient bin을 가리킴
			od = o - o0; // 잔차 값 od에 저장

			o0 *= nb; // o0에 영상 크기를 곱함으로 써 해당 orient 저장소의 시작 인덱스로 변경함
			if (o0 >= oMax) // 배열을 넘어가면 안되므로
				o0 = 0;

			O0[i] = o0; // O0의 사이즈는 h이고, 해당 인덱스 i에 oO값(나중에 사용할 인덱스)을 저장함

			o1 = o0 + nb; // quantize 를 수행했기 때문에 잔차는 다음 orient bin에 저장되어야함
			if (o1 == oMax) // 이것도 역시 배열을 넘어가면 안되므로
				o1 = 0;

			O1[i] = o1; // O1의 사이즈는 h이고, 해당 인덱스 i에 o1값(나중에 사용할 인덱스)을 저장함

			m = M[i] * norm; // magnitude를 norm을 곱해서 사용함
			M1[i] = od*m; // M1 은 잔차에 해당하는 magnitude 라서 od를 곱함
			M0[i] = m - M1[i]; // M0 은 잔차가 아닌 magnitude라서 1-od를 곱함. (즉, M1:M0 = od:(1-od) 의 비율임)
		}
	}
#endif
}

/**
* @param l
* @param r
* @return
*/
bool CPedestrianACF2::Comparator(const pair<float, Rect_<float> >& l, const pair<float, Rect_<float> >& r) // 첫번째 값을 기준으로 내림차순 비교하는 comparator
{
	return l.first > r.first;
}

/**
* @param bbs
* @return
* @see http://www.pyimagesearch.com/2014/11/17/non-maximum-suppression-object-detection-python/
*/
vector<Rect_<int> > CPedestrianACF2::bbNMS(vector<pair<float, Rect_<float> > >& bbs)
{
	const float m_fOverlap = 0.65f;

	stable_sort(bbs.begin(), bbs.end(), Comparator);

	int n = (int)bbs.size();
	int* kp = new int[n];
	for (int i = 0; i < n; i++)
		kp[i] = 1;

	for (int i = 0; i < n; i++)
	{
		if (kp[i] == 0)
			continue;

		for (int j = i + 1; j < n; j++)
		{
			if (kp[j] == 0)
				continue;

			float iw = min(bbs[i].second.br().x, bbs[j].second.br().x) - max(bbs[i].second.x, bbs[j].second.x);
			if (iw <= 0)
				continue;

			float ih = min(bbs[i].second.br().y, bbs[j].second.br().y) - max(bbs[i].second.y, bbs[j].second.y);
			if (ih <= 0)
				continue;

			float o = iw*ih;
			float u = min(bbs[i].second.area(), bbs[j].second.area());

			if (o / u > m_fOverlap)
				kp[j] = 0;
		}
	}

	m_vecfScore.clear();

	vector<Rect_<int> > vecRetVal;
	for (int i = 0; i < n; i++)
	{
		if (kp[i] > 0)
		{
			Rect_<int> rt;
			rt.x = (int)bbs[i].second.x;
			rt.y = (int)bbs[i].second.y;
			rt.width = (int)bbs[i].second.width;
			rt.height = (int)bbs[i].second.height;
			//vecRetVal.push_back((Rect_<int>)bbs[i].second);
			vecRetVal.push_back(rt);
			m_vecfScore.push_back(bbs[i].first);
		}
	}

	delete[] kp;

	return vecRetVal;
}

void CPedestrianACF2::GetResultScore(vector<float> &vecScore)
{
	vecScore.clear();
	vecScore.assign(m_vecfScore.begin(), m_vecfScore.end());
}

void CPedestrianACF2::InitThread()
{
	m_flgThread = true;
	m_threadDetection = std::thread(&CPedestrianACF2::RunThread, this);
}

void CPedestrianACF2::SetImageSrc(Mat& imgSrc)
{
	m_mtxImageSrc.lock();

	m_imgSrc = imgSrc.clone();
	m_isNewestImg = true;

	m_mtxImageSrc.unlock();
}

void CPedestrianACF2::SetStixelROI(vector<Rect_<int> >& vecrectROI)
{
	m_mtxStixelROI.lock();

	m_vecrectStixelROI.clear();
	//m_vecrectStixelROI.assign(vecrectROI.begin(), vecrectROI.end()); // src.begin   src.end
	m_isNewestROI = true;////////////// this thread has some problems when new ROI is provided.

	m_mtxStixelROI.unlock();
}

void CPedestrianACF2::GetResultBoundingBox(bool &bDetectFlg, vector<Rect_<int> >& vecrectBB)
{
	m_mtxDetectedBB.lock();

	bDetectFlg = m_bDetection;
	if (bDetectFlg)
	{
		vecrectBB.clear();
		vecrectBB.assign(m_vecrectSharedBB.begin(), m_vecrectSharedBB.end());
		m_bDetection = false;
	}

	m_mtxDetectedBB.unlock();
}

void CPedestrianACF2::GetDetectionImg(Mat& imgSrc)
{
	m_mtx_ImgDetection.lock();

	if (m_imgDetection.empty())
		return;

	imgSrc = m_imgDetection.clone();

	m_mtx_ImgDetection.unlock();
}

void CPedestrianACF2::RunThread()
{
	while (m_flgThread)
	{
		bool isNew;
		m_mtxStixelROI.lock();
		isNew = m_isNewestROI;

		m_mtxImageSrc.lock();
		isNew = isNew & m_isNewestImg;

		if (isNew == false)
		{
			m_mtxStixelROI.unlock();
			m_mtxImageSrc.unlock();
			std::this_thread::sleep_for(chrono::milliseconds(30));
			continue;
		}

		m_isNewestROI = m_isNewestImg = false; // m_isNewest 변수가 true로 바뀌자마자 여기 라인으로 넘어옴

		m_vecrectROIs.clear();
		m_vecrectROIs.assign(m_vecrectStixelROI.begin(), m_vecrectStixelROI.end()); // src.begin   src.end
		Mat imgSrc = m_imgSrc.clone();

		m_mtxStixelROI.unlock();
		m_mtxImageSrc.unlock();

		Detect(imgSrc);

		m_mtxDetectedBB.lock();
		if ((int)m_vecrectResultBB.size())
		{
			m_bDetection = true;
			m_vecrectSharedBB.clear();
			m_vecrectSharedBB.assign(m_vecrectResultBB.begin(), m_vecrectResultBB.end()); // src.begin   src.end
		}
		m_mtxDetectedBB.unlock();

		m_mtx_ImgDetection.lock();	//Detection 시점의 영상 키핑
		m_imgDetection = imgSrc.clone();
		m_mtx_ImgDetection.unlock();
	}
}
