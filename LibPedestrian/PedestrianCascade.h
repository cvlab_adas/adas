#pragma once

#include "PedestrianDetector.h"

class CPedestrianCascade : public CPedestrianDetector
{
public:
	CPedestrianCascade();
	virtual ~CPedestrianCascade();

	/// Classfier을 불러옴
	virtual bool LoadClassifier(string strClassifierFile);

	/// 검출 시작 함수
	virtual void Detect(Mat& imgSrc);

private:
	CascadeClassifier m_objClassifier;
};