#pragma once

#include "PedestrianDetector.h"

#ifdef WIN32
#pragma warning (disable:4996)
#endif // WIN32

class CPedestrianACF : public CPedestrianDetector
{
public:
	/// ctor, 파라미터 초기화 및 동적 메모리 할당
	CPedestrianACF();

	/// dtor, 동적 메모리 해제
	virtual ~CPedestrianACF();

	/// Classfier를 txt에서 불러옴
	virtual bool LoadClassifier(string strClassifierFile);

	/// 검출 시작 함수
	virtual void Detect(Mat& imgSrc);

private:
	//vector<Rect_<int> > m_vecrectDetectedBB; ///////< Detect함수의 결과물로써 검출된 보행자의 bounding box를 vector형태로 저장하고 있음

	Size_<float> m_sizeSmallestPyramid; // smallest scale of the pyramid

	/// Non-maximum suppression을 위한 사전 작업으로써 T.B.D 값 기준 내림차순으로 정렬을 위한 비교 연산 함수
	static bool Comparator(const pair<float, Rect_<float> >& l, const pair<float, Rect_<float> >& r);

	/// T.B.D
	void GetChild(float* chns1, unsigned int* cids, unsigned int* fids, float* thrs, unsigned int offset, unsigned int& k0, unsigned int& k);

	/// Classifier를 이용하여 보행자를 검출하는 부분으로 soft-cascade 방식이 적용되어 있음
	void AcfDetect(Mat& matData, vector<pair<float, Rect_<float> > >& vecRetVal);

	/// Non-maximum suppression
	vector<Rect_<int> > bbNMS(vector<pair<float, Rect_<float> > >& bbs);

	/// feature pyramid를 구성
	void BuildPyramid(Mat& imgSrc, vector<Mat>& matData);

	/// (현재는 사용하고 있지 않다)
	void RgbConvertTo(Mat& imgSrc, Mat& imgDst);

	/// 일반적인 OpenCV 24-bit 컬러 이미지(BGR)를 LUV 컬러 이미지로 변환
	void ConvertBGR2LUV(Mat& imgSrc, Mat& imgDst);

	/// 보행자의 최소 검출 사이즈와 입력 영상 사이즈를 기반으로 총 몇개의 스케일이 필요한지 계산하고
	/// 피라미드 사이즈에 대한 테이블을 만들어둠
	void GetPyramidScales();

	/// (현재는 사용하고 있지 않다)
	void ResampleImg(Mat& imgSrc, Mat& imgDst, int nReHeight, int nReWidth, float ratioVal = 1.0f);

	/// 순차적으로 feature를 계산하고 merge 함
	void ComputeChannelFeature(Mat& imgChn, Mat& imgChns);

	/// 하나의 채널을 여러 채널 이미지에 merge하는 역할
	void AddChannel(Mat& imgChn, vector<Mat>& vecimgChns, int h, int w);

	/// 각 픽셀에서의 Gradient를 계산하고 magnitude와 orientation을 저장
	void CalculateGradMag(Mat& imgSrc, Mat& imgDstMag, Mat& imgDstOrient);

	/// Gradient magnitude를 normalization 하기 위한 전처리 과정으로 삼각형 형태의 필터를 컨볼루션 함
	void ConvTriangle(Mat& imgSrc, Mat& imgDst);

	/// Gradient magnitude를 normalization 함
	void NormalizeGradMag(Mat& imgSrc, Mat& imgDst);

	/// 계산해둔 orientation을 Histogram 화 시킴
	void CalculateGradHist(Mat& imgSrcMag, Mat& imgSrcOrient, Mat& imgDstHist);

	/// (현재는 사용하고 있지 않다) 계산이 완료된 특징 채널을 padding하고 smooth 함\n
	/// 속도는 많이 잡아먹지만, 성능에는 크게 영향이 없는 것으로 보고있음
	void SmoothAndPadChannels(vector<vector<Mat> >& matData);

	/// (현재는 사용하고 있지 않다)
	void ConvFilterChannels(vector<vector<Mat> >& matData, float* chns);

	/// BGR --> LUV 변환 시 생기는 반복 계산과정을 최소화 하기 위하여 lookup table을 만들어 두는 함수
	void BuildLUTofLUV(float* mr, float* mg, float* mb, float& minu, float& minv, float& un, float& vn, float* lut);

	Size_<float> m_sizeInputImage;
	int m_nScales;
	vector<float> m_vecfScales;
	vector<pair<float, float> > m_vecfScalesHW;
	float m_fOverlap;

	// classifier data
	Size_<int> m_sizeModelPad;
	int m_nStride;
	float m_fCascadeThresh;
	int m_nTreeDepth;
	int m_nTreeNodes;
	int m_nTrees;
	float *m_pThrs;
	float *m_pHs;
	unsigned int *m_pFids;
	unsigned int *m_pChild;

	// filter data from file
	vector<Mat> m_matFilter;

	// params
	float m_fShrink;
	float m_fPerOct;
	int m_nAppox;
	int m_nChns;

	int m_nColorFlag; // rgb2luv
	float m_fLambda[10];
	Size2i m_sizePad;

	// gradient params
	bool m_bFullOrient;
	float m_fNormConst;
	int m_nBinSize;
	int m_nOrients;
	int m_nSoftBin;

	// conv_tri params
	int m_nDownSampling;
	int m_nRadius;

	/// T.B.D
	float* acosTable();

	/// T.B.D
	void gradQuantize(float* O, float* M, int* O0, int* O1, float* M0, float* M1, int nb, int n, float norm, int nOrients, bool full, bool interpolate);

	/// T.B.D
	void gradHist(float* M, float* O, float* H, int h, int w, int bin, int nOrients, int softBin, bool full);
};
