#pragma once

#include <thread> // ★★★★★
#include <mutex> // ★★★★★

#define USE_SSE_HR 1

#include "PedestrianDetector.h"

#if USE_SSE_HR
#if defined(__ARM_NEON_FP)
#include "SSE2NEON.h"
#else // !defined(__ARM_NEON_FP)
#include <xmmintrin.h>
#endif // !defined(__ARM_NEON_FP)
#endif // USE_SSE_HR

typedef unsigned int uint32;

class CPedestrianACF2 : public CPedestrianDetector
{
public:
	CPedestrianACF2();
	virtual ~CPedestrianACF2();
	virtual bool LoadClassifier(string strClassifierFile);
	virtual void Detect(Mat& imgSrc);

	/// 내부 Thread 생성 ★★★★★
	void InitThread();

	/// 내부 Thread에서 사용할 원본 이미지를 입력 ★★★★★
	void SetImageSrc(Mat& imgSrc);

	/// 내부 Thread에서 사용할 Stixel ROI 입력 ★★★★★
	void SetStixelROI(vector<Rect_<int> >& vecrectROI);

	/// 내부 Thread에서 계산된 pedestrian bounding box 출력 ★★★★★
	void GetResultBoundingBox(bool &bDetectFlg, vector<Rect_<int> >& vecrectBB);

	/// Detection 시점의 영상 Tracker에 전달
	void GetDetectionImg(Mat& imgSrc);

	bool m_bDetection;		//Detection 되었다 정의

	void GetResultScore(vector<float> &vecScore);
	vector<float> m_vecfScore;

private:

	/// 내부 thread 무한 루프용 변수(dtor에서 해제) ★★★★★
	bool m_flgThread = false;

	/// thread 인스턴스 ★★★★★
	std::thread m_threadDetection;

	/// 내부 thread 호출 함수 ★★★★★
	void RunThread();

	/// 양쪽 thread에서 번갈아가며 호출할 입력 영상 ★★★★★
	Mat m_imgSrc;
	vector<Rect_<int> > m_vecrectStixelROI;
	vector<Rect_<int> > m_vecrectSharedBB;

	/// Tracker에 전달할 
	Mat m_imgDetection;		//Detection 시점의 영상 

	/// m_imgSrc가 최신인지 아닌지 확인하는 변수 ★★★★★
	bool m_isNewestImg = false;
	bool m_isNewestROI = false;

	/// Tracker에 전달 목적
	std::mutex m_mtx_ImgDetection;

	/// m_imgSrc 공유용 mutex ★★★★★
	std::mutex m_mtxImageSrc;

	/// m_vecrectStixelROI 공유용 mutex ★★★★★
	std::mutex m_mtxStixelROI;

	/// m_vecrectDetectedBB 공유용 mutex ★★★★★
	std::mutex m_mtxDetectedBB;

	void BuildPyramid(Mat &imgRGB, vector<Mat> &vecimgChnFtrs);
	// step 1
	void ConvertRGB2LUV(Mat &imgRGB, float* pImgLUV);
	float* rgb2luv_setup(float z, float *mr, float *mg, float *mb, float &minu, float &minv, float &un, float &vn);
	void rgb2luv(unsigned char *I, float *J, int n, float nrm);
	void rgb2luv_sse(unsigned char *I, float *J, int n, float nrm);
	// step 2
	void GetPyramidScales();
	// step 3-1
	void ResampleImage(float *A, float *B, int ha, int hb, int wa, int wb, int d, float r);
	void CalcResampleCoef(int ha, int hb, int &n, int *&yas, int *&ybs, float *&wts, int bd[2], int pad = 0);
	// step 3-2
	void ComputeChannelFeature(Mat &imgLUV, vector<Mat> &vecimgChnFtrs, int h, int w, int d, int idx);
	void AddChannel(Mat &imgChn, Mat &imgChnFtrs, int h, int w, int d, int &posStart);
	// step 5
	void SmoothChannel(vector<Mat> &vecimgChnFtrs);

	void AcfDetect(Mat &imgChnFtrs, vector<pair<float, Rect_<float> > >& vecRetVal);
	vector<Rect_<int> > bbNMS(vector<pair<float, Rect_<float> > >& bbs);
	static bool Comparator(const pair<float, Rect_<float> >& l, const pair<float, Rect_<float> >& r);

	void CalculateGradMag(Mat& imgSrc, Mat& imgDstMag, Mat& imgDstOrient, int d);
	void NormalizeGradMag(Mat& imgSrcDst);
	void CalculateGradHist(Mat& imgSrcMag, Mat& imgSrcOrient, Mat& imgDstHist);
	void gradMag(float *I, float *M, float *O, int h, int w, int d, bool full);
	void grad1(float *I, float *Gx, float *Gy, int h, int w, int x);
	void gradMagNorm(float *M, float *S, int h, int w, float norm);
	float* acosTable();
	void gradHist(float *M, float *O, float *H, int h, int w, int bin, int nOrients, int softBin, bool full);
	void gradQuantize(float *O, float *M, int *O0, int *O1, float *M0, float *M1, int nb, int n, float norm, int nOrients, bool full, bool interpolate);

	void ConvTriangle(Mat& imgSrc, Mat& imgDst);
	void convTriY(float *I, float *O, int h, int r, int s);
	void convTri(float *I, float *O, int h, int w, int d, int r, int s);
	void convTri1Y(float *I, float *O, int h, float p, int s);
	void convTri1(float *I, float *O, int h, int w, int d, float p, int s);

	float m_fWidth;
	float m_fHeight;
	int m_nScales;

	int m_nChns;
	int m_nPadW;
	int m_nPadH;

	int m_nRadius; // convTri 와 관계있음
	int m_nDownSampling; // convTri와 관계있음
	float m_fNormConst; // normalization과 관계있음

	float m_fLambda[10];
	int m_nBinSize;
	int m_nOrients;
	int m_nSoftBin;
	bool m_bFullOrient;

	float m_fPerOct;
	int m_nApprox;
	float m_fShrink;
	vector<float> m_vecfScales;
	vector<pair<float, float> > m_vecfScalesHW;
	float m_fDetectSizeW;
	float m_fDetectSizeH;

	int m_nModelPadH;
	int m_nModelPadW;

	int m_nTreeNodes;// txt와 관계있음
	int m_nTrees;// txt와 관계있음
	float *m_pThrs;
	float *m_pHs;
	unsigned int *m_pFids;
	unsigned int *m_pChild;

	// wrapper functions if compiling from C/C++
	inline void wrError(const char *errormsg) { throw errormsg; }
	inline void* wrCalloc(size_t num, size_t size) { return calloc(num, size); }
	inline void* wrMalloc(size_t size) { return malloc(size); }
	inline void wrFree(void * ptr) { free(ptr); }

	// platform independent aligned memory allocation (see also alFree)
	void* alMalloc(size_t size, int alignment) {
		const size_t pSize = sizeof(void*), a = alignment - 1;
		void *raw = wrMalloc(size + a + pSize);
		void *aligned = (void*)(((size_t)raw + pSize + a) & ~a);
		*(void**)((size_t)aligned - pSize) = raw;
		return aligned;
	}

	// platform independent alignned memory de-allocation (see also alMalloc)
	void alFree(void* aligned) {
		void* raw = *(void**)((char*)aligned - sizeof(void*));
		wrFree(raw);
	}

	inline void getChild(float *chns1, uint32 *cids, uint32 *fids,
		float *thrs, uint32 offset, uint32 &k0, uint32 &k)
	{
		float ftr = chns1[cids[fids[k]]];
		k = (ftr < thrs[k]) ? 1 : 2;
		k0 = k += k0 * 2;
		k += offset;
	}

#if USE_SSE_HR

	// set, load and store values
	inline __m128 SET(const float &x) { return _mm_set1_ps(x); }
	inline __m128 SET(float x, float y, float z, float w) { return _mm_set_ps(x, y, z, w); }
	inline __m128i SET(const int &x) { return _mm_set1_epi32(x); }
	inline __m128 LD(const float &x) { return _mm_load_ps(&x); }
	inline __m128 LDu(const float &x) { return _mm_loadu_ps(&x); }
	inline __m128 STR(float &x, const __m128 y) { _mm_store_ps(&x, y); return y; }
	inline __m128 STR1(float &x, const __m128 y) { _mm_store_ss(&x, y); return y; }
	inline __m128 STRu(float &x, const __m128 y) { _mm_storeu_ps(&x, y); return y; }
	inline __m128 STR(float &x, const float y) { return STR(x, SET(y)); }

	// arithmetic operators
	inline __m128i ADD(const __m128i x, const __m128i y) { return _mm_add_epi32(x, y); }
	inline __m128 ADD(const __m128 x, const __m128 y) { return _mm_add_ps(x, y); }
	inline __m128 ADD(const __m128 x, const __m128 y, const __m128 z) {
		return ADD(ADD(x, y), z);
	}
	inline __m128 ADD(const __m128 a, const __m128 b, const __m128 c, const __m128 &d) {
		return ADD(ADD(ADD(a, b), c), d);
	}
	inline __m128 SUB(const __m128 x, const __m128 y) { return _mm_sub_ps(x, y); }
	inline __m128 MUL(const __m128 x, const __m128 y) { return _mm_mul_ps(x, y); }
	inline __m128 MUL(const __m128 x, const float y) { return MUL(x, SET(y)); }
	inline __m128 MUL(const float x, const __m128 y) { return MUL(SET(x), y); }
	inline __m128 INC(__m128 &x, const __m128 y) { return x = ADD(x, y); }
	inline __m128 INC(float &x, const __m128 y) { __m128 t = ADD(LD(x), y); return STR(x, t); }
	inline __m128 DEC(__m128 &x, const __m128 y) { return x = SUB(x, y); }
	inline __m128 DEC(float &x, const __m128 y) { __m128 t = SUB(LD(x), y); return STR(x, t); }
	inline __m128 MIN_(const __m128 x, const __m128 y) { return _mm_min_ps(x, y); }
	inline __m128 RCP(const __m128 x) { return _mm_rcp_ps(x); }
	inline __m128 SQRT_(const __m128 x) { return _mm_sqrt_ps(x); }
#if !defined(__ARM_NEON_FP)
	inline __m128 RCPSQRT(const __m128 x) { return _mm_rsqrt_ps(x); }
#endif

	// logical operators
	inline __m128 AND(const __m128 x, const __m128 y) { return _mm_and_ps(x, y); }
	inline __m128i AND(const __m128i x, const __m128i y) { return _mm_and_si128(x, y); }
	inline __m128 ANDNOT(const __m128 x, const __m128 y) { return _mm_andnot_ps(x, y); }
	inline __m128 OR(const __m128 x, const __m128 y) { return _mm_or_ps(x, y); }
	inline __m128 XOR(const __m128 x, const __m128 y) { return _mm_xor_ps(x, y); }

	// comparison operators
	inline __m128 CMPGT(const __m128 x, const __m128 y) { return _mm_cmpgt_ps(x, y); }
	inline __m128 CMPLT(const __m128 x, const __m128 y) { return _mm_cmplt_ps(x, y); }
	inline __m128i CMPGT(const __m128i x, const __m128i y) { return _mm_cmpgt_epi32(x, y); }
	inline __m128i CMPLT(const __m128i x, const __m128i y) { return _mm_cmplt_epi32(x, y); }

	// conversion operators
	inline __m128 CVT(const __m128i x) { return _mm_cvtepi32_ps(x); }
	inline __m128i CVT(const __m128 x) { return _mm_cvttps_epi32(x); }
#endif
};

