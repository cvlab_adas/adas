#include "IntegrationPD.h"


CIntegrationPD::CIntegrationPD()
{
}

CIntegrationPD::CIntegrationPD(bool &flgReady)
{
	flgReady = m_objPD.LoadClassifier("../LibPedestrian/ACF_Caltech+.txt");
}

CIntegrationPD::~CIntegrationPD()
{
}

void CIntegrationPD::RunOnce(Mat &imgInput, vector<Object_t> &vecobjPed)
{
	m_objPD.Detect(imgInput);

	vector<Rect> vecrectBB;
	vector<float> vecfScore;
	m_objPD.GetResultRects(vecrectBB);
	m_objPD.GetResultScore(vecfScore);
	
// 	Mat temp = imgInput.clone();
// 	m_objPD.DrawResult(temp);
// 	imshow("sdfsdfdsf", temp);

	for (size_t i = 0; i < vecobjPed.size() && vecrectBB.size(); i++)
	{
		if (vecobjPed[i].nClass != Else)
			continue;

		double dOverlapMax = -1;
		int idxOverlapMax = -1;
		for (size_t j = 0; j < vecrectBB.size(); j++)
		{
			double overlap = boxoverlap(vecobjPed[i].rectBB, vecrectBB[j]);
			if (overlap >= 0.1 && overlap >= dOverlapMax)
			{
				dOverlapMax = overlap;
				idxOverlapMax = j;
			}
		}

		if (idxOverlapMax < 0)
			continue;

		float fNormScore = max(0.f, min(1.f, vecfScore[idxOverlapMax] / 120.f)); // 120 은 나중에 수정 필요

		if (fNormScore > .6f)
		{
			//vecobjPed[i].rectBB = vecrectBB[idxOverlapMax];
			vecobjPed[i].dClassScore = (double)fNormScore;
			vecobjPed[i].nClass = Ped;

			vecrectBB.erase(vecrectBB.begin() + idxOverlapMax);
			vecfScore.erase(vecfScore.begin() + idxOverlapMax);
		}
	}
}

double CIntegrationPD::boxoverlap(Rect_<double> a, Rect_<double> b, int32_t criterion)
{

	// overlap is invalid in the beginning
	double o = -1;

	// get overlapping area
	double x1 = max(a.tl().x, b.tl().x);
	double y1 = max(a.tl().y, b.tl().y);
	double x2 = min(a.br().x, b.br().x);
	double y2 = min(a.br().y, b.br().y);

	// compute width and height of overlapping area
	double w = x2 - x1;
	double h = y2 - y1;

	// set invalid entries to 0 overlap
	if (w <= 0 || h <= 0)
		return 0;

	// get overlapping areas
	double inter = w*h;
	double a_area = a.area();
	double b_area = b.area();

	// intersection over union overlap depending on users choice
	if (criterion == -1)     // union
		o = inter / (a_area + b_area - inter);
	else if (criterion == 0) // bbox_a
		o = inter / a_area;
	else if (criterion == 1) // bbox_b
		o = inter / b_area;

	// overlap
	return o;
}

