#include "PedestrianACF.h"
#include <algorithm>
#include <functional>

CPedestrianACF::CPedestrianACF()
{
	m_fLambda[0] = 0;			m_fLambda[1] = 0;
	m_fLambda[2] = 0;			m_fLambda[3] = 0.11047f;
	m_fLambda[4] = 0.10826f;	m_fLambda[5] = 0.10826f;
	m_fLambda[6] = 0.10826f;	m_fLambda[7] = 0.10826f;
	m_fLambda[8] = 0.10826f;	m_fLambda[9] = 0.10826f;

	m_fShrink = 2;
	m_fPerOct = 8;
	m_nAppox = 8;
	m_sizeSmallestPyramid = Size_<float>(20.5, 50); // smallest scale of the pyramid
	m_sizePad = Size_<int>(6, 8); // not use
	m_nChns = 10; // number of L,U,V,Hist,Orient1,2,3,4,5,6

	m_sizeModelPad = Size_<int>(32, 64);
	m_nStride = 4;
	m_fCascadeThresh = -1.0f;
	m_nTreeNodes = 63;
	m_nTrees = 4096;
	m_nTreeDepth = 0;
	
	m_nColorFlag = 2;

	m_bFullOrient = false;
	m_fNormConst = 0.005f;
	m_nBinSize = 2;
	m_nOrients = 6;
	m_nSoftBin = 1;

	m_nRadius = 5;
	m_nDownSampling = 1;

	m_fOverlap = 0.65f;

	m_pThrs = new float[m_nTreeNodes*m_nTrees];
	m_pHs = new float[m_nTreeNodes*m_nTrees];
	m_pFids = new unsigned int[m_nTreeNodes*m_nTrees];
	m_pChild = new unsigned int[m_nTreeNodes*m_nTrees];
}

CPedestrianACF::~CPedestrianACF()
{
	delete[] m_pThrs;
	delete[] m_pHs;
	delete[] m_pFids;
	delete[] m_pChild;
}

/**
* @param strClassifierFile classifier txt 파일의 경로를 입력함
* @return txt 파일 존재 유무
*/
bool CPedestrianACF::LoadClassifier(string strClassifierFile)
{
	FILE *fp;

	fp = fopen(strClassifierFile.c_str(), "rt");

	if (fp == NULL) return false;

	for (int i = 0; i < m_nTreeNodes*m_nTrees; i++)
		fscanf(fp, "%f", m_pThrs + i);

	for (int i = 0; i < m_nTreeNodes*m_nTrees; i++)
		fscanf(fp, "%f", m_pHs + i);

	for (int i = 0; i < m_nTreeNodes*m_nTrees; i++)
		fscanf(fp, "%d", m_pFids + i);

	for (int i = 0; i < m_nTreeNodes*m_nTrees; i++)
		fscanf(fp, "%d", m_pChild + i);

	fclose(fp);

	return true;
}

/**
* @param imgSrc 보행자 검출을 수행할 영역의 이미지
* @return 보행자 Bounding box
*/
void CPedestrianACF::Detect(Mat& imgSrc)
{
	m_vecrectResultBB.clear();

	bool bEmptyROI = false;
	if (m_vecrectROIs.empty())
		bEmptyROI = true;

	for (int i = 0; i < (int)m_vecrectROIs.size() || bEmptyROI; i++)
	{
		Mat imgROI;
		if (bEmptyROI)
			imgROI = imgSrc;
		else
			imgROI = imgSrc(m_vecrectROIs[i]);

		if ((float)imgROI.cols < m_sizeSmallestPyramid.width || (float)imgROI.rows < m_sizeSmallestPyramid.height)
			continue;

		vector<Rect_<int> > vecrectDetected;

		vector<Mat> chnsFtrs;
		BuildPyramid(imgROI, chnsFtrs);

		vector<pair<float, Rect_<float> > > vecRawBB;
		vector<pair<float, Rect_<float> > > vecShiftBB;
		for (int i = 0; i < (int)chnsFtrs.size(); i++)
		{
			vecRawBB.clear();
			AcfDetect(chnsFtrs[i], vecRawBB);

			for (int j = 0; j < (int)vecRawBB.size(); j++)
			{
				vecRawBB[j].second.x /= m_vecfScalesHW[i].second;
				vecRawBB[j].second.y /= m_vecfScalesHW[i].first;
				vecRawBB[j].second.width = m_sizeSmallestPyramid.width / m_vecfScales[i];
				vecRawBB[j].second.height = m_sizeSmallestPyramid.height / m_vecfScales[i];
				vecShiftBB.push_back(vecRawBB[j]);
			}
		}

		vector<Rect_<int> > vecResultBB = bbNMS(vecShiftBB);
		for (int i = 0; i < (int)vecResultBB.size(); i++)
			m_vecrectResultBB.push_back(vecResultBB[i]);

		bEmptyROI = false;
	}
}

/**
* @param l 
* @param r 
* @return 
*/
bool CPedestrianACF::Comparator(const pair<float, Rect_<float> >& l, const pair<float, Rect_<float> >& r) // 첫번째 값을 기준으로 내림차순 비교하는 comparator
{
	return l.first > r.first;
}

/**
* @param chns1 
* @param cids 
* @param fids 
* @param thrs 
* @param offset 
* @param k0 
* @param k 
* @return 
*/
void CPedestrianACF::GetChild(float *chns1, unsigned int *cids, unsigned int *fids,
	float *thrs, unsigned int offset, unsigned int &k0, unsigned int &k)
{
	float ftr = chns1[cids[fids[k]]];
	k = (ftr < thrs[k]) ? 1 : 2;
	k0 = k += k0 * 2; k += offset;
}

/**
* @param bbs 
* @return 
* @see http://www.pyimagesearch.com/2014/11/17/non-maximum-suppression-object-detection-python/
*/
vector<Rect_<int> > CPedestrianACF::bbNMS(vector<pair<float, Rect_<float> > >& bbs)
{
	stable_sort(bbs.begin(), bbs.end(), Comparator);

	int n = (int)bbs.size();
	int* kp = new int[n];
	for (int i = 0; i < n; i++)
		kp[i] = 1;

	for (int i = 0; i < n; i++)
	{
		if (kp[i] == 0)
			continue;

		for (int j = i + 1; j < n; j++)
		{
			if (kp[j] == 0)
				continue;

			float iw = min(bbs[i].second.br().x, bbs[j].second.br().x) - max(bbs[i].second.x, bbs[j].second.x);
			if (iw <= 0)
				continue;

			float ih = min(bbs[i].second.br().y, bbs[j].second.br().y) - max(bbs[i].second.y, bbs[j].second.y);
			if (ih <= 0)
				continue;

			float o = iw*ih;
			float u = min(bbs[i].second.area(), bbs[j].second.area());

			if (o / u > m_fOverlap)
				kp[j] = 0;
		}
	}

	vector<Rect_<int> > vecRetVal;
	for (int i = 0; i < n; i++)
	{
		if (kp[i] > 0)
		{
			Rect_<int> rt;
			rt.x = (int)bbs[i].second.x;
			rt.y = (int)bbs[i].second.y;
			rt.width = (int)bbs[i].second.width;
			rt.height = (int)bbs[i].second.height;
			//vecRetVal.push_back((Rect_<int>)bbs[i].second);
			vecRetVal.push_back(rt);
		}
	}

	delete[] kp;

	return vecRetVal;
}

/**
* @param matData 
* @param vecRetVal 
* @return 
*/
void CPedestrianACF::AcfDetect(Mat& matData, vector<pair<float, Rect_<float> > >& vecRetVal)
{
	int cntLen = matData.total() * matData.channels();

	float* chns = new float[cntLen];

	int height = matData.rows;
	int width = matData.cols;
	int cntAdd = 0;

	vector<Mat> vecimgSplit;
	split(matData, vecimgSplit);
	for (int i = 0; i < (int)vecimgSplit.size(); i++)
	{
		Mat temp;
		transpose(vecimgSplit[i], temp);
		float* idx = (float*)temp.data;
		memcpy(chns + cntAdd, idx, sizeof(float)*height*width);
		cntAdd += height*width;
	}

	int nShrink = (int)m_fShrink;
	const int height1 = (int)ceil(float(height*nShrink - m_sizeModelPad.height + 1) / m_nStride);
	const int width1 = (int)ceil(float(width*nShrink - m_sizeModelPad.width + 1) / m_nStride);

	// construct cids array
	int nFtrs = m_sizeModelPad.height / nShrink*m_sizeModelPad.width / nShrink*m_nChns;
	unsigned int *cids = new unsigned int[nFtrs];
	int m = 0;
	for (int z = 0; z < m_nChns; z++)
	{
		for (int c = 0; c < m_sizeModelPad.width / nShrink; c++)
		{
			for (int r = 0; r < m_sizeModelPad.height / nShrink; r++)
			{
				cids[m++] = z*height*width + c*height + r;
			}
		}
	}

	// apply classifier to each patch
	vector<int> rs, cs; vector<float> hs1;
	for (int c = 0; c < width1; c++)
	{
		for (int r = 0; r < height1; r++)
		{
			float h = 0;
			float *chns1 = chns + (r*m_nStride / nShrink) + (c*m_nStride / nShrink)*height;

			// general case (variable tree depth)
			for (int t = 0; t < m_nTrees; t++)
			{
				unsigned int offset = t*m_nTreeNodes;
				unsigned int k = offset;
				unsigned int k0 = k;

				while (m_pChild[k])
				{
					float ftr = chns1[cids[m_pFids[k]]];

					k = (ftr < m_pThrs[k]) ? 1 : 0;

					k0 = k = m_pChild[k0] - k + offset;
				}
				h += m_pHs[k];

				if (h <= m_fCascadeThresh)
					break;
			}
			if (h > m_fCascadeThresh)
			{
				cs.push_back(c);
				rs.push_back(r);
				hs1.push_back(h);
			}
		}
	}
	delete[] cids;
	delete[] chns;
	m = cs.size();

	// convert to bbs
	pair<float, Rect_<float> > pair1;
	for (int i = 0; i < m; i++)
	{
		pair1.first = hs1[i];
		//pair1.second = Rect_<float>(Rect_<int>(cs[i] * m_nStride, rs[i] * m_nStride, m_sizeModelPad.width, m_sizeModelPad.height));
		//pair1.second = Rect_<float>(Rect_<int>(cs[i] * m_nStride+5, rs[i] * m_nStride+5, m_sizeModelPad.width, m_sizeModelPad.height));
		pair1.second = Rect_<float>((float)(cs[i] * m_nStride + 5), (float)(rs[i] * m_nStride + 5),
			(float)m_sizeModelPad.width, (float)m_sizeModelPad.height);
		vecRetVal.push_back(pair1);
	}
}

/**
* @param imgSrc 
* @param matData 
* @return 
*/
void CPedestrianACF::BuildPyramid(Mat& imgSrc, vector<Mat>& matData)
{
	// step 1 : RGB to LUV
	Mat imgLUV32f;
#if USE_SSE
	RgbConvertTo(imgSrc, imgLUV32f);
#else
	ConvertBGR2LUV(imgSrc, imgLUV32f);
#endif

	// step 2 : get pyramid scales
	if (m_sizeInputImage.operator Size_<int>() != imgSrc.size())
	{
		m_sizeInputImage = imgSrc.size();
		GetPyramidScales();
	}

	// step 3 : channel computation along the real scales
	matData.resize(m_nScales);
	for (int i = 0; i < m_nScales; i += m_nAppox) // "isR" part
	{
		float s = m_vecfScales[i];
		int nDownHeight = (int)(round(m_sizeInputImage.height * s / m_fShrink)*m_fShrink);
		int nDownWidth = (int)(round(m_sizeInputImage.width* s / m_fShrink)*m_fShrink);

		Mat imgOneScale;
		if (i == 0)
			imgOneScale = imgLUV32f.clone();
		else
		{
			resize(imgLUV32f, imgOneScale, Size(nDownWidth, nDownHeight));
		}

		if (s == 0.5)
			imgLUV32f = imgOneScale.clone();

		ComputeChannelFeature(imgOneScale, matData[i]);
	}

	// step 4 : channel computation along the approx. scales
	vector<int> vecRef;
	for (int i = 0; i < m_nScales; i += m_nAppox)
	{
		if (i + m_nAppox < m_nScales)
		{
			int j = (int)((i + i + m_nAppox) / 2);
			vecRef.push_back(j);
		}
	}
	vecRef.push_back(m_nScales - 1);

	for (int i = 0, jcnt = 0; i < m_nScales; i++) // "isA" part
	{
		if (i % m_nAppox == 0)
			continue;

		if (i <= vecRef[jcnt])
		{
			int iR = jcnt*m_nAppox;
			int h1 = (int)round(m_sizeInputImage.height*m_vecfScales[i] / m_fShrink);
			int w1 = (int)round(m_sizeInputImage.width*m_vecfScales[i] / m_fShrink);

#if USE_SSE
			vector<Mat> vecSplit;
			split(matData[iR], vecSplit);

			vector<Mat> vecMerge;
			vecMerge.resize((int)vecSplit.size());
			for (int j = 0; j < (int)vecSplit.size(); j++)
			{
				float ratio_ = pow(m_vecfScales[i] / m_vecfScales[iR], -m_fLambda[j]);
				Mat imgReszed;
				ResampleImg(vecSplit[j], imgReszed, h1, w1, (float)ratio_);
				vecMerge[j] = imgReszed.clone();
			}
			merge(vecMerge, matData[i]);
#else
			Mat imgReszed;
			resize(matData[iR], imgReszed, Size(w1, h1));
			matData[i] = imgReszed.clone();
#endif
		}
		else
		{
			jcnt++;
			i--;
		}
	}

	// step 5 : smooth and padding channels
	//SmoothAndPadChannels(matData); // 이것이 속도를 꽤 잡아먹는다. 별거 아닌것같은데...
}

/**
* @param imgSrc 
* @param imgDst 
* @return 
*/
void CPedestrianACF::ConvertBGR2LUV(Mat& imgSrc, Mat& imgDst)
{
	static bool bInit = false;
	static float minu, minv, un, vn, mr[3], mg[3], mb[3];
	static float lTable[1064];
	if (!bInit)
	{
		BuildLUTofLUV(mr, mg, mb, minu, minv, un, vn, lTable);
		bInit = true;
	}

	vector<Mat> vecimgSrc, vecimgDst;
	split(imgSrc, vecimgSrc);

	imgDst = Mat::zeros(imgSrc.size(), CV_32FC3);
	split(imgDst, vecimgDst);

	const int n = imgSrc.rows * imgSrc.cols;

	unsigned char *B = vecimgSrc[0].data;
	unsigned char *G = vecimgSrc[1].data;
	unsigned char *R = vecimgSrc[2].data;

	float *L = (float*)vecimgDst[0].data;
	float *U = (float*)vecimgDst[1].data;
	float *V = (float*)vecimgDst[2].data;

	for (int i = 0; i < n; i++)
	{
		float r, g, b, x, y, z, l;

		r = (float)*R++;
		g = (float)*G++;
		b = (float)*B++;

		x = mr[0] * r + mg[0] * g + mb[0] * b;
		y = mr[1] * r + mg[1] * g + mb[1] * b;
		z = mr[2] * r + mg[2] * g + mb[2] * b;

		l = lTable[(int)(y * 1024)];

		*(L++) = l;
		z = 1 / (x + 15 * y + 3 * z + (float)1e-35);

		*(U++) = l * (13 * 4 * x*z - 13 * un) - minu;

		*(V++) = l * (13 * 9 * y*z - 13 * vn) - minv;
	}

	merge(vecimgDst, imgDst);
}

/**
* @param mr 
* @param mg 
* @param mb 
* @param minu 
* @param minv 
* @param un 
* @param vn 
* @param lut 
* @return 
*/
void CPedestrianACF::BuildLUTofLUV(float* mr, float* mg, float* mb, float& minu, float& minv, float& un, float& vn, float* lut)
{
	// set constants for conversion
	const float y0 = (float)((6.0 / 29)*(6.0 / 29)*(6.0 / 29));
	const float a = (float)((29.0 / 3)*(29.0 / 3)*(29.0 / 3));
	const float z = 1.f / 255.f;
	un = (float) 0.197833;
	vn = (float) 0.468331;
	mr[0] = (float) 0.430574*z; mr[1] = (float) 0.222015*z; mr[2] = (float) 0.020183*z;
	mg[0] = (float) 0.341550*z; mg[1] = (float) 0.706655*z; mg[2] = (float) 0.129553*z;
	mb[0] = (float) 0.178325*z; mb[1] = (float) 0.071330*z; mb[2] = (float) 0.939180*z;
	float maxi = (float)(1.0 / 270);
	minu = -88 * maxi;
	minv = -134 * maxi;
	// build (padded) lookup table for y->l conversion assuming y in [0,1]
	float y, l;
	for (int i = 0; i < 1025; i++) {
		y = (float)(i / 1024.0);
		l = y > y0 ? 116 * (float)pow((double)y, 1.0 / 3.0) - 16 : y*a;
		lut[i] = l*maxi;
	}
	for (int i = 1025; i < 1064; i++)
		lut[i] = lut[i - 1];
}

/**
* @param imgSrc 
* @param imgDst 
* @return 
*/
void CPedestrianACF::RgbConvertTo(Mat& imgSrc, Mat& imgDst)
{
#if USE_SSE
	int n = imgSrc.rows * imgSrc.cols;
	int d = imgSrc.channels();

	unsigned char* temp = new unsigned char[n*d];
	int cnt = 0;
	for (int k = d - 1; k >= 0; k--)
		for (int j = 0; j < imgSrc.cols; j++)
			for (int i = 0; i < imgSrc.rows; i++)
				temp[cnt++] = imgSrc.data[(i*imgSrc.cols + j) * d + k];

	void *J = (void*)rgbConvert(temp, n, d, m_nColorFlag, 1.0f / 255);

	imgDst = Mat::zeros(imgSrc.rows, imgSrc.cols, CV_32FC3);

	float* temp2 = (float*)imgDst.data;
	cnt = 0;
	for (int k = 0; k < d; k++)
		for (int j = 0; j < imgDst.cols; j++)
			for (int i = 0; i < imgDst.rows; i++)
				temp2[(i*imgDst.cols + j) * d + k] = ((float*)J)[cnt++];

	delete[] temp;
	free(J);
#endif
}

/**
* @return 
*/
void CPedestrianACF::GetPyramidScales()
{
	// ksizeSmallestPyramid 만큼까지 작아지려면 몇 개의 스케일이 필요한가?
	m_nScales = (int)floor(m_fPerOct*(log2(min(m_sizeInputImage.height / m_sizeSmallestPyramid.height,
		m_sizeInputImage.width / m_sizeSmallestPyramid.width))) + 1);

	m_vecfScales.clear();
	for (int i = 0; i < m_nScales; i++)
	{
		// 2^0, 2^(-1/8), 2^(-2/8), ... 하면서 계속 스케일을 작게 만듬.
		float dScale = pow(2.0f, (float)-i / m_fPerOct);

		if (i == 0)
		{
			m_vecfScales.push_back(dScale);
			continue;
		}

		float s0 = (float)(round(m_sizeInputImage.height*dScale / m_fShrink)*m_fShrink - 0.25*m_fShrink) / m_sizeInputImage.height;
		float s1 = (float)(round(m_sizeInputImage.height*dScale / m_fShrink)*m_fShrink + 0.25*m_fShrink) / m_sizeInputImage.height;
		float ss[101];
		float es0[101];
		float es1[101];
		float maxEs0Es1[101];
		int minIdx = -1;
		float minVal = 9999;
		for (int j = 0; j <= 100; j++)
		{
			ss[j] = (float)j*0.01f*(s1 - s0) + s0;
			es0[j] = m_sizeInputImage.height * ss[j];
			es0[j] = abs(es0[j] - round(es0[j] / m_fShrink)*m_fShrink);
			es1[j] = m_sizeInputImage.width * ss[j];
			es1[j] = abs(es1[j] - round(es1[j] / m_fShrink)*m_fShrink);
			maxEs0Es1[j] = (es0[j] > es1[j]) ? es0[j] : es1[j];
			if (maxEs0Es1[j] < minVal)
			{
				minVal = maxEs0Es1[j];
				minIdx = j;
			}
		}
		m_vecfScales.push_back(ss[minIdx]);
	}

	m_vecfScalesHW.clear();

	for (int i = 0; i < m_nScales - 1; i++)
	{
		if (abs(m_vecfScales[i] - m_vecfScales[i + 1]) < 0.00001f)
		{
			m_vecfScales.erase(m_vecfScales.begin() + i);
			i--;
			continue;
		}
		pair<float, float> scaleHW;
		scaleHW.first = round(m_sizeInputImage.height*m_vecfScales[i] / m_fShrink)*m_fShrink / m_sizeInputImage.height;
		scaleHW.second = round(m_sizeInputImage.width*m_vecfScales[i] / m_fShrink)*m_fShrink / m_sizeInputImage.width;
		m_vecfScalesHW.push_back(scaleHW);
	}
}

/**
* @param imgSrc 
* @param imgDst 
* @param nReHeight 
* @param nReWidth 
* @param ratioVal 
* @return 
*/
void CPedestrianACF::ResampleImg(Mat& imgSrc, Mat& imgDst, int nReHeight, int nReWidth, float ratioVal)
{
#if USE_SSE

	if (imgSrc.channels() > 1)
	{
		vector<Mat> vecimgOrigScale;
		vector<Mat> vecimgDownScale;
		Mat imgOut;
		if (imgSrc.type() == CV_32FC3)
			imgOut = Mat::zeros(nReHeight, nReWidth, CV_32FC1);
		else if (imgSrc.type() == CV_8UC3)
			imgOut = Mat::zeros(nReHeight, nReWidth, CV_8UC1);

		split(imgSrc, vecimgOrigScale);
		for (int i = 0; i < imgSrc.channels(); i++)
		{
			Mat imgIn = vecimgOrigScale[i];
			if (imgSrc.type() == CV_32FC3)
				resample((float*)imgIn.data, (float*)imgOut.data, imgSrc.cols, nReWidth, imgSrc.rows, nReHeight, 1, ratioVal);
			else if (imgSrc.type() == CV_8UC3)
				resample((unsigned char*)imgIn.data, (unsigned char*)imgOut.data, imgSrc.cols, nReWidth, imgSrc.rows, nReHeight, 1, (unsigned char)ratioVal);

			vecimgDownScale.push_back(imgOut.clone());
		}

		imgDst = Mat::zeros(nReWidth, nReWidth, imgSrc.type());
		merge(vecimgDownScale, imgDst);
	}
	else
	{
		Mat imgOut;
		if (imgSrc.type() == CV_32FC1)
		{
			imgOut = Mat::zeros(nReHeight, nReWidth, CV_32FC1);
			resample((float*)imgSrc.data, (float*)imgOut.data, imgSrc.cols, nReWidth, imgSrc.rows, nReHeight, 1, (float)ratioVal);
		}
		else if (imgSrc.type() == CV_8UC1)
		{
			imgOut = Mat::zeros(nReHeight, nReWidth, CV_8UC1);
			resample((unsigned char*)imgSrc.data, (unsigned char*)imgOut.data, imgSrc.cols, nReWidth, imgSrc.rows, nReHeight, 1, (unsigned char)ratioVal);
		}
		imgDst = imgOut.clone();
	}

#endif
}

/**
* @param imgChn 
* @param imgChns 
* @return 
*/
void CPedestrianACF::ComputeChannelFeature(Mat& imgChn, Mat& imgChns)
{
	int nShrink = (int)m_fShrink;

	// step 0 : crop image
	int h = imgChn.rows;
	int w = imgChn.cols;

	int cr1 = h%nShrink;
	int cr2 = w%nShrink;
	if (cr1 || cr2)
	{
		h -= cr1;
		w -= cr2;
		Mat imgCrop = imgChn(Range(0, h), Range(0, w));
		imgChn = imgCrop.clone();
	}

	h /= nShrink;
	w /= nShrink;

	vector<Mat> vecimgChns;

	// step 1 : compute color channels
	AddChannel(imgChn, vecimgChns, h, w);

	// step 2 : compute gradient magnitude channel
	Mat imgMag, imgOrient, imgMagNorm;
	CalculateGradMag(imgChn, imgMag, imgOrient);
	NormalizeGradMag(imgMag, imgMagNorm);
	AddChannel(imgMagNorm, vecimgChns, h, w);

	// step 3 : compute gradient histogram channel
	Mat imgHist;
	CalculateGradHist(imgMagNorm, imgOrient, imgHist);
	AddChannel(imgHist, vecimgChns, h, w);

	merge(vecimgChns, imgChns);
}

/**
* @param imgChn 
* @param vecimgChns 
* @param h 
* @param w 
* @return 
*/
void CPedestrianACF::AddChannel(Mat& imgChn, vector<Mat>& vecimgChns, int h, int w)
{
	int h1 = imgChn.rows;
	int w1 = imgChn.cols;

	vector<Mat> vecimgSrc;
	if (imgChn.channels() > 1)
		split(imgChn, vecimgSrc);
	else
		vecimgSrc.push_back(imgChn);

	for (int i = 0; i < (int)vecimgSrc.size(); i++)
	{
		Mat imgShrink;
		if (h1 != h || w1 != w)
#if USE_SSE
			ResampleImg(vecimgSrc[i], imgShrink, h, w);
#else
			resize(vecimgSrc[i], imgShrink, Size(w, h));
#endif
		else
			imgShrink = vecimgSrc[i].clone();

		//vecimgChns.push_back(imgShrink.clone());
		vecimgChns.push_back(imgShrink);
	}
}

/**
* @param imgSrc 
* @param imgDstMag 
* @param imgDstOrient 
* @return 
*/
void CPedestrianACF::CalculateGradMag(Mat& imgSrc, Mat& imgDstMag, Mat& imgDstOrient)
{
	int h = imgSrc.rows;
	int w = imgSrc.cols;
	int d = imgSrc.channels();

#if USE_SSE

	Mat temp = Mat::zeros(w, h, CV_32FC1);
	Mat temp2 = Mat::zeros(w, h, CV_32FC1);

	float* I = new float[h*w*d];
	float* M = (float*)temp.data;
	float* O = (float*)temp2.data;

	Mat imgT;
	transpose(imgSrc, imgT);

	vector<Mat> vecimg;
	split(imgT, vecimg);

	float* idx;
	for (int i = 0; i < d; i++)
	{
		idx = (float*)vecimg[i].data;
		memcpy(I + h*w*i, idx, sizeof(float)*h*w);
	}

	gradMag(I, M, O, h, w, d, m_bFullOrient);

	transpose(temp, imgDstMag);
	transpose(temp2, imgDstOrient);

	delete[] I;

#else

	Mat imgT;
	transpose(imgSrc, imgT);

	vector<Mat> vecimg;
	split(imgT, vecimg);

	Mat matKernelX = Mat::zeros(1, 3, CV_32FC1);
	Mat matKernelY = Mat::zeros(3, 1, CV_32FC1);
	matKernelX.at<float>(0) = matKernelY.at<float>(0) = -0.5f;
	matKernelX.at<float>(2) = matKernelY.at<float>(2) = 0.5f;

	Mat imgGx;
	Mat imgGy;
	Mat imgM2;

	Mat imgMagMax = Mat::zeros(vecimg[0].size(), CV_32FC1) - 10000.f;
	Mat imgGxMax = Mat::zeros(vecimg[0].size(), CV_32FC1) - 10000.f;
	Mat imgGyMax = Mat::zeros(vecimg[0].size(), CV_32FC1) - 10000.f;

	for (int i = 0; i < d; i++)
	{
		filter2D(vecimg[i], imgGy, -1, matKernelX, Point(-1, -1), 0.0, BORDER_REPLICATE);
		filter2D(vecimg[i], imgGx, -1, matKernelY, Point(-1, -1), 0.0, BORDER_REPLICATE);
		magnitude(imgGx, imgGy, imgM2);

		if (i == 0)
		{
			imgM2.copyTo(imgMagMax);
			imgGx.copyTo(imgGxMax);
			imgGy.copyTo(imgGyMax);
		}
		else
		{
			Mat cmp = (imgM2 > imgMagMax) / 255;
			Mat cmp2;
			cmp.convertTo(cmp2, CV_32FC1);
			cmp2 *= -0.f;
			imgMagMax = (cmp2 & imgM2) | (~cmp2 & imgMagMax);
			imgGxMax = (cmp2 & imgGx) | (~cmp2 & imgGxMax);
			imgGyMax = (cmp2 & imgGy) | (~cmp2 & imgGyMax);
		}
	}

	Mat imgGxMax2 = imgGxMax / imgMagMax;
	Mat imgGxMax3 = imgGxMax2 * 10000.f;

	Mat imgOrient = Mat::zeros(vecimg[0].size(), CV_32FC1);
	float* pOri = (float*)imgOrient.data;
	float* pGx = (float*)imgGxMax3.data;
	float* pGy = (float*)imgGyMax.data;
	float *acost = acosTable();

	for (int i = 0; i < w*h; i++)
	{
		if (pGy[i] < 0)
			pGx[i] = -pGx[i];
		pOri[i] = acost[(int)pGx[i]];
	}

	transpose(imgMagMax, imgDstMag);
	transpose(imgOrient, imgDstOrient);

#endif

}

// private
void CPedestrianACF::ConvTriangle(Mat& imgSrc, Mat& imgDst)
{
#if USE_SSE

	int h = imgSrc.rows;
	int w = imgSrc.cols;
	int d = imgSrc.channels();

	Mat temp1;
	transpose(imgSrc, temp1);
	Mat temp2 = Mat::zeros(w, h, CV_32FC1);

	float* A = (float*)temp1.data;
	float* B = (float*)temp2.data;

	convTri(A, B, h, w, d, m_nRadius, m_nDownSampling);

	transpose(temp2, imgDst);

#else

	Mat temp1;
	transpose(imgSrc, temp1);

	float fKernel[11*11] = {
		1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1,
		2, 4, 6, 8,10,12,10, 8, 6, 4, 2,
		3, 6, 9,12,15,18,15,12, 9, 6, 3,
		4, 8,12,16,20,24,20,16,12, 8, 4,
		5,10,15,20,25,30,25,20,15,10, 5,
		6,12,18,24,30,36,30,24,18,12, 6,
		5,10,15,20,25,30,25,20,15,10, 5,
		4, 8,12,16,20,24,20,16,12, 8, 4,
		3, 6, 9,12,15,18,15,12, 9, 6, 3,
		2, 4, 6, 8,10,12,10, 8, 6, 4, 2,
		1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1
	};
	Mat matKernel(11, 11, CV_32FC1, fKernel);
	matKernel *= 1 / 1296.f;

	Mat imgSrc1 = temp1.clone();
	Mat imgFilt;
	filter2D(imgSrc1, imgFilt, -1, matKernel);

	transpose(imgFilt, imgDst);

#endif
}

// private
void CPedestrianACF::NormalizeGradMag(Mat& imgSrc, Mat& imgDst)
{
	Mat imgSmooth;

	ConvTriangle(imgSrc, imgSmooth);

#if USE_SSE

 	int h = imgSrc.rows;
 	int w = imgSrc.cols;
 
 	Mat temp1, temp2;
 	transpose(imgSrc, temp1);
 	transpose(imgSmooth, temp2);
 
 	float* M = (float*)temp1.data;
 	float* S = (float*)temp2.data;
 
 	gradMagNorm(M, S, h, w, m_fNormConst);
 
 	transpose(temp1, imgDst);

#else

	imgSmooth += m_fNormConst;

	imgDst = imgSrc / imgSmooth;

#endif
}

// private
void CPedestrianACF::CalculateGradHist(Mat& imgSrcMag, Mat& imgSrcOrient, Mat& imgDstHist)
{
	int h = imgSrcMag.rows;
	int w = imgSrcMag.cols;

	int h2 = h / m_nBinSize;
	int w2 = w / m_nBinSize;

	float* H = new float[h2 * w2 * m_nOrients];
	memset(H, 0, sizeof(float)*h2 * w2 * m_nOrients);

	Mat temp1, temp2;
	transpose(imgSrcMag, temp1);
	transpose(imgSrcOrient, temp2);

	float* M = (float*)temp1.data;
	float* O = (float*)temp2.data;

	gradHist(M, O, H, h, w, m_nBinSize, m_nOrients, m_nSoftBin, m_bFullOrient);

	Mat imgDstHistPre;
	Mat dummy = Mat::zeros(w2, h2, CV_32FC1);
	vector<Mat> vecimgHist;
	for (int i = 0; i < m_nOrients; i++)
	{
		vecimgHist.push_back(dummy.clone());
		float* idx = (float*)vecimgHist[i].data;
		memcpy(idx, H + h2*w2*i, sizeof(float)*h2*w2);
	}

	merge(vecimgHist, imgDstHistPre);

	transpose(imgDstHistPre, imgDstHist);

	delete[] H;

	//////////////////////////////////////////////////////////////////////////
	


	//////////////////////////////////////////////////////////////////////////
}

// private
void CPedestrianACF::SmoothAndPadChannels(vector<vector<Mat> >& matData)
{
	int nShrink = (int)m_fShrink;

	for (int i = 0; i < (int)matData.size(); i++)
	{
		for (int j = 0; j < (int)matData[i].size(); j++)
		{
			int h = matData[i][j].rows;
			int w = matData[i][j].cols;
			int d = matData[i][j].channels();

			Mat temp1;
			transpose(matData[i][j], temp1);
			Mat temp2 = Mat::zeros(w, h, CV_32FC1);

			float* A = (float*)temp1.data;
			float* B = (float*)temp2.data;

			//convTri1(A, B, h, w, d, 2.0f, 1);

			Mat temp3 = Mat::zeros(w + m_sizePad.width, h + m_sizePad.height, CV_32FC1);
			float* C = (float*)temp3.data;

			int boundary = j < 3 ? 1 : 0;

			//imPad(B, C, h, w, d, m_sizePad.height / nShrink, m_sizePad.height / nShrink,
			//	m_sizePad.width / nShrink, m_sizePad.width / nShrink, boundary, 0.0f);

			
			transpose(temp3, matData[i][j]);
		}
	}

}

// private
void CPedestrianACF::ConvFilterChannels(vector<vector<Mat> >& matData, float* chns)
{
	vector<vector<Mat> > matData40;
	matData40.resize(m_nScales);
	int cnt = 0;
	for (int i = 0; i < m_nScales; i++)
	{
		matData40[i].resize(40);
		for (int j = 0; j < m_nChns * 4; j++)
		{
			int k = j % m_nChns;
			Mat temp;
			filter2D(matData[i][k], temp, -1, m_matFilter[j]);

			Mat temp2;
#if USE_SSE
			ResampleImg(temp, temp2, (temp.rows + 1) / 2, (temp.cols + 1) / 2);
#else
			resize(temp, temp2, Size((temp.rows + 1) / 2, (temp.cols + 1) / 2));
#endif

			matData40[i][j] = temp2.clone();
			cnt += temp2.total();
		}
	}

	chns = new float[cnt];
	int cnt2 = 0;
	for (int i = 0; i < m_nScales; i++)
	{
		for (int j = 0; j < m_nChns * 4; j++)
		{
			int h = matData40[i][j].rows;
			int w = matData40[i][j].cols;
			float* idx = (float*)matData40[i][j].data;
			memcpy(chns + cnt2, idx, sizeof(float)*h*w);
			cnt2 += h*w;
		}
	}
}

// build lookup table a[] s.t. a[x*n]~=acos(x) for x in [-1,1]
float* CPedestrianACF::acosTable()
{
	const int n = 10000, b = 10;
	int i;
	static float a[n * 2 + b * 2];
	static bool init = false;

	float *a1 = a + n + b;
	if (init)
		return a1;

	for (i = -n - b; i < -n; i++)
		a1[i] = (float)CV_PI;

	for (i = -n; i < n; i++)
		a1[i] = float(acos(i / float(n)));

	for (i = n; i < n + b; i++)
		a1[i] = 0;

	for (i = -n - b; i<n / 10; i++)
		if (a1[i] > (float)CV_PI - 1e-6f)
			a1[i] = (float)CV_PI - 1e-6f;

	init = true;

	return a1;
}

// compute nOrients gradient histograms per bin x bin block of pixels
void CPedestrianACF::gradHist(float* M, float* O, float* H, int h, int w,
	int bin, int nOrients, int softBin, bool full)
{
	const int hb = h / bin;
	const int wb = w / bin;
	const int h0 = hb*bin;
	const int w0 = wb*bin;
	const int nb = wb*hb;
	const float s = (float)bin;
	const float sInv = 1 / s;
	const float sInv2 = 1 / s / s;

	float *H0, *M0, *M1;
	int x, y;
	int *O0, *O1;
	float xb, init;

	O0 = (int*)malloc(h*sizeof(int));
	M0 = (float*)malloc(h*sizeof(float));

	O1 = (int*)malloc(h*sizeof(int));
	M1 = (float*)malloc(h*sizeof(float));

	// main loop
	for (x = 0; x < w0; x++)
	{
		// compute target orientation bins for entire column - very fast
		gradQuantize(O + x*h, M + x*h, O0, O1, M0, M1, nb, h0, sInv2, nOrients, full, softBin >= 0);

		// interpolate using trilinear interpolation
		float ms[4], xyd, yb, xd, yd;
		bool hasLf, hasRt;
		int xb0, yb0;

		if (x == 0)
		{
			init = (0 + .5f)*sInv - 0.5f;
			xb = init;
		}

		hasLf = xb >= 0;
		xb0 = hasLf ? (int)xb : -1;
		hasRt = xb0 < wb - 1;

		xd = xb - xb0;
		xb += sInv;
		yb = init;
		y = 0;

		// leading rows, no top bin
		for (; y < bin / 2; y++)
		{
			yb0 = -1;
			/*GHinit;*/

			//
			yd = yb - yb0;
			yb += sInv;
			H0 = H + xb0*hb + yb0; // H에 -181 을 한것과 같음.
			xyd = xd*yd;

			ms[0] = 1 - xd - yd + xyd;
			ms[1] = yd - xyd;
			ms[2] = xd - xyd;
			ms[3] = xyd;
			//

			if (hasLf)
			{
				H0[O0[y] + 1] += ms[1] * M0[y];
				H0[O1[y] + 1] += ms[1] * M1[y];
			}
			if (hasRt)
			{
				H0[O0[y] + hb + 1] += ms[3] * M0[y]; // hb+1 을 하기 때문에 -181은 보상됨
				H0[O1[y] + hb + 1] += ms[3] * M1[y];
			}
		}
		// main rows, has top and bottom bins, use SSE for minor speedup
		for (;; y++)
		{
			yb0 = (int)yb; // yb0는 몇 번째 bin인가를 나타내는 인덱스
			if (yb0 >= hb - 1)
				break;
			/*GHinit;*/

			//
			yd = yb - yb0;
			yb += sInv;
			H0 = H + xb0*hb + yb0;
			xyd = xd*yd;

			ms[0] = 1 - xd - yd + xyd;
			ms[1] = yd - xyd;
			ms[2] = xd - xyd;
			ms[3] = xyd;
			//

			if (hasLf)
			{
				H0[O0[y]] += ms[0] * M0[y];
				H0[O1[y]] += ms[0] * M1[y];

				H0[O0[y] + 1] += ms[1] * M0[y];
				H0[O1[y] + 1] += ms[1] * M1[y];
			}
			if (hasRt)
			{
				H0[O0[y] + hb] += ms[2] * M0[y];
				H0[O1[y] + hb] += ms[2] * M1[y];

				H0[O0[y] + hb + 1] += ms[3] * M0[y];
				H0[O1[y] + hb + 1] += ms[3] * M1[y];
			}
		}
		// final rows, no bottom bin
		for (; y < h0; y++)
		{
			yb0 = (int)yb;
			/*GHinit;*/

			//
			yd = yb - yb0;
			yb += sInv;
			H0 = H + xb0*hb + yb0;
			xyd = xd*yd;

			ms[0] = 1 - xd - yd + xyd;
			ms[1] = yd - xyd;
			ms[2] = xd - xyd;
			ms[3] = xyd;
			//

			if (hasLf)
			{
				H0[O0[y]] += ms[0] * M0[y];
				H0[O1[y]] += ms[0] * M1[y];
			}
			if (hasRt)
			{
				H0[O0[y] + hb] += ms[2] * M0[y];
				H0[O1[y] + hb] += ms[2] * M1[y];
			}
		}
	}
	free(O0);
	free(O1);
	free(M0);
	free(M1);

	// normalize boundary bins which only get 7/8 of weight of interior bins
	if (softBin % 2 != 0)
		for (int o = 0; o < nOrients; o++)
		{
			x = 0;

			for (y = 0; y < hb; y++)
				H[o*nb + x*hb + y] *= 8.f / 7.f;

			y = 0;
			for (x = 0; x < wb; x++)
				H[o*nb + x*hb + y] *= 8.f / 7.f;

			x = wb - 1;
			for (y = 0; y < hb; y++)
				H[o*nb + x*hb + y] *= 8.f / 7.f;

			y = hb - 1;
			for (x = 0; x < wb; x++)
				H[o*nb + x*hb + y] *= 8.f / 7.f;
		}
}

// helper for gradHist, quantize O and M into O0, O1 and M0, M1 (uses sse)
void CPedestrianACF::gradQuantize(float* O, float* M, int* O0, int* O1, float* M0, float* M1,
	int nb, int n, float norm, int nOrients, bool full, bool interpolate)
{
	// assumes all *OUTPUT* matrices are 4-byte aligned
	int i, o0, o1;
	float o, od, m;

	// define useful constants
	const float oMult = (float)nOrients / (full ? 2 * (float)CV_PI : (float)CV_PI);
	const int oMax = nOrients*nb;

	// compute trailing locations without sse
	if (interpolate)
		for (i = 0; i < n; i++)
		{
			o = O[i] * oMult; // pi로 나누고 nOrient를 곱함
			o0 = (int)o; // quantize 시켜서 해당 orient bin을 가리킴
			od = o - o0; // 잔차 값 od에 저장

			o0 *= nb; // o0에 영상 크기를 곱함으로 써 해당 orient 저장소의 시작 인덱스로 변경함
			if (o0 >= oMax) // 배열을 넘어가면 안되므로
				o0 = 0;

			O0[i] = o0; // O0의 사이즈는 h이고, 해당 인덱스 i에 oO값(나중에 사용할 인덱스)을 저장함

			o1 = o0 + nb; // quantize 를 수행했기 때문에 잔차는 다음 orient bin에 저장되어야함
			if (o1 == oMax) // 이것도 역시 배열을 넘어가면 안되므로
				o1 = 0;

			O1[i] = o1; // O1의 사이즈는 h이고, 해당 인덱스 i에 o1값(나중에 사용할 인덱스)을 저장함

			m = M[i] * norm; // magnitude를 norm을 곱해서 사용함
			M1[i] = od*m; // M1 은 잔차에 해당하는 magnitude 라서 od를 곱함
			M0[i] = m - M1[i]; // M0 은 잔차가 아닌 magnitude라서 1-od를 곱함. (즉, M1:M0 = od:(1-od) 의 비율임)
		}
}
