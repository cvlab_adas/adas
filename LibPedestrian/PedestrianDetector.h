#pragma once

#define _CRT_SECURE_NO_WARNINGS_GLOBALS

#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class CPedestrianDetector
{
public:
	virtual ~CPedestrianDetector() {};

	virtual bool LoadClassifier(string strClassifierFile) = 0; // classifier를 불러온다. 여러 classifier를 대응하기 위해 xml로 해야할 듯 하다
	virtual void Detect(Mat& imgSrc) = 0; // 영상을 입력받아서 검출한다. 미리 SetROI 함수로 ROI를 만들어놨으면 그 부분에서만 검출을 시도한다
	inline void SetROI(vector<Rect_<int> >& vecrectROIs)
	{
		m_vecrectROIs.clear();
		m_vecrectROIs.assign(vecrectROIs.begin(), vecrectROIs.end());
	}
	inline void GetResultRects(vector<Rect_<int> >& vecrectBB)
	{
		vecrectBB.clear();
		vecrectBB.assign(m_vecrectResultBB.begin(), m_vecrectResultBB.end());
	}
	inline void GetResultImages(vector<Mat>& vecimgResults)
	{
		vecimgResults.clear();
		vecimgResults.resize((int)m_vecrectResultBB.size());
		for (int i = 0; i < (int)m_vecrectResultBB.size(); i++)
		{
			Mat imgBB = m_imgSrcLast(m_vecrectResultBB[i]);
			imgBB.copyTo(vecimgResults[i]);
		}
	}
	inline void DrawResult(Mat& imgDisp, const Scalar color = CV_RGB(255, 255, 0))
	{
		for (int i = 0; i < (int)m_vecrectResultBB.size(); i++)
			rectangle(imgDisp, m_vecrectResultBB[i], color, 2);
	}

protected:
	Mat m_imgSrcLast;
	vector<Rect_<int> > m_vecrectROIs;
	vector<Rect_<int> > m_vecrectResultBB;
};
