#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

#include "../LibLane/CMultiROILaneDetection.h"
#include "../LibStereo/StereoVisionForADAS.h"
#include "../UtilDatasetLoader/DatasetLoader.h"
#include "../UtilTimeChecker/TimeChecker.h"

#ifdef _DEBUG
#pragma comment(lib, "../Debug/LibLane.lib")
#pragma comment(lib, "../Debug/UtilDatasetLoader.lib")
#pragma comment(lib, "../Debug/LibStereo.lib")
#else
#pragma comment(lib, "../Release/LibLane.lib")
#pragma comment(lib, "../Release/LibStereo.lib")
#pragma comment(lib, "../Release/UtilDatasetLoader.lib")
#endif

#include"../include/DefStruct.h"
#include"../Integration_Detection/Detection.h"


#define _KITTI_

#ifdef _KITTI_

#define IMAGE_WIDTH 1242
#define IMAGE_HEIGHT 375
#define FX 722//984.2439
#define FY 722//980.8141
#define OX 610//690.
#define OY 173//233.196
#define HEIGHT 1650//1730.
#define PITCH 1
#define YAW -1.2

#endif

#define NUM_ROI 6



int main()
{
	printf("This is Lane detection project..\n");

#ifdef _KITTI_
	int nImage_width = IMAGE_WIDTH;
	int nImage_height = IMAGE_HEIGHT;
	double dPitch = PITCH;
	double dYaw = YAW;
	double dFx = FX;
	double dFy = FY;
	double dCx = OX;
	double dCy = OY;
	double dHeight = HEIGHT;
#endif
	
	CDatasetCreator dc;
	if (!dc.LoadConfigFile("Config.xml")) return -1;
	CDatasetLoader* objLoader = dc.Create();

	SDataFormat sData;
	Mat matInput;
	double nSize = 2;
	int nDelay = 0;
	CTimeChecker_<msec> tc;


	// LaneDection Object intantiate
	CMultiROILaneDetection objLD(nImage_width, nImage_height, dPitch, dYaw, dFx, dFy, dCx, dCy, dHeight);
	objLD.setRoiIpmConfig();
	vector<SLane> lanes; //검출된 차선을 받아오기 위한 변수.
	vector<Rect> rectResults; //검출된 차선으로 기반으로 ROI를 받아오기 위한 변수.
	

	//stereo
	StereoCamParam_t objParam = CStereoVisionForADAS::InitStereoParam(KITTI);
	CStereoVisionForADAS objStereoVision(objParam);
	bool flgPedOK = false;

	Mat imgLeftInput, imgRightInput;
	Mat imgFree;

	
	while (1)
	{
		*objLoader >> sData;

		if (sData.img[0].empty())
		{
			cout << "no images\n";
			break;
		}
		//sData.img[0].copyTo(matInput);
		sData.img[0].copyTo(imgLeftInput);
		sData.img[1].copyTo(imgRightInput);

		objStereoVision.Objectness(imgLeftInput, imgRightInput);

		imshow("freespace", objStereoVision.m_imgGround);
		imgFree = objStereoVision.m_imgGround.clone();
		
	

		tc.tic();
		//objLD.Detection(imgLeftInput);
		objLD.Detection(imgLeftInput,imgFree);
		tc.toc();

		//objLD.GetLaneResult(lanes); 검출된 최종 차선 결과만 뽑아오고 싶을 때 사용
		
		objLD.DisplayResult(lanes, rectResults);
		//ShowImageNormalize("IPM test", objLD.m_filteredThreshold[CENTER_ROI]); // VD모듈에서 사용할 수 있게끔 테스트

		//display run time
		stringstream ssTime;
		ssTime << tc.to_string(2).c_str();
		char szMs[10] = "ms";
		ssTime << szMs;
		putText(objLD.m_imgResizeOrigin, ssTime.str(), Point(10, 50),
			FONT_HERSHEY_COMPLEX, 0.8, Scalar(255, 0, 0), 2, 8, false);




		imshow("Result", objLD.m_imgResizeOrigin);
		char key = waitKey(nDelay);
		if (key == 27)
			break;
		else if (key >= '1' && key <= '4')
			nSize = (double)(key - '0');
		else if (key == ' ')
			nDelay = 1 - nDelay;
		else if (key == ']')
		{
			nDelay = 0;
			(*objLoader)++;
		}
		else if (key == '[')
		{
			nDelay = 0;
			(*objLoader)--;
		}

		if (nDelay != 0)
			(*objLoader)++ ? nDelay = 1 : nDelay = 0;

		
		//objLD.ReleaseDetection();

	
	}

	
	return 0;
}