/**
@file CMultiROILaneDetection.h
@brief 헤더파일.
@brief ver03.
@author 이영완.
@date 2016.02.01.
*/


/************************************************************************/
/*
다차선 ver.03
작성자 : 이영완
작성일 : 2015.11.09.
설 명 : 모듈화 & declaration & implementation 분리
마지막백업날짜 : 1109
*/
/************************************************************************/






/////////////////////EVALUATION///////////////////////////////////////////////////////
#define COMPARE_STANDARD 0.2
//#define EMPTY -1
////////////////////////////////////////////////////////////////////////////





/***********************  Header  *************************************************/

#pragma once
#include <opencv\cxcore.hpp>
#include <highgui.h>
#include <cv.h>
#include "opencv2/opencv.hpp"
#include <vector>

#include <stdio.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string.h>
#include <iomanip>
#include <ctime>
#include <math.h>
#include "ranker.h"

#include "../include/DefStruct.h"
/************************  end of Header  ************************************************/



/**************************  constant  **********************************************/

using namespace cv;
using namespace std;

//#define _HY_
#define _SANE_
//#define _CURVE_
#define _ADD_ROI_ 0
#define _KALMAN_
#define _VEHICLE_ 0

#define MULTIROINUMBER 12

//Tracking module
#define TRACKINGNUMBER 12

#define MAXCOMP -99999
#define MINCOMP 99999
#define DEBUG_LINE 1
#define IPM_WIDTH_SCALE 1 //origin:1 [LYW_1103] : 이 값을 줄이면 계산양이 줄어드는 대신 다른 trade-off가 발생함.
#define IPM_HEIGHT_SCALE 1.5 //origin:1.5
#define DIST_TRACKING_WIDTH 200
#define MOVING_AVERAGE_NUM 4//7
#define TRACKING_FLAG_NUM 3 //4
#define TRACKING_ERASE_LEVEL 1
#define TRACKINGERASE 4
#define MIN_WORLD_WIDTH 2.5		//Left,Right lane minimum interval
#define MAX_WORLD_WIDTH 5 //[LYW_0922] : 간격이 너무 벌어져도 버려
#define MIN_GUARD_WIDTH 4.2	//Left,Right lane minimum interval

//[LYW_1109] : ROI setting을 위한
#ifdef _SANE_


#define RECT_LB_ROI_X 120
#define RECT_LB_ROI_Y 238
#define RECT_LB_ROI_WIDTH 160
#define RECT_LB_ROI_HEIGHT 38

#define RECT_RB_ROI_X 330
#define RECT_RB_ROI_Y 238
#define RECT_RB_ROI_WIDTH 160 
#define RECT_RB_ROI_HEIGHT 38  

#define RECT_LS_ROI_X 73
#define RECT_LS_ROI_Y 200
#define RECT_LS_ROI_WIDTH 113
#define RECT_LS_ROI_HEIGHT 38

#define RECT_RS_ROI_X 412
#define RECT_RS_ROI_Y 200
#define RECT_RS_ROI_WIDTH 113
#define RECT_RS_ROI_HEIGHT 38  



#define IPM_WIDTH_SCALE 1
#define IPM_HEIGHT_SCALE 1.5

#define RESIZE_FACTOR 2

#define PI 3.14159265358979323846
#define PITCH 0
#define YAW 1

#define RESIZE_IMAGE_WIDTH 640
#define RESIZE_IMAGE_HEIGHT 360


#endif



/**********************  end of constant  **************************************************/

/************************ start of type define  ************************************************/

enum EROINUMBER{
	CENTER_ROI = 0,//0
	LEFT_ROI0, //1
	LEFT_ROI1, //2
	LEFT_ROI2, //3
	LEFT_ROI3, //4
	RIGHT_ROI0, //5
	RIGHT_ROI1, //6
	RIGHT_ROI2, //7
	RIGHT_ROI3, //8
	RIGHT_ROI4, //9
	AUTOCALIB, //10
	GROUND, //11
};

typedef enum LineType_ {
	LINE_HORIZONTAL = 0,
	LINE_VERTICAL = 1
} LineType;

typedef struct SFileInformation{
	char szDataDir[200];
	char szDataName[200];
	//int nTotalFrame;

}SFileInformation;

typedef struct SCameraInformation{
	Size_<double> sizeFocalLength;
	Point_<double> ptOpticalCenter;
	Size sizeCameraImage;

	double fHeight = 0.;
	double fPitch = 0.;
	double fYaw;
	Point_<float> ptVanishingPoint;
	float fGroundTop;
	float fGroundBottom;

}SCameraInformation;

typedef struct SConfiguration{
	float fLineWidth;
	float fLineHeight;
	int nRansacIteration;
	float nRansacThreshold;
	float fVanishPortion; ///< [LYW_1109] : SetRoiIpmCofig 사용됨.
	float fLowerQuantile; ///< [LYW_1109] : FilterLinesIPM에 사용됨.
	int nLocalMaxIgnore; ///< [LYW_1109] : GetLinesIPM에 사용됨.

}SConfiguration;

typedef struct SRoiInformation{
	Size sizeRoi;
	Point ptRoi;
	Point ptRoiEnd;
	int nLeft;
	int nRight;
	int nTop;
	int nBottom;
	Size sizeIPM;

	double dXLimit[2];
	double dYLimit[2];
	double dXScale;
	double dYScale;
	int nIpm2WorldWidth;
	int nIpm2WorldHeight;

	//[LYW_1108] : 모듈화 를 위한.

	double fWidthIPMScale; ///< IPM이미지의 스케일을 결정. 낮추면 속도증가, 성능저하
	double fHeightIPMScale;


	//detection information
	int nDetectionThreshold; ///< GetLineIPM 에 사용
	int nGetEndPoint; ///< LineFitting에서 RANSAC계산에 사용
	int nGroupThreshold; ///< LineFitting에서 line Grouping에 사용
	float fOverlapThreshold; ///< LineFitting에 사용

	//RansacLine
	int nRansacNumSamples;
	int nRansacNumIterations;
	int nRansacNumGoodFit;
	float fRansacThreshold;
	int nRansacScoreThreshold;
	int nRansacLineWindow;


}SRoiInformation;

typedef struct SLine{
	//start pt world
	Point_<double> ptStartLine;
	//end pt world
	Point_<double> ptEndLine;
	//start pt uv
	Point_<double> ptUvStartLine;
	//end pt world
	Point_<double> ptUvEndLine;
	////score
	//float fScore;
	float fXcenter;
	float fXderiv;
	float fGroundHeight;
	//r & theta

	float fR;
	float fTheta;
}SLine;

typedef struct SWorldLane{
	float fXcenter;
	float fXderiv;
	float fYtop;
	float fYBottom;
	Point_<double> ptStartLane;
	Point_<double> ptEndLane;
	//start pt uv
	Point_<double> ptUvStartLine;
	//end pt world
	Point_<double> ptUvEndLine;
}SWorldLane;

typedef struct SKalman
{
	KalmanFilter KF;
	Mat_<float> matState;        // X_k = [XCenter Xderiv velocity_XC velocity_XD] ^ T : 4 by 1 state vector
	Mat matProcessNoise;
	Mat_<float> matMeasurement; //  Z_k = [ XCenter XDeriv ] ^ T : 2 by 1 measure vector
	SLine SKalmanTrackingLine;
	SLine SKalmanTrackingLineBefore;
	int cntNum;
	int cntErase;
	SKalman()
	{
		KF = KalmanFilter(4, /// dynamPraram : Dimensionality of the state 
			2, /// measurePrarm : Dimensionality of the measurement
			0);
		matState = Mat_<float>(4, 1);
		matMeasurement = Mat_<float>(2, 1);
		cntNum = 0;
		cntErase = 0;
	}
}SKalman;


typedef struct SEvaluation
{
	int LeftTP;
	int LeftFP;
	int LeftFN;
	int LeftTN;

	int RightTP;
	int RightFP;
	int RightFN;
	int RightTN;

	int Left2TP;
	int Left2FP;
	int Left2FN;
	int Left2TN;

	int Right2TP;
	int Right2FP;
	int Right2FN;
	int Right2TN;

	int nLeftGroundTruth;
	int nRightGroundTruth;
	int nTotalFrame;
	int nLeftDetectedFrame;
	int nRightDetectedFrame;
	int nLeftNonDetectedFrame;
	int nRightNonDetectedFrame;

	float fRateTP;
	float fRateFP;
	float fRateTN;
	float fRateFN;

	SEvaluation()
	{
		LeftTP = 0;
		LeftFP = 0;
		LeftFN = 0;
		LeftTN = 0;

		RightTP = 0;
		RightFP = 0;
		RightFN = 0;
		RightTN = 0;

		Left2TP = 0;
		Left2FP = 0;
		Left2FN = 0;
		Left2TN = 0;

		Right2TP = 0;
		Right2FP = 0;
		Right2FN = 0;
		Right2TN = 0;

		nLeftGroundTruth = 0;
		nRightGroundTruth = 0;
		nTotalFrame = 0;

		fRateTP = 0.;
		fRateFP = 0.;
		fRateTN = 0.;
		fRateFN = 0.;
	}
}SEvaluation;

typedef struct STrackingFlag{ 
	bool bTracking = false;
	int nTargetTracker = -1;
}STrackingFlag;



//[LYW_1105] : 모듈통합을 위한 final lane정보 구조체

/**
@struct SLane
@date 2015.11.06
@author 이영완
@brief 통합모듈에 최종 결과를 넘겨주기 위한 lane구조체
@warning
*/
//typedef struct SLane
//{
//
//	Point_<double> ptUvStartLine; ///< 영상좌표계에서의 차선 시작점
//	Point_<double> ptUvEndLine; ///< 영상좌표계에서의 차선 종료점
//
//}SLane;

/*********************  end of type define  ***************************************************/





/**
@class CMultiROILaneDetection
@brief Multi-Lane Detection module
@warning
@author 이영완.
@date 2015.11.09.

*/
class CMultiROILaneDetection{




	/******************** start of variable delaration ****************************************************/
public:
	Mat m_imgOrigin;
	Mat m_imgResizeOrigin;
	Mat m_imgOriginScale;
	Mat m_imgResizeScaleGray;

	unsigned int m_nFrameNum;

	SCameraInformation m_sCameraInfo;
	SConfiguration m_sConfig;


	SRoiInformation m_sRoiInfo[MULTIROINUMBER];
	Mat m_matXYGrid[MULTIROINUMBER]; 
	Mat m_matUVGrid[MULTIROINUMBER];
	Mat m_imgIPM[MULTIROINUMBER];
	Mat m_ipmFiltered[MULTIROINUMBER];
	Mat m_filteredThreshold[MULTIROINUMBER];

	vector<SLine> m_lanes[MULTIROINUMBER]; 
	vector<float> m_laneScore[MULTIROINUMBER];
	vector<SLine> m_lanesResult[MULTIROINUMBER];
	vector<SLine> m_lanesGroundResult[MULTIROINUMBER]; 

	//tracking module
	SLine m_sImgCenter;
	SWorldLane m_sWorldCenterInit;
	STrackingFlag m_sTracking[MULTIROINUMBER];
	bool m_bTracking[MULTIROINUMBER];
	vector<SLine> m_leftTracking;
	vector<SLine>::iterator m_iterLeft;
	vector<SLine> m_rightTracking;
	vector<SLine>::iterator m_iterRight;

	vector<SLine> m_leftGroundTracking;
	vector<SLine>::iterator m_iterGroundLeft;
	vector<SLine> m_rightGroundTracking;
	vector<SLine>::iterator m_iterGroundRight;


	//Tracking module
	bool m_bTrackingFlag[TRACKINGNUMBER];
	vector<SLine> m_Tracking[TRACKINGNUMBER];
	vector<SLine>::iterator m_iterTracking[TRACKINGNUMBER];
	vector<SLine> m_GroundTracking[TRACKINGNUMBER];
	vector<SLine>::iterator m_iterGroundTracking[TRACKINGNUMBER];
	SWorldLane m_sTrackingLane[TRACKINGNUMBER]; // moving average 값.(ground값)
	SKalman m_SKalmanLane[TRACKINGNUMBER];
	bool m_bDraw[TRACKINGNUMBER]; // TrackingStage에서 최종적으로 그릴지 말지 결정할 때 쓰는 변수.
	int nCnt[TRACKINGNUMBER]; ///< DetermineTracking에서 계쏙 트래킹할지 판단할 때는 변수.
	vector<int> m_vecTrackingFlag[TRACKINGNUMBER]; //tracking 모듈에 포함되는 ROInumber 저장하기 위한 자료



	int m_nTotalCnt;


	//[LYW_1015] : LDWS
	//LYW_1015] : LDWS
	int nDiffUVLeft = 0; ///LDWS를 위한 변수. ROI의 검출된 차선의 DIFF(startPoint.X - endPoint.X)를 계산
	int nDiffUVRight = 0;

	//[LYW_1106] : refactoring
	int m_nResizeFactor = RESIZE_FACTOR; ///< 영상의 resize를 위한. HD급 --> VGA
	double m_fWidthScale = IPM_WIDTH_SCALE; ///< IPM의 사이즈에 영향.
	double m_fHeightScale = IPM_HEIGHT_SCALE; ///< IPM의 사이즈에 영향.

	//[LYW_1222] : ROI셋팅을 위한 global 변수
	//////////////////////////////////////////////////////////////////////////
	int m_nImageWidth = 0; ///< Constructor & Init에서 사용
	int m_nImageHeight = 0; ///< Constructor & Init에서 사용

	int RECT_LT1_ROI_X;
	int RECT_LT1_ROI_Y;
	int RECT_LT1_ROI_WIDTH;
	int RECT_LT1_ROI_HEIGHT;

	int RECT_LT2_ROI_X;
	int RECT_LT2_ROI_Y;
	int RECT_LT2_ROI_WIDTH;
	int RECT_LT2_ROI_HEIGHT;

	int RECT_LT3_ROI_X;
	int RECT_LT3_ROI_Y;
	int RECT_LT3_ROI_WIDTH;
	int RECT_LT3_ROI_HEIGHT;

	int RECT_LT4_ROI_X;
	int RECT_LT4_ROI_Y;
	int RECT_LT4_ROI_WIDTH;
	int RECT_LT4_ROI_HEIGHT;

	int RECT_LT0_ROI_X;
	int RECT_LT0_ROI_Y;
	int RECT_LT0_ROI_WIDTH;
	int RECT_LT0_ROI_HEIGHT;

	int RECT_RT1_ROI_X;
	int RECT_RT1_ROI_Y;
	int RECT_RT1_ROI_WIDTH;
	int RECT_RT1_ROI_HEIGHT;

	int RECT_RT2_ROI_X;
	int RECT_RT2_ROI_Y;
	int RECT_RT2_ROI_WIDTH;
	int RECT_RT2_ROI_HEIGHT;

	int RECT_RT3_ROI_X;
	int RECT_RT3_ROI_Y;
	int RECT_RT3_ROI_WIDTH;
	int RECT_RT3_ROI_HEIGHT;

	int RECT_RT4_ROI_X;
	int RECT_RT4_ROI_Y;
	int RECT_RT4_ROI_WIDTH;
	int RECT_RT4_ROI_HEIGHT;

	int RECT_RT0_ROI_X;
	int RECT_RT0_ROI_Y;
	int RECT_RT0_ROI_WIDTH;
	int RECT_RT0_ROI_HEIGHT;

	//160406 : VD모듈에서 사용될 ROI
	int RECT_CENTER_ROI_X;
	int RECT_CENTER_ROI_Y;
	int RECT_CENTER_ROI_WIDTH;
	int RECT_CENTER_ROI_HEIGHT;

	//////////////////////////////////////////////////////////////////////////


	bool m_bStartTrackingFlag;

	bool m_bROISwitch[MULTIROINUMBER];


private:
	SVD m_SvdCalc;
	Mat m_MatFx; ///< filtering에 사용되는 커널
	Mat m_MatFy; ///< filtering에 사용되는 커널



	/******************** end of variable declaration ****************************************************/


	/********************* start of member function declaration ***************************************************/
public:


	CMultiROILaneDetection(int nImageWidth, int nImageHeight, double dPitch, double dYaw, double dFocalX, double dFocalY, double dPrinciX, double dPrinciY, double dHeight);
	~CMultiROILaneDetection();

	void setRoiIpmConfig(); ///<set ROI, IPM all configuration.
	bool PreProcess(Mat &src); ///< Preprocess(resize & normalize & RGB2GRAY before detection
	void Detection(Mat &srcImg); ///< detection start
	void Detection(Mat &srcImg, Mat &imgFree); ///< detection start with free space info from SV
	void DetermineLDWS(); ///< determine LDWS
	void PostProcessing(); ///< After tracking PostProcessing  & clean Memory & draw
	void GetLaneResult(vector<SLane>& lanes); ///< get final tracked lanes
	void DisplayResult(vector<SLane> &lanes, vector<Rect>& rectResults); ///< Display final result
	void DisplayResult(vector<SLane>& lanes, Mat& matDisplay); ///< Display final lane result on destination Mat
	void ReleaseDetection(); ///< release detection resource
	void DispalyProfiling(); ///< 각 함수별, ROI별 평균 pTime.
	void GetIPMImage(const Mat &srcImg, Mat& dstImg); ///< 다른 모듈에서 IPM이미지를 사용하고 싶을 때 사용됨


private:


	void CheckFreespace(Mat &imgFree); ///< freespace정보를 이용하여 ROI on/off
	void CheckROIWithFreeSpace(Mat &imgFree, EROINUMBER nROIFlag);

	void SetIpmConfig(EROINUMBER nFlag); //[LYW_0824]: Calibration 결과값을 이용한 ROI초기화셋팅--> VP,LUT만들기 
	void StartLanedetection(EROINUMBER nFlag);
	void InitialResizeFunction(Size sizeResize);
	void GetIPM(EROINUMBER nFlag);
	void GetIPM(const Mat &srcImg, Mat &dstImg); ///< GetIPMImage함수에 사용됨
	void FilterLinesIPM(const Mat &srcImg, Mat &dstImg); ///< GetIPMImage함수에 사용됨
	void FilterLinesIPM(EROINUMBER nFlag);
	void GetLinesIPM(EROINUMBER nFlag);
	void LineFitting(EROINUMBER nFlag);
	void IPM2ImLines(EROINUMBER nFlag);
	void PostDetection(); ///< [LYW_20160224] : 검출 후 트래킹 전 예외처리(false alarm 제거)
	//Auto Calibration
	void PushBackResult(EROINUMBER nFlag, Vector<Mat> &vecMat){
		vecMat.push_back(m_ipmFiltered[nFlag].clone());
	}
	void GetCameraPose(EROINUMBER nFlag, Vector<Mat> &vecMat);
	void TransformImage2Ground(const Mat &matInPoints, Mat &matOutPoints);
	void TransformGround2Image(const Mat &matInPoints, Mat &matOutPoints);
	void ClearResultVector(EROINUMBER nFlag);
	Point TransformPointImage2Ground(Point ptIn);
	Point TransformPointGround2Image(Point ptIn);


	//tracking module
	bool StartTracking(); ///< tracking module start. This function has a tracking module function. TrackingSetting(), DetermineTracking(), TrackingStage()
	bool TrackingSetting(); ///< tracking stage를 위한 준비단계.
	bool DetermineTracking(); ///< tracking stage로 넘어가기 전에 계속 tracking할지 말지를 결정하는 단계
	bool TrackingStage(); ///< 본격적으로 칼만필터트래킹을 하는 단계

	void TrackingStageGround(EROINUMBER nflag, int nTrackingFlag);
	void TrackingContinue(int nTrackingFlag);
	void KalmanTrackingStage(int nTrackingFlag);
	void KalmanSetting(SKalman &SKalmanInput);
	void ClearDetectionResult(int nTrackingFlag);

	void SetIPMConfig(); ///< Set ROI IPM LUT
	//void setMultiROIConfig(); ///< Set Multi ROI configuration
	void setROI(EROINUMBER nFlag, int nLeft, int nRight, int nTop, int nBottom); ///<Set each ROI
	void Init(double fPitch, double fYaw); ///< 카메라파라미터셋팅
	void Init(double fPitch, double fYaw, double dFx, double dFy, double dCx, double dCy, double dHeight); ///< [LYW_160201] : 카메라파라미터셋팅
	void setMatFx(); ///<생성자에서 호출
	void setMatFy();

	void ShowResults(EROINUMBER nflag); ///< display intermediate reseult such as IPMimage, filteredIPM etc.
	void ReleaseTracking(); ///< release tracking resource
	bool StartDetection(); ///< Lane Detection start


	void SetVanishingPoint();

	//void TransformImage2Ground(const Mat &matInPoints,Mat &matOutPoints);
	//void TransformGround2Image(const Mat &matInPoints,Mat &matOutPoints);
	void GetVectorMax(const Mat &matInVector, double &dMax, int &nMaxLoc, int nIgnore);
	double GetLocalMaxSubPixel(double dVal1, double dVal2, double dVal3);
	//mFunc
	void GetMaxLineScore(EROINUMBER nFlag);
	void GetTrackingLineCandidateModule(EROINUMBER nFlag);
	void GetTrackingLineCandidate(EROINUMBER nFlag);

	void GetMaxLineScoreTwo(EROINUMBER nFlag);
	void Lines2Mat(const vector<SLine> &lines, Mat &mat);
	void Mat2Lines(const Mat &mat, vector<SLine> &lines);
	void GroupLines(vector<SLine> &lines, vector<float> &lineScores,
		float groupThreshold, Size_<float> bbox);
	void LineXY2RTheta(const SLine &line, float &r, float &theta);
	void IntersectLineRThetaWithBB(float r, float theta, const Size_<float> bbox, SLine *outLine);
	bool IsPointInside(Point2d point, Size_<int> bbox);
	bool IsPointInside(Point2d point, Size_<float> bbox);
	void GetLinesBoundingBoxes(const vector<SLine> &lines, LineType type,
		Size_<int> size, vector<Rect> &boxes);
	void GroupBoundingBoxes(vector<Rect> &boxes, LineType type,
		float groupThreshold);
	void  SetMat(Mat& imgInMat, Rect_<int> RectMask, double val);
	void FitRansacLine(const Mat& matImage, int numSamples, int numIterations,
		float threshold, float scoreThreshold, int numGoodFit,
		bool getEndPoints, LineType lineType,
		SLine *lineXY, float *lineRTheta, float *lineScore, EROINUMBER nFlag);
	bool GetNonZeroPoints(const Mat& matInMat, Mat& matOutMat, bool floatMat);

	void CumSum(const Mat &inMat, Mat &outMat);

	void SampleWeighted(const Mat &cumSum, int numSamples, Mat &randInd, RNG &rng);

	void FitRobustLine(const Mat &matPoints, float *lineRTheta, float *lineAbc);



	/********************* end of member function declaration ***************************************************/
	
};



//other custom functionShowImageNormalize
void SetFrameName(char* szDataName, char* szDataDir, int nFrameNum);
void ScaleMat(const Mat &inMat, Mat &outMat);
void ShowImageNormalize(const char str[], const Mat &pmat);
void ShowResults(CMultiROILaneDetection &obj, EROINUMBER nflag);
void SetFrameNameBMP(char* szDataName, char* szDataDir, int nFrameNum);





void EvaluationFunc(CMultiROILaneDetection &obj, SEvaluation &structEvaluation,
	SWorldLane GroundLeft, SWorldLane GroundRight,
	SWorldLane FindLeft, SWorldLane FindRight);

void EvaluationFunc_multi(CMultiROILaneDetection &obj, SEvaluation &structEvaluation,
	SWorldLane GroundLeft1, SWorldLane GroundLeft2, SWorldLane GroundRight1, SWorldLane GroundRight2,
	SWorldLane FindLeft1, SWorldLane FindLeft2, SWorldLane FindRight1, SWorldLane FindRight2,
	Mat& matCanvas , FILE* fp);

float CompareLineDiff(Point2d FixedStart, Point2d FixedEnd, Point2d GroundStart, Point2d GroundEnd);
void LoadExtractedPoint(FILE* fp, Point_<double>& ptUpLeft, Point_<double>& ptMidLeft, Point_<double>& ptUpRight, Point_<double>& ptMidRight);
void LoadExtractedPoint_multi(FILE* fp, SWorldLane& GroundLeft1, SWorldLane& GroundLeft2, SWorldLane& GroundRight1, SWorldLane& GroundRight2);