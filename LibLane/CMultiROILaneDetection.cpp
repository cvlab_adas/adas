/**
@file CMultiROILaneDetection.cpp
@brief 구현파일.
@brief ver03.
@author 이영완.
@date 2016.02.01.

*/



#include "CMultiROILaneDetection.h"

/**
@brief DefaultConstructor
@param void
@return void
@warning
@author 이영완.
@date 2015.11.09.
*/


CMultiROILaneDetection::CMultiROILaneDetection(int nImageWidth, int nImageHeight, double dPitch, double dYaw, double dFocalX, double dFocalY, double dPrinciX, double dPrinciY, double dHeight){
	cout << "contructor start" << endl;

	m_nImageWidth = nImageWidth;
	m_nImageHeight = nImageHeight;

	setMatFx();
	setMatFy();


	Init(dPitch, dYaw, dFocalX, dFocalY, dPrinciX, dPrinciY, dHeight);
	printf("in Constructor: final Real pitch, real yaw(%f,%f)\n", m_sCameraInfo.fPitch, m_sCameraInfo.fYaw);
	cout << "camera info settting completed\n" << endl;
	m_bStartTrackingFlag = false;

}



CMultiROILaneDetection::~CMultiROILaneDetection()
{
	cout << "end of Multi-Lane Detection\nBye!\n" << endl;
}



void CMultiROILaneDetection::SetVanishingPoint(){
	//get vanishing point in world coordinates
	float fArrVp[] = {
		sin(m_sCameraInfo.fYaw) / cos(m_sCameraInfo.fPitch),
		cos(m_sCameraInfo.fYaw) / cos(m_sCameraInfo.fPitch),
		0
	};
	Mat matVp = Mat(3, 1, CV_32FC1, fArrVp);
	//cout<<"mat vp "<<matVp<<endl;
	//Yaw rotation matrix
	float fArrTyaw[] = {
		cos(m_sCameraInfo.fYaw), -sin(m_sCameraInfo.fYaw), 0,
		sin(m_sCameraInfo.fYaw), cos(m_sCameraInfo.fYaw), 0,
		0, 0, 1
	};
	Mat matTyaw = Mat(3, 3, CV_32FC1, fArrTyaw);

	//Pitch rotation matrix
	float fArrPitchp[] = {
		1, 0, 0,
		0, -sin(m_sCameraInfo.fPitch), -cos(m_sCameraInfo.fPitch),
		0, cos(m_sCameraInfo.fPitch), -sin(m_sCameraInfo.fPitch)
	};
	Mat matTpitch = Mat(3, 3, CV_32FC1, fArrPitchp);
	//combine transform matrix
	Mat matTransform = matTpitch * matTyaw;
	//cout<<matTransform<<endl;
	//
	//transformation from (xc, yc) in camera coordinates
	// to (u,v) in image frame
	//
	//matrix to shift optical center and focal length
	float fArrCamerap[] = {
		m_sCameraInfo.sizeFocalLength.width, 0, m_sCameraInfo.ptOpticalCenter.x,
		0, m_sCameraInfo.sizeFocalLength.height, m_sCameraInfo.ptOpticalCenter.y,
		0, 0, 1
	};
	Mat matCameraTransform = Mat(3, 3, CV_32FC1, fArrCamerap);
	//combine transform
	matTransform = matCameraTransform * matTransform;
	matVp = matTransform * matVp;
	float *pVP = (float*)&matVp.data[0];
	m_sCameraInfo.ptVanishingPoint.x = pVP[0];
	m_sCameraInfo.ptVanishingPoint.y = pVP[1];
	//cout<<"vanishing point "<<m_sCameraInfo.ptVanishingPoint<<endl;


}
//pointer need

/*LYW_0724*/
// (u,v) --> (x,y) 나오게끔. h는 사전에 미리 셋팅.
// Input : row의 개수는 1 이상만 넣어주면 됨. col : 반드시 2개.
// Input/Output matrix의 dim은 같아야함.
// 
void CMultiROILaneDetection::TransformImage2Ground(const Mat &matInPoints, Mat &matOutPoints){
	//	cout<<*matInPoints<<endl;
	//	cout<<"확인1\n"<<endl;

	//add two rows to the input points

	Mat matInPoints4;
	matInPoints4.create(matInPoints.rows + 2, matInPoints.cols, matInPoints.type());

	//copy inPoints to first two rows

	//call by reference
	Mat matInPoints2 = matInPoints4.rowRange(0, 2);
	Mat matInPoints3 = matInPoints4.rowRange(0, 3);
	Mat matInPointsr3 = matInPoints4.row(2);
	Mat matInPointsr4 = matInPoints4.row(3);


	matInPointsr3.setTo(1);

	matInPoints.copyTo(matInPoints2);
	//	cout<<matInPoints4<<endl;
	//	cout<<"확인2\n"<<endl;

	//create the transformation matrix
	float fC1 = cos(m_sCameraInfo.fPitch);
	float fS1 = sin(m_sCameraInfo.fPitch);
	float fC2 = cos(m_sCameraInfo.fYaw);
	float fS2 = sin(m_sCameraInfo.fYaw);
	float fArrT[] = {
		-m_sCameraInfo.fHeight*fC2 / m_sCameraInfo.sizeFocalLength.width,
		m_sCameraInfo.fHeight*fS1*fS2 / m_sCameraInfo.sizeFocalLength.height,
		(m_sCameraInfo.fHeight*fC2*m_sCameraInfo.ptOpticalCenter.x / m_sCameraInfo.sizeFocalLength.width) -
		(m_sCameraInfo.fHeight *fS1*fS2* m_sCameraInfo.ptOpticalCenter.y /
		m_sCameraInfo.sizeFocalLength.height) - m_sCameraInfo.fHeight *fC1*fS2,

		m_sCameraInfo.fHeight *fS2 / m_sCameraInfo.sizeFocalLength.width,
		m_sCameraInfo.fHeight *fS1*fC2 / m_sCameraInfo.sizeFocalLength.height,
		(-m_sCameraInfo.fHeight *fS2* m_sCameraInfo.ptOpticalCenter.x
		/ m_sCameraInfo.sizeFocalLength.width) - (m_sCameraInfo.fHeight *fS1*fC2*
		m_sCameraInfo.ptOpticalCenter.y / m_sCameraInfo.sizeFocalLength.height) -
		m_sCameraInfo.fHeight *fC1*fC2,

		0,
		m_sCameraInfo.fHeight *fC1 / m_sCameraInfo.sizeFocalLength.height,
		(-m_sCameraInfo.fHeight *fC1* m_sCameraInfo.ptOpticalCenter.y / m_sCameraInfo.sizeFocalLength.height) + m_sCameraInfo.fHeight *fS1,

		0,
		-fC1 / m_sCameraInfo.sizeFocalLength.height,
		(fC1* m_sCameraInfo.ptOpticalCenter.y / m_sCameraInfo.sizeFocalLength.height) - fS1,
	};// constant


	Mat matMat = Mat(4, 3, CV_32FC1, fArrT);
	matInPoints4 = matMat*matInPoints3;


	float *pMatInPoints4 = (float*)(&matInPoints4.data[0]);
	float *pMatInPointsr4 = (float*)(&matInPointsr4.data[0]);

	for (int i = 0; i < matInPoints.cols; i++)
	{
		double div = pMatInPointsr4[matInPointsr4.cols * 0 + i];
		pMatInPoints4[matInPoints4.cols * 0 + i] /= div;
		pMatInPoints4[matInPoints4.cols * 1 + i] /= div;
	}


	//put back the result into outPoints
	matInPoints2.copyTo(matOutPoints);
	//cout<<matOutPoints<<endl<<endl;

}
void CMultiROILaneDetection::TransformGround2Image(const Mat &matInPoints, Mat &matOutPoints){
	//add two rows to the input points
	Mat matInPoints3(matInPoints.rows + 1, matInPoints.cols, matInPoints.type()); //(X,Y,-H)

	Mat matInPoints2 = matInPoints3.rowRange(0, 2);
	Mat matInPointsr3 = matInPoints3.row(2);

	matInPointsr3.setTo(-m_sCameraInfo.fHeight);
	matInPoints.copyTo(matInPoints2);

	//create the transformation matrix
	float c1 = cos(m_sCameraInfo.fPitch);
	float s1 = sin(m_sCameraInfo.fPitch);
	float c2 = cos(m_sCameraInfo.fYaw);
	float s2 = sin(m_sCameraInfo.fYaw);
	float matp[] = {
		m_sCameraInfo.sizeFocalLength.width * c2 + c1*s2* m_sCameraInfo.ptOpticalCenter.x,
		-m_sCameraInfo.sizeFocalLength.width * s2 + c1*c2* m_sCameraInfo.ptOpticalCenter.x,
		-s1 * m_sCameraInfo.ptOpticalCenter.x,

		s2 * (-m_sCameraInfo.sizeFocalLength.height * s1 + c1* m_sCameraInfo.ptOpticalCenter.y),
		c2 * (-m_sCameraInfo.sizeFocalLength.height * s1 + c1* m_sCameraInfo.ptOpticalCenter.y),
		-m_sCameraInfo.sizeFocalLength.height * c1 - s1* m_sCameraInfo.ptOpticalCenter.y,

		c1*s2,
		c1*c2,
		-s1
	};
	Mat matMat(3, 3, CV_32FC1, matp);
	matInPoints3 = matMat*matInPoints3;
	for (int i = 0; i < matInPoints.cols; i++)
	{
		float div = matInPointsr3.at<float>(0, i);
		matInPoints3.at<float>(0, i) = matInPoints3.at<float>(0, i) / div;
		matInPoints3.at<float>(1, i) = matInPoints3.at<float>(1, i) / div;
	}
	matInPoints2.copyTo(matOutPoints);
}

//[LYW_0724] : LUT( uvGrid & xyGrid), VP만드는 함수
void CMultiROILaneDetection::SetIpmConfig(EROINUMBER nFlag){

#ifdef _PROFILING_
	m_profiler[nFlag].timeSetConfig.timeStart();
#endif

	m_bTracking[nFlag] = false;
	SetVanishingPoint(); // (ptVanishingPoint.x, ptVanishingPoint.y)
	Point_<float> ptVp = m_sCameraInfo.ptVanishingPoint;
	ptVp.y = MAX(0, ptVp.y);
	//[LYW_1109] : 모듈화하려면 detection단계 전에서 한번 만 호출해야될 함수 안에 있는데, 영상의 사이즈가 필요하네...-_-;;;
	//float fWidth = m_imgResizeOrigin.cols; 
	//float fHeight = m_imgResizeOrigin.rows;
	float fWidth = m_nImageWidth / m_nResizeFactor;
	float fHeight = m_nImageHeight / m_nResizeFactor;
	float fEps = m_sConfig.fVanishPortion * fHeight; //#Q: [LYW_0824] Eps는 뭘까?


	//vanishing point validation
	m_sRoiInfo[nFlag].nLeft = MAX(0, m_sRoiInfo[nFlag].nLeft);
	m_sRoiInfo[nFlag].nRight = MIN(fWidth, m_sRoiInfo[nFlag].nRight);
	m_sRoiInfo[nFlag].nTop = MAX(ptVp.y + fEps, m_sRoiInfo[nFlag].nTop);
	m_sRoiInfo[nFlag].nBottom = MIN(fHeight - 1, m_sRoiInfo[nFlag].nBottom);

	//ROI boundary limits
	float fArrLimits[] = {
		ptVp.x, m_sRoiInfo[nFlag].nRight, m_sRoiInfo[nFlag].nLeft, ptVp.x,
		m_sRoiInfo[nFlag].nTop, m_sRoiInfo[nFlag].nTop, m_sRoiInfo[nFlag].nTop, m_sRoiInfo[nFlag].nBottom
	};

	Mat matUvLimits(2, 4, CV_32FC1, fArrLimits);
	Mat matXyLimits(2, 4, CV_32FC1);

	TransformImage2Ground(matUvLimits, matXyLimits);
	//	cout<<"matXyLimits \n"<<matXyLimits<<endl;
	double xfMax, xfMin, yfMax, yfMin;

	Mat matRow1 = matXyLimits.row(0);
	Mat matRow2 = matXyLimits.row(1);

	minMaxLoc(matRow1, (double*)&xfMin, (double*)&xfMax);
	minMaxLoc(matRow2, (double*)&yfMin, (double*)&yfMax);

	int outRow = m_sRoiInfo[nFlag].sizeIPM.height;
	int outCol = m_sRoiInfo[nFlag].sizeIPM.width;
	float stepRow = (yfMax - yfMin) / outRow;
	float stepCol = (xfMax - xfMin) / outCol;

	//construct the grid to sample

	Mat matXyGrid(2, outRow*outCol, CV_32FC1);
	//Grid is LUT
	float *pMatXyGrid = (float*)&matXyGrid.data[0];
	int i, j;
	float x, y;
	for (i = 0, y = yfMax - .5*stepRow; i < outRow; i++, y -= stepRow)//delete .at() complete
	{
		for (j = 0, x = xfMin + .5*stepCol; j < outCol; j++, x += stepCol)
		{
			pMatXyGrid[matXyGrid.cols * 0 + i*outCol + j] = x;
			pMatXyGrid[matXyGrid.cols * 1 + i*outCol + j] = y;
		}
	}
	matXyGrid.copyTo(m_matXYGrid[nFlag]);
	Mat matUvGrid(2, outRow*outCol, CV_32FC1);

	float* pMatUvGrid = (float*)&matUvGrid.data[0];

	TransformGround2Image(matXyGrid, matUvGrid);
	matUvGrid.copyTo(m_matUVGrid[nFlag]);

	m_imgIPM[nFlag].create(m_sRoiInfo[nFlag].sizeIPM, CV_32FC1);


	//#Q : [LYW_0824] :용도가 뭘까????
	m_sRoiInfo[nFlag].dXLimit[0] = matXyGrid.at<float>(0, 0);
	m_sRoiInfo[nFlag].dXLimit[1] = matXyGrid.at<float>(0, (outRow - 1)*outCol + outCol - 1);
	m_sRoiInfo[nFlag].dYLimit[1] = matXyGrid.at<float>(1, 0);
	m_sRoiInfo[nFlag].dYLimit[0] = matXyGrid.at<float>(1, (outRow - 1)*outCol + outCol - 1);
	m_sRoiInfo[nFlag].dXScale = 1 / stepCol;
	m_sRoiInfo[nFlag].dYScale = 1 / stepRow;
	m_sRoiInfo[nFlag].nIpm2WorldHeight = m_sRoiInfo[nFlag].sizeIPM.height; //not used
	m_sRoiInfo[nFlag].nIpm2WorldWidth = m_sRoiInfo[nFlag].sizeIPM.width; //not used


	//#ifdef _PROFILING_
	//	m_profiler[nFlag].timeSetConfig.timeEnd();
	//	if (nFlag == AUTOCALIB)
	//		cout << "avg pTime of AUTOCALIB setConfig: " << m_profiler[nFlag].timeSetConfig.avgTime();
	//	else if (nFlag == LEFT_ROI0)
	//		cout << "avg pTime of LEFT_ROI0 setConfig: " << m_profiler[nFlag].timeSetConfig.avgTime();
	//	else if (nFlag == LEFT_ROI2)
	//		cout << "avg pTime of LEFT_ROI0 setConfig: " << m_profiler[nFlag].timeSetConfig.avgTime();
	//	else if (nFlag == LEFT_ROI3)
	//		cout << "avg pTime of LEFT_ROI0 setConfig: " << m_profiler[nFlag].timeSetConfig.avgTime();
	//	else if (nFlag == RIGHT_ROI0)
	//		cout << "avg pTime of RIGHT_ROI0 setConfig: " << m_profiler[nFlag].timeSetConfig.avgTime();
	//	else if (nFlag == RIGHT_ROI2)
	//		cout << "avg pTime of RIGHT_ROI2 setConfig: " << m_profiler[nFlag].timeSetConfig.avgTime();
	//	else if (nFlag == RIGHT_ROI3)
	//		cout << "avg pTime of RIGHT_ROI3 setConfig: " << m_profiler[nFlag].timeSetConfig.avgTime();
	//#endif


}

//[LYW] : IPM이미지 만드는 함수
//사용되는 변수 : 각ROI의 grayScaleIamge & uvGrid & xyGrid LUT
//처리결과 : 각ROI의 IPM image
void CMultiROILaneDetection::GetIPM(EROINUMBER nFlag){

	//[LYW_0923] : Profiling
#ifdef _PROFILING_
	m_profiler[nFlag].timeGetIPM.timeStart();
#endif

	Scalar sMean = mean(m_imgResizeScaleGray);
	double dmean = sMean.val[0];
	//	cout<<"dmean\n";
	//	cout<<dmean<<endl;
	int i, j;
	float ui, vi;

	float* ppMatOutImage = (float*)&m_imgIPM[nFlag].data[0]; //결과 : m_imgIPM
	float* ppMatInImage = (float*)&m_imgResizeScaleGray.data[0];
	float* pMatUvGrid = (float*)&m_matUVGrid[nFlag].data[0]; //uvGrid(LUT, uv-->xy)
	int nResizeImgWidth = m_imgResizeScaleGray.cols;
	int nResizeImgHeight = m_imgResizeScaleGray.rows;
	int nIpmWidth = m_sRoiInfo[nFlag].sizeIPM.width;
	int nIpmHeight = m_sRoiInfo[nFlag].sizeIPM.height;
	int nUvGridWidth = m_matUVGrid[nFlag].cols;
	int nUvGridHeight = m_matUVGrid[nFlag].rows;
	//IPM image make process
	for (i = 0; i < nIpmHeight; i++)
		for (j = 0; j < nIpmWidth; j++){
			/*get pixel coordiantes*/

			ui = pMatUvGrid[nUvGridWidth * 0 + i*nIpmWidth + j];
			vi = pMatUvGrid[nUvGridWidth * 1 + i*nIpmWidth + j];
			/*check if out-of-bounds*/
			if (ui<m_sRoiInfo[nFlag].nLeft || ui>m_sRoiInfo[nFlag].nRight ||
				vi<m_sRoiInfo[nFlag].nTop || vi>m_sRoiInfo[nFlag].nBottom) {
				ppMatOutImage[nIpmWidth*i + j] = (float)dmean;
				//if()
			}
			/*not out of bounds, then get nearest neighbor*/
			else
			{
				/*Bilinear interpolation*/
				{
					int x1 = int(ui), x2 = int(ui + 1);
					int y1 = int(vi), y2 = int(vi + 1);
					float x = ui - x1, y = vi - y1;

					float val =
						ppMatInImage[x1 + nResizeImgWidth*y1] * (1 - x)*(1 - y) +
						ppMatInImage[x2 + nResizeImgWidth*y1] * (x)*(1 - y) +
						ppMatInImage[x1 + nResizeImgWidth*y2] * (1 - x)*(y)+
						ppMatInImage[x2 + nResizeImgWidth*y2] * (x)*(y);
					//		cout<<"x "<<x<<"y "<<y<<endl;
					//		cout<<val<<endl;
					ppMatOutImage[j + i*nIpmWidth] = val;
					//	cout<<val<<endl;
					//pMatOutImage->at<float>(i,j) = (float)val;
				}
				/*nearest-neighbor interpolation*/
				/*else
				{

				pMatOutImage->at<float>(i,j)=pMatInImage->at<float>(int(vi+.5),int(ui+.5));
				}*/
			}
			/*if (outPoints &&
			(ui<ipmInfo->ipmLeft+10 || ui>ipmInfo->ipmRight-10 ||
			vi<ipmInfo->ipmTop || vi>ipmInfo->ipmBottom-2) )	{
			outPoints->push_back(cvPoint(j, i));
			}*/
		}

#ifdef _PROFILING_
	m_profiler[nFlag].timeGetIPM.timeEnd();
#endif

	//double dEndTime = getTickCount();
	//m_profiling[nFlag].dTotalTimeGetIPM += (dEndTime - dStartTime) / getTickFrequency();

}




//[LYW_0824]
//사용변수 : 각ROI의IPM image | derivative & smoothing kernel | Quantile param(0.97)
//처리결과 : Filtered Threhold IPM image
void CMultiROILaneDetection::FilterLinesIPM(EROINUMBER nFlag){
	//define the two kernels

	//[LYW_0923] : Profiling
	//double dStartTime = (double)getTickCount();
#ifdef _PROFILING_
	m_profiler[nFlag].timeFilterLinesIPM.timeStart();
#endif

	Scalar dMean = mean(m_imgIPM[nFlag]);

	subtract(m_imgIPM[nFlag], dMean, m_ipmFiltered[nFlag]);

	//[LYW_0824] : m_MatFx : 2nd-oder derivative || m_MatFy : smoothing
	filter2D(m_ipmFiltered[nFlag], m_ipmFiltered[nFlag], m_ipmFiltered[nFlag].depth(),
		m_MatFx, Point(-1, -1), 0.0, BORDER_REPLICATE);
	filter2D(m_ipmFiltered[nFlag], m_ipmFiltered[nFlag], m_ipmFiltered[nFlag].depth(),
		m_MatFy, Point(-1, -1), 0.0, BORDER_REPLICATE);
	//double dStartTick = (double)getTickCount();

	Mat rowMat;
	rowMat = Mat(m_ipmFiltered[nFlag]).reshape(0, 1); //1row로 누적시킴 || num of channel = '0' 채널변경x, rows=1 --> A : histogram과 같은 역할.
	//#Q : 각 열의 (idx,value)가 의미하는 바는? (영상의 col, 누적시킨 픽셀 intensity의 합?)

	//get the quantile
	float fQval;
	fQval = quantile((float*)&rowMat.data[0], rowMat.cols, m_sConfig.fLowerQuantile);//Quantile 97% --> A: intensity찾기위함이였어
	//[LYW_0824] : 1row의 누적된 이미지에서 값이 상위97%인 column(??)를 찾음 --> threshold value로 지정
	//#Q : [LYW_0824] : row값이 아냐... 그냥 pixel의 intensity값 같은데???? 맞나?? 그래야 다음줄이 이해가 되는데??
	//threshold(m_ipmFiltered[nFlag], m_filteredThreshold[nFlag], fQval, NULL, THRESH_TOZERO);	//Threshold 미만 value를 zero로, 나머지 그대로
	//[LYW_20151113] : 4번째 인자 값 넣어줌.
	threshold(m_ipmFiltered[nFlag], m_filteredThreshold[nFlag], fQval, 255, THRESH_TOZERO);	//Threshold 미만 value를 zero로, 나머지 그대로

	//ThresholdLower(imgSubImage,imgSubImage, fQtileThreshold);
	//double dEndTick = (double)getTickCount();
	//cout<<"reshape & quantile & threshold time  "<<(dEndTick-dStartTick) / getTickFrequency()*1000.0<<" msec"<<endl;

	/*
	double dEndTime = (double)getTickCount();
	m_profiling[nFlag].dTotalTimeFilterLinesIPM = (dEndTime - dStartTime) / getTickFrequency();*/
#ifdef _PROFILING_
	m_profiler[nFlag].timeFilterLinesIPM.timeEnd();
#endif

}

void CMultiROILaneDetection::GetLinesIPM(EROINUMBER nFlag){
	//로컬맥시마를 구하는 과정 --> 최종 맥스라인까지 구하는과정(원본코드는 로컬맥시마까지만 구하고 라인피팅에서 걸러내는 작업을 하는거였음)

#ifdef _PROFILING_
	m_profiler[nFlag].timeGetLinesIPM.timeStart();
#endif



	Mat matImage;
	matImage = m_filteredThreshold[nFlag].clone();

	//get sum of lines through horizontal or vertical
	//sumLines is a column vector

	Mat matSumLines, matSumLinesp;

	int maxLineLoc = 0;

	matSumLinesp.create(1, matImage.cols, CV_32FC1);
	reduce(matImage, matSumLinesp, 0, CV_REDUCE_SUM); //reshape비슷한데, 1-row압축 reshape와 차이는 몰라 --> 결론은 진짜 누적!!!
	matSumLines = Mat(matSumLinesp).reshape(0, matImage.cols);

	//max location for a detected line
	maxLineLoc = matImage.cols - 1;//width-1;

	int smoothWidth = 21;
	float smoothp[] = {
		0.000003726653172, 0.000040065297393, 0.000335462627903, 0.002187491118183,
		0.011108996538242, 0.043936933623407, 0.135335283236613, 0.324652467358350,
		0.606530659712633, 0.882496902584595, 1.000000000000000, 0.882496902584595,
		0.606530659712633, 0.324652467358350, 0.135335283236613, 0.043936933623407,
		0.011108996538242, 0.002187491118183, 0.000335462627903, 0.000040065297393,
		0.000003726653172
	};

	Mat matSmooth = Mat(1, smoothWidth, CV_32FC1, smoothp);
	filter2D(matSumLines, matSumLines, CV_32FC1, matSmooth, Point(-1, -1), 0.0, 1);

	//get the max and its location
	vector <int> sumLinesMaxLoc;
	vector <double> sumLinesMax;
	int nMaxLoc;
	double nMax;
	//필터 결과 영상을 누적시키고 노이즈 제거 이후 후보군에 대해서 누적값의 순위를 결정하여 벡터라이즈화
	GetVectorMax(matSumLines, nMax, nMaxLoc, m_sConfig.nLocalMaxIgnore); //#Q : [LYW_0824] : 이건 뭐지?? 봐도 모름...

	float *pfMatSumLinesData = (float*)matSumLines.data;
	//pfMatSumLinesData[matSumLines.cols*j+i];
	//loop to get local maxima
	for (int i = 1 + m_sConfig.nLocalMaxIgnore; i < matSumLines.rows - 1 - m_sConfig.nLocalMaxIgnore; i++){
		//get that value
		//	FLOAT val = CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, i, 0);
		float val = pfMatSumLinesData[matSumLines.cols*i + 0];
		//check if local maximum
		if ((val > pfMatSumLinesData[matSumLines.cols*(i - 1) + 0])
			&& (val > pfMatSumLinesData[matSumLines.cols*(i + 1) + 0])
			//		&& (i != maxLoc)
			&& (val >= m_sRoiInfo[nFlag].nDetectionThreshold)){
			//iterators for the two vectors
			vector<double>::iterator j;
			vector<int>::iterator k;
			//loop till we find the place to put it in descendingly
			for (j = sumLinesMax.begin(), k = sumLinesMaxLoc.begin(); j != sumLinesMax.end() && val <= *j; j++, k++);
			//add its index
			sumLinesMax.insert(j, val);
			sumLinesMaxLoc.insert(k, i);
		}
	}

	//check if didnt find local maxima
	if (sumLinesMax.size() == 0 && nMax > m_sRoiInfo[nFlag].nDetectionThreshold){
		//put maximum
		sumLinesMaxLoc.push_back(nMaxLoc);
		sumLinesMax.push_back(nMax);
	}

	//     //sort it descendingly

	//plot the line scores and the local maxima

	//process the found maxima
	pfMatSumLinesData = (float*)matSumLines.data;
	for (int i = 0; i < (int)sumLinesMax.size(); i++){
		//get subpixel accuracy
		double maxLocAcc = GetLocalMaxSubPixel(
			(double)pfMatSumLinesData[matSumLines.cols*MAX(sumLinesMaxLoc[i] - 1, 0) + 0],
			(double)pfMatSumLinesData[matSumLines.cols*sumLinesMaxLoc[i] + 0],
			(double)pfMatSumLinesData[matSumLines.cols*MIN(sumLinesMaxLoc[i] + 1, maxLineLoc) + 0]
			);
		maxLocAcc += sumLinesMaxLoc[i];
		maxLocAcc = MIN(MAX(0, maxLocAcc), maxLineLoc);
		//TODO: get line extent
		//put the extracted line
		SLine line;
		line.ptStartLine.x = (double)maxLocAcc + 0.5;//sumLinesMaxLoc[i]+.5;
		line.ptStartLine.y = 0.5;
		line.ptEndLine.x = line.ptStartLine.x;
		line.ptEndLine.y = m_filteredThreshold[nFlag].rows - 0.5;//inImage->height-.5;

		(m_lanes[nFlag]).push_back(line);
		//		if (lineScores)
		(m_laneScore[nFlag]).push_back(sumLinesMax[i]);
	}





	////tracking module 20150804
	if ((nFlag != CENTER_ROI) && (nFlag != AUTOCALIB) && (m_lanes[nFlag].size() > 0)){
		if (m_sTracking[nFlag].bTracking){
			GetTrackingLineCandidateModule(nFlag);//이전에 추적한 결과가 있을 경우, 검출 대상 차선의 위치가 월드좌표상으로 멀리 떨어진 값인지 확인
		}
		else{
			GetMaxLineScore(nFlag); //[LYW_0825] : score가 가장 높은 line과 그것의 score를 저장 1개.

		}

	}


	//cout<<"nFlag"<<nFlag<<endl;
	//for(int i=0;i<m_laneScore[nFlag].size();i++)
	//	cout<<m_laneScore[nFlag].at(i)<<","<<endl;

	//clean
	sumLinesMax.clear();
	sumLinesMaxLoc.clear();

#ifdef _PROFILING_
	m_profiler[nFlag].timeGetLinesIPM.timeEnd();
#endif

}
void CMultiROILaneDetection::LineFitting(EROINUMBER nFlag){

#ifdef _PROFILING_
	m_profiler[nFlag].timeLineFitting.timeStart();
#endif


	vector<SLine> lines = m_lanes[nFlag]; // [LYW_0724] : line fitting해야할 후보들, m_laneScore와 짝을 이룬다. (같은 index에 score가 저장되어 있음)
	//그렇지만 이 프로그램(승준이꺼)에서는 max값만 넘어오기때문에 딱 하나만 넘겨옮

	vector<float> lineScores = m_laneScore[nFlag];

	/*cout<<m_lanes[nFlag].at(1).ptStartLine<<endl;
	cout<<lines.at(1).ptStartLine<<endl;
	cout<<m_laneScore[nFlag].at(1)<<endl;
	cout<<lineScores.at(1)<<endl;*/
	int width = m_sRoiInfo[nFlag].sizeIPM.width - 1;
	int height = m_sRoiInfo[nFlag].sizeIPM.height - 1;

	//#Q : [LYW_0824] : GroupLines는 왜 하는지 --> 클러스터링하는건가?? 승준이도 모른데.
	GroupLines(lines, lineScores, m_sRoiInfo[nFlag].nGroupThreshold, Size_<float>((float)width, (float)height));
	float overlapThreshold = m_sRoiInfo[nFlag].fOverlapThreshold; //0.5; //.8;

	vector<Rect> vecRectBoxes;

	GetLinesBoundingBoxes(lines, LINE_VERTICAL, Size_<int>(width, height), vecRectBoxes);
	GroupBoundingBoxes(vecRectBoxes, LINE_VERTICAL, overlapThreshold); //[LYW_0825] : 바운딩박스 클러스터링

	int window = m_sRoiInfo[nFlag].nRansacLineWindow; //15;
	vector<SLine> newLines;
	vector<float> newScores;
	for (int i = 0; i < (int)vecRectBoxes.size(); i++) //lines
	{
		// 	fprintf(stderr, "i=%d\n", i);
		//Line line = lines[i];
		//CvRect mask, box;
		Rect rectMask, rectBox;
		//get box
		//box = boxes[i];
		rectBox = vecRectBoxes[i];

		//get extent of window to search in
		//int xstart = (int)fmax(fmin(line.startPoint.x, line.endPoint.x)-window, 0);
		//int xend = (int)fmin(fmax(line.startPoint.x, line.endPoint.x)+window, width-1);
		int xstart = (int)max(rectBox.x - window, 0);
		int xend = (int)min(rectBox.x + rectBox.width + window, width - 1);
		//get the mask
		//mask = cvRect(xstart, 0, xend-xstart+1, height);
		rectMask = Rect(xstart, 0, xend - xstart + 1, height);

		//get the subimage to work on

		Mat matSubImage = m_filteredThreshold[nFlag].clone();
		//clear all but the mask

		SetMat(matSubImage, rectMask, 0);

		float lineRTheta[2] = { -1, 0 };
		float lineScore;
		SLine line;
		//RANSAC 결과

		FitRansacLine(matSubImage, m_sRoiInfo[nFlag].nRansacNumSamples,
			m_sRoiInfo[nFlag].nRansacNumIterations,
			m_sRoiInfo[nFlag].fRansacThreshold,
			m_sRoiInfo[nFlag].nRansacScoreThreshold,
			m_sRoiInfo[nFlag].nRansacNumGoodFit,
			m_sRoiInfo[nFlag].nGetEndPoint, LINE_VERTICAL,
			&line, lineRTheta, &lineScore, nFlag);

		//store the line if found and make sure it's not
		//near horizontal or vertical (depending on type)
		//  #warning "check this screening in ransacLines"
		if (lineRTheta[0] >= 0){
			bool put = true;
			//make sure it's not horizontal
			if ((fabs(lineRTheta[1]) > 20 * CV_PI / 180))
				put = false;

			//IPM ROI border line rejection
			int nBorderX = (line.ptStartLine.x + line.ptEndLine.x) / 2;
			int nBorderGap = m_sRoiInfo[nFlag].sizeIPM.width / 10;
			if (nBorderX<(nBorderGap) || nBorderX>(m_sRoiInfo[nFlag].sizeIPM.width - nBorderGap))
				put = false;


			if (put){
				newLines.push_back(line);
				newScores.push_back(lineScore);
			}
		} // if
		//clear
	} // for i

	lines.clear();
	lineScores.clear();
	//#warning "not grouping at end of getRansacLines"
	//lines = newLines;
	//lineScores = newScores;

	m_lanes[nFlag].clear();
	m_laneScore[nFlag].clear();

	if (newScores.size() > 2 && nFlag == AUTOCALIB){ // [LYW_0829] : AUTOCALIB할 때 라인이 3개이상 들어왔을 때 문제를 해결하기위함
		//int nFirst,nSecond;
		int nFirstIdx, nSecondIdx;
		if (newScores[0] > newScores[1]){
			nFirstIdx = 0, nSecondIdx = 1;
			//nFirst=newScores[0],nSecond=newScores[1];
		}
		else{
			nFirstIdx = 1, nSecondIdx = 0;
			//nFirst=newScores[1],nSecond=newScores[0];
		}
		for (int i = 2; i < newScores.size(); i++)
		{
			if (newScores[i] > newScores[nFirstIdx]){
				nSecondIdx = nFirstIdx;
				nFirstIdx = i;
			}
			else if (newScores[i] > newScores[nSecondIdx]){
				nSecondIdx = i;
			}
		}
		m_lanes[nFlag].push_back(newLines[nFirstIdx]);
		m_laneScore[nFlag].push_back(newScores[nFirstIdx]);
		m_lanes[nFlag].push_back(newLines[nSecondIdx]);
		m_laneScore[nFlag].push_back(newScores[nSecondIdx]);
	}
	else{ // [LYW_0724] : Auto Calib가 아닌 일반적인 detection단계에서 찾은 차선을 다 넣어주는거야! 
		m_lanes[nFlag] = newLines;
		m_laneScore[nFlag] = newScores;
	}

	//clean
	//boxes.clear();
	newLines.clear();
	newScores.clear();

#ifdef _PROFILING_
	m_profiler[nFlag].timeLineFitting.timeEnd();
#endif

}

//IPM에서 검출된 lines --> Image로
void CMultiROILaneDetection::IPM2ImLines(EROINUMBER nFlag){ // 
	//[LYW_0825] : lines --> mat -( TransformGround2Image() )-> mat - ( Mat2Lines() ) -> Lines ( m_lanesResult )

#ifdef _PROFILING_
	m_profiler[nFlag].timeIPM2ImLines.timeStart();
#endif


	if (m_lanes[nFlag].size() != 0){
		//PointImIPM2World
		//m_lanes[nFlag].
		//IPM2WORLD
		for (int i = 0; i < m_lanes[nFlag].size(); i++){
			m_lanesResult[nFlag].push_back(m_lanes[nFlag].at(i));
			//cout<<m_lanes[nFlag].at(i).ptStartLine<<endl;
			//cout<<m_lanesResult[nFlag].at(i).ptStartLine<<endl;


			//#Q : [LYW_0825] : IPMLines --> ImageLines로 변환하는 과정인 것 같지???  A: IPMLines -> groundLines까지만 했음. 아직 ImageLines안바꿈
			//[LYW_0829]
			//IPMimage의 좌표 --> ground로 변환하는 방법 --> 카메라중심으로부터 가까이에 있을수록 월드y좌표는 작다.즉, ptStart의 y가 ptEnd보다 작다.
			//주의 ! ground좌표를 아직 영상좌표로 바꾸지 않았음.
			//아래와 같은 방법 : IPMLine(포인트) --> groundLine(포인트)바꾸는 방법!! 중요해! 나중에 응용해서 쓸 수 있으니깐 잘 기억해둬!@@
			//x-direction
			m_lanesResult[nFlag].at(i).ptStartLine.x = m_lanes[nFlag].at(i).ptStartLine.x / (m_sRoiInfo[nFlag].dXScale);
			m_lanesResult[nFlag].at(i).ptStartLine.x += m_sRoiInfo[nFlag].dXLimit[0];
			//y-direction
			m_lanesResult[nFlag].at(i).ptStartLine.y = m_lanes[nFlag].at(i).ptStartLine.y / (m_sRoiInfo[nFlag].dYScale);
			m_lanesResult[nFlag].at(i).ptStartLine.y = m_sRoiInfo[nFlag].dYLimit[1] - m_lanesResult[nFlag].at(i).ptStartLine.y;
			//x-direction
			m_lanesResult[nFlag].at(i).ptEndLine.x = m_lanes[nFlag].at(i).ptEndLine.x / m_sRoiInfo[nFlag].dXScale;
			m_lanesResult[nFlag].at(i).ptEndLine.x += m_sRoiInfo[nFlag].dXLimit[0];
			//y-direction
			m_lanesResult[nFlag].at(i).ptEndLine.y = m_lanes[nFlag].at(i).ptEndLine.y / m_sRoiInfo[nFlag].dYScale;
			m_lanesResult[nFlag].at(i).ptEndLine.y = m_sRoiInfo[nFlag].dYLimit[1] - m_lanesResult[nFlag].at(i).ptEndLine.y;
			//record ground location
			m_lanesGroundResult[nFlag].push_back(m_lanesResult[nFlag].at(i));
		}

		// IPM image에서의 경계를 오검출 하는 문제 예외처리
		Point ptUvSt = TransformPointGround2Image(m_lanesGroundResult[nFlag][0].ptStartLine);
		Point ptUvEnd = TransformPointGround2Image(m_lanesGroundResult[nFlag][0].ptEndLine);

		float diffX = abs(ptUvSt.x - ptUvEnd.x);
		float diffRoi_1 = abs(m_sRoiInfo[nFlag].nLeft - ptUvSt.x);
		float diffRoi_2 = abs(m_sRoiInfo[nFlag].nLeft - ptUvEnd.x);
		float diffRightRoi_1 = abs(m_sRoiInfo[nFlag].nRight - ptUvSt.x);
		float diffRightRoi_2 = abs(m_sRoiInfo[nFlag].nRight - ptUvEnd.x);

		//		printf("[usual ROI %d] %2f %2f %2f\n", nFlag,diffX, diffRoi_1, diffRoi_2);
		if (diffX < 8 && diffRoi_1 < 8 && diffRoi_2 < 8){
			//		printf("[%d ROI LEFT Boundary]: %2f %2f %2f\n", nFlag, diffX, diffRoi_1, diffRoi_2);
			//printf("[LEFT ROI %d boundary exception!]\n", nFlag);
			ClearResultVector(nFlag);
			return;
		}
		else if (diffX < 8 && diffRightRoi_1 < 8 && diffRightRoi_2 < 8)
		{
			//	printf("[%d ROI RIGHT Boundary]: %2f %2f %2f\n", nFlag, diffX, diffRightRoi_1, diffRightRoi_2);
			//printf("[RIGHT ROI %d boundary exception!]\n", nFlag);
			ClearResultVector(nFlag);
			return;
		}

	}



#ifdef _PROFILING_
	m_profiler[nFlag].timeIPM2ImLines.timeEnd();
#endif


}
void CMultiROILaneDetection::GetVectorMax(const Mat &matInVector, double &dMax, int &nMaxLoc, int nIgnore){
	double tmax;
	int tmaxLoc;

	float *pfMatInVectorData = (float*)matInVector.data;
	if (matInVector.rows == 1){
		/*initial value*/

		tmax = (double)pfMatInVectorData[0 * matInVector.cols + matInVector.cols - 1];

		tmaxLoc = matInVector.cols - 1;//inVector->width-1; 
		/*loop*/
		for (int i = matInVector.cols - 1 - nIgnore; i >= 0 + nIgnore; i--){

			if (tmax < (double)pfMatInVectorData[0 * matInVector.cols + i]){

				tmax = (double)pfMatInVectorData[0 * matInVector.cols + i];
				tmaxLoc = i;
			}
		}
	}
	/*column vector */
	else{
		/*initial value*/
		tmax = (double)pfMatInVectorData[(matInVector.rows - 1)*matInVector.cols + 0];
		tmaxLoc = matInVector.rows - 1;
		/*loop*/
		for (int i = matInVector.rows - 1 - nIgnore; i >= 0 + nIgnore; i--){

			if (tmax < (double)pfMatInVectorData[i*matInVector.cols + 0]){
				tmax = (double)pfMatInVectorData[i*matInVector.cols + 0];
				tmaxLoc = i;
			}
		}
	}
	//return
	if (dMax)
		dMax = tmax;
	if (nMaxLoc)
		nMaxLoc = tmaxLoc;
}
double CMultiROILaneDetection::GetLocalMaxSubPixel(double dVal1, double dVal2, double dVal3)
{
	//build an array to hold the x-values
	double Xp[] = { 1, -1, 1, 0, 0, 1, 1, 1, 1 };
	Mat X = Mat(3, 3, CV_64FC1, Xp);

	//array to hold the y values
	double yp[] = { dVal1, dVal2, dVal3 };
	Mat y = Mat(3, 1, CV_64FC1, yp);

	//solve to get the coefficients
	double Ap[3];
	Mat A = Mat(3, 1, CV_64FC1, Ap);

	solve(X, y, A, DECOMP_SVD);
	//get the local max
	double nMax;
	nMax = -0.5 * Ap[1] / Ap[0];

	//return
	return nMax;
}
void CMultiROILaneDetection::GetMaxLineScore(EROINUMBER nFlag){
	float fMaxScore = MAXCOMP;
	int nMaxIter = 0;
	for (int i = 0; i < m_laneScore[nFlag].size(); i++){
		nMaxIter = (m_laneScore[nFlag].at(i) > fMaxScore ? i : nMaxIter);
		fMaxScore = m_laneScore[nFlag].at(nMaxIter);
	}

	SLine SMAxLane = m_lanes[nFlag].at(nMaxIter);
	//clear
	m_lanes[nFlag].clear();
	m_laneScore[nFlag].clear();

	//reload
	m_lanes[nFlag].push_back(SMAxLane);
	m_laneScore[nFlag].push_back(fMaxScore);

}
//tracking module
//m_sTracking[nflag].bTracking

/**
@brief getLineIPM에서 호출.IPM에서 구해진 라인과 트래킹되는 라인과의 비교.
@param ROI flag.
@return void
@warning
@author 이영완
@date 2015.11.09.
*/
void CMultiROILaneDetection::GetTrackingLineCandidateModule(EROINUMBER nFlag){
	vector<SLine> vecLanes;
	vector<SWorldLane> vecWorldLane;
	SLine sLineTemp;
	SWorldLane sWorldLaneTemp;
	//#Q : [LYW_0825] : dXScale로 나누고, dXLimit더하는 이유는 뭘까???
	for (int i = 0; i < m_lanes[nFlag].size(); i++){//m_lanes는 각각의 ROI에 대한 line fitting 결과임
		sLineTemp.ptStartLine.x = m_lanes[nFlag].at(i).ptStartLine.x / m_sRoiInfo[nFlag].dXScale; //IPM image 2 World 변환을 위한 계산
		sLineTemp.ptStartLine.x += m_sRoiInfo[nFlag].dXLimit[0]; //IPM image 2 World 변환을 위한 계산
		sLineTemp.ptStartLine.y = m_lanes[nFlag].at(i).ptStartLine.y / m_sRoiInfo[nFlag].dYScale; //IPM image 2 World 변환을 위한 계산
		sLineTemp.ptStartLine.y = m_sRoiInfo[nFlag].dYLimit[1] - m_lanes[nFlag].at(i).ptStartLine.y; //IPM image 2 World 변환을 위한 계산

		sLineTemp.ptEndLine.x = m_lanes[nFlag].at(i).ptEndLine.x / m_sRoiInfo[nFlag].dXScale;
		sLineTemp.ptEndLine.x += m_sRoiInfo[nFlag].dXLimit[0];
		sLineTemp.ptEndLine.y = m_lanes[nFlag].at(i).ptEndLine.y / m_sRoiInfo[nFlag].dYScale;
		sLineTemp.ptEndLine.y = m_sRoiInfo[nFlag].dYLimit[1] - m_lanes[nFlag].at(i).ptEndLine.y;

		// [LYW_0825] : fXcenter, fXderiv는 tracking 때 쓰려고 하는 변수인가???
		sWorldLaneTemp.fXcenter = (sLineTemp.ptStartLine.x + sLineTemp.ptEndLine.x) / 2;
		sWorldLaneTemp.fXderiv = sLineTemp.ptStartLine.x - sLineTemp.ptEndLine.x;
		//20150519/////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////
		vecLanes.push_back(sLineTemp);
		vecWorldLane.push_back(sWorldLaneTemp);
	}

	//	m_lanes[nFlag]
	int nMaxIter = 0;
	float fMinDist = MINCOMP;
	float fTempComp;
	int nTargetTracker = m_sTracking[nFlag].nTargetTracker;

	//ROI 내에서 검출된 여러 라인을 추적한 차선과의 거리를 비교하여 가장 가까운 라인을 남김 --> false alaram제거하는 기능
	//[LYW_151216] : 아예 제거하지는 않네 --> 거리를 이용해서 제거해볼까?
	//[LYW_151216] : m_SKalmanLane[nTargetTracker].SKalmanTrackingLine.fXcenter은 moving average라서 이놈과 비교하면 안되 최종적으로 추정된 차선과 비교해야지 : m_sTrackingLane
	for (int i = 0; i < vecWorldLane.size(); i++){
		//fTempComp = abs(m_SKalmanLane[nTargetTracker].SKalmanTrackingLine.fXcenter - vecWorldLane.at(i).fXcenter);//m_sTrackingLane
		fTempComp = abs(m_sTrackingLane[nTargetTracker].fXcenter - vecWorldLane.at(i).fXcenter);//m_sTrackingLane
		if (fTempComp < fMinDist){
			fMinDist = fTempComp;
			nMaxIter = i;
		}
	}
	SLine SMAxLane = m_lanes[nFlag].at(nMaxIter);
	float fMaxScore = m_laneScore[nFlag].at(nMaxIter);
	//clear
	m_lanes[nFlag].clear();
	m_laneScore[nFlag].clear();
	//reload
	m_lanes[nFlag].push_back(SMAxLane);
	m_laneScore[nFlag].push_back(fMaxScore);
	//#debug
	if (fMinDist > DIST_TRACKING_WIDTH){
		m_lanes[nFlag].clear();
		m_laneScore[nFlag].clear();
	}
}


void CMultiROILaneDetection::GetMaxLineScoreTwo(EROINUMBER nFlag){
	float fMaxScore = MAXCOMP;
	int nMaxIter = 0;
	float fScoreArr[2];
	for (int i = 0; i < m_laneScore[nFlag].size(); i++){
		nMaxIter = (m_laneScore[nFlag].at(i) < fMaxScore ? i : nMaxIter);
	}
	fMaxScore = m_laneScore[nFlag].at(nMaxIter);
	SLine SMAxLane = m_lanes[nFlag].at(nMaxIter);
	//clear
	m_lanes[nFlag].clear();
	m_laneScore[nFlag].clear();

	//reload
	m_lanes[nFlag].push_back(SMAxLane);
	m_laneScore[nFlag].push_back(fMaxScore);
}
void CMultiROILaneDetection::Lines2Mat(const vector<SLine> &lines, Mat &mat)
{
	//allocate the matrix
	//*mat = cvCreateMat(2, size*2, FLOAT_MAT_TYPE);

	//loop and put values
	int j;
	float* pfMat = (float*)mat.data;
	for (int i = 0; i < (int)lines.size(); i++)
	{
		j = 2 * i;

		pfMat[mat.cols * 0 + j] = (lines)[i].ptStartLine.x;
		pfMat[mat.cols * 1 + j] = (lines)[i].ptStartLine.y;
		pfMat[mat.cols * 0 + j + 1] = (lines)[i].ptEndLine.x;
		pfMat[mat.cols * 1 + j + 1] = (lines)[i].ptEndLine.y;
	}
}
void CMultiROILaneDetection::Mat2Lines(const Mat &mat, vector<SLine> &lines)
{

	SLine line;
	float* pfMat = (float*)mat.data;
	//loop and put values
	for (int i = 0; i<int(mat.cols / 2); i++)
	{
		int j = 2 * i;
		//get the line
		line.ptStartLine.x = pfMat[mat.cols * 0 + j];
		line.ptStartLine.y = pfMat[mat.cols * 1 + j];
		line.ptEndLine.x = pfMat[mat.cols * 0 + j + 1];
		line.ptEndLine.y = pfMat[mat.cols * 1 + j + 1];
		//push it
		lines.push_back(line);
	}
}
void CMultiROILaneDetection::GroupLines(vector<SLine> &lines, vector<float> &lineScores,
	float groupThreshold, Size_<float> bbox)
{

	//convert the lines into r-theta parameters
	//#Q : [LYW_0825] : GroupLines이건 왜 하는걸까?? line2R-theta왜 하는거야?
	int numInLines = lines.size();
	vector<float> rs(numInLines);
	vector<float> thetas(numInLines);
	for (int i = 0; i < numInLines; i++)
		LineXY2RTheta(lines[i], rs[i], thetas[i]);

	//flag for stopping
	bool stop = false;
	while (!stop)
	{
		//minimum distance so far
		float minDist = groupThreshold + 5, dist;
		vector<float>::iterator ir, jr, itheta, jtheta, minIr, minJr, minItheta, minJtheta,
			iscore, jscore, minIscore, minJscore;
		//compute pairwise distance between detected maxima
		for (ir = rs.begin(), itheta = thetas.begin(), iscore = lineScores.begin();
			ir != rs.end(); ir++, itheta++, iscore++)
			for (jr = ir + 1, jtheta = itheta + 1, jscore = iscore + 1;
				jr != rs.end(); jr++, jtheta++, jscore++)
		{
			//add pi if neg
			float t1 = *itheta < 0 ? *itheta : *itheta + CV_PI;
			float t2 = *jtheta < 0 ? *jtheta : *jtheta + CV_PI;
			//get distance
			dist = 1 * fabs(*ir - *jr) + 1 * fabs(t1 - t2);//fabs(*itheta - *jtheta);
			//check if minimum
			if (dist < minDist)
			{
				minDist = dist;
				minIr = ir; minItheta = itheta;
				minJr = jr; minJtheta = jtheta;
				minIscore = iscore; minJscore = jscore;
			}
		}
		//check if minimum distance is less than groupThreshold
		if (minDist >= groupThreshold)
			stop = true;
		else
		{
			//put into the first
			*minIr = (*minIr + *minJr) / 2;
			*minItheta = (*minItheta + *minJtheta) / 2;
			*minIscore = (*minIscore + *minJscore) / 2;
			//delete second one
			rs.erase(minJr);
			thetas.erase(minJtheta);
			lineScores.erase(minJscore);
		}
	}//while

	//put back the lines
	lines.clear();
	//lines.resize(rs.size());
	vector<float> newScores = lineScores;
	lineScores.clear();
	for (int i = 0; i < (int)rs.size(); i++)
	{
		//get the line
		SLine line;
		//mcvIntersectLineRThetaWithBB(rs[i], thetas[i], bbox, &line);
		IntersectLineRThetaWithBB(rs[i], thetas[i], bbox, &line);
		//put in place descendingly
		vector<float>::iterator iscore;
		vector<SLine>::iterator iline;
		for (iscore = lineScores.begin(), iline = lines.begin();
			iscore != lineScores.end() && newScores[i] <= *iscore; iscore++, iline++);
			lineScores.insert(iscore, newScores[i]);
		lines.insert(iline, line);
	}
	//clear
	newScores.clear();
}
void CMultiROILaneDetection::LineXY2RTheta(const SLine &line, float &r, float &theta)
{
	//check if vertical line x1==x2
	if (line.ptStartLine.x == line.ptEndLine.x)
	{
		//r is the x
		r = fabs(line.ptStartLine.x);
		//theta is 0 or pi
		theta = line.ptStartLine.x >= 0 ? 0. : CV_PI;
	}
	//check if horizontal i.e. y1==y2
	else if (line.ptStartLine.y == line.ptEndLine.y)
	{
		//r is the y
		r = fabs(line.ptStartLine.y);
		//theta is pi/2 or -pi/2
		theta = (float)line.ptStartLine.y >= 0 ? CV_PI / 2 : -CV_PI / 2;
	}
	//general line
	else
	{
		//tan(theta) = (x2-x1)/(y1-y2)
		theta = atan2(line.ptEndLine.x - line.ptStartLine.x,
			line.ptStartLine.y - line.ptEndLine.y);
		//r = x*cos(theta)+y*sin(theta)
		float r1 = line.ptStartLine.x * cos(theta) + line.ptStartLine.y * sin(theta);
		r = line.ptEndLine.x * cos(theta) + line.ptEndLine.y * sin(theta);
		//adjust to add pi if necessary
		if (r1 < 0 || r < 0)
		{
			//add pi
			theta += CV_PI;
			if (theta > CV_PI)
				theta -= 2 * CV_PI;
			//take abs
			r = fabs(r);
		}
	}
}
void CMultiROILaneDetection::IntersectLineRThetaWithBB(float r, float theta, const Size_<float> bbox, SLine *outLine)
{
	//hold parameters
	double xup, xdown, yleft, yright;

	//intersect with top and bottom borders: y=0 and y=bbox.height-1
	if (cos(theta) == 0) //horizontal line
	{
		xup = xdown = bbox.width + 2;
	}
	else
	{
		xup = r / cos(theta);
		xdown = (r - bbox.height*sin(theta)) / cos(theta);
	}

	//intersect with left and right borders: x=0 and x=bbox.widht-1
	if (sin(theta) == 0) //horizontal line
	{
		yleft = yright = bbox.height + 2;
	}
	else
	{
		yleft = r / sin(theta);
		yright = (r - bbox.width*cos(theta)) / sin(theta);
	}

	//points of intersection

	Point2d pts[4] = { Point2d(xup, 0), Point2d(xdown, bbox.height),
		Point2d(0, yleft), Point2d(bbox.width, yright) };
	//get the starting point
	int i;
	for (i = 0; i < 4; i++)
	{
		//if point inside, then put it

		if (IsPointInside(pts[i], bbox))
		{
			outLine->ptStartLine.x = pts[i].x;
			outLine->ptStartLine.y = pts[i].y;
			//get out of for loop
			break;
		}
	}
	//get the ending point
	for (i++; i < 4; i++)
	{
		//if point inside, then put it
		if (IsPointInside(pts[i], bbox))
		{
			outLine->ptEndLine.x = pts[i].x;
			outLine->ptEndLine.y = pts[i].y;
			//get out of for loop
			break;
		}
	}
}
bool CMultiROILaneDetection::IsPointInside(Point2d point, Size_<int> bbox)
{
	return (point.x >= 0 && point.x <= bbox.width
		&& point.y >= 0 && point.y <= bbox.height) ? true : false;
}
bool CMultiROILaneDetection::IsPointInside(Point2d point, Size_<float> bbox)
{
	return (point.x >= 0 && point.x <= bbox.width
		&& point.y >= 0 && point.y <= bbox.height) ? true : false;
}
void CMultiROILaneDetection::GetLinesBoundingBoxes(const vector<SLine> &lines, LineType type,
	Size_<int> size, vector<Rect> &boxes)
{
	//copy lines to boxes
	int start, end;
	//clear
	boxes.clear();
	switch (type)
	{
	case LINE_VERTICAL:
		for (unsigned int i = 0; i < lines.size(); ++i)
		{
			//get min and max x and add the bounding box covering the whole height
			start = (int)min(lines[i].ptStartLine.x, lines[i].ptEndLine.x);
			end = (int)max(lines[i].ptStartLine.x, lines[i].ptEndLine.x);
			boxes.push_back(Rect_<int>(start, 0, end - start + 1, size.height - 1));
		}
		break;

	case LINE_HORIZONTAL:
		for (unsigned int i = 0; i < lines.size(); ++i)
		{
			//get min and max y and add the bounding box covering the whole width
			start = (int)min(lines[i].ptStartLine.y, lines[i].ptEndLine.y);
			end = (int)max(lines[i].ptStartLine.y, lines[i].ptEndLine.y);
			boxes.push_back(Rect_<int>(0, start, size.width - 1, end - start + 1));
		}
		break;
	}
}
//[LYW_0825]
//바운딩박스 클러스터링
void CMultiROILaneDetection::GroupBoundingBoxes(vector<Rect> &boxes, LineType type, float groupThreshold){
	bool cont = true;

	//Todo: check if to intersect with bounding box or not

	//save boxes
	//vector<CvRect> tboxes = boxes;

	//loop to get the largest overlap (according to type) and check
	//the overlap ratio
	float overlap, maxOverlap;
	while (cont)
	{
		maxOverlap = overlap = -1e5;
		//loop on lines and get max overlap
		vector<Rect>::iterator i, j, maxI, maxJ;
		for (i = boxes.begin(); i != boxes.end(); i++)
		{
			for (j = i + 1; j != boxes.end(); j++)
			{
				switch (type)
				{
				case LINE_VERTICAL:
					//get one with smallest x, and compute the x2 - x1 / width of smallest
					//i.e. (x12 - x21) / (x22 - x21)
					overlap = i->x < j->x ?
						(i->x + i->width - j->x) / (float)j->width :
						(j->x + j->width - i->x) / (float)i->width;

					break;

				case LINE_HORIZONTAL:
					//get one with smallest y, and compute the y2 - y1 / height of smallest
					//i.e. (y12 - y21) / (y22 - y21)
					overlap = i->y < j->y ?
						(i->y + i->height - j->y) / (float)j->height :
						(j->y + j->height - i->y) / (float)i->height;

					break;

				} //switch

				//get maximum
				if (overlap > maxOverlap)
				{
					maxI = i;
					maxJ = j;
					maxOverlap = overlap;
				}
			} //for j
		} // for i
		// 	//debug
		// 	if(DEBUG_LINES) {
		// 	    cout << "maxOverlap=" << maxOverlap << endl;
		// 	    cout << "Before grouping\n";
		// 	    for(unsigned int k=0; k<boxes.size(); ++k)
		// 		SHOW_RECT(boxes[k]);
		// 	}

		//now check the max overlap found against the threshold
		if (maxOverlap >= groupThreshold)
		{
			//combine the two boxes
			*maxI = Rect_<float>(min((*maxI).x, (*maxJ).x),
				min((*maxI).y, (*maxJ).y),
				max((*maxI).width, (*maxJ).width),
				max((*maxI).height, (*maxJ).height));
			//delete the second one
			boxes.erase(maxJ);
		}
		else
			//stop
			cont = false;

		// 	//debug
		// 	if(DEBUG_LINES) {
		// 	    cout << "After grouping\n";
		// 	    for(unsigned int k=0; k<boxes.size(); ++k)
		// 		SHOW_RECT(boxes[k]);
		// 	}
	} //while
}
void CMultiROILaneDetection::SetMat(Mat& imgInMat, Rect_<int> RectMask, double val)
{

	//get x-end points of region to work on, and work on the whole image height
	//(int)fmax(fmin(line.startPoint.x, line.endPoint.x)-xwindow, 0);

	int xstart = RectMask.x;
	int xend = RectMask.x + RectMask.width - 1;

	int ystart = RectMask.y;
	int yend = RectMask.y + RectMask.height - 1;

	//set other two windows to zero

	Mat imgMask;

	Rect_<int> rectInfunction;
	//part to the left of required region


	rectInfunction = Rect(0, 0, xstart - 1, imgInMat.rows);
	//cout<<rectInfunction<<endl;
	if (rectInfunction.x < imgInMat.cols && rectInfunction.y < imgInMat.rows &&
		rectInfunction.x >= 0 && rectInfunction.y >= 0 && rectInfunction.width > 0 && rectInfunction.height > 0)
	{
		imgInMat(rectInfunction) = val;
	}
	//part to the right of required region

	rectInfunction = Rect(xend + 1, 0, imgInMat.cols - xend - 1, imgInMat.rows);
	//cout<<rectInfunction<<endl;
	if (rectInfunction.x < imgInMat.cols && rectInfunction.y < imgInMat.rows &&
		rectInfunction.x >= 0 && rectInfunction.y >= 0 && rectInfunction.width > 0 && rectInfunction.height > 0)
	{
		imgInMat(rectInfunction) = val;
	}

	//part to the top

	rectInfunction = Rect(xstart, 0, RectMask.width, ystart - 1);
	//cout<<rectInfunction<<endl;
	if (rectInfunction.x < imgInMat.cols && rectInfunction.y < imgInMat.rows &&
		rectInfunction.x >= 0 && rectInfunction.y >= 0 && rectInfunction.width > 0 && rectInfunction.height > 0)
	{

		imgInMat(rectInfunction) = val;

	}

	//part to the bottom
	//rect = cvRect(xstart, yend+1, mask.width, inMat->height-yend-1);
	rectInfunction = Rect(xstart, yend + 1, RectMask.width, imgInMat.rows - yend - 1);
	//cout<<rectInfunction<<endl;
	if (rectInfunction.x < imgInMat.cols && rectInfunction.y < imgInMat.rows &&
		rectInfunction.x >= 0 && rectInfunction.y >= 0 && rectInfunction.width > 0 && rectInfunction.height > 0)
	{

		imgInMat(rectInfunction) = val;
	}


}
void CMultiROILaneDetection::FitRansacLine(const Mat& matImage, int numSamples, int numIterations,
	float threshold, float scoreThreshold, int numGoodFit,
	bool getEndPoints, LineType lineType,
	SLine *lineXY, float *lineRTheta, float *lineScore, EROINUMBER nFlag)
{

	float* pfMatImage = (float*)matImage.data;


	//get the points with non-zero pixels

	Mat matPoints;
	bool bZeroPoint;

	bZeroPoint = GetNonZeroPoints(matImage, matPoints, true);

	if (!bZeroPoint)
		return;

	if (matPoints.cols != 1)
		if (numSamples > matPoints.cols)
			numSamples = matPoints.cols;
	//subtract half

	matPoints += 0.5;

	//normalize pixels values to get weights of each non-zero point
	//get third row of points containing the pixel values

	Mat matW = matPoints.row(2);

	//normalize it

	Mat matWeights = matW.clone();


	normalize(matWeights, matWeights, 1, 0, CV_L1);

	//get cumulative    sum

	CumSum(matWeights, matWeights);

	//random number generator	
	RNG rngNum(0xffffffff);

	//matrix to hold random sample
	Mat matRandInd = Mat(numSamples, 1, CV_32SC1);
	Mat matSamplePoints = Mat(2, numSamples, CV_32FC1);
	//flag for points currently included in the set
	Mat matPointIn = Mat(1, matPoints.cols, CV_8SC1);

	//returned lines
	float curLineRTheta[2], curLineAbc[3];
	float bestLineRTheta[2] = { -1.f, 0.f }, bestLineAbc[3];
	float bestScore = 0, bestDist = 1e5;
	float dist, score;
	SLine curEndPointLine, bestEndPointLine;
	curEndPointLine.ptStartLine.x = -1.0;
	curEndPointLine.ptStartLine.y = -1.0;
	curEndPointLine.ptEndLine.x = -1.0;
	curEndPointLine.ptEndLine.y = -1.0;
	bestEndPointLine.ptStartLine.x = -1.0;
	bestEndPointLine.ptStartLine.y = -1.0;
	bestEndPointLine.ptEndLine.x = -1.0;
	bestEndPointLine.ptEndLine.y = -1.0;

	//variabels for getting endpoints
	//int mini, maxi;
	float minc = 1e5f, maxc = -1e5f, mind, maxd;
	float x, y, c = 0.;

	Point_<float> ptMin = Point_<float>(-1., -1.);
	Point_<float> ptMax = Point_<float>(-1., -1.);

	int *piMatRandIndData = (int*)matRandInd.data;
	float * pfMatSamplePoins = (float*)matSamplePoints.data;
	char *pcMatPointIn = (char*)matPointIn.data;
	float *pfMatPoints = (float*)matPoints.data;
	//cout<<"Iterations"<<numIterations<<endl;
	//outer loop
	for (int i = 0; i < numIterations; i++)
	{
		//set flag to zero
		matPointIn.zeros(1, matPoints.cols, CV_8SC1);
		//get random sample from the points
		SampleWeighted(matWeights, numSamples, matRandInd, rngNum);


		for (int j = 0; j < numSamples; j++)
		{
			//flag it as included

			pcMatPointIn[matPointIn.cols * 0 + piMatRandIndData[matRandInd.cols*j + 0]] = 1;
			//put point
			pfMatSamplePoins[matSamplePoints.cols * 0 + j] = pfMatPoints[matPoints.cols * 0 + piMatRandIndData[matRandInd.cols*j + 0]];
			pfMatSamplePoins[matSamplePoints.cols * 1 + j] = pfMatPoints[matPoints.cols * 1 + piMatRandIndData[matRandInd.cols*j + 0]];
		}
		//fit the line
		FitRobustLine(matSamplePoints, curLineRTheta, curLineAbc);
		//get end points from points in the samplePoints
		minc = 1e5; mind = 1e5; maxc = -1e5; maxd = -1e5;
		for (int j = 0; getEndPoints && j < numSamples; ++j)
		{
			//get x & y

			x = pfMatSamplePoins[matSamplePoints.cols * 0 + j];  //CV_MAT_ELEM(*samplePoints, float, 0, j);//
			y = pfMatSamplePoins[matSamplePoints.cols * 1 + j];//////////////////////////////////////////////////////////////////////////???

			//get the coordinate to work on
			if (lineType == LINE_HORIZONTAL)
				c = x;
			else if (lineType == LINE_VERTICAL)
				c = y;
			//compare
			if (c > maxc)
			{
				maxc = c;

				ptMax = Point_<float>(x, y);		//////////////////////////////////////////////////////////////////////////
			}
			if (c < minc)
			{
				minc = c;

				ptMin = Point_<float>(x, y);		//////////////////////////////////////////////////////////////////////////
			}
		} //for

		//loop on other points and compute distance to the line
		score = 0;
		for (int j = 0; j < matPoints.cols; j++)//
		{
			// 	    //if not already inside

			dist = fabs(pfMatPoints[matPoints.cols * 0 + j] * curLineAbc[0] + pfMatPoints[matPoints.cols * 1 + j] * curLineAbc[1] + curLineAbc[2]);//
			//check distance
			if (dist <= threshold)
			{
				//add this point

				pcMatPointIn[matPointIn.cols * 0 + j] = 1;//
				//update score

				score += pfMatImage[matImage.cols*(int)(pfMatPoints[matPoints.cols * 1 + j] - 0.5) + (int)(pfMatPoints[matPoints.cols * 0 + j] - 0.5)];//
			}
			// 	    }
		}

		//check the number of close points and whether to consider this a good fit
		//int numClose = cvCountNonZero(pointIn);
		int numClose = countNonZero(matPointIn);
		//cout << "numClose=" << numClose << "\n";
		if (numClose >= numGoodFit)
		{
			//get the points included to fit this line	
			Mat matFitPoints = Mat(2, numClose, CV_32FC1);//
			float* pfMatFitPoints = (float*)matFitPoints.data;
			int k = 0;
			//loop on points and copy points included
			for (int j = 0; j < matPoints.cols; j++)//
			{


				if (pcMatPointIn[matPointIn.cols * 0 + j])
				{

					pfMatFitPoints[matFitPoints.cols * 0 + k] = pfMatPoints[matPoints.cols * 0 + j];
					pfMatFitPoints[matFitPoints.cols * 1 + k] = pfMatPoints[matPoints.cols * 1 + j];
					k++;
				}
			}
			//fit the line
			FitRobustLine(matSamplePoints, curLineRTheta, curLineAbc);

			//compute distances to new line
			dist = 0.;
			//compute distances to new line
			for (int j = 0; j < matFitPoints.cols; j++)
				//for (int j=0; j<fitPoints->cols; j++)
			{////	
				x = pfMatFitPoints[matFitPoints.cols * 0 + j];//
				y = pfMatFitPoints[matFitPoints.cols * 1 + j];//

				float d = fabs(x * curLineAbc[0] + y * curLineAbc[1] + curLineAbc[2])
					* pfMatImage[matImage.cols*(int)(y - 0.5) + (int)(x - 0.5)];//

				dist += d;
				////
			}

			//now check if we are getting the end points
			if (getEndPoints)
			{

				//get distances


				mind = ptMin.x * curLineAbc[0] + ptMin.y * curLineAbc[1] + curLineAbc[2];  //
				maxd = ptMax.x * curLineAbc[0] + ptMax.y * curLineAbc[1] + curLineAbc[2];//
				//we have the index of min and max points, and
				//their distance, so just get them and compute
				//the end points

				//////////////////////////////////////////////////////////////////////////
				curEndPointLine.ptStartLine.x = ptMin.x - mind * curLineAbc[0];//
				curEndPointLine.ptStartLine.y = ptMin.y - mind * curLineAbc[1];//

				curEndPointLine.ptEndLine.x = ptMax.x - maxd * curLineAbc[0];//
				curEndPointLine.ptEndLine.y = ptMax.y - maxd * curLineAbc[1];//


			}

			//dist /= score;

			//clear fitPoints

			//check if to keep the line as best
			if (score >= scoreThreshold && score > bestScore)//dist<bestDist //(numClose > bestScore)
			{
				//update max
				bestScore = score; //numClose;
				bestDist = dist;
				//copy
				bestLineRTheta[0] = curLineRTheta[0];
				bestLineRTheta[1] = curLineRTheta[1];
				bestLineAbc[0] = curLineAbc[0];
				bestLineAbc[1] = curLineAbc[1];
				bestLineAbc[2] = curLineAbc[2];
				bestEndPointLine = curEndPointLine;
			}
		}
	} // for i

	//return
	if (lineRTheta)
	{
		lineRTheta[0] = bestLineRTheta[0];
		lineRTheta[1] = bestLineRTheta[1];
	}
	if (lineXY)
	{
		if (getEndPoints)
			*lineXY = bestEndPointLine;
		else
		{
			IntersectLineRThetaWithBB(lineRTheta[0], lineRTheta[1], Size(matImage.cols - 1, matImage.rows - 1), lineXY);
		}
	}
	if (lineScore)
		*lineScore = bestScore;

	//clear

}
bool CMultiROILaneDetection::GetNonZeroPoints(const Mat& matInMat, Mat& matOutMat, bool floatMat)
{

	int k = 0;
	//get number of non-zero points
	//int numnz = cvCountNonZero(inMat);
	int numnz = countNonZero(matInMat);
	//allocate the point array and get the points
	if (numnz)
	{
		if (floatMat)
		{
			matOutMat = Mat(3, numnz, CV_32FC1);
		}
		else
		{
			matOutMat = Mat(3, numnz, CV_32SC1);//=Mat(3,numnz,CV_32FC1);
		}
	}
	else
		return false;


	float* pfMatInMat = (float*)matInMat.data;
	float* pfMatOutMat = (float*)matOutMat.data;
	/*loop and allocate the points*/
	for (int i = 0; i < matInMat.rows; i++)
		for (int j = 0; j < matInMat.cols; j++)
			if (pfMatInMat[i*matInMat.cols + j])
			{
				pfMatOutMat[0 * matOutMat.cols + k] = (float)j;
				pfMatOutMat[1 * matOutMat.cols + k] = (float)i;
				pfMatOutMat[2 * matOutMat.cols + k] = (float)pfMatInMat[i*matInMat.cols + j];

				k++;
			}

	//return
	return true;
}

void CMultiROILaneDetection::CumSum(const Mat &inMat, Mat &outMat)
{


	float* pfInMatData = (float*)inMat.data;
	float* pfOutMatData = (float*)outMat.data;

	if (inMat.rows == 1)
		for (int i = 1; i < outMat.cols; i++)
			pfOutMatData[0 * outMat.cols + i] += pfOutMatData[0 * outMat.cols + i - 1];
	else
		for (int i = 1; i < outMat.rows; i++)
			pfOutMatData[i*outMat.cols + 0] += pfOutMatData[(i - 1)*outMat.cols + 0];

}

void CMultiROILaneDetection::SampleWeighted(const Mat &cumSum, int numSamples, Mat &randInd, RNG &rng)
{
	//     //get cumulative sum of the weights
	//     //OPTIMIZE:should pass it later instead of recomputing it

	//check if numSamples is equal or more
	int i = 0;
	int* piRandInd = (int*)randInd.data;
	float* pfCumSumData = (float*)cumSum.data;

	if (numSamples >= cumSum.cols)
	{
		for (; i < numSamples; i++)
		{
			piRandInd[randInd.cols*i + 0] = i;
		}

	}
	else
	{
		//loop
		while (i < numSamples)
		{
			//get random number

			double r = rng.uniform(0., 1.);
			//get the index from cumSum
			int j;

			for (j = 0; j<cumSum.cols && r>pfCumSumData[cumSum.cols * 0 + j]; j++);

			//make sure this index wasnt chosen before
			bool put = true;
			for (int k = 0; k < i; k++)
			{

				if (piRandInd[randInd.cols*k + 0] == j)
					//put it
					put = false;
			}

			if (put)
			{
				//put it in array
				piRandInd[randInd.cols*i + 0] = j;
				//inc
				i++;
			}
		} //while
	} //if
}


void CMultiROILaneDetection::FitRobustLine(const Mat &matPoints, float *lineRTheta, float *lineAbc)
{

	//clone the points

	Mat matClPoints = matPoints.clone();

	//get mean of the points and subtract from the original points
	float meanX = 0, meanY = 0;

	Scalar SMean;

	Mat matRow1, matRow2;
	//get first row, compute avg and store

	matRow1 = matClPoints.row(0);

	SMean = mean(matRow1);

	meanX = (float)SMean.val[0];

	subtract(matRow1, SMean, matRow1);

	//same for second row

	matRow2 = matClPoints.row(1);

	SMean = mean(matRow2);
	meanY = (float)SMean.val[0];

	subtract(matRow2, SMean, matRow2);

	//compute the SVD for the centered points array

	Mat matW = Mat(2, 1, CV_32FC1);
	Mat matV = Mat(2, 2, CV_32FC1);

	Mat matCPointst = Mat(matClPoints.cols, matClPoints.rows, CV_32FC1);


	transpose(matClPoints, matCPointst);

	m_SvdCalc(matCPointst, SVD::FULL_UV);
	matV = m_SvdCalc.vt;


	transpose(matV, matV);

	//get the [a,b] which is the second column corresponding to
	//smaller singular value
	float a, b, c;
	float* pfMatV = (float*)matV.data;
	//////////////////////////????????????????????

	float tempa = pfMatV[matV.cols * 0 + 1];
	float tempb = pfMatV[matV.cols * 1 + 1];
	//


	float tempc = -meanX*tempa - meanY*tempb;

	//printf("original\n %f	%f	%f\n",a,b,c);
	//	printf("mat\n %f	%f	%f\n\n",tempa,tempb,tempc);

	//	cvWaitKey(0);
	a = tempa;
	b = tempb;
	c = tempc;


	//compute r and theta

	float r, theta;
	theta = atan2(b, a);
	r = meanX * cos(theta) + meanY * sin(theta);
	//correct
	if (r < 0)
	{
		//correct r
		r = -r;
		//correct theta
		theta += CV_PI;
		if (theta > CV_PI)
			theta -= 2 * CV_PI;
	}
	//return
	if (lineRTheta)
	{
		lineRTheta[0] = r;
		lineRTheta[1] = theta;
	}
	if (lineAbc)
	{
		lineAbc[0] = a;
		lineAbc[1] = b;
		lineAbc[2] = c;
	}

}

void CMultiROILaneDetection::GetCameraPose(EROINUMBER nFlag, Vector<Mat> &vecMat){
	int nFrameSize = vecMat.size();
	m_imgIPM[nFlag].create(m_sRoiInfo[nFlag].sizeIPM, CV_32FC1);
	Mat imgSum = Mat::zeros(vecMat[0].size(), CV_32FC1);
	for (int i = 0; i < nFrameSize; i++)
	{
		//vecMat[i].convertTo(vecMat[i],CV_64FC1);
		//imgSum = imgSum*(i+1);
		imgSum += vecMat[i];
		//imgSum /=(i+2);
	}
	imgSum /= nFrameSize;
	imshow("filter sum", imgSum);
	ShowImageNormalize("filter sum norm", imgSum);
	Mat rowMat;
	rowMat = Mat(imgSum).reshape(0, 1);
	//get the quantile
	float fQval;
	fQval = quantile((float*)&rowMat.data[0], rowMat.cols, m_sConfig.fLowerQuantile);
	Mat imgSumThres;
	//threshold(imgSum, imgSumThres, fQval, NULL, THRESH_TOZERO);
	threshold(imgSum, imgSumThres, fQval, 255, THRESH_TOZERO);
	ShowImageNormalize("filter sum thres norm", imgSumThres);
	waitKey(0);
	m_ipmFiltered[nFlag] = imgSum;
	m_filteredThreshold[nFlag] = imgSumThres;
	GetLinesIPM(nFlag);
	LineFitting(nFlag);
	cout << "lane size" << m_lanes[nFlag].size() << endl;
	IPM2ImLines(nFlag);

}
void CMultiROILaneDetection::ClearResultVector(EROINUMBER nFlag){
	m_lanes[nFlag].clear();
	m_laneScore[nFlag].clear();
	m_lanesResult[nFlag].clear();
	m_lanesGroundResult[nFlag].clear();
}
Point CMultiROILaneDetection::TransformPointImage2Ground(Point ptIn){
	Point ptResult;
	float fArrPt[] = { ptIn.x, ptIn.y };
	Mat matUvPt(2, 1, CV_32FC1, fArrPt);
	Mat matXyPt(2, 1, CV_32FC1);
	TransformImage2Ground(matUvPt, matXyPt);
	ptResult.x = matXyPt.at<float>(0, 0);
	ptResult.y = matXyPt.at<float>(1, 0);
	return ptResult;
}
Point CMultiROILaneDetection::TransformPointGround2Image(Point ptIn){

	Mat matMat = Mat(2, 1, CV_32FC1);
	float* pfMat = (float*)matMat.data;
	pfMat[0] = ptIn.x;	pfMat[1] = ptIn.y;
	TransformGround2Image(matMat, matMat);
	return Point(pfMat[0], pfMat[1]);
}

/**
@brief 실제로 KalmanTracking추정하는 함수.
@param trackingFlag
@return void
@warning
@author 이영완.
@date 2015.11.09.
*/
void CMultiROILaneDetection::KalmanTrackingStage(int nTrackingFlag){

	//연속으로 detected 되지 않아서 continue()에서 cntErase==0이 되는 경우
	if (m_bTrackingFlag[nTrackingFlag] == false){
		return;
	}
	//error시 이부분 확인 여기까지

	if (m_SKalmanLane[nTrackingFlag].cntNum == 0){ //[LYW_1215] : 트래킹 처음일 때 Kalman setting만하고 끝내??
		m_SKalmanLane[nTrackingFlag].SKalmanTrackingLineBefore = m_SKalmanLane[nTrackingFlag].SKalmanTrackingLine; //[LYW_0917] : moving average결과를 꽂아주네
		KalmanSetting(m_SKalmanLane[nTrackingFlag]);
	}
	else //tracked!!
	{
		//prediction은 이전 frame의 posterior estimate( statePost ) 가지고 predict()수행(prior estiamte )
		//predict()결과에 현재 frame의 measurement 이용해서 검사하기 : correct()
		Mat matPrediction = m_SKalmanLane[nTrackingFlag].KF.predict(); // [LYW_2051215] : predict()한 결과는 correct()(Posterior Estimate)에 쓰임.
		SLine SLinePredict;
		SLinePredict.fXcenter = matPrediction.at<float>(0);
		SLinePredict.fXderiv = matPrediction.at<float>(1);

		// [LYW_151216] : measurement값에 moving average결과값을 넣기
		m_SKalmanLane[nTrackingFlag].matMeasurement.at<float>(0) = m_SKalmanLane[nTrackingFlag].SKalmanTrackingLine.fXcenter;
		m_SKalmanLane[nTrackingFlag].matMeasurement.at<float>(1) = m_SKalmanLane[nTrackingFlag].SKalmanTrackingLine.fXderiv;


		// [LYW_151215] : 최종적으로 추정된 line의 state
		Mat matEstimated = m_SKalmanLane[nTrackingFlag].KF.correct(m_SKalmanLane[nTrackingFlag].matMeasurement);
		SLine SLineEstimated;
		SLineEstimated.fXcenter = matEstimated.at<float>(0);
		SLineEstimated.fXderiv = matEstimated.at<float>(1);


		m_sTrackingLane[nTrackingFlag].ptStartLane.x = (m_sTrackingLane[nTrackingFlag].ptStartLane.y - m_sTrackingLane[nTrackingFlag].ptEndLane.y) / 2
			* SLineEstimated.fXderiv + SLineEstimated.fXcenter;
		m_sTrackingLane[nTrackingFlag].ptEndLane.x = (-m_sTrackingLane[nTrackingFlag].ptStartLane.y + m_sTrackingLane[nTrackingFlag].ptEndLane.y) / 2
			* SLineEstimated.fXderiv + SLineEstimated.fXcenter;
		Point ptUvSt = TransformPointGround2Image(m_sTrackingLane[nTrackingFlag].ptStartLane);
		Point ptUvEnd = TransformPointGround2Image(m_sTrackingLane[nTrackingFlag].ptEndLane);
		m_sTrackingLane[nTrackingFlag].ptUvStartLine = ptUvSt;
		m_sTrackingLane[nTrackingFlag].ptUvEndLine = ptUvEnd;
		m_sTrackingLane[nTrackingFlag].fXcenter = SLineEstimated.fXcenter;
		m_sTrackingLane[nTrackingFlag].fXderiv = SLineEstimated.fXderiv;

		//line(m_imgResizeOrigin, ptUvSt, ptUvEnd, Scalar(0, 0, 255), 2);
		//m_bLeftDraw = true;
		m_bDraw[nTrackingFlag] = true;
		m_bStartTrackingFlag = true;
		//tracking continue 함수에서 사용
		//m_SKalmanLane[nTrackingFlag].SKalmanTrackingLineBefore = m_SKalmanLane[nTrackingFlag].SKalmanTrackingLine; //Q[LYW_151216] :이걸 굳이 왜 넣지? 넣을꺼면 새로 추정된 m_sTrackingLane[nTrackingFlag].ptStartLane 이게 더 낫지 않나?
	}
	m_SKalmanLane[nTrackingFlag].cntNum++;

}

/**
@brief KalmanTrackingStage에서 호출되는 함수. Kalman Filter추정에 필요한 파라미터 셋팅.
@brief X_k = [XCenter Xderiv velocity_XC velocity_XD ]^T : 4 by 1 state vector
@brief Z_k = [XCenter XDeriv 0 0 ]^T : 4 by 1 measure vector
@param Kalman struct객체.
@return void
@warning
@author 이영완
@date 2015.12.15
*/
void CMultiROILaneDetection::KalmanSetting(SKalman &SKalmanInput){
	//[LYW_151215] : Kalman Filter Tracking 수정 시작
	/// state transion Matrix : A (stateSize * stateSize  )
	// Note: set dT at each processing step! but dT를 0으로 하면 더 잘되는지 반드시 체크

	//X_k = [XCenter Xderiv velocity_XC velocity_XD ]^T : 4 by 1 state vector

	// [ 1 0 dT 0 ]
	// [ 0 1 0  dT]
	// [ 0 0 1  0 ]
	// [ 0 0 0  1 ]

	SKalmanInput.KF.transitionMatrix = *(Mat_<float>(4, 4) <<
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1);


	/*measurement Matrix : H ( measureSize * stateSize )*/
	/// measurement Matrix : H ( measureSize * stateSize )

	// Z_k = [XCenter XDeriv 0 0 ]^T : 4 by 1 measure vector
	// measurement Matrix : H

	// [ 1 0 0 0 ]
	// [ 0 1 0 0 ]
	// [ 0 0 0 0 ]
	// [ 0 0 0 0 ]

	SKalmanInput.KF.measurementMatrix = *(Mat_<float>(2, 4) <<
		1, 0, 0, 0,
		0, 1, 0, 0);


	//initialize state : x0
	// 검출되지 않았을 경우 이전의 추정값을 측정값으로 넣어주면 될텐데??
	//SKalmanInput.matMeasurement.setTo(Scalar(0));
	//SKalmanInput.KF.statePre.at<float>(0) = SKalmanInput.SKalmanTrackingLineBefore.fXcenter;
	//SKalmanInput.KF.statePre.at<float>(1) = SKalmanInput.SKalmanTrackingLineBefore.fXderiv;
	//SKalmanInput.KF.statePre.at<float>(2) = 0;
	//SKalmanInput.KF.statePre.at<float>(3) = 0;

	SKalmanInput.matMeasurement.setTo(Scalar(0));
	SKalmanInput.KF.statePost.at<float>(0) = SKalmanInput.SKalmanTrackingLineBefore.fXcenter;
	SKalmanInput.KF.statePost.at<float>(1) = SKalmanInput.SKalmanTrackingLineBefore.fXderiv;
	SKalmanInput.KF.statePost.at<float>(2) = 0;
	SKalmanInput.KF.statePost.at<float>(3) = 0;


	// Q increase --> Kalman gain increase --> measure value weight increase
	// R increase --> Kalman gain decrease --> estimate value weight increase
	setIdentity(SKalmanInput.KF.processNoiseCov, Scalar::all(1e-2));
	setIdentity(SKalmanInput.KF.measurementNoiseCov, Scalar::all(1e-1));
	setIdentity(SKalmanInput.KF.errorCovPost, Scalar::all(0.1));
}


/**
@brief Kalman Tracking을 위한 셋팅단계. tracking flag에 맞게 검출된 차선을 누적시키며, Moving Average진행.
@param ROIflag, tracking flag
@return void
@warning
@author 이영완
@date 2015.11.09.
*/
void CMultiROILaneDetection::TrackingStageGround(EROINUMBER nflag, int nTrackingFlag){

	// 트래킹모듈 시작하기 전에 각각의 ROI에서 찾은 차선의 Ground값, moving average값을 계산
	// m_lanesGroundResult --> m_GroundTracking
	SLine SLineTemp;
	if (!m_lanesGroundResult[nflag].empty())
	{
		m_vecTrackingFlag[nTrackingFlag].push_back(nflag);	// nflag에 결과가 있을 경우 tracking flag on 및 ROInumber 저장

		//[LYW_0829] : 어떤 ROI가 어떤 tracking을 할건지 정해줌
		m_sTracking[nflag].nTargetTracker = nTrackingFlag;   // 20150804 candidate 함수 개량을 위한 추가
		m_sTracking[nflag].bTracking = true; // LineFitting에서 score계산할 때 검출된 차선 거리 검사에 쓰임

		SLineTemp = m_lanesGroundResult[nflag][0]; // 
		SLineTemp.fGroundHeight = m_lanesGroundResult[nflag][0].ptStartLine.y
			- m_lanesGroundResult[nflag][0].ptEndLine.y;
		SLineTemp.fXcenter = (m_lanesGroundResult[nflag][0].ptStartLine.x + m_lanesGroundResult[nflag][0].ptEndLine.x) / 2;
		SLineTemp.fXderiv = (m_lanesGroundResult[nflag][0].ptStartLine.x - m_lanesGroundResult[nflag][0].ptEndLine.x)
			/ SLineTemp.fGroundHeight;// #Q : [LYW_0829] : 기울기가 dx/dy 되있음... 왜??? --> X의변화량을 표현하기위함.


		//#Q : [LYW_0829] : 화면에 CENTER_ROI의 탑-바텀으로 이어지게끔 그려주기 위해!!!
		//[LYW_0916] : 각각의 ROI에서 검출된 line들을 탑-바텁으로 늘려준 후 push_back해준다. 같은 nTrackingFlag을 가진 애들은 같은 저장소로 누적된다.
		//그래서 결국 같은 라인으로 트래킹하게 된다. 
		//결국 늘려준 라인으로 화면에 그려주게된다.
		// Tracking module을 독립적으로 하고 싶으면 그냥 flag숫자만 바꿔주면 되!!!
		// [LYW_0907] : ground값으로 트래킹하려고!!
		SLineTemp.ptStartLine.x = (SLineTemp.ptStartLine.y - m_sCameraInfo.fGroundTop)*SLineTemp.fXderiv
			+ SLineTemp.ptStartLine.x;
		SLineTemp.ptStartLine.y = m_sCameraInfo.fGroundTop; // ROI 맨 위

		SLineTemp.ptEndLine.x = (SLineTemp.ptEndLine.y - m_sCameraInfo.fGroundBottom)*SLineTemp.fXderiv
			+ SLineTemp.ptEndLine.x;
		SLineTemp.ptEndLine.y = m_sCameraInfo.fGroundBottom; // ROI 맨 아래

		m_GroundTracking[nTrackingFlag].push_back(SLineTemp); //[LYW_0829] : LEFT_ROI2,3이 같은 nTrackingFlag을 하니깐 같이 넣겠네


		//[LYW_151109] : Moving Average를 통해 최신값 업데이트 및 노이즈 제거
		//#Q : [LYW_0829] : 이 작업은 왜 하는걸까? --> A : 누적시키면서 맨 앞에 있는 놈 제거해서 업데이트해야지
		if (m_GroundTracking[nTrackingFlag].size() > MOVING_AVERAGE_NUM){
			m_iterGroundTracking[nTrackingFlag] = m_GroundTracking[nTrackingFlag].begin();
			m_GroundTracking[nTrackingFlag].erase(m_iterGroundTracking[nTrackingFlag]);
		}

	}//end of if

}

void CMultiROILaneDetection::ClearDetectionResult(int nTrackingFlag){
	m_bDraw[nTrackingFlag] = false;

	nCnt[nTrackingFlag] = 0;
	m_Tracking[nTrackingFlag].clear();
	m_GroundTracking[nTrackingFlag].clear();
	m_bTrackingFlag[nTrackingFlag] = false;
	m_SKalmanLane[nTrackingFlag].cntNum = 0;

	m_sTrackingLane[nTrackingFlag].ptUvStartLine.x = EMPTY;
	m_sTrackingLane[nTrackingFlag].ptUvStartLine.y = EMPTY;
	m_sTrackingLane[nTrackingFlag].ptUvEndLine.x = EMPTY;
	m_sTrackingLane[nTrackingFlag].ptUvEndLine.y = EMPTY;
	m_sTrackingLane[nTrackingFlag].ptStartLane.x = EMPTY;
	m_sTrackingLane[nTrackingFlag].ptStartLane.y = EMPTY;
	m_sTrackingLane[nTrackingFlag].ptEndLane.x = EMPTY;
	m_sTrackingLane[nTrackingFlag].ptEndLane.y = EMPTY;

}

/**
@brief 계속해서 트래킹을 할지 말지를 결정. DeterminTracking에서 호출됨.
@param trackingFlag
@return void
@warning
@author 이영완.
@date 2015.11.09.
*/
void CMultiROILaneDetection::TrackingContinue(int nTrackingFlag){
	if (m_vecTrackingFlag[nTrackingFlag].empty()){ // [LYW_0907]lane이 검출되지 않았을 때!!

		nCnt[nTrackingFlag] += TRACKING_ERASE_LEVEL;
		m_SKalmanLane[nTrackingFlag].cntErase += TRACKING_ERASE_LEVEL; //#Q:[LYW_0829] : nCnt와 m_SKalmanLane.cntErase 차이는?


		//[LYW_160324] : 옆차선ROI는 하나 밖에 없어서 쉽게 탈락되는 걸 방지하기 위함
		int nThErase;
		if (nTrackingFlag == 0 || nTrackingFlag == 1)
			nThErase = TRACKINGERASE;
		else
			nThErase = 20;

		if (m_SKalmanLane[nTrackingFlag].cntErase >= nThErase){ //[LYW_0922] : TRACKINGERASE보다 작으면 검출되지 않아도 그래도 화면에 그려진다.
			m_SKalmanLane[nTrackingFlag].cntErase = 0;
			nCnt[nTrackingFlag] = 0;
			m_Tracking[nTrackingFlag].clear();
			m_GroundTracking[nTrackingFlag].clear(); //[LYW_0921] : 추가했더니 오검출 문제 해결 --> Kalman Tracking Stage에 들어가자마자 return
			m_SKalmanLane[nTrackingFlag].cntNum = 0; //[LYW_0921] : 
			m_bTrackingFlag[nTrackingFlag] = false; //[LYW_0921] : KalmanTrackingStage에서 바로 return 됨
			m_bDraw[nTrackingFlag] = false; //[LYW_1229] : 
			ClearDetectionResult(nTrackingFlag); //[LYW_160330]
			m_sTrackingLane[nTrackingFlag].ptUvStartLine.x = EMPTY;
			m_sTrackingLane[nTrackingFlag].ptUvStartLine.y = EMPTY;
			m_sTrackingLane[nTrackingFlag].ptUvEndLine.x = EMPTY;
			m_sTrackingLane[nTrackingFlag].ptUvEndLine.y = EMPTY;
		}
		//else // [LYW_160201] : 이렇게 할 경우 한양 CES데모에서는 잘 되는데 우리 SANE에서는 깜빡거리는 문제 발생. 일단 주석처리
		//{ // [LYW_1229] : 차선이 들어오지 않았을 경우, 이전 추정의 결과를 measurement로 넣어준다.
		//	if (m_bTrackingFlag[nTrackingFlag] = true){ // 차선이 검출되지 않았지만 이전에 추정되고 있을 경우
		//		SLine laneTemp;
		//		laneTemp.fXcenter = m_sTrackingLane[nTrackingFlag].fXcenter;
		//		laneTemp.fXderiv = m_sTrackingLane[nTrackingFlag].fXderiv;
		//		m_SKalmanLane[nTrackingFlag].SKalmanTrackingLineBefore = laneTemp;
		//		KalmanSetting(m_SKalmanLane[nTrackingFlag]);
		//	}
		//}

		return;
	}
	else ////[LYW_0916] : 검출되면 cntErase감소시켜
	{
		//if (nCnt[nTrackingFlag] >= 0){
		//if ((nCnt[nTrackingFlag] -= TRACKING_ERASE_LEVEL) == 0)
		//nCnt[nTrackingFlag] = 0;
		//else
		//nCnt[nTrackingFlag] -= TRACKING_ERASE_LEVEL;
		//}
		//else
		nCnt[nTrackingFlag] = 0;

		if (m_SKalmanLane[nTrackingFlag].cntErase >= 0){
			if ((m_SKalmanLane[nTrackingFlag].cntErase -= TRACKING_ERASE_LEVEL) == 0) //[LYW_0916] : 0이하로 내려가는걸 방지
				m_SKalmanLane[nTrackingFlag].cntErase = 0;
			else
				m_SKalmanLane[nTrackingFlag].cntErase -= TRACKING_ERASE_LEVEL;
		}
		else
			m_SKalmanLane[nTrackingFlag].cntErase = 0;//[LYW_0916] : 0이하로 내려가는걸 방지
	}

	if (m_GroundTracking[nTrackingFlag].size() > TRACKING_FLAG_NUM){ //[LYW_151215] : 첫프레임은 여기로 못들어가... 프레임적정수보다 높아야 트래킹 시작 그럼 트래킹 안 될 경우 어떻게 그려?
		m_bTrackingFlag[nTrackingFlag] = true; // //[LYW_151215] : 이게 되야 TrackingStage로 넘어갈 수 있다.

		//Moving Average Filter
		Point_<double> ptStart = Point_<double>(0, 0);
		Point_<double> ptEnd = Point_<double>(0, 0);
		SLine SGroundLeftLine;
		SGroundLeftLine.fXcenter = 0;
		SGroundLeftLine.fXderiv = 0;
		for (int i = 0; i < m_GroundTracking[nTrackingFlag].size(); i++){ //#Q:[LYW_0829] : 위,아래 ROI에서 검출된 레인들의 평균을 계산???
			ptStart += m_GroundTracking[nTrackingFlag][i].ptStartLine; // //[LYW_0916] : ROI2,3든 비슷한 지점 탑라인에서
			ptEnd += m_GroundTracking[nTrackingFlag][i].ptEndLine;
			SGroundLeftLine.fXcenter += m_GroundTracking[nTrackingFlag][i].fXcenter;
			SGroundLeftLine.fXderiv += m_GroundTracking[nTrackingFlag][i].fXderiv;
		}
		int nSize = m_GroundTracking[nTrackingFlag].size();

		ptStart.x /= nSize;
		ptStart.y /= nSize;
		ptEnd.x /= nSize;
		ptEnd.y /= nSize;

		SGroundLeftLine.fXcenter /= nSize;
		SGroundLeftLine.fXderiv /= nSize;

		m_sTrackingLane[nTrackingFlag].fXcenter = SGroundLeftLine.fXcenter;
		m_sTrackingLane[nTrackingFlag].fXderiv = SGroundLeftLine.fXderiv;
		m_sTrackingLane[nTrackingFlag].fYtop = ptStart.y;
		m_sTrackingLane[nTrackingFlag].fYBottom = ptEnd.y;
		m_sTrackingLane[nTrackingFlag].ptStartLane = ptStart;
		m_sTrackingLane[nTrackingFlag].ptEndLane = ptEnd;

		//[LYW_0829] : groundLines -> ImageLines로 바꿔줘야 그릴 수 있다.	
		Point ptUvSt = TransformPointGround2Image(ptStart);
		Point ptUvEnd = TransformPointGround2Image(ptEnd);
		m_sTrackingLane[nTrackingFlag].ptUvStartLine = ptUvSt;
		m_sTrackingLane[nTrackingFlag].ptUvEndLine = ptUvEnd;

		//m_sTrackingLane으로 
		SLine SLineResult;
		SLineResult.fXcenter = m_sTrackingLane[nTrackingFlag].fXcenter;
		SLineResult.fXderiv = m_sTrackingLane[nTrackingFlag].fXderiv;
		SLineResult.ptStartLine = m_sTrackingLane[nTrackingFlag].ptStartLane;
		SLineResult.ptEndLine = m_sTrackingLane[nTrackingFlag].ptEndLane;

		//ground값
		m_SKalmanLane[nTrackingFlag].SKalmanTrackingLine = SLineResult;//평균내서 m_SKalmanLane에 넣어줌 --> 이것이 measurement

	}
	m_vecTrackingFlag[nTrackingFlag].clear();
}




// my function
void SetFrameName(char* szDataName, char* szDataDir, int nFrameNum){
	strcpy(szDataName, szDataDir);
	char szNumPng[10];
	sprintf(szNumPng, "%05d.png", nFrameNum);
	strcat(szDataName, szNumPng);
}
void SetFrameNameBMP(char* szDataName, char* szDataDir, int nFrameNum){
	strcpy(szDataName, szDataDir);
	char szNumPng[10];
	sprintf(szNumPng, "%05d.BMP", nFrameNum);
	strcat(szDataName, szNumPng);
}

void ShowImageNormalize(const char str[], const Mat &pmat){
	Mat mat = pmat.clone();
	ScaleMat(mat, mat);
	namedWindow(str);
	imshow(str, mat);
}

void ScaleMat(const Mat &inMat, Mat &outMat){
	inMat.convertTo(outMat, inMat.type());
	double min;
	minMaxLoc(inMat, &min);
	subtract(inMat, min, outMat);
	double max;
	minMaxLoc(outMat, NULL, &max);
	convertScaleAbs(outMat, outMat, 255.0 / max);
}
void ShowResults(CMultiROILaneDetection &obj, EROINUMBER nflag){
	int nNum = nflag;
	//Draw lane IPM
	/*for(unsigned int i = 0; i < obj.m_lanes[nflag].size(); i++)
	line(obj.m_imgIPM[nflag],
	Point(obj.m_lanes[nflag][i].ptStartLine.x,obj.m_lanes[nflag][i].ptStartLine.y),
	Point(obj.m_lanes[nflag][i].ptEndLine.x,obj.m_lanes[nflag][i].ptEndLine.y),
	Scalar(0,0,255),2);*/

	rectangle(obj.m_imgResizeOrigin, obj.m_sRoiInfo[nflag].ptRoi, obj.m_sRoiInfo[nflag].ptRoiEnd, Scalar(255, 0, 0), 2);

	if (nflag == LEFT_ROI0 || nflag == RIGHT_ROI0)
		rectangle(obj.m_imgResizeOrigin, obj.m_sRoiInfo[nflag].ptRoi, obj.m_sRoiInfo[nflag].ptRoiEnd, Scalar(220, 0, 220), 2);






	//Draw lane Orignin //[LYW_0815] : 트래킹 결과를 나중에 그릴려고 이 부분은 일단 주석처리
	/*for(unsigned int i=0; i< obj.m_lanesResult[nflag].size();i++)
	line(obj.m_imgResizeOrigin,
	Point((int)obj.m_lanesResult[nflag][i].ptStartLine.x,(int)obj.m_lanesResult[nflag][i].ptStartLine.y),
	Point((int)obj.m_lanesResult[nflag][i].ptEndLine.x,(int)obj.m_lanesResult[nflag][i].ptEndLine.y),
	Scalar(0,0,255),2);*/

	//if (nflag == LEFT_ROI0) //[LYW_0815]:LEFT_ROI0 --> 틀린 곳 : m_lanesReuslt는 ground다! --> 영상이미지좌표로 바꿔줘야되!!
	//{
	//	printf("in LEFT_ROI0|| numLanes : %d in ShowResults\n", obj.m_lanesResult[nflag].size());
	//	for (unsigned int i = 0; i < obj.m_lanesResult[nflag].size(); i++){
	//		Point ptUvSt = obj.TransformPointGround2Image(Point((int)obj.m_lanesResult[nflag][i].ptStartLine.x, (int)obj.m_lanesResult[nflag][i].ptStartLine.y));
	//		Point ptUvEnd = obj.TransformPointGround2Image(Point((int)obj.m_lanesResult[nflag][i].ptEndLine.x, (int)obj.m_lanesResult[nflag][i].ptEndLine.y));
	//		line(obj.m_imgResizeOrigin,
	//			ptUvSt,
	//			ptUvEnd,
	//			Scalar(0, 0, 255), 2);
	//		obj.m_lanesResult[nflag].clear();
	//	}
	//}

	char strImg[20];
	//sprintf(strImgIpm,)
	sprintf(strImg, "IPM%d", nNum);
	imshow(strImg, obj.m_imgIPM[nflag]);
	sprintf(strImg, "Filt%d", nNum);
	imshow(strImg, obj.m_ipmFiltered[nflag]); //#Q : [LYW_0825] : RANSAC의 결과를 그려줘야되는거아냐?? m_ipmFiltered 이것을 왜 또 그려? A : 그 다음꺼는 normalized
	sprintf(strImg, "FN%d", nNum);
	ShowImageNormalize(strImg, obj.m_ipmFiltered[nflag]);
	sprintf(strImg, "FT%d", nNum);
	ShowImageNormalize(strImg, obj.m_filteredThreshold[nflag]);



	////[LYW_0907] : RANSAC결과 그려주기
	//Mat matMat = Mat(2, 2 * m_lanesResult[nFlag].size(), CV_32FC1);
	//Lines2Mat(m_lanesResult[nFlag], matMat);
	//TransformGround2Image(matMat, matMat);
	//

}



vector<Point> LineDivNum(Point ptTop, Point ptBottom, int num){
	vector<Point> vecResult;
	//num--;
	int nWidth = ptTop.x - ptBottom.x;
	int nWidthDiff = nWidth / (num - 1);
	int nHeight = ptTop.y - ptBottom.y;
	int nHeightDiff = nHeight / (num - 1);
	for (int i = 0; i < num; i++){
		Point ptTemp;
		//cout << nWidthDiff << endl;
		ptTemp.x = ptTop.x - nWidthDiff*i;
		ptTemp.y = ptTop.y - nHeightDiff*i;
		vecResult.push_back(ptTemp);
	}
	return vecResult;

}




void LoadExtractedPoint(FILE* fp, Point_<double>& ptUpLeft, Point_<double>& ptMidLeft, Point_<double>& ptUpRight, Point_<double>& ptMidRight){
	char szTemp[10];
	int nScFrameNum;
	fscanf(fp, "%s", &szTemp);//#
	fscanf(fp, "%d", &nScFrameNum);//#
	fscanf(fp, "%lf", &ptUpLeft.x); fscanf(fp, "%lf", &ptUpLeft.y);  fscanf(fp, "%lf", &ptMidLeft.x);  fscanf(fp, "%lf", &ptMidLeft.y); //#
	fscanf(fp, "%lf", &ptUpRight.x); fscanf(fp, "%lf", &ptUpRight.y); fscanf(fp, "%lf", &ptMidRight.x); fscanf(fp, "%lf", &ptMidRight.y);//#

}

void LoadExtractedPoint_multi(FILE* fp, SWorldLane& GroundLeft1, SWorldLane& GroundLeft2, SWorldLane& GroundRight1, SWorldLane& GroundRight2){

	char szTemp[10];
	int nScFrameNum;

	fscanf(fp, "%s", &szTemp);//#
	fscanf(fp, "%d", &nScFrameNum);//#

	fscanf(fp, "%lf", &GroundLeft1.ptUvStartLine.x); fscanf(fp, "%lf", &GroundLeft1.ptUvStartLine.y);  fscanf(fp, "%lf", &GroundLeft1.ptUvEndLine.x);  fscanf(fp, "%lf", &GroundLeft1.ptUvEndLine.y); //#
	fscanf(fp, "%lf", &GroundLeft2.ptUvStartLine.x); fscanf(fp, "%lf", &GroundLeft2.ptUvStartLine.y);  fscanf(fp, "%lf", &GroundLeft2.ptUvEndLine.x);  fscanf(fp, "%lf", &GroundLeft2.ptUvEndLine.y); //#

	fscanf(fp, "%lf", &GroundRight1.ptUvStartLine.x); fscanf(fp, "%lf", &GroundRight1.ptUvStartLine.y);  fscanf(fp, "%lf", &GroundRight1.ptUvEndLine.x);  fscanf(fp, "%lf", &GroundRight1.ptUvEndLine.y); //#
	fscanf(fp, "%lf", &GroundRight2.ptUvStartLine.x); fscanf(fp, "%lf", &GroundRight2.ptUvStartLine.y);  fscanf(fp, "%lf", &GroundRight2.ptUvEndLine.x);  fscanf(fp, "%lf", &GroundRight2.ptUvEndLine.y); //#


}

float CompareLineDiff(Point2d FixedStart, Point2d FixedEnd, Point2d GroundStart, Point2d GroundEnd){
	float fHeight = abs(FixedStart.y - FixedEnd.y);
	float fDiffStart = abs(FixedStart.x - GroundStart.x);
	float fDiffEnd = abs(FixedEnd.x - GroundEnd.x);
	float fScore = (fDiffStart + fDiffEnd) / fHeight;
	return fScore;
}
void EvaluationFunc(CMultiROILaneDetection &obj, SEvaluation &structEvaluation,
	SWorldLane GroundLeft, SWorldLane GroundRight,
	SWorldLane FindLeft, SWorldLane FindRight,
	Mat& matCanvas, FILE* fp){
	int nTop = GroundLeft.ptUvStartLine.y;
	int nBottom = GroundLeft.ptUvEndLine.y;
	Point2i ptLeftText = Point2i(obj.m_imgResizeOrigin.cols / 4, GroundLeft.ptUvStartLine.y - 80);
	Point2i ptRightText = Point2i(obj.m_imgResizeOrigin.cols / 4, GroundLeft.ptUvStartLine.y - 40);

	Point2d GroundLeftCenter;
	Point2d GroundRightCenter;

	Point2d FindLeftCenter;
	Point2d FindRightCenter;
	double FindLeftAngle;
	double FindRightAngle;
	Point2d FixedLeftStart;
	Point2d FixedLeftEnd;
	Point2d FixedRightStart;
	Point2d FixedRightEnd;

	//GroundLeftCenter.x = (GroundLeft.ptUvStartLine.x + GroundLeft.ptUvEndLine.x) / 2;
	//GroundLeftCenter.y = (GroundLeft.ptUvStartLine.y + GroundLeft.ptUvEndLine.y) / 2;


	//[LYW_160225] : 이과정은 GT와 같은 위치와 같은 길이로 만들기 위함
	FindLeftCenter.x = (FindLeft.ptUvStartLine.x + FindLeft.ptUvEndLine.x) / 2;
	FindLeftCenter.y = (FindLeft.ptUvStartLine.y + FindLeft.ptUvEndLine.y) / 2;
	FindLeftAngle = (FindLeft.ptUvEndLine.x - FindLeft.ptUvStartLine.x)
		/ (FindLeft.ptUvEndLine.y - FindLeft.ptUvStartLine.y);
	FixedLeftStart.x = FindLeftCenter.x + (nTop - FindLeftCenter.y)*FindLeftAngle;
	FixedLeftStart.y = nTop;
	FixedLeftEnd.x = FindLeftCenter.x + (nBottom - FindLeftCenter.y)*FindLeftAngle;
	FixedLeftEnd.y = nBottom;

	FindRightCenter.x = (FindRight.ptUvStartLine.x + FindRight.ptUvEndLine.x) / 2;
	FindRightCenter.y = (FindRight.ptUvStartLine.y + FindRight.ptUvEndLine.y) / 2;
	FindRightAngle = (FindRight.ptUvEndLine.x - FindRight.ptUvStartLine.x)
		/ (FindRight.ptUvEndLine.y - FindRight.ptUvStartLine.y);
	FixedRightStart.x = FindRightCenter.x + (nTop - FindRightCenter.y)*FindRightAngle;
	FixedRightStart.y = nTop;
	FixedRightEnd.x = FindRightCenter.x + (nBottom - FindRightCenter.y)*FindRightAngle;
	FixedRightEnd.y = nBottom;


	char szTruePositive[20] = "TruePositive";
	char szFalsePositive[20] = "FalsePositive";
	char szFalseNegative[20] = "FalseNegative";
	char szTrueNegative[20] = "TrueNegative";

	float fLeftScore;
	float fRightScore;
	float fStandard = COMPARE_STANDARD; //0.2



	if ((GroundLeft.ptUvStartLine.x != EMPTY) && (GroundLeft.ptUvEndLine.x != EMPTY)){ // Ground truth 위,아래 둘다 있을 경우에만
		structEvaluation.nLeftDetectedFrame++;
		//if (FindLeft.ptUvStartLine.x == EMPTY){
		if (obj.m_bDraw[0] == false || obj.m_bDraw[1] == false){
			structEvaluation.LeftFN++;
			cout << "Left FN" << endl;
			putText(obj.m_imgResizeOrigin, szFalseNegative, ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 0, 255), 1.8, 8, false);
			putText(matCanvas, szFalseNegative, ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 0, 255), 1.8, 8, false);
			fprintf(fp, ",,,\n");
			structEvaluation.RightFN++;
			return;

		}
		else{
			fLeftScore = CompareLineDiff(FixedLeftStart, FixedLeftEnd, GroundLeft.ptUvStartLine, GroundLeft.ptUvEndLine);
			cout << "fLeftScore : " << fLeftScore << endl;
			if (fLeftScore < fStandard){
				structEvaluation.LeftTP++;
				cout << "Left TP" << endl;

				stringstream ssTP;
				ssTP << fLeftScore;
				ssTP << " [L] ";
				ssTP << szTruePositive;
				putText(obj.m_imgResizeOrigin, ssTP.str(), ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 255, 0), 1.8, 8, false);
				putText(matCanvas, ssTP.str(), ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 255, 0), 1.8, 8, false);
				//putText(obj.m_imgResizeOrigin, szTruePositive, LeftText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 255, 255), 1, 8, false);
				fprintf(fp, "%f, ", fLeftScore);
			}
			else{
				structEvaluation.LeftFP++;
				cout << "Left FP" << endl;
				stringstream ssFP;
				ssFP << fLeftScore;
				ssFP << " [L] ";
				ssFP << szFalsePositive;

				putText(obj.m_imgResizeOrigin, ssFP.str(), ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(255, 0, 0), 1.8, 8, false);
				putText(matCanvas, ssFP.str(), ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(255, 0, 0), 1.8, 8, false);
				fprintf(fp, ",%f", fLeftScore);

			}
		}
		structEvaluation.nLeftGroundTruth++;
	}
	else{ //GT가 없는 경우
		structEvaluation.nLeftNonDetectedFrame++;
		//if (FindLeft.ptUvStartLine.x == EMPTY){ 
		if (obj.m_bDraw[0] == false || obj.m_bDraw[1] == false){
			structEvaluation.LeftTN++;
			//cout << "Left TN" << endl;
			//putText(obj.m_imgResizeOrigin, szTrueNegative, LeftText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 255, 255), 1, 8, false);
		}
		else
		{
			structEvaluation.LeftFP++;
			//cout << "Left FP" << endl;
			//putText(obj.m_imgResizeOrigin, szFalsePositive, LeftText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 0, 255), 1, 8, false);
		}
	}


	if ((GroundRight.ptUvStartLine.x != EMPTY) && (GroundRight.ptUvEndLine.x != EMPTY)){
		structEvaluation.nRightDetectedFrame++;
		//if (FindRight.ptUvStartLine.x == EMPTY){
		if (obj.m_bDraw[0] == false || obj.m_bDraw[1] == false){
			structEvaluation.RightFN++;
			cout << "Right FN" << endl;
			putText(obj.m_imgResizeOrigin, szFalseNegative, ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 0, 255), 1.8, 8, false);
			putText(matCanvas, szFalseNegative, ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 0, 255), 1.8, 8, false);
			fprintf(fp, ",,\n");

		}
		else{
			fRightScore = CompareLineDiff(FixedRightStart, FixedRightEnd, GroundRight.ptUvStartLine, GroundRight.ptUvEndLine);
			cout << "fRightScore : " << fRightScore << endl;
			if (fRightScore < fStandard){
				structEvaluation.RightTP++;
				cout << "Right TP" << endl;
				stringstream ssTP;
				ssTP << fRightScore;
				ssTP << " [R] ";
				ssTP << szTruePositive;
				putText(obj.m_imgResizeOrigin, ssTP.str(), ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 255, 0), 1.8, 8, false);
				putText(matCanvas, ssTP.str(), ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 255, 0), 1.8, 8, false);
				fprintf(fp, ",%f,\n", fRightScore);

			}
			else{
				structEvaluation.RightFP++;
				cout << "Right FP" << endl;
				stringstream ssFP;
				ssFP << fRightScore;
				ssFP << " [R] ";
				ssFP << szFalsePositive;

				putText(obj.m_imgResizeOrigin, ssFP.str(), ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(255, 0, 0), 1.8, 8, false);
				putText(matCanvas, ssFP.str(), ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(255, 0, 0), 1.8, 8, false);
				fprintf(fp, ", ,%f\n", fRightScore);

			}
		}

	}
	else{
		structEvaluation.nRightNonDetectedFrame++;
		//	if (FindRight.ptUvStartLine.x == EMPTY){
		if (obj.m_bDraw[0] == false || obj.m_bDraw[1] == false){
			structEvaluation.RightTN++;
			//cout << "Right TN" << endl;
			//putText(obj.m_imgResizeOrigin, szTrueNegative, RightText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 255, 255), 1, 8, false);
		}
		else
		{
			structEvaluation.RightFP++;
			//cout << "Right FP" << endl;
			//putText(obj.m_imgResizeOrigin, szFalsePositive, RightText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 0, 255), 1, 8, false);
		}
	}

	//#debug : 검출된 차선을 그리기 위함
	//if (FindLeft.ptUvStartLine.x != EMPTY){
	if (obj.m_bDraw[0] != false){
		line(obj.m_imgResizeOrigin, FixedLeftStart, FixedLeftEnd, Scalar(255, 0, 0), 1);
		line(matCanvas, FixedLeftStart, FixedLeftEnd, Scalar(255, 0, 0), 1);

	}
	//if (FindRight.ptUvStartLine.x != EMPTY){
	if (obj.m_bDraw[1] != false){
		line(obj.m_imgResizeOrigin, FixedRightStart, FixedRightEnd, Scalar(255, 0, 0), 1);
		line(matCanvas, FixedRightStart, FixedRightEnd, Scalar(255, 0, 0), 1);

	}
}///end

void EvaluationFunc_multi(CMultiROILaneDetection &obj, SEvaluation &structEvaluation,
	SWorldLane GroundLeft1, SWorldLane GroundLeft2, SWorldLane GroundRight1, SWorldLane GroundRight2,
	SWorldLane FindLeft1, SWorldLane FindLeft2, SWorldLane FindRight1, SWorldLane FindRight2,
	Mat& matCanvas, FILE* fp){

	int nTop = GroundLeft1.ptUvStartLine.y;
	int nBottom = GroundLeft1.ptUvEndLine.y;
	Point2i ptLeftText = Point2i(obj.m_imgResizeOrigin.cols / 4, GroundLeft1.ptUvStartLine.y - 80);
	Point2i ptRightText = Point2i(obj.m_imgResizeOrigin.cols / 4, GroundLeft1.ptUvStartLine.y - 40);

	double FindLeft1Angle;
	Point2d GroundLeftCenter;
	Point2d FindLeft1Center;
	Point2d FixedLeft1Start;
	Point2d FixedLeft1End;

	double FindLeft2Angle;
	Point2d GroundLeft2Center;
	Point2d FindLeft2Center;
	Point2d FixedLeft2Start;
	Point2d FixedLeft2End;

	double FindRight1Angle;
	Point2d GroundRightCenter;
	Point2d FindRight1Center;
	Point2d FixedRight1Start;
	Point2d FixedRight1End;

	double FindRight2Angle;
	Point2d GroundRight2Center;
	Point2d FindRight2Center;
	Point2d FixedRight2Start;
	Point2d FixedRight2End;

	//GroundLeftCenter.x = (GroundLeft.ptUvStartLine.x + GroundLeft.ptUvEndLine.x) / 2;
	//GroundLeftCenter.y = (GroundLeft.ptUvStartLine.y + GroundLeft.ptUvEndLine.y) / 2;


	// 이과정은 GT와 같은 위치와 같은 길이로 만들기 위함

	//Left1
	FindLeft1Center.x = (FindLeft1.ptUvStartLine.x + FindLeft1.ptUvEndLine.x) / 2;
	FindLeft1Center.y = (FindLeft1.ptUvStartLine.y + FindLeft1.ptUvEndLine.y) / 2;
	FindLeft1Angle = (FindLeft1.ptUvEndLine.x - FindLeft1.ptUvStartLine.x)
		/ (FindLeft1.ptUvEndLine.y - FindLeft1.ptUvStartLine.y);
	FixedLeft1Start.x = FindLeft1Center.x + (nTop - FindLeft1Center.y)*FindLeft1Angle;
	FixedLeft1Start.y = nTop;
	FixedLeft1End.x = FindLeft1Center.x + (nBottom - FindLeft1Center.y)*FindLeft1Angle;
	FixedLeft1End.y = nBottom;

	//Left2
	FindLeft2Center.x = (FindLeft2.ptUvStartLine.x + FindLeft2.ptUvEndLine.x) / 2;
	FindLeft2Center.y = (FindLeft2.ptUvStartLine.y + FindLeft2.ptUvEndLine.y) / 2;
	FindLeft2Angle = (FindLeft2.ptUvEndLine.x - FindLeft2.ptUvStartLine.x)
		/ (FindLeft2.ptUvEndLine.y - FindLeft2.ptUvStartLine.y);
	FixedLeft2Start.x = FindLeft2Center.x + (nTop - FindLeft2Center.y)*FindLeft2Angle;
	FixedLeft2Start.y = nTop;
	FixedLeft2End.x = FindLeft2Center.x + (nBottom - FindLeft2Center.y)*FindLeft2Angle;
	FixedLeft2End.y = nBottom;

	//Right1
	FindRight1Center.x = (FindRight1.ptUvStartLine.x + FindRight1.ptUvEndLine.x) / 2;
	FindRight1Center.y = (FindRight1.ptUvStartLine.y + FindRight1.ptUvEndLine.y) / 2;
	FindRight1Angle = (FindRight1.ptUvEndLine.x - FindRight1.ptUvStartLine.x)
		/ (FindRight1.ptUvEndLine.y - FindRight1.ptUvStartLine.y);
	FixedRight1Start.x = FindRight1Center.x + (nTop - FindRight1Center.y)*FindRight1Angle;
	FixedRight1Start.y = nTop;
	FixedRight1End.x = FindRight1Center.x + (nBottom - FindRight1Center.y)*FindRight1Angle;
	FixedRight1End.y = nBottom;

	//Right2
	FindRight2Center.x = (FindRight2.ptUvStartLine.x + FindRight2.ptUvEndLine.x) / 2;
	FindRight2Center.y = (FindRight2.ptUvStartLine.y + FindRight2.ptUvEndLine.y) / 2;
	FindRight2Angle = (FindRight2.ptUvEndLine.x - FindRight2.ptUvStartLine.x)
		/ (FindRight2.ptUvEndLine.y - FindRight2.ptUvStartLine.y);
	FixedRight2Start.x = FindRight2Center.x + (nTop - FindRight2Center.y)*FindRight2Angle;
	FixedRight2Start.y = nTop;
	FixedRight2End.x = FindRight2Center.x + (nBottom - FindRight2Center.y)*FindRight2Angle;
	FixedRight2End.y = nBottom;


	char szTruePositive[20] = "TruePositive";
	char szFalsePositive[20] = "FalsePositive";
	char szFalseNegative[20] = "FalseNegative";
	char szTrueNegative[20] = "TrueNegative";

	float fLeftScore1, fLeftScore2;
	float fRightScore1, fRightScore2;
	float fStandard = COMPARE_STANDARD; //0.2


	// Left 1
	structEvaluation.nTotalFrame++;
	if ((GroundLeft1.ptUvStartLine.x != EMPTY) && (GroundLeft1.ptUvEndLine.x != EMPTY)){ // Ground truth 위,아래 둘다 있을 경우에만
		structEvaluation.nLeftDetectedFrame++;
		if (obj.m_bDraw[2] == false){ // Miss ( False Negative )
			cout << "Left1 FN" << endl;
			putText(obj.m_imgResizeOrigin, szFalseNegative, ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 0, 255), 1.8, 8, false);
			putText(matCanvas, szFalseNegative, ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 0, 255), 1.8, 8, false);
			//fprintf(fp, ",,,\n");
			structEvaluation.LeftFN++;
			return;

		} // Detected
		else{ // TP
			fLeftScore1 = CompareLineDiff(FixedLeft1Start, FixedLeft1End, GroundLeft1.ptUvStartLine, GroundLeft1.ptUvEndLine);
			cout << "fLeftScore 1 : " << fLeftScore1 << endl;
			if (fLeftScore1 < fStandard){
				structEvaluation.LeftTP++;
				cout << "Left1 TP" << endl;

				stringstream ssTP;
				ssTP << fLeftScore1;
				ssTP << " [L] ";
				ssTP << szTruePositive;
				putText(obj.m_imgResizeOrigin, ssTP.str(), ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 255, 0), 1.8, 8, false);
				putText(matCanvas, ssTP.str(), ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 255, 0), 1.8, 8, false);
				//putText(obj.m_imgResizeOrigin, szTruePositive, LeftText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 255, 255), 1, 8, false);
				//fprintf(fp, "%f, ", fLeftScore1);
			}
			else{ //FP
				structEvaluation.LeftFP++;
				cout << "Left FP" << endl;
				stringstream ssFP;
				ssFP << fLeftScore1;
				ssFP << " [L] ";
				ssFP << szFalsePositive;

				putText(obj.m_imgResizeOrigin, ssFP.str(), ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(255, 0, 0), 1.8, 8, false);
				putText(matCanvas, ssFP.str(), ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(255, 0, 0), 1.8, 8, false);
				//fprintf(fp, ",%f", fLeftScore1);

			}
		}
		structEvaluation.nLeftGroundTruth++;
	}
	else{ //GT가 없는 경우 ( Negative )
		structEvaluation.nLeftNonDetectedFrame++;
		if (obj.m_bDraw[2] == false){
			structEvaluation.LeftTN++;
			//cout << "Left TN" << endl;
			//putText(obj.m_imgResizeOrigin, szTrueNegative, LeftText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 255, 255), 1, 8, false);
		}
		else
		{
			structEvaluation.LeftFP++;
			//cout << "Left FP" << endl;
			//putText(obj.m_imgResizeOrigin, szFalsePositive, LeftText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 0, 255), 1, 8, false);
		}
	}

	// Left 2
	if ((GroundLeft2.ptUvStartLine.x != EMPTY) && (GroundLeft2.ptUvEndLine.x != EMPTY)){ // Ground truth 위,아래 둘다 있을 경우에만
		structEvaluation.nLeftDetectedFrame++;
		if (obj.m_bDraw[0] == false){ // Miss ( False Negative )
			cout << "Left2 FN" << endl;
			putText(obj.m_imgResizeOrigin, szFalseNegative, ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 0, 255), 1.8, 8, false);
			putText(matCanvas, szFalseNegative, ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 0, 255), 1.8, 8, false);
			//fprintf(fp, ",,,\n");
			structEvaluation.Left2FN++;
			return;

		} // Detected
		else{ // TP
			fLeftScore2 = CompareLineDiff(FixedLeft2Start, FixedLeft2End, GroundLeft2.ptUvStartLine, GroundLeft2.ptUvEndLine);
			cout << "fLeftScore 2 : " << fLeftScore2 << endl;
			if (fLeftScore2 < fStandard){
				structEvaluation.Left2TP++;
				cout << "Left2 TP" << endl;

				stringstream ssTP;
				ssTP << fLeftScore2;
				ssTP << " [L] ";
				ssTP << szTruePositive;
				putText(obj.m_imgResizeOrigin, ssTP.str(), ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 255, 0), 1.8, 8, false);
				putText(matCanvas, ssTP.str(), ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 255, 0), 1.8, 8, false);
				//putText(obj.m_imgResizeOrigin, szTruePositive, LeftText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 255, 255), 1, 8, false);
				//fprintf(fp, "%f, ", fLeftScore2);
			}
			else{ //FP
				structEvaluation.Left2FP++;
				cout << "Left2 FP" << endl;
				stringstream ssFP;
				ssFP << fLeftScore2;
				ssFP << " [L] ";
				ssFP << szFalsePositive;

				putText(obj.m_imgResizeOrigin, ssFP.str(), ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(255, 0, 0), 1.8, 8, false);
				putText(matCanvas, ssFP.str(), ptLeftText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(255, 0, 0), 1.8, 8, false);
				//fprintf(fp, ",%f", fLeftScore2);

			}
		}
		structEvaluation.nLeftGroundTruth++;
	}
	else{ //GT가 없는 경우 ( Negative )
		structEvaluation.nLeftNonDetectedFrame++;
		if (obj.m_bDraw[0] == false){
			structEvaluation.Left2TN++;
			//cout << "Left TN" << endl;
			//putText(obj.m_imgResizeOrigin, szTrueNegative, LeftText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 255, 255), 1, 8, false);
		}
		else
		{
			structEvaluation.Left2FP++;
			//cout << "Left FP" << endl;
			//putText(obj.m_imgResizeOrigin, szFalsePositive, LeftText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 0, 255), 1, 8, false);
		}
	}

	// Right 1
	if ((GroundRight1.ptUvStartLine.x != EMPTY) && (GroundRight1.ptUvEndLine.x != EMPTY)){ // Ground truth Positive
		structEvaluation.nRightDetectedFrame++;
		if (obj.m_bDraw[1] == false){ // False negative, Miss ( 미인식 )
			structEvaluation.RightFN++;
			cout << "Right1 FN" << endl;
			putText(obj.m_imgResizeOrigin, szFalseNegative, ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 0, 255), 1.8, 8, false);
			putText(matCanvas, szFalseNegative, ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 0, 255), 1.8, 8, false);
			//fprintf(fp, ",,\n");

		}
		else{ // Detected
			fRightScore1 = CompareLineDiff(FixedRight1Start, FixedRight1End, GroundRight1.ptUvStartLine, GroundRight1.ptUvEndLine);
			cout << "fRightScore : " << fRightScore1 << endl;
			if (fRightScore1 < fStandard){ // True Positive
				structEvaluation.RightTP++;
				cout << "Right TP" << endl;
				stringstream ssTP;
				ssTP << fRightScore1;
				ssTP << " [R] ";
				ssTP << szTruePositive;
				putText(obj.m_imgResizeOrigin, ssTP.str(), ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 255, 0), 1.8, 8, false);
				putText(matCanvas, ssTP.str(), ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 255, 0), 1.8, 8, false);
				//fprintf(fp, ",%f,\n", fRightScore1);

			}
			else{ // False Positive ( 오인식 )
				structEvaluation.RightFP++;
				cout << "Right FP" << endl;
				stringstream ssFP;
				ssFP << fRightScore1;
				ssFP << " [R] ";
				ssFP << szFalsePositive;

				putText(obj.m_imgResizeOrigin, ssFP.str(), ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(255, 0, 0), 1.8, 8, false);
				putText(matCanvas, ssFP.str(), ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(255, 0, 0), 1.8, 8, false);
				//fprintf(fp, ", ,%f\n", fRightScore1);

			}
		}

	}
	else{ // Ground truth Negative
		structEvaluation.nRightNonDetectedFrame++;
		if (obj.m_bDraw[1] == false){ // True negative
			structEvaluation.RightTN++;
			//cout << "Right TN" << endl;
			//putText(obj.m_imgResizeOrigin, szTrueNegative, RightText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 255, 255), 1, 8, false);
		}
		else // False Positvie
		{
			structEvaluation.RightFP++;
			//cout << "Right FP" << endl;
			//putText(obj.m_imgResizeOrigin, szFalsePositive, RightText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 0, 255), 1, 8, false);
		}
	}

	// Right 2
	if ((GroundRight2.ptUvStartLine.x != EMPTY) && (GroundRight2.ptUvEndLine.x != EMPTY)){ // Ground truth Positive
		structEvaluation.nRightDetectedFrame++;
		if (obj.m_bDraw[3] == false){ // False negative, Miss ( 미인식 )
			structEvaluation.Right2FN++;
			cout << "Right2 FN" << endl;
			putText(obj.m_imgResizeOrigin, szFalseNegative, ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 0, 255), 1.8, 8, false);
			putText(matCanvas, szFalseNegative, ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 0, 255), 1.8, 8, false);
			//fprintf(fp, ",,\n");

		}
		else{ // Detected
			fRightScore2 = CompareLineDiff(FixedRight2Start, FixedRight2End, GroundRight2.ptUvStartLine, GroundRight2.ptUvEndLine);
			cout << "fRightScore : " << fRightScore2 << endl;
			if (fRightScore2 < fStandard){ // True Positive
				structEvaluation.Right2TP++;
				cout << "Right2 TP" << endl;
				stringstream ssTP;
				ssTP << fRightScore2;
				ssTP << " [R] ";
				ssTP << szTruePositive;
				putText(obj.m_imgResizeOrigin, ssTP.str(), ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 255, 0), 1.8, 8, false);
				putText(matCanvas, ssTP.str(), ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 255, 0), 1.8, 8, false);
				//fprintf(fp, ",%f,\n", fRightScore2);

			}
			else{ // False Positive ( 오인식 )
				structEvaluation.Right2FP++;
				cout << "Right2 FP" << endl;
				stringstream ssFP;
				ssFP << fRightScore2;
				ssFP << " [R] ";
				ssFP << szFalsePositive;

				putText(obj.m_imgResizeOrigin, ssFP.str(), ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(255, 0, 0), 1.8, 8, false);
				putText(matCanvas, ssFP.str(), ptRightText, FONT_HERSHEY_COMPLEX, 0.6, Scalar(255, 0, 0), 1.8, 8, false);
				//fprintf(fp, ", ,%f\n", fRightScore2);
			}
		}

	}
	else{ // Ground truth Negative
		structEvaluation.nRightNonDetectedFrame++;
		if (obj.m_bDraw[3] == false){ // True negative
			structEvaluation.Right2TN++;
			//cout << "Right TN" << endl;
			//putText(obj.m_imgResizeOrigin, szTrueNegative, RightText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 255, 255), 1, 8, false);
		}
		else // False Positvie
		{
			structEvaluation.Right2FP++;
			//cout << "Right FP" << endl;
			//putText(obj.m_imgResizeOrigin, szFalsePositive, RightText,FONT_HERSHEY_COMPLEX, 1, Scalar(0, 0, 255), 1, 8, false);
		}
	}

	//#debug : 검출된 차선을 그리기 위함
	//if (FindLeft.ptUvStartLine.x != EMPTY){
	if (obj.m_bDraw[2] != false)
		line(matCanvas, FixedLeft1Start, FixedLeft1End, Scalar(255, 0, 0), 1);
	if (obj.m_bDraw[0] != false)
		line(matCanvas, FixedLeft2Start, FixedLeft2End, Scalar(255, 0, 0), 1);
	if (obj.m_bDraw[1] != false)
		line(matCanvas, FixedRight1Start, FixedRight1End, Scalar(255, 0, 0), 1);
	if (obj.m_bDraw[3] != false)
		line(matCanvas, FixedRight2Start, FixedRight2End, Scalar(255, 0, 0), 1);

}///end



void DifferentialImgProcess(vector<Mat> &vecImgDiff, Mat origin, vector<Point> &vecRoiBottom){
	Mat matSumDiff;
	matSumDiff.create(vecImgDiff[0].rows, 1, CV_32FC1);
	//matSumDiff.zeros(vecImgDiff[0].rows, 1, CV_32FC1);
	vector<Mat> vecSumDiff;
	Mat matColSum = Mat::zeros(vecImgDiff[0].rows, 1, CV_32FC1);
	//matColSum.create(vecImgDiff[0].rows, 1, CV_32FC1);
	//matColSum.zeros(vecImgDiff[0].rows, 1, CV_32FC1);
	for (int i = 0; i < vecImgDiff.size(); i++){
		reduce(vecImgDiff[i], matSumDiff, 1, CV_REDUCE_SUM);
		vecSumDiff.push_back(matSumDiff.clone());
		matColSum += matSumDiff;

		imshow("diff", vecImgDiff[i]);
		printf("diff #%d\n", i);
		//waitKey(0);
	}
	matColSum /= vecImgDiff.size();
	/*double tMax = MAXCOMP;
	float *pMatColSumData = (float*)matColSum.data;
	for (int i = 0; i < matColSum.rows; i++){
	if (tMax < pMatColSumData[i])
	tMax = pMatColSumData[i];
	}
	matColSum /= tMax/vecImgDiff[0].cols;
	printf("tMax = %f\n", tMax);*/
	Mat matDrawDiffSum = Mat::zeros(vecImgDiff[0].size(), CV_32FC3);
	for (int i = 0; i < matDrawDiffSum.rows; i++){
		//if ((matColSum.at<float>(i) / vecImgDiff[0].cols)<0.1)
		if ((matColSum.at<float>(i)) < 2.5)
		{
			line(origin, Point(0, i), Point(matColSum.at<float>(i), i), Scalar(255, 0, 0), 1);
			if (i > matColSum.rows / 2)
				vecRoiBottom.push_back(Point(matColSum.at<float>(i), i));
		}
		else
			line(origin, Point(0, i), Point(matColSum.at<float>(i), i), Scalar(0, 255, 0), 1);
	}
	imshow("matDrawDiffSum", origin);
	waitKey(0);

}

/**
@author 이영완
@date 2015.11.06
@return errCheck
@warning
@brief Kalman Tracking. 크게 3단계로 이루어짐.
*/
bool CMultiROILaneDetection::StartTracking()
{
	//m_timeTracking.timeStart();
	bool bErrCheck = false;


	if (!TrackingSetting())
	{
		cout << "Error TrackingSetting() in StartTracking:" << __LINE__ << endl;
		return bErrCheck;
	}
	if (!DetermineTracking())
	{
		cout << "Error DetermineTracking in StartTracking::" << __LINE__ << endl;
		return bErrCheck;
	}
	if (!TrackingStage())
	{
		cout << "Error TrackingStage in StartTracking::" << __LINE__ << endl;
		return bErrCheck;
	}

	//m_timeTracking.timeEnd();
	return bErrCheck;
}


/**
@author 이영완
@date 2015.11.06
@return bErrCheck
@warning
@brief StartTracking 1단계 : Kalman Tracking을 위한 준비단계. 파라미터셋팅.
*/
bool CMultiROILaneDetection::TrackingSetting()
{
	bool bErrCheck = false;

	TrackingStageGround(LEFT_ROI2, 0);
	TrackingStageGround(RIGHT_ROI2, 1);

	TrackingStageGround(LEFT_ROI3, 0);
	TrackingStageGround(RIGHT_ROI3, 1);

#if _ADD_ROI_
	TrackingStageGround(LEFT_ROI0, 2);
	TrackingStageGround(RIGHT_ROI0, 3);

#endif
	return bErrCheck = true;
}


/**
@author 이영완
@date 2015.11.06
@return bool
@warning
@brief StartTracking 2단계 : 계속해서 tracking할지 말지 판단
*/
bool CMultiROILaneDetection::DetermineTracking()
{
	bool bErrCheck = false;

	TrackingContinue(0);
	TrackingContinue(1);

#if _ADD_ROI_
	TrackingContinue(2);
	TrackingContinue(3);
#endif

	if (m_bTrackingFlag[m_sTracking[LEFT_ROI2].nTargetTracker] == false){
		m_sTracking[LEFT_ROI2].bTracking = false;
	}
	if (m_bTrackingFlag[m_sTracking[LEFT_ROI3].nTargetTracker] == false){
		m_sTracking[LEFT_ROI3].bTracking = false;
	}
	if (m_bTrackingFlag[m_sTracking[RIGHT_ROI2].nTargetTracker] == false){
		m_sTracking[RIGHT_ROI2].bTracking = false;
	}
	if (m_bTrackingFlag[m_sTracking[RIGHT_ROI3].nTargetTracker] == false){
		m_sTracking[RIGHT_ROI3].bTracking = false;
	}
	if (m_bTrackingFlag[m_sTracking[LEFT_ROI0].nTargetTracker] == false){
		m_sTracking[LEFT_ROI0].bTracking = false;
	}//[LYW_0815] : ROI추가(3)
	if (m_bTrackingFlag[m_sTracking[RIGHT_ROI0].nTargetTracker] == false){
		m_sTracking[RIGHT_ROI0].bTracking = false;
	}//[LYW_0917]: RIFHT_ROI0추가			

	return bErrCheck = true;
}


/**
@author 이영완
@date 2015.11.06
@return bool
@warning
@brief StartTracking 3단계:본격적인 Kalman Tracking 계산 & 추정.
*/

bool CMultiROILaneDetection::TrackingStage()
{
	bool bErrCheck = false;

	KalmanTrackingStage(0);
	KalmanTrackingStage(1);

#if _ADD_ROI_
	KalmanTrackingStage(2);
	KalmanTrackingStage(3);
#endif

	return bErrCheck = true;

}

/**
@author 이영완
@date 2015.11.05
@return void
@brief tracking후에 후처리 & draw.
@warning
*/
void CMultiROILaneDetection::PostProcessing()
{
	//m_timePostProcessing.timeStart();
	bool bErrCheck = false;


	/* 후처리 - 검사 */

	//추정된 주행차선의 양쪽 간격이 좁아질 경우, false로 인식해서 탈락시킴. 추 후 옆차선과의 관계들을 통해 false제거할 수 있도록 해보자 
	if ((m_bDraw[0] == true) && (m_bDraw[1] == true)){
		float fRightGround = m_sTrackingLane[1].fXcenter / 1000; // meter단위로 환산
		float fLeftGround = m_sTrackingLane[0].fXcenter / 1000;
		if ((fRightGround - fLeftGround) < MIN_WORLD_WIDTH)
		{
			printf("[Exception] Tracking lanes are narrow in PostProcessing\n");
			ClearDetectionResult(0);
			ClearDetectionResult(1);
		}
	}
	if ((m_bDraw[0] == true) && (m_bDraw[2] == true)){
		float fLeftGround = m_sTrackingLane[0].fXcenter / 1000; // meter단위로 환산
		float fEndGround = m_sTrackingLane[2].fXcenter / 1000;
		float fDis = abs((abs(fLeftGround) - abs(fEndGround)));
		if (fDis > MIN_GUARD_WIDTH)
		{
			printf("[Exception] GuardLane removed: %lf in PostProcessing\n", fDis);
			ClearDetectionResult(2);
		}
	}

	//return bErrCheck = true;
	//m_timePostProcessing.timeEnd();
}

/**
@author 이영완
@date 2015.11.06
@return void
@brief : 중간결과들을 출력.
*/
void CMultiROILaneDetection::ShowResults(EROINUMBER nflag){
	int nNum = nflag;

	if (m_bROISwitch[nflag])
		rectangle(m_imgResizeOrigin, m_sRoiInfo[nflag].ptRoi, m_sRoiInfo[nflag].ptRoiEnd, Scalar(255, 0, 0), 2);

	char strImg[20];
	//sprintf(strImgIpm,)
	sprintf(strImg, "IPM%d", nNum);
	imshow(strImg, m_imgIPM[nflag]);
	sprintf(strImg, "Filt%d", nNum);
	imshow(strImg, m_ipmFiltered[nflag]);
	sprintf(strImg, "FN%d", nNum);
	ShowImageNormalize(strImg, m_ipmFiltered[nflag]);
	sprintf(strImg, "FT%d", nNum);
	ShowImageNormalize(strImg, m_filteredThreshold[nflag]);



#if DETECTION_RESULT 
	for (unsigned int i = 0; i < m_lanesResult[nflag].size(); i++){
		Point ptUvSt = TransformPointGround2Image(Point((int)m_lanesResult[nflag][i].ptStartLine.x, (int)m_lanesResult[nflag][i].ptStartLine.y));
		Point ptUvEnd = TransformPointGround2Image(Point((int)m_lanesResult[nflag][i].ptEndLine.x, (int)m_lanesResult[nflag][i].ptEndLine.y));

		line(m_imgResizeOrigin,
			ptUvSt,
			ptUvEnd,
			Scalar(0, 0, 255), 2);
		m_lanesResult[nflag].clear();
	}
#endif


#ifndef _KALMAN_
	for (unsigned int i = 0; i < m_lanesResult[nflag].size(); i++){
		Point ptUvSt = TransformPointGround2Image(Point((int)m_lanesResult[nflag][i].ptStartLine.x, (int)m_lanesResult[nflag][i].ptStartLine.y));
		Point ptUvEnd = TransformPointGround2Image(Point((int)m_lanesResult[nflag][i].ptEndLine.x, (int)m_lanesResult[nflag][i].ptEndLine.y));

		// //[LYW_1031] : boundary exception
		// if(nflag == LEFT_ROI2){
		// 	if(abs(ptUvSt.x-obj.m_sRoiInfo[LEFT_ROI2].nRight) < 10 ||  abs(ptUvEnd.x - obj.m_sRoiInfo[LEFT_ROI2].nRight) < 10)
		// 		return;
		// 	if(ptUvSt.x > obj.m_sRoiInfo[LEFT_ROI2].nRight) return;	
		// }
		// if(nflag == RIGHT_ROI2)
		// {
		// 	if(abs(ptUvSt.x - obj.m_sRoiInfo[RIGHT_ROI2].nLeft )< 10 || abs(ptUvEnd.x -obj.m_sRoiInfo[RIGHT_ROI2].nLeft ) < 10 )
		// 		return;
		// 	if(ptUvSt.x < obj.m_sRoiInfo[RIGHT_ROI2].nLeft) return;
		// }



		line(m_imgResizeOrigin,
			ptUvSt,
			ptUvEnd,
			Scalar(0, 0, 255), 2);
		m_lanesResult[nflag].clear();
	}



#endif
}

/**
@author 이영완
@date 2015.11.06
@brief display final result.
@return void
@warning 소요되는 시간 길어서 통합모듈에는 호출 비추.

*/
void CMultiROILaneDetection::DisplayResult(vector<SLane>& lanes, vector<Rect>& rectResults)
{
	//m_timeDisplayResult.timeStart();

	stringstream ssLeft, ssRight;
	stringstream ssLeft2;//[LYW_0815] : ROI추가(8)
	stringstream ssRight2;//[LYW_0917]: RIGHT_ROI0추가

	float fLeftGround, fRightGround;
	float fLeftGround2;//[LYW_0815] : ROI추가(9)
	float fRightGround2; //[LYW_0917]: RIFHT_ROI0추가


	//drawing intermediate results
	//ShowResults(LEFT_ROI2);
	//ShowResults(LEFT_ROI3);
	//ShowResults(RIGHT_ROI2);
	//ShowResults(RIGHT_ROI3);
#if _VEHICLE_
	ShowResults(CENTER_ROI);
#endif

#if _ADD_ROI_
	ShowResults(LEFT_ROI0);
	ShowResults(RIGHT_ROI0);
#endif

	//drawing vanishing line
	line(m_imgResizeOrigin, Point(0, m_sCameraInfo.ptVanishingPoint.y), Point(m_imgResizeOrigin.cols - 1, m_sCameraInfo.ptVanishingPoint.y), Scalar(0, 255, 0), 2);




#ifdef _KALMAN_

	//[LYW_1028]--> A : 월드좌표계에서의 기준(0,0)은 카메라 중심
	// m로 표현하기 위해 1000으로 나눠준다.
	// 100으로 곱해줬다가 나눠주는 것은 round처리하고 소수점 2자리까지 표현하기 위함 

	if (m_bTrackingFlag[0]){
		int ssTemp = m_sTrackingLane[0].fXcenter / 1000 * 100;
		fLeftGround = float(ssTemp) / 100;
		ssLeft << fLeftGround;
	}
	if (m_bTrackingFlag[1]){
		int ssTemp = m_sTrackingLane[1].fXcenter / 1000 * 100;
		fRightGround = float(ssTemp) / 100;
		ssRight << fRightGround;
	}

#if _ADD_ROI_
	if (m_bTrackingFlag[2]){  //[LYW_0815] : ROI추가(10)
		int ssTemp = m_sTrackingLane[2].fXcenter / 1000 * 100;
		fLeftGround2 = float(ssTemp) / 100;
		ssLeft2 << fLeftGround2;
	}
	if (m_bTrackingFlag[3]){  //[LYW_0815] : ROI추가(10)
		int ssTemp = m_sTrackingLane[3].fXcenter / 1000 * 100;
		fRightGround2 = float(ssTemp) / 100;
		ssRight2 << fRightGround2;
	}

#endif


	/*drawing & display */



	//drawing and store final lanes
	if ((m_bDraw[0] == true) && (m_bDraw[1] == true)){

		if (m_bTrackingFlag[0]){
			line(m_imgResizeOrigin, m_sTrackingLane[0].ptUvStartLine,
				m_sTrackingLane[0].ptUvEndLine, Scalar(0, 255, 0), 2);
			putText(m_imgResizeOrigin, ssLeft.str(), m_sTrackingLane[0].ptUvEndLine,
				FONT_HERSHEY_COMPLEX, 1, Scalar(50, 50, 200), 2, 8, false);

			//최종 차선을 저장
			SLane line;
			line.ptUvStartLine.x = m_sTrackingLane[0].ptUvStartLine.x;
			line.ptUvStartLine.y = m_sTrackingLane[0].ptUvStartLine.y;
			line.ptUvEndLine.x = m_sTrackingLane[0].ptUvEndLine.x;
			line.ptUvEndLine.y = m_sTrackingLane[0].ptUvEndLine.y;

			lanes.push_back(line);

		}
		if (m_bTrackingFlag[1]){
			line(m_imgResizeOrigin, m_sTrackingLane[1].ptUvStartLine,
				m_sTrackingLane[1].ptUvEndLine, Scalar(0, 255, 0), 2);
			putText(m_imgResizeOrigin, ssRight.str(), m_sTrackingLane[1].ptUvEndLine,
				FONT_HERSHEY_COMPLEX, 1, Scalar(50, 50, 200), 2, 8, false);

			SLane line;
			line.ptUvStartLine.x = m_sTrackingLane[1].ptUvStartLine.x;
			line.ptUvStartLine.y = m_sTrackingLane[1].ptUvStartLine.y;
			line.ptUvEndLine.x = m_sTrackingLane[1].ptUvEndLine.x;
			line.ptUvEndLine.y = m_sTrackingLane[1].ptUvEndLine.y;

			lanes.push_back(line);
		}
	}
#if _ADD_ROI_
	if ((m_bDraw[2] == true))
	{
		if (m_bTrackingFlag[2]){ //[LYW_0815] : ROI추가(11)
			line(m_imgResizeOrigin, m_sTrackingLane[2].ptUvStartLine,
				m_sTrackingLane[2].ptUvEndLine, Scalar(0, 255, 0), 2);
			putText(m_imgResizeOrigin, ssLeft2.str(), m_sTrackingLane[2].ptUvStartLine,
				FONT_HERSHEY_COMPLEX, 1, Scalar(50, 200, 200), 2, 8, false);

			SLane line;
			line.ptUvStartLine.x = m_sTrackingLane[2].ptUvStartLine.x;
			line.ptUvStartLine.y = m_sTrackingLane[2].ptUvStartLine.y;
			line.ptUvEndLine.x = m_sTrackingLane[2].ptUvEndLine.x;
			line.ptUvEndLine.y = m_sTrackingLane[2].ptUvEndLine.y;

			lanes.push_back(line);
		}
	}
	if ((m_bDraw[3] == true))
	{
		if (m_bTrackingFlag[3]){ //[LYW_0917]: RIFHT_ROI0추가
			line(m_imgResizeOrigin, m_sTrackingLane[3].ptUvStartLine,
				m_sTrackingLane[3].ptUvEndLine, Scalar(0, 255, 0), 2);
			putText(m_imgResizeOrigin, ssRight2.str(), m_sTrackingLane[3].ptUvStartLine,
				FONT_HERSHEY_COMPLEX, 1, Scalar(50, 200, 200), 2, 8, false);

			SLane line;
			line.ptUvStartLine.x = m_sTrackingLane[3].ptUvStartLine.x;
			line.ptUvStartLine.y = m_sTrackingLane[3].ptUvStartLine.y;
			line.ptUvEndLine.x = m_sTrackingLane[3].ptUvEndLine.x;
			line.ptUvEndLine.y = m_sTrackingLane[3].ptUvEndLine.y;

			lanes.push_back(line);
		}
	}
#endif


#endif
	//drawing processing time
	stringstream ssTime;
	//char szProcTime[20] = "FPS : ";
	//ssTime << m_timeTotal.avgTime();
	char szMs[10] = "ms";
	ssTime << szMs;
	//putText(m_imgResizeOrigin, ssTime.str(), Point(10, 90), FONT_HERSHEY_COMPLEX, 0.8, Scalar(255, 0, 50), 2, 8, false);

	//imshow("Result Img", m_imgResizeOrigin);



	//m_timeDisplayResult.timeEnd();
}

/**
@author 이영완
@date 2016.02.12.
@brief display final result.
@param final result : lane
@param for drawing the lane : Mat
@return void
@warning 소요되는 시간 길어서 통합모듈에는 호출 비추.

*/
void CMultiROILaneDetection::DisplayResult(vector<SLane>& lanes, Mat& matDisplay)
{
	//m_timeDisplayResult.timeStart();

	stringstream ssLeft, ssRight;
	stringstream ssLeft2;//[LYW_0815] : ROI추가(8)
	stringstream ssRight2;//[LYW_0917]: RIGHT_ROI0추가

	float fLeftGround, fRightGround;
	float fLeftGround2;//[LYW_0815] : ROI추가(9)
	float fRightGround2; //[LYW_0917]: RIFHT_ROI0추가


	//drawing intermediate results
	ShowResults(LEFT_ROI2);
	ShowResults(LEFT_ROI3); // commented by raykim
	ShowResults(RIGHT_ROI2);
	ShowResults(RIGHT_ROI3);

#if _ADD_ROI_
	ShowResults(LEFT_ROI0);
	ShowResults(RIGHT_ROI0);
#endif

	//drawing vanishing line
	line(matDisplay, Point(0, m_sCameraInfo.ptVanishingPoint.y), Point(m_imgResizeOrigin.cols - 1, m_sCameraInfo.ptVanishingPoint.y), Scalar(0, 255, 0), 2);




#ifdef _KALMAN_

	//[LYW_1028]--> A : 월드좌표계에서의 기준(0,0)은 카메라 중심
	// m로 표현하기 위해 1000으로 나눠준다.
	// 100으로 곱해줬다가 나눠주는 것은 round처리하고 소수점 2자리까지 표현하기 위함 

	if (m_bTrackingFlag[0]){
		int ssTemp = m_sTrackingLane[0].fXcenter / 1000 * 100;
		fLeftGround = float(ssTemp) / 100;
		ssLeft << fLeftGround;
	}
	if (m_bTrackingFlag[1]){
		int ssTemp = m_sTrackingLane[1].fXcenter / 1000 * 100;
		fRightGround = float(ssTemp) / 100;
		ssRight << fRightGround;
	}

#if _ADD_ROI_
	if (m_bTrackingFlag[2]){  //[LYW_0815] : ROI추가(10)
		int ssTemp = m_sTrackingLane[2].fXcenter / 1000 * 100;
		fLeftGround2 = float(ssTemp) / 100;
		ssLeft2 << fLeftGround2;
	}
	if (m_bTrackingFlag[3]){  //[LYW_0815] : ROI추가(10)
		int ssTemp = m_sTrackingLane[3].fXcenter / 1000 * 100;
		fRightGround2 = float(ssTemp) / 100;
		ssRight2 << fRightGround2;
	}

#endif


	/*drawing & display */



	//drawing and store final lanes
	if ((m_bDraw[0] == true) && (m_bDraw[1] == true)){

		if (m_bTrackingFlag[0]){
			line(matDisplay, m_sTrackingLane[0].ptUvStartLine,
				m_sTrackingLane[0].ptUvEndLine, Scalar(0, 255, 0), 2);
			putText(matDisplay, ssLeft.str(), m_sTrackingLane[0].ptUvEndLine,
				FONT_HERSHEY_COMPLEX, 1, Scalar(50, 50, 200), 2, 8, false);

			//최종 차선을 저장
			SLane line;
			line.ptUvStartLine.x = m_sTrackingLane[0].ptUvStartLine.x;
			line.ptUvStartLine.y = m_sTrackingLane[0].ptUvStartLine.y;
			line.ptUvEndLine.x = m_sTrackingLane[0].ptUvEndLine.x;
			line.ptUvEndLine.y = m_sTrackingLane[0].ptUvEndLine.y;

			lanes.push_back(line);

		}
		if (m_bTrackingFlag[1]){
			line(matDisplay, m_sTrackingLane[1].ptUvStartLine,
				m_sTrackingLane[1].ptUvEndLine, Scalar(0, 255, 0), 2);
			putText(matDisplay, ssRight.str(), m_sTrackingLane[1].ptUvEndLine,
				FONT_HERSHEY_COMPLEX, 1, Scalar(50, 50, 200), 2, 8, false);

			SLane line;
			line.ptUvStartLine.x = m_sTrackingLane[1].ptUvStartLine.x;
			line.ptUvStartLine.y = m_sTrackingLane[1].ptUvStartLine.y;
			line.ptUvEndLine.x = m_sTrackingLane[1].ptUvEndLine.x;
			line.ptUvEndLine.y = m_sTrackingLane[1].ptUvEndLine.y;

			lanes.push_back(line);
		}
	}
#if _ADD_ROI_
	if ((m_bDraw[2] == true))
	{
		if (m_bTrackingFlag[2]){ //[LYW_0815] : ROI추가(11)
			line(matDisplay, m_sTrackingLane[2].ptUvStartLine,
				m_sTrackingLane[2].ptUvEndLine, Scalar(0, 255, 0), 2);
			putText(matDisplay, ssLeft2.str(), m_sTrackingLane[2].ptUvStartLine,
				FONT_HERSHEY_COMPLEX, 1, Scalar(50, 200, 200), 2, 8, false);

			SLane line;
			line.ptUvStartLine.x = m_sTrackingLane[2].ptUvStartLine.x;
			line.ptUvStartLine.y = m_sTrackingLane[2].ptUvStartLine.y;
			line.ptUvEndLine.x = m_sTrackingLane[2].ptUvEndLine.x;
			line.ptUvEndLine.y = m_sTrackingLane[2].ptUvEndLine.y;

			lanes.push_back(line);
		}
	}
	if ((m_bDraw[3] == true))
	{
		if (m_bTrackingFlag[3]){ //[LYW_0917]: RIFHT_ROI0추가
			line(matDisplay, m_sTrackingLane[3].ptUvStartLine,
				m_sTrackingLane[3].ptUvEndLine, Scalar(0, 255, 0), 2);
			putText(matDisplay, ssRight2.str(), m_sTrackingLane[3].ptUvStartLine,
				FONT_HERSHEY_COMPLEX, 1, Scalar(50, 200, 200), 2, 8, false);

			SLane line;
			line.ptUvStartLine.x = m_sTrackingLane[3].ptUvStartLine.x;
			line.ptUvStartLine.y = m_sTrackingLane[3].ptUvStartLine.y;
			line.ptUvEndLine.x = m_sTrackingLane[3].ptUvEndLine.x;
			line.ptUvEndLine.y = m_sTrackingLane[3].ptUvEndLine.y;

			lanes.push_back(line);
		}
	}
#endif


#endif
	//drawing processing time
	stringstream ssTime;
	//char szProcTime[20] = "FPS : ";
	//ssTime << m_timeTotal.avgTime();
	char szMs[10] = "ms";
	ssTime << szMs;
	//putText(matDisplay, ssTime.str(), Point(10, 90), FONT_HERSHEY_COMPLEX, 0.8, Scalar(255, 0, 50), 2, 8, false);

	//imshow("Result Img", matDisplay);



	//m_timeDisplayResult.timeEnd();
}

/**
@author 이영완
@date 2016.06.03.
@brief get final tracked lanes.
@brief 차선결과를 그리지 않고, 차선 결과만 얻고 싶을 때 사용
@param final result : lanes
@param for drawing the lane : Mat
@return void
*/
void CMultiROILaneDetection::GetLaneResult(vector<SLane>& lanes)
{

	//drawing and store final lanes
	if ((m_bDraw[0] == true) && (m_bDraw[1] == true)){

		if (m_bTrackingFlag[0]){
			//최종 차선을 저장
			SLane line;
			line.ptUvStartLine.x = m_sTrackingLane[0].ptUvStartLine.x;
			line.ptUvStartLine.y = m_sTrackingLane[0].ptUvStartLine.y;
			line.ptUvEndLine.x = m_sTrackingLane[0].ptUvEndLine.x;
			line.ptUvEndLine.y = m_sTrackingLane[0].ptUvEndLine.y;

			lanes.push_back(line);

		}
		if (m_bTrackingFlag[1]){
			SLane line;
			line.ptUvStartLine.x = m_sTrackingLane[1].ptUvStartLine.x;
			line.ptUvStartLine.y = m_sTrackingLane[1].ptUvStartLine.y;
			line.ptUvEndLine.x = m_sTrackingLane[1].ptUvEndLine.x;
			line.ptUvEndLine.y = m_sTrackingLane[1].ptUvEndLine.y;

			lanes.push_back(line);
		}
	}
#if _ADD_ROI_
	if ((m_bDraw[2] == true))
	{
		if (m_bTrackingFlag[2]){ //[LYW_0815] : ROI추가(11)
			SLane line;
			line.ptUvStartLine.x = m_sTrackingLane[2].ptUvStartLine.x;
			line.ptUvStartLine.y = m_sTrackingLane[2].ptUvStartLine.y;
			line.ptUvEndLine.x = m_sTrackingLane[2].ptUvEndLine.x;
			line.ptUvEndLine.y = m_sTrackingLane[2].ptUvEndLine.y;

			lanes.push_back(line);
		}
	}
	if ((m_bDraw[3] == true))
	{
		if (m_bTrackingFlag[3]){ //[LYW_0917]: RIFHT_ROI0추가
			SLane line;
			line.ptUvStartLine.x = m_sTrackingLane[3].ptUvStartLine.x;
			line.ptUvStartLine.y = m_sTrackingLane[3].ptUvStartLine.y;
			line.ptUvEndLine.x = m_sTrackingLane[3].ptUvEndLine.x;
			line.ptUvEndLine.y = m_sTrackingLane[3].ptUvEndLine.y;

			lanes.push_back(line);
		}
	}
#endif
}



/**
@author 이영완
@date 2015.11.06
@brief detection단계에서 사용되었던 메모리 해제 such as m_laneResult...
@return
@warning
*/
void CMultiROILaneDetection::ReleaseDetection()
{
	ClearResultVector(LEFT_ROI2);
	ClearResultVector(LEFT_ROI3);
	ClearResultVector(RIGHT_ROI2);
	ClearResultVector(RIGHT_ROI3);
	ClearResultVector(LEFT_ROI0);
	ClearResultVector(RIGHT_ROI0);

}
/**
@author 이영완
@date 2015.11.06
@brief tracking단계에서 사용되었던 메모리 해제 such as m_sTracking....
@return
@warning
*/
void CMultiROILaneDetection::ReleaseTracking()
{
	ClearDetectionResult(0);
	ClearDetectionResult(1);
	ClearDetectionResult(2);
	ClearDetectionResult(3);
}

/**
@author 이영완
@date 2015.11.06
@return void
@brief LDWS검사.영상좌표계에서 검출된 차선의 dx와 변화추이를 통해 LDWS구현
@warning
*/
void CMultiROILaneDetection::DetermineLDWS()
{
	if (m_bDraw[0] == true && m_bDraw[1] == true)
	{
		float fDisLeft = m_sTrackingLane[0].fXcenter / 1000;
		float fDisRight = m_sTrackingLane[1].fXcenter / 1000;



		float fDis = abs(fDisRight - fDisLeft);
		float fRatioDis = abs(fDisLeft) / abs(fDisRight);
		float fThLeft = 0.40;
		float fThRight = (float)100 / (float)40;
		if (fRatioDis < fThLeft || fRatioDis > fThRight || fDis > 5) {
			printf("[LDW] : Ratio:%f, Dis:%f\n", fRatioDis, fDis);
			putText(m_imgResizeOrigin, "Departure Warning", Point(320, 100),
				FONT_HERSHEY_COMPLEX, 1, Scalar(255, 153, 0), 2, 8, false);
			//m_bDraw[0] = false;
			//m_bDraw[1] = false;
			//ClearDetectionResult(0);
			//ClearDetectionResult(1);
		}

	}
	//m_timeDetermineLDWS.timeEnd();
}


/**
@author 이영완
@date 2015.11.06
@return errCheck 만약 input영상이 없을 경우, 예외처리
@brief. 차선검출단계.
@warning
*/
bool CMultiROILaneDetection::StartDetection()
{
	//m_timeStartDetection.timeStart();

	bool errCheck = false;
	if (!m_imgResizeOrigin.data)
	{
		cout << "\nThere is no image in StartDetection\n" << endl;
		return errCheck;
	}
	
	if (m_bROISwitch[LEFT_ROI2])
		StartLanedetection(LEFT_ROI2);
	if (m_bROISwitch[LEFT_ROI3])
		StartLanedetection(LEFT_ROI3);
	if (m_bROISwitch[RIGHT_ROI2])
		StartLanedetection(RIGHT_ROI2);
	if (m_bROISwitch[RIGHT_ROI3])
		StartLanedetection(RIGHT_ROI3);

#if _VEHICLE_
	StartLanedetection(CENTER_ROI);
#endif

#if _ADD_ROI_
	if (m_bROISwitch[LEFT_ROI0])
		StartLanedetection(LEFT_ROI0);
	if (m_bROISwitch[RIGHT_ROI0])
		StartLanedetection(LEFT_ROI0);
#endif

	//m_timeStartDetection.timeEnd();

	return errCheck = true;
}

/**
@brief 원본영상 들어오면 m_imgOrigin copy 후 초기화 단계. -->  sub call  InitialResizeFunction
@return bErrCheck
@warning 영상이 안들어오는 경우 예외처리
@author 이영완
@date 2015.11.06
*/
bool CMultiROILaneDetection::PreProcess(Mat &src)
{
	//m_timePreProcess.timeStart();
	bool errCheck = false;
	if (src.empty())
	{
		cout << "There is no image in Init()" << endl;
		return errCheck;
	}

	Size sizeOrigImg;
	Size sizeResizeImg;

	src.copyTo(m_imgOrigin);
	sizeOrigImg.width = m_imgOrigin.cols;
	sizeOrigImg.height = m_imgOrigin.rows;

	//[LYW_0824] : HD급은 반띵 g_nResizeFacor --> m_nResizeFactor : 2
	sizeResizeImg = Size(sizeOrigImg.width / m_nResizeFactor, sizeOrigImg.height / m_nResizeFactor);
	m_sCameraInfo.sizeCameraImage = sizeResizeImg;
	InitialResizeFunction(sizeResizeImg);

	//m_timePreProcess.timeEnd();

	return errCheck = true;
}

/**
@brief 들어오는 원본영상을 VGA로 resize & normalize & RGB2GRAY 저장.
@return void
@warning
@author 이영완
@date 2015.11.06
*/
void CMultiROILaneDetection::InitialResizeFunction(Size sizeResize){
	resize(m_imgOrigin, m_imgResizeOrigin, sizeResize);
	m_imgResizeOrigin.convertTo(m_imgOriginScale, CV_32FC1, 1.0 / 255); //normalized
	cvtColor(m_imgOriginScale, m_imgResizeScaleGray, CV_RGB2GRAY); //RGB2GRAY
}

/**
@brief IPM의 LUT를 만드는 함수 호출 --> sub call : SetRoiIpmCofig
@return void
@warning loop밖에서 한 번만 호출하면 된다. 만약 ROI정보가 바뀔 경우, 그때마다 다시 호출 해야 한다.
@author 이영완
@date 2015.11.06
*/

void CMultiROILaneDetection::SetIPMConfig()
{
	SetIpmConfig(LEFT_ROI2);
	SetIpmConfig(LEFT_ROI3);
	SetIpmConfig(RIGHT_ROI2);
	SetIpmConfig(RIGHT_ROI3);
#if _VEHICLE_
	SetIpmConfig(CENTER_ROI);
#endif
#if _ADD_ROI_
	SetIpmConfig(LEFT_ROI0);
	SetIpmConfig(RIGHT_ROI0);
#endif

}


/**
@brief 각각의 ROI셋팅.
@param nFlag : 어떤 ROI인지 식별.
@param nLeft : ROI의 left x.
@param nRight : ROI의 right x.
@param nTop : ROI의 top y.
@param nBottm : ROI의 Bottom y.
@return void
@warning
@author 이영완.
@date 2015.11.08.
*/


/**
@brief ROI, IPM setting. sub call : setROI(), setIPMConfig()
@param void
@return void
@warning
@author 이영완.
@date 2015.11.09.
*/
void CMultiROILaneDetection::setRoiIpmConfig()
{
	setROI(LEFT_ROI2,
		RECT_LT1_ROI_X,
		RECT_LT1_ROI_X + RECT_LT1_ROI_WIDTH,
		RECT_LT1_ROI_Y,
		RECT_LT1_ROI_Y + RECT_LT1_ROI_HEIGHT
		);

	setROI(LEFT_ROI3,
		RECT_LT2_ROI_X,
		RECT_LT2_ROI_X + RECT_LT2_ROI_WIDTH,
		RECT_LT2_ROI_Y,
		RECT_LT2_ROI_Y + RECT_LT2_ROI_HEIGHT
		);

	setROI(LEFT_ROI0,
		RECT_LT0_ROI_X,
		RECT_LT0_ROI_X + RECT_LT0_ROI_WIDTH,
		RECT_LT0_ROI_Y,
		RECT_LT0_ROI_Y + RECT_LT0_ROI_HEIGHT
		);
	setROI(RIGHT_ROI2,
		RECT_RT1_ROI_X,
		RECT_RT1_ROI_X + RECT_RT1_ROI_WIDTH,
		RECT_RT1_ROI_Y,
		RECT_RT1_ROI_Y + RECT_RT1_ROI_HEIGHT
		);
	setROI(RIGHT_ROI3,
		RECT_RT2_ROI_X,
		RECT_RT2_ROI_X + RECT_RT2_ROI_WIDTH,
		RECT_RT2_ROI_Y,
		RECT_RT2_ROI_Y + RECT_RT2_ROI_HEIGHT
		);

	setROI(RIGHT_ROI0,
		RECT_RT0_ROI_X,
		RECT_RT0_ROI_X + RECT_RT0_ROI_WIDTH,
		RECT_RT0_ROI_Y,
		RECT_RT0_ROI_Y + RECT_RT0_ROI_HEIGHT
		);
#if _VEHICLE_
	setROI(CENTER_ROI,
		RECT_CENTER_ROI_X,
		RECT_CENTER_ROI_X+ RECT_CENTER_ROI_WIDTH,
		RECT_CENTER_ROI_Y,
		RECT_CENTER_ROI_Y + RECT_CENTER_ROI_HEIGHT
		);
#endif

	SetIPMConfig();
}






void CMultiROILaneDetection::setROI(EROINUMBER nFlag, int nLeft, int nRight, int nTop, int nBottom)
{

	m_sRoiInfo[nFlag].nLeft = nLeft;
	m_sRoiInfo[nFlag].nRight = nRight;
	m_sRoiInfo[nFlag].nTop = nTop;
	m_sRoiInfo[nFlag].nBottom = nBottom;
	m_sRoiInfo[nFlag].sizeRoi.width = m_sRoiInfo[nFlag].nRight - m_sRoiInfo[nFlag].nLeft;
	m_sRoiInfo[nFlag].sizeRoi.height = m_sRoiInfo[nFlag].nBottom - m_sRoiInfo[nFlag].nTop;
	m_sRoiInfo[nFlag].ptRoi.x = m_sRoiInfo[nFlag].nLeft;
	m_sRoiInfo[nFlag].ptRoi.y = m_sRoiInfo[nFlag].nTop;
	m_sRoiInfo[nFlag].ptRoiEnd.x = m_sRoiInfo[nFlag].ptRoi.x + m_sRoiInfo[nFlag].sizeRoi.width;
	m_sRoiInfo[nFlag].ptRoiEnd.y = m_sRoiInfo[nFlag].ptRoi.y + m_sRoiInfo[nFlag].sizeRoi.height;

	m_sRoiInfo[nFlag].sizeIPM.width =
		(m_sRoiInfo[nFlag].nRight - m_sRoiInfo[nFlag].nLeft)*m_fWidthScale;
	m_sRoiInfo[nFlag].sizeIPM.height =
		(m_sRoiInfo[nFlag].nBottom - m_sRoiInfo[nFlag].nTop)*m_fHeightScale;


	m_sRoiInfo[nFlag].nDetectionThreshold = 1.5;
	m_sRoiInfo[nFlag].nGetEndPoint = 0;
	m_sRoiInfo[nFlag].nGroupThreshold = 10;
	m_sRoiInfo[nFlag].fOverlapThreshold = 0.3;

	m_sRoiInfo[nFlag].nRansacNumSamples = 2;	//Ransac
	m_sRoiInfo[nFlag].nRansacNumIterations = 40;
	m_sRoiInfo[nFlag].nRansacNumGoodFit = 10;
	m_sRoiInfo[nFlag].fRansacThreshold = 0.2;
	m_sRoiInfo[nFlag].nRansacScoreThreshold = 0;
	m_sRoiInfo[nFlag].nRansacLineWindow = 15;
}





/**
@brief Lane Detection의 본론. sub call : GetIPM->FilterLinesIPM->GetLinesIPM->IPM2ImLines
@return void
@warning
@author 이영완
@date 2015.11.06
*/
void CMultiROILaneDetection::StartLanedetection(EROINUMBER nFlag){

#ifdef _PROFILING_
	m_profiler[nFlag].timeLaneDetection.timeStart();
#endif

	GetIPM(nFlag); //input = grayImg,uvGrid & xyGrid LUT || output = IpmImg
	FilterLinesIPM(nFlag);	//input = m_imgIPM, Output1= m_ipmFiltered, Output2= m_ipmFilteredThreshold
	GetLinesIPM(nFlag);
	LineFitting(nFlag);
	IPM2ImLines(nFlag);

//debug
	//printf("[%d]detected result:%d\n",nFlag,m_lanesResult[nFlag].size());

#ifdef _PROFILING_
	m_profiler[nFlag].timeLaneDetection.timeEnd();
#endif
}


/**
@brief camera parameter settting. configurationInfo. imgeCenter값을 셋팅.
@param pitch,yaw, focal length(X), focal length(Y), principal point(X), principalPoint(Y)
@return void이지만 함수 결과는 fGroundTop, fGroundBottom값이 셋팅됨(tracking에 사용되는).
@warning 나중에 AutoCalib값과 연동되려면 imgCenter값을 어떻게 받을 것인지 고민좀 해야됨.
@author 이영완

@date 2016.02.01.
*/
void CMultiROILaneDetection::Init(double fPitch, double fYaw, double dFx, double dFy, double dCx, double dCy, double dHeight)
{
	m_sCameraInfo.fPitch = fPitch * PI / 180.;
	m_sCameraInfo.fYaw = fYaw * PI / 180.;

	//m_sCameraInfo.sizeFocalLength.width = 656.;
	//m_sCameraInfo.sizeFocalLength.height = 656.;
	//m_sCameraInfo.ptOpticalCenter.x = 320.;
	//m_sCameraInfo.ptOpticalCenter.y = 180.;
	//m_sCameraInfo.fHeight = 1050.;



	m_sConfig.fVanishPortion = (float) 0.0; ///< [LYW_1109] : SetRoiIpmCofig 사용됨.
	m_sConfig.fLowerQuantile = (float) 0.97; ///< [LYW_0824] : 상위 3%인 값들만 걸러내기 위함 in FilterLineIPM()
	m_sConfig.nLocalMaxIgnore = 0;		//Local maxima boundary reject pixels



	if (m_nImageWidth == 1280 && m_nImageHeight == 720 && dFx == 1312.) //SANE
	{
		m_nResizeFactor = 2;

		int Resize_Width = m_nImageWidth / m_nResizeFactor;
		int Resize_Height = m_nImageHeight / m_nResizeFactor;
		int Resize_Center_X = Resize_Width / m_nResizeFactor;
		int Resize_Center_Y = Resize_Height / m_nResizeFactor;

		RECT_LT1_ROI_X = Resize_Center_X * 0.6;
		RECT_LT1_ROI_Y = Resize_Center_Y * 1.2;
		RECT_LT1_ROI_WIDTH = Resize_Width * 0.15;
		RECT_LT1_ROI_HEIGHT = Resize_Height * 0.08;

		RECT_LT2_ROI_X = Resize_Center_X * 0.5;
		RECT_LT2_ROI_Y = Resize_Center_Y  * 1.36;
		RECT_LT2_ROI_WIDTH = Resize_Width * 0.18;
		RECT_LT2_ROI_HEIGHT = Resize_Height * 0.07;

		RECT_LT0_ROI_X = Resize_Center_X * 0.07;
		RECT_LT0_ROI_Y = Resize_Center_Y * 1.2;
		RECT_LT0_ROI_WIDTH = Resize_Width * 0.15;
		RECT_LT0_ROI_HEIGHT = Resize_Height * 0.1;

		RECT_RT1_ROI_X = Resize_Center_X * 1.0;
		RECT_RT1_ROI_Y = Resize_Center_Y * 1.2;
		RECT_RT1_ROI_WIDTH = Resize_Width * 0.15;
		RECT_RT1_ROI_HEIGHT = Resize_Height * 0.08;

		RECT_RT2_ROI_X = Resize_Center_X * 1.2;
		RECT_RT2_ROI_Y = Resize_Center_Y  * 1.36;
		RECT_RT2_ROI_WIDTH = Resize_Width * 0.18;
		RECT_RT2_ROI_HEIGHT = Resize_Height * 0.07;

		RECT_RT0_ROI_X = Resize_Center_X * 1.65;
		RECT_RT0_ROI_Y = Resize_Center_Y * 1.2;
		RECT_RT0_ROI_WIDTH = Resize_Width * 0.15;
		RECT_RT0_ROI_HEIGHT = Resize_Height * 0.1;

		RECT_CENTER_ROI_X= Resize_Center_X * 1.;
		RECT_CENTER_ROI_Y= Resize_Center_Y * 1.;
		RECT_CENTER_ROI_WIDTH= Resize_Width * 0.5;
		RECT_CENTER_ROI_HEIGHT = Resize_Height * 0.5;


		m_sCameraInfo.sizeFocalLength.width = dFx / m_nResizeFactor;
		m_sCameraInfo.sizeFocalLength.height = dFy / m_nResizeFactor;
		m_sCameraInfo.ptOpticalCenter.x = dCx / m_nResizeFactor;
		m_sCameraInfo.ptOpticalCenter.y = dCy / m_nResizeFactor;

		m_sCameraInfo.fHeight = dHeight;

		//m_sCameraInfo.sizeFocalLength.width = (float)1609.162532 / m_nResizeFactor;
		//m_sCameraInfo.sizeFocalLength.height = (float)1597.657725 / m_nResizeFactor;
		//m_sCameraInfo.ptOpticalCenter.x = 320.;
		//m_sCameraInfo.ptOpticalCenter.y = 180.;
		//m_sCameraInfo.fHeight = 1270.;
	}
	else if (m_nImageWidth == 1280 && m_nImageHeight == 720 && dFx == 954.) 	///< NEW_CAMERA
	{
		m_nResizeFactor = 2;

		int Resize_Width = m_nImageWidth / m_nResizeFactor;
		int Resize_Height = m_nImageHeight / m_nResizeFactor;
		int Resize_Center_X = Resize_Width / 2;
		int Resize_Center_Y = Resize_Height / 2;

		RECT_LT1_ROI_X = Resize_Center_X * 0.6;
		RECT_LT1_ROI_Y = Resize_Center_Y * 1.2;
		RECT_LT1_ROI_WIDTH = Resize_Width * 0.15;
		RECT_LT1_ROI_HEIGHT = Resize_Height * 0.15;

		RECT_LT2_ROI_X = Resize_Center_X * 0.37;
		RECT_LT2_ROI_Y = Resize_Center_Y  * 1.55;
		RECT_LT2_ROI_WIDTH = Resize_Width * 0.2;
		RECT_LT2_ROI_HEIGHT = Resize_Height * 0.15;

		RECT_LT0_ROI_X = Resize_Center_X * 0.07;
		RECT_LT0_ROI_Y = Resize_Center_Y * 1.4;
		RECT_LT0_ROI_WIDTH = Resize_Width * 0.15;
		RECT_LT0_ROI_HEIGHT = Resize_Height * 0.15;

		RECT_RT1_ROI_X = Resize_Center_X * 1.0;
		RECT_RT1_ROI_Y = Resize_Center_Y * 1.2;
		RECT_RT1_ROI_WIDTH = Resize_Width * 0.15;
		RECT_RT1_ROI_HEIGHT = Resize_Height * 0.15;

		RECT_RT2_ROI_X = Resize_Center_X * 1.15;
		RECT_RT2_ROI_Y = Resize_Center_Y  * 1.55;
		RECT_RT2_ROI_WIDTH = Resize_Width * 0.2;
		RECT_RT2_ROI_HEIGHT = Resize_Height * 0.15;

		RECT_RT0_ROI_X = Resize_Center_X * 1.60;
		RECT_RT0_ROI_Y = Resize_Center_Y * 1.3;
		RECT_RT0_ROI_WIDTH = Resize_Width * 0.15;
		RECT_RT0_ROI_HEIGHT = Resize_Height * 0.15;


		m_sCameraInfo.sizeFocalLength.width = dFx / (float)m_nResizeFactor;
		m_sCameraInfo.sizeFocalLength.height = dFy / (float)m_nResizeFactor;
		m_sCameraInfo.ptOpticalCenter.x = dCx / (float)m_nResizeFactor;
		m_sCameraInfo.ptOpticalCenter.y = dCy / (float)m_nResizeFactor;

		m_sCameraInfo.fHeight = dHeight;

	}
	else if (m_nImageWidth == 1242 && m_nImageHeight == 375) ///< KITTI
	{
		m_nResizeFactor = 1;

		int Resize_Width = m_nImageWidth / m_nResizeFactor;
		int Resize_Height = m_nImageHeight / m_nResizeFactor;
		int Resize_Center_X = Resize_Width / 2;
		int Resize_Center_Y = Resize_Height / 2;

		RECT_LT1_ROI_X = Resize_Center_X * 0.75;
		RECT_LT1_ROI_Y = Resize_Center_Y * 1.4;
		RECT_LT1_ROI_WIDTH = Resize_Width * 0.11;
		RECT_LT1_ROI_HEIGHT = Resize_Height * 0.08;

		RECT_LT2_ROI_X = Resize_Center_X * 0.6;
		RECT_LT2_ROI_Y = Resize_Center_Y  * 1.7;
		RECT_LT2_ROI_WIDTH = Resize_Width * 0.13;
		RECT_LT2_ROI_HEIGHT = Resize_Height * 0.07;

		RECT_LT0_ROI_X = Resize_Center_X * 0.07;
		RECT_LT0_ROI_Y = Resize_Center_Y * 1.3;
		RECT_LT0_ROI_WIDTH = Resize_Width * 0.15;
		RECT_LT0_ROI_HEIGHT = Resize_Height * 0.1;

		RECT_RT1_ROI_X = Resize_Center_X * 1.05;
		RECT_RT1_ROI_Y = Resize_Center_Y * 1.4;
		RECT_RT1_ROI_WIDTH = Resize_Width * 0.11;
		RECT_RT1_ROI_HEIGHT = Resize_Height * 0.08;

		RECT_RT2_ROI_X = Resize_Center_X * 1.15;
		RECT_RT2_ROI_Y = Resize_Center_Y  * 1.7;
		RECT_RT2_ROI_WIDTH = Resize_Width * 0.13;
		RECT_RT2_ROI_HEIGHT = Resize_Height * 0.07;

		RECT_RT0_ROI_X = Resize_Center_X * 1.65;
		RECT_RT0_ROI_Y = Resize_Center_Y * 1.2;
		RECT_RT0_ROI_WIDTH = Resize_Width * 0.15;
		RECT_RT0_ROI_HEIGHT = Resize_Height * 0.1;
#if _VEHICLE_
		RECT_CENTER_ROI_X = Resize_Center_X * 0.85;
		RECT_CENTER_ROI_Y = Resize_Center_Y * 1.;
		RECT_CENTER_ROI_WIDTH = Resize_Width * 0.2;
		RECT_CENTER_ROI_HEIGHT = Resize_Height * 0.2;
#endif

		m_sCameraInfo.sizeFocalLength.width = dFx / m_nResizeFactor;
		m_sCameraInfo.sizeFocalLength.height = dFy / m_nResizeFactor;
		m_sCameraInfo.ptOpticalCenter.x = dCx / m_nResizeFactor;
		m_sCameraInfo.ptOpticalCenter.y = dCy / m_nResizeFactor;

		m_sCameraInfo.fHeight = dHeight;
	}
	if (m_nImageWidth == 1920 && m_nImageHeight == 1080) // HI_CES2016 DEMO video & camera parameters
	{
		m_nResizeFactor = 3;

		int Resize_Width = m_nImageWidth / m_nResizeFactor;
		int Resize_Height = m_nImageHeight / m_nResizeFactor;
		int Resize_Center_X = Resize_Width / 2;
		int Resize_Center_Y = Resize_Height / 2;

		RECT_LT1_ROI_X = Resize_Center_X * 0.61;
		RECT_LT1_ROI_Y = Resize_Center_Y * 1.2;
		RECT_LT1_ROI_WIDTH = Resize_Width * 0.17;
		RECT_LT1_ROI_HEIGHT = Resize_Height * 0.08;

		RECT_LT2_ROI_X = Resize_Center_X * 0.5;
		RECT_LT2_ROI_Y = Resize_Center_Y  * 1.36;
		RECT_LT2_ROI_WIDTH = Resize_Width * 0.23;
		RECT_LT2_ROI_HEIGHT = Resize_Height * 0.07;

		RECT_LT0_ROI_X = Resize_Center_X * 0.07;
		RECT_LT0_ROI_Y = Resize_Center_Y * 1.3;
		RECT_LT0_ROI_WIDTH = Resize_Width * 0.15;
		RECT_LT0_ROI_HEIGHT = Resize_Height * 0.1;

		RECT_RT1_ROI_X = Resize_Center_X * 0.97;
		RECT_RT1_ROI_Y = Resize_Center_Y * 1.2;
		RECT_RT1_ROI_WIDTH = Resize_Width * 0.17;
		RECT_RT1_ROI_HEIGHT = Resize_Height * 0.08;

		RECT_RT2_ROI_X = Resize_Center_X * 0.98;
		RECT_RT2_ROI_Y = Resize_Center_Y  * 1.36;
		RECT_RT2_ROI_WIDTH = Resize_Width * 0.2;
		RECT_RT2_ROI_HEIGHT = Resize_Height * 0.07;

		RECT_RT0_ROI_X = Resize_Center_X * 1.65;
		RECT_RT0_ROI_Y = Resize_Center_Y * 1.31;
		RECT_RT0_ROI_WIDTH = Resize_Width * 0.15;
		RECT_RT0_ROI_HEIGHT = Resize_Height * 0.1;


		m_sCameraInfo.sizeFocalLength.width = (float)1102.52625 / m_nResizeFactor;
		m_sCameraInfo.sizeFocalLength.height = (float)1108.25503 / m_nResizeFactor;
		m_sCameraInfo.ptOpticalCenter.x = 946 / m_nResizeFactor;
		m_sCameraInfo.ptOpticalCenter.y = 565 / m_nResizeFactor;

		m_sCameraInfo.fHeight = (float)1200;

		//m_sCameraInfo.sizeFocalLength.width = (float)1609.162532 / m_nResizeFactor;
		//m_sCameraInfo.sizeFocalLength.height = (float)1597.657725 / m_nResizeFactor;
		//m_sCameraInfo.ptOpticalCenter.x = 320.;
		//m_sCameraInfo.ptOpticalCenter.y = 180.;
		//m_sCameraInfo.fHeight = 1270.;
	}
	else if (m_nImageWidth == 640 && m_nImageHeight == 480) ///< CALTECH
	{
		m_nResizeFactor = 1;

		int Resize_Width = m_nImageWidth / m_nResizeFactor;
		int Resize_Height = m_nImageHeight / m_nResizeFactor;
		int Resize_Center_X = Resize_Width / 2;
		int Resize_Center_Y = Resize_Height / 2;

		RECT_LT1_ROI_X = Resize_Center_X * 0.65;
		RECT_LT1_ROI_Y = Resize_Center_Y * 0.9;
		RECT_LT1_ROI_WIDTH = Resize_Width * 0.15;
		RECT_LT1_ROI_HEIGHT = Resize_Height * 0.08;

		RECT_LT2_ROI_X = Resize_Center_X * 0.5;
		RECT_LT2_ROI_Y = Resize_Center_Y  * 1.25;
		RECT_LT2_ROI_WIDTH = Resize_Width * 0.2;
		RECT_LT2_ROI_HEIGHT = Resize_Height * 0.08;

		RECT_LT0_ROI_X = Resize_Center_X * 0.07;
		RECT_LT0_ROI_Y = Resize_Center_Y * 1.3;
		RECT_LT0_ROI_WIDTH = Resize_Width * 0.15;
		RECT_LT0_ROI_HEIGHT = Resize_Height * 0.1;

		RECT_RT1_ROI_X = Resize_Center_X * 1.05;
		RECT_RT1_ROI_Y = Resize_Center_Y * 0.9;
		RECT_RT1_ROI_WIDTH = Resize_Width * 0.15;
		RECT_RT1_ROI_HEIGHT = Resize_Height * 0.08;

		RECT_RT2_ROI_X = Resize_Center_X * 1.15;
		RECT_RT2_ROI_Y = Resize_Center_Y  * 1.25;
		RECT_RT2_ROI_WIDTH = Resize_Width * 0.2;
		RECT_RT2_ROI_HEIGHT = Resize_Height * 0.08;

		RECT_RT0_ROI_X = Resize_Center_X * 1.65;
		RECT_RT0_ROI_Y = Resize_Center_Y * 1.2;
		RECT_RT0_ROI_WIDTH = Resize_Width * 0.15;
		RECT_RT0_ROI_HEIGHT = Resize_Height * 0.1;


		m_sCameraInfo.sizeFocalLength.width = dFx / m_nResizeFactor;
		m_sCameraInfo.sizeFocalLength.height = dFy / m_nResizeFactor;
		m_sCameraInfo.ptOpticalCenter.x = dCx / m_nResizeFactor;
		m_sCameraInfo.ptOpticalCenter.y = dCy / m_nResizeFactor;

		m_sCameraInfo.fHeight = dHeight;
	}




	m_sImgCenter.ptStartLine.x = 0;
	m_sImgCenter.ptStartLine.y = RECT_LT1_ROI_Y;
	m_sImgCenter.ptEndLine.x = m_nImageWidth;
	m_sImgCenter.ptEndLine.y = RECT_LT2_ROI_Y + RECT_LT2_ROI_HEIGHT;

	m_sWorldCenterInit.ptStartLane = TransformPointImage2Ground(m_sImgCenter.ptStartLine);
	m_sWorldCenterInit.ptEndLane = TransformPointImage2Ground(m_sImgCenter.ptEndLine);
	m_sWorldCenterInit.fXcenter = (m_sWorldCenterInit.ptStartLane.x + m_sWorldCenterInit.ptEndLane.x) / 2;
	m_sWorldCenterInit.fXderiv = m_sWorldCenterInit.ptStartLane.x - m_sWorldCenterInit.ptEndLane.x;

	//[LYW_0915] : obj.m_sCameraInfo.fGroundTop,fGroundBottom은 AUTOCALIB단계에서 검출된 두 라인의 평균점의 TOP,Bottom 초기 ROI범위에서 벗어나지는 않는다.
	//fGroundTop & fGroundBottom이 트래킹할 때 적용된다.
	m_sCameraInfo.fGroundTop = m_sWorldCenterInit.ptStartLane.y;
	m_sCameraInfo.fGroundBottom = m_sWorldCenterInit.ptEndLane.y;



	//tracking 모듈 초기화 --> init함수에 들어가야되
	nCnt[0] = 0;
	nCnt[1] = 0;
	nCnt[2] = 0;//[LYW_0815]:roi추가
	nCnt[3] = 0;//[LYW_0917]: RIFHT_ROI0추가


	m_bDraw[0] = false;
	m_bDraw[1] = false;
	m_bDraw[2] = false; ////[LYW_0815]:roi추가
	m_bDraw[3] = false; //[LYW_0917]: RIFHT_ROI0추가
}



/**
@brief FilterLinesIPM에 Filtering에 쓰이는 변수.
@param void
@return void
@warning
@author 이영완
@date 2015.11.09.
*/
void CMultiROILaneDetection::setMatFx()
{
	int derivLen = 33; //23; 13; 33;

	//this is for 5-pixels wide
	float derivp[] = {
		1.000000e-16, 1.280000e-14, 7.696000e-13, 2.886400e-11, 7.562360e-10,
		1.468714e-08, 2.189405e-07, 2.558828e-06, 2.374101e-05, 1.759328e-04,
		1.042202e-03, 4.915650e-03,
		1.829620e-02, 5.297748e-02,
		1.169560e-01, 1.918578e-01,
		2.275044e-01,
		1.918578e-01, 1.169560e-01,
		5.297748e-02, 1.829620e-02,
		4.915650e-03, 1.042202e-03,
		1.759328e-04, 2.374101e-05, 2.558828e-06, 2.189405e-07, 1.468714e-08,
		7.562360e-10, 2.886400e-11, 7.696000e-13, 1.280000e-14, 1.000000e-16
	};
	m_MatFx = Mat(derivLen, 1, CV_32FC1, derivp).clone();
}

/**
@brief FilterLinesIPM에 Filtering에 쓰이는 변수.
@return void
@warning
@author 이영완
@date 2015.11.09.
*/
void CMultiROILaneDetection::setMatFy()
{

	int smoothLen = 9; //9; 17;
	float smoothp[] = {
		-1.000000e-03,
		-2.200000e-02,
		-1.480000e-01,
		-1.940000e-01,
		7.300000e-01,
		-1.940000e-01,
		-1.480000e-01,
		-2.200000e-02,
		-1.000000e-03
	};

	m_MatFy = Mat(1, smoothLen, CV_32FC1, smoothp).clone();
}



void CMultiROILaneDetection::DispalyProfiling()
{
	//	//cout << "setROIconfig pTime of LEFT_ROI2 : " << m_profiler[LEFT_ROI2].timeSetConfig.avgTime() << endl;
	//	cout << "getIPM pTime of LEfT_ROI2 : " << m_profiler[LEFT_ROI2].timeGetIPM.avgTime() << endl;
	//	cout << "FilterLinesIPM pTime of LEfT_ROI2 : " << m_profiler[LEFT_ROI2].timeFilterLinesIPM.avgTime() << endl;
	//	cout << "GetLinesIPM pTime of LEfT_ROI2 : " << m_profiler[LEFT_ROI2].timeGetLinesIPM.avgTime() << endl;
	//	cout << "LineFitting pTime of LEfT_ROI2 : " << m_profiler[LEFT_ROI2].timeLineFitting.avgTime() << endl;
	//	cout << "IPM2ImLines pTime of LEfT_ROI2 : " << m_profiler[LEFT_ROI2].timeIPM2ImLines.avgTime() << endl << endl;
	//
	//	//cout << "setROIconfig pTime of RIGHT_ROI2 : " << m_profiler[RIGHT_ROI2].timeSetConfig.avgTime() << endl;
	//	cout << "getIPM pTime of RIGHT_ROI2 : " << m_profiler[RIGHT_ROI2].timeGetIPM.avgTime() << endl;
	//	cout << "FilterLinesIPM pTime of RIGHT_ROI2 : " << m_profiler[RIGHT_ROI2].timeFilterLinesIPM.avgTime() << endl;
	//	cout << "GetLinesIPM pTime of RIGHT_ROI2 : " << m_profiler[RIGHT_ROI2].timeGetLinesIPM.avgTime() << endl;
	//	cout << "LineFitting pTime of RIGHT_ROI2 : " << m_profiler[RIGHT_ROI2].timeLineFitting.avgTime() << endl;
	//	cout << "IPM2ImLines pTime of RIGHT_ROI2 : " << m_profiler[RIGHT_ROI2].timeIPM2ImLines.avgTime() << endl << endl;
	//
	//	//cout << "setROIconfig pTime of LEFT_ROI3 : " << m_profiler[LEFT_ROI3].timeSetConfig.avgTime() << endl;
	//	cout << "getIPM pTime of LEFT_ROI3 : " << m_profiler[LEFT_ROI2].timeGetIPM.avgTime() << endl;
	//	cout << "FilterLinesIPM pTime of LEFT_ROI3 : " << m_profiler[LEFT_ROI3].timeFilterLinesIPM.avgTime() << endl;
	//	cout << "GetLinesIPM pTime of LEFT_ROI3 : " << m_profiler[LEFT_ROI3].timeGetLinesIPM.avgTime() << endl;
	//	cout << "LineFitting pTime of LEFT_ROI3 : " << m_profiler[LEFT_ROI3].timeLineFitting.avgTime() << endl;
	//	cout << "IPM2ImLines pTime of LEFT_ROI3 : " << m_profiler[LEFT_ROI3].timeIPM2ImLines.avgTime() << endl << endl;
	//
	//	//cout << "setROIconfig pTime of RIGHT_ROI3 : " << m_profiler[RIGHT_ROI3].timeSetConfig.avgTime() << endl;
	//	cout << "getIPM pTime of RIGHT_ROI3 : " << m_profiler[RIGHT_ROI3].timeGetIPM.avgTime() << endl;
	//	cout << "FilterLinesIPM pTime of RIGHT_ROI3 : " << m_profiler[RIGHT_ROI3].timeFilterLinesIPM.avgTime() << endl;
	//	cout << "GetLinesIPM pTime of RIGHT_ROI3 : " << m_profiler[RIGHT_ROI3].timeGetLinesIPM.avgTime() << endl;
	//	cout << "LineFitting pTime of RIGHT_ROI3 : " << m_profiler[RIGHT_ROI3].timeLineFitting.avgTime() << endl;
	//	cout << "IPM2ImLines pTime of RIGHT_ROI3 : " << m_profiler[RIGHT_ROI3].timeIPM2ImLines.avgTime() << endl << endl;
	//
	//
	//	cout << "lane Detection pTime of LEFT_ROI2 : " << m_profiler[LEFT_ROI2].timeLaneDetection.avgTime() << endl;
	//	cout << "lane Detection pTime of LEFT_ROI3 : " << m_profiler[LEFT_ROI3].timeLaneDetection.avgTime() << endl;
	//
	//	cout << "lane Detection pTime of RIGHT_ROI2 : " << m_profiler[RIGHT_ROI2].timeLaneDetection.avgTime() << endl;
	//	cout << "lane Detection pTime of RIGHT_ROI3 : " << m_profiler[RIGHT_ROI3].timeLaneDetection.avgTime() << endl;
	//
	//
	//
	//#if _ADD_ROI_
	//	cout << "setROIconfig pTime of LEFT_ROI0 : " << m_profiler[LEFT_ROI0].timeSetConfig.avgTime() << endl;
	//	cout << "getIPM pTime of LEFT_ROI0 : " << m_profiler[LEFT_ROI0].timeGetIPM.avgTime() << endl;
	//	cout << "FilterLinesIPM pTime of LEFT_ROI0 : " << m_profiler[LEFT_ROI0].timeFilterLinesIPM.avgTime() << endl;
	//	cout << "GetLinesIPM pTime of LEFT_ROI0 : " << m_profiler[LEFT_ROI0].timeGetLinesIPM.avgTime() << endl;
	//	cout << "LineFitting pTime of LEFT_ROI0 : " << m_profiler[LEFT_ROI0].timeLineFitting.avgTime() << endl;
	//	cout << "IPM2ImLines pTime of LEFT_ROI0 : " << m_profiler[LEFT_ROI0].timeIPM2ImLines.avgTime() << endl << endl;
	//
	//	cout << "setROIconfig pTime of RIGHT_ROI0 : " << m_profiler[RIGHT_ROI0].timeSetConfig.avgTime() << endl;
	//	cout << "getIPM pTime of RIGHT_ROI0 : " << m_profiler[RIGHT_ROI0].timeGetIPM.avgTime() << endl;
	//	cout << "FilterLinesIPM pTime of RIGHT_ROI0 : " << m_profiler[RIGHT_ROI0].timeFilterLinesIPM.avgTime() << endl;
	//	cout << "GetLinesIPM pTime of RIGHT_ROI0 : " << m_profiler[RIGHT_ROI0].timeGetLinesIPM.avgTime() << endl;
	//	cout << "LineFitting pTime of RIGHT_ROI0 : " << m_profiler[RIGHT_ROI0].timeLineFitting.avgTime() << endl;
	//	cout << "IPM2ImLines pTime of RIGHT_ROI0 : " << m_profiler[RIGHT_ROI0].timeIPM2ImLines.avgTime() << endl << endl;
	//
	//
	//
	//	cout << "lane Detection pTime of LEFT_ROI0 : " << m_profiler[LEFT_ROI0].timeLaneDetection.avgTime() << endl;
	//	cout << "lane Detection pTime of RIGHT_ROI0 : " << m_profiler[RIGHT_ROI0].timeLaneDetection.avgTime() << endl;
	//
	//#endif
	//
	//	cout << "PreProcess pTime in hiCheckTimer:" << m_timePreProcess.avgTime() << endl;
	//	cout << "StartDetection pTime in hiCheckTimer:" << m_timeStartDetection.avgTime() << endl;
	//	cout << "DetermineLDWS pTime in hiCheckeTimer:" << m_timeDetermineLDWS.avgTime() << endl;
	//	cout << "tracking pTime in hiCheckTimer:" << m_timeTracking.avgTime() << endl;
	//	cout << "PostProcessing time in hiCheckerTimer:" << m_timePostProcessing.avgTime() << endl;
	//	cout << "Display pTime : in hiCheckerTimer:" << m_timeDisplayResult.avgTime() << endl;
	//
	//	cout << "Average processing time from hiCheckTimer :" << m_timeTotal.avgTime() << endl;
}
//
void CMultiROILaneDetection::Detection(Mat &srcImg)
{
	//m_timeTotal.timeStart();

	PreProcess(srcImg);

	if (!StartDetection())
	{
		cout << "\nThere is no images in StartDeTection:" << __LINE__ << " line" << endl;
		return;
	}

	PostDetection();

#ifdef _KALMAN_
	StartTracking();
	DetermineLDWS();
	PostProcessing();
#endif


	//m_timeTotal.timeEnd();

	ReleaseDetection(); //[LYW_151216] : 이게 여기에 있으면 안될 것 같애
}

void CMultiROILaneDetection::Detection(Mat &srcImg, Mat &imgFree)
{

	//[LYW_160602] : check with free space
	if (!imgFree.empty())
		CheckFreespace(imgFree);
	else
	{
		m_bROISwitch[LEFT_ROI2] = true;
		m_bROISwitch[LEFT_ROI3] = true;
		m_bROISwitch[RIGHT_ROI2] = true;
		m_bROISwitch[RIGHT_ROI3] = true;
	}
	PreProcess(srcImg);
	if (!StartDetection())
	{
		cout << "\nThere is no images in StartDeTection:" << __LINE__ << " line" << endl;
		return;
	}

	PostDetection();

#ifdef _KALMAN_
	StartTracking();
	DetermineLDWS();
	PostProcessing();
#endif



	
	ReleaseDetection(); //[LYW_151216] : 이게 여기에 있으면 안될 것 같애
}

void CMultiROILaneDetection::CheckFreespace(Mat &imgFree)
{
	CheckROIWithFreeSpace(imgFree, LEFT_ROI2);
	CheckROIWithFreeSpace(imgFree, RIGHT_ROI2);
	CheckROIWithFreeSpace(imgFree, LEFT_ROI3);
	CheckROIWithFreeSpace(imgFree, RIGHT_ROI3);

#if _ADD_ROI_
	CheckROIWithFreeSpace(imgFree, LEFT_ROI0);
	CheckROIWithFreeSpace(imgFree, RIGHT_ROI0);
#endif



}
/**
@brief freeSpace를 받아서 바이너리 이미지를 만든 후 ROI영상과 겹치는 비율이 50%이상만 검출하게끔 안될 경우, 아예 detection이 넘어가지 않도록 플래그를 걸었음
@return void
@warning
@author 이영완
@date 2016.06.03.
*/
void CMultiROILaneDetection::CheckROIWithFreeSpace(Mat &imgFree, EROINUMBER nROIFlag)
{
	Mat matRoi = imgFree(Rect(m_sRoiInfo[LEFT_ROI0].nLeft, m_sRoiInfo[LEFT_ROI0].nTop,
		m_sRoiInfo[LEFT_ROI0].sizeRoi.width, m_sRoiInfo[LEFT_ROI0].sizeRoi.height));
	Mat matThres;

	double dRatio = 0;
	int nArea = m_sRoiInfo[LEFT_ROI0].sizeRoi.width*m_sRoiInfo[LEFT_ROI0].sizeRoi.height;
	
	threshold(matRoi, matThres, 1, 1, THRESH_BINARY);
	Scalar sum = cv::sum(matThres);
	
	dRatio = sum(0) / (double)nArea;

	
	if (dRatio > 0.4)
	{
		m_bROISwitch[nROIFlag] = true;
		//cout << "Ratio : " << dRatio << endl;
	}
	else
	{
		//cout << "[LD] Region reject from free space(dRatio):" << dRatio<<endl;
		m_bROISwitch[nROIFlag] = false;
	}
}


/**
@brief Detection 후 Tracking하기 전에 하는 후처리. road writings제거. 현재까지 주행차선에만 적용 Inter-ROI post processing
@brief 위 ROI의 x좌표와 아래 ROI의 x좌표를 비교하여 예외처리 ( 위.x > 아래.x )
@return void
@warning
@author 이영완
@date 2016.02.24.
*/
void CMultiROILaneDetection::PostDetection()
{

	//LEFT ROI 
	if (m_lanesResult[LEFT_ROI2].size() != 0 && m_lanesResult[LEFT_ROI3].size() != 0)
	{
		Point ptUvLeftUpSt = TransformPointGround2Image(m_lanesGroundResult[LEFT_ROI2][0].ptStartLine);
		Point ptUvLeftUpEnd = TransformPointGround2Image(m_lanesGroundResult[LEFT_ROI2][0].ptEndLine);
		float PtUvLeftUpCenterX = (ptUvLeftUpSt.x + ptUvLeftUpEnd.x) / 2;

		Point ptUvLeftDnSt = TransformPointGround2Image(m_lanesGroundResult[LEFT_ROI3][0].ptStartLine);
		Point ptUvLeftDnEnd = TransformPointGround2Image(m_lanesGroundResult[LEFT_ROI3][0].ptEndLine);
		float PtUvLeftDnCenterX = (ptUvLeftDnSt.x + ptUvLeftDnEnd.x) / 2;

		if (PtUvLeftUpCenterX < PtUvLeftDnCenterX)
		{
			printf("[Exception] : false alram Left in PostDetection \n");
			ClearResultVector(LEFT_ROI2);

		}
	}

	if (m_lanesResult[RIGHT_ROI2].size() != 0 && m_lanesResult[RIGHT_ROI3].size() != 0)
	{
		Point ptUvRightUpSt = TransformPointGround2Image(m_lanesGroundResult[RIGHT_ROI2][0].ptStartLine);
		Point ptUvRightUpEnd = TransformPointGround2Image(m_lanesGroundResult[RIGHT_ROI2][0].ptEndLine);
		float PtUvRightUpCenterX = (ptUvRightUpSt.x + ptUvRightUpEnd.x) / 2;

		Point ptUvRightDnSt = TransformPointGround2Image(m_lanesGroundResult[RIGHT_ROI3][0].ptStartLine);
		Point ptUvRightDnEnd = TransformPointGround2Image(m_lanesGroundResult[RIGHT_ROI3][0].ptEndLine);
		float PtUvRightnCenterX = (ptUvRightDnSt.x + ptUvRightDnEnd.x) / 2;

		if (PtUvRightUpCenterX > PtUvRightnCenterX)
		{
			printf("[Exception] : false alram Right in PostDetection\n");
			ClearResultVector(RIGHT_ROI2);
		}
	}


}


