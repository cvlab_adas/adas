#pragma once

#define _CRT_SECURE_NO_WARNINGS

#if defined _MSC_VER || defined __BORLANDC__
typedef __int64 int64;
typedef unsigned __int64 uint64;
#else
typedef int64_t int64;
typedef uint64_t uint64;
#endif

#if defined WIN32 || defined _WIN32 || defined WINCE
#include <windows.h>
#undef small
#undef min
#undef max
#undef abs
#include <tchar.h>
#else //defined WIN32 || defined _WIN32 || defined WINCE
#include <pthread.h>
#include <sys/time.h>
#include <time.h>

#if defined __MACH__ && defined __APPLE__
#include <mach/mach.h>
#include <mach/mach_time.h>
#endif
#endif //defined WIN32 || defined _WIN32 || defined WINCE

#include <sstream>

#include <ratio>
typedef std::ratio<1, 1> sec;
typedef std::milli msec;
typedef std::micro usec;

template<typename _U>
class CTimeCheckerP
{
public:
	CTimeCheckerP() {};
	~CTimeCheckerP() {};

	double tic()
	{
		m_dTicTime = (double)this->getTickCount();
		return m_dTicTime;
	}
	double toc()
	{
		m_dTocTime = (double)this->getTickCount();
		m_dDuration = m_dTocTime - m_dTicTime;
		return m_dTocTime;
	}
	std::string to_string(unsigned int dgt = 6)
	{
		std::stringstream ss;
		ss.setf(std::ios::fixed);
		ss.precision(dgt);
		ss << (m_dDuration / this->getTickFrequency()) * _U::den;
		return ss.str();
	}
	template<typename _O> _O to_()
	{
		return (_O)((m_dDuration / this->getTickFrequency()) * _U::den);
	}

	double m_dDuration = 0;

protected:
	double m_dTicTime = 0;
	double m_dTocTime = 0;

	int64 getTickCount();
	double getTickFrequency();
};

template<typename _U>
inline int64 CTimeCheckerP<_U>::getTickCount()
{
#if defined WIN32 || defined _WIN32 || defined WINCE
	LARGE_INTEGER counter;
	QueryPerformanceCounter(&counter);
	return (int64)counter.QuadPart;
#elif defined __linux || defined __linux__
	struct timespec tp;
	clock_gettime(CLOCK_MONOTONIC, &tp);
	return (int64)tp.tv_sec * 1000000000 + tp.tv_nsec;
#elif defined __MACH__ && defined __APPLE__
	return (int64)mach_absolute_time();
#else
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv, &tz);
	return (int64)tv.tv_sec * 1000000 + tv.tv_usec;
#endif
}

template<typename _U>
inline double CTimeCheckerP<_U>::getTickFrequency()
{
#if defined WIN32 || defined _WIN32 || defined WINCE
	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);
	return (double)freq.QuadPart;
#elif defined __linux || defined __linux__
	return 1e9;
#elif defined __MACH__ && defined __APPLE__
	static double freq = 0;
	if (freq == 0)
	{
		mach_timebase_info_data_t sTimebaseInfo;
		mach_timebase_info(&sTimebaseInfo);
		freq = sTimebaseInfo.denom*1e9 / sTimebaseInfo.numer;
	}
	return freq;
#else
	return 1e6;
#endif
}

template<typename _U>
class CTimeChecker_ : public CTimeCheckerP<_U>
{
public:
	CTimeChecker_() { reset(); }
	~CTimeChecker_() {}

	void reset() { m_cnt = 0; avg.m_dDuration = 0; }

	//override
	double tic()
	{
		m_dTicTime = (double)this->getTickCount();
		return m_dTicTime;
	}

	//override
	double toc()
	{
		m_dTocTime = (double)this->getTickCount();
		m_dDuration = m_dTocTime - m_dTicTime;

		m_cnt++;
		avg.m_dDuration = ((m_cnt - 1) / (double)m_cnt)*avg.m_dDuration + (1 / (double)m_cnt)*this->m_dDuration;

		return m_dTocTime;
	}

	CTimeCheckerP<_U> avg;

private:
	unsigned int m_cnt = 0;
};

typedef CTimeChecker_<msec> CTimeChecker;
