#include "TimeChecker.h"

void f(const int a)
{
	long long sum = 0;
	for (long long i = 1; i <= 100000*a; i++)
		sum += i;

	printf("sum %lld\n", sum);
}

int main()
{
	CTimeChecker tc; // same as ---> CTimeChecker_<msec> tc;

	printf("======Each Loop Processing Time======\n");
	for (int i = 0; i < 10; i++)
	{
		tc.tic();
		f(i);
		tc.toc();

		printf("%s\n", tc.to_string().c_str());
		printf("%s\n", tc.to_string(2).c_str());
		printf("%d\n", tc.to_<int>());
		printf("%f\n", tc.to_<float>());
		printf("%lf\n\n", tc.to_<double>());
	}

	printf("======Average Processing Time======\n");
	printf("%s\n", tc.avg.to_string().c_str());
	printf("%s\n", tc.avg.to_string(2).c_str());
	printf("%d\n", tc.avg.to_<int>());
	printf("%f\n", tc.avg.to_<float>());
	printf("%lf\n\n", tc.avg.to_<double>());

	return 0;
}