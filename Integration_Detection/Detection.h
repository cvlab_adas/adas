/**
@file Detection.h
@date 2016/05/25
@author ���°�(tkwoo@inha.edu) Inha Univ.
@brief
*/
#pragma once

#include "../include/DefStruct.h"
#include "VisionBrain.h"

#include "../UtilDatasetLoader/DatasetLoader.h"
#include "../LibStereo/StereoVisionForADAS.h"
#include "../LibLane/CMultiROILaneDetection.h"
#include "../LibPedestrian/IntegrationPD.h"
#include "../LibVehicle/IntegrationVD.h"

#ifdef _DEBUG
#pragma comment(lib, "../Debug/UtilDatasetLoader.lib")
#pragma comment(lib, "../Debug/LibStereo.lib")
#pragma comment(lib, "../Debug/LibLane.lib")
#pragma comment(lib, "../Debug/LibPedestrian.lib")
#pragma comment(lib, "../Debug/LibVehicle.lib")
#else
#pragma comment(lib, "../Release/UtilDatasetLoader.lib")
#pragma comment(lib, "../Release/LibStereo.lib")
#pragma comment(lib, "../Release/LibLane.lib")
#pragma comment(lib, "../Release/LibPedestrian.lib")
#pragma comment(lib, "../Release/LibVehicle.lib")
#endif

class CDetection{
private:
	// var
	Mat m_imgLeftInput;
	Mat m_imgRightInput;

	Mat m_imgFreeSpace;
	SLanes m_objLanes;
	double m_dVelocityKph;
	
	StereoCamParam_t m_objStereoParam;

	// function
	
public:
	// output
	vector<Object_t> vecobjObjectResult;

	// module component
	CStereoVisionForADAS objStereoVision;
	CMultiROILaneDetection objLD;
	CIntegrationPD objPD;
	CIntegrationVD objVD;

	CVisionBrain objVisionBrain;

	// function
	CDetection(StereoCamParam_t& objStereoParam, bool& flgOK);
	int DetectObstacle(Mat& imgLeft, Mat& imgRight, double dVeloKph=30.);

	void Display(Mat& imgDisplay, Mat& imgStixelDisplay);
	void DrawLane(Mat& imgDisplay);

};