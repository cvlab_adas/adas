#include "Detection.h"

#include "../UtilDatasetLoader/DatasetLoader.h"
#ifdef _DEBUG
#pragma comment(lib, "../Debug/UtilDatasetLoader.lib")
#else
#pragma comment(lib, "../Release/UtilDatasetLoader.lib")
#endif

int main()
{

	CDatasetCreator dc;
	//if (!dc.LoadConfigFile("Config_daimler.xml"))
	if (!dc.LoadConfigFile("Config.xml"))// Config_kitti0011.xml
		return -1;
	CDatasetLoader* objLoader = dc.Create();

	//
	// SDataFormat는 스테레오 영상과 기타 센서값이 포함되어 있는 구조체
	//
	SDataFormat sData;
	Mat imgDisplay[2];
	double nSize = 2;
	int nDelay = 0;
	double dtime = 0;//time var

	Mat imgLeftInput, imgRightInput;

	StereoCamParam_t objParam = CStereoVisionForADAS::InitStereoParam(KITTI);
	bool flgPedOK = false;

	CDetection objDetection(objParam, flgPedOK);

	int cntframe = 0;
	while (1){
		cntframe++;

		int64 t = getTickCount();
		*objLoader >> sData;

		imgLeftInput = sData.img[0];
		imgRightInput = sData.img[1];

		Mat imgStixel = imgLeftInput.clone();
		Mat imgDisplay = imgLeftInput.clone();

		//line(imgLeftInput, Point(0, objParam.objCamParam.m_nVanishingY), Point(imgLeftInput.cols, objParam.objCamParam.m_nVanishingY), Scalar(0, 0, 0), 3);
		//imshow("original", imgLeftInput);

		objDetection.DetectObstacle(imgLeftInput, imgRightInput, 10.);

		t = getTickCount() - t;
		dtime = t * 1000 / getTickFrequency();
		printf("Detection Time elapsed: %.3fms, [# of Stixels : %4d]\n", dtime, objDetection.objStereoVision.m_vecobjStixels.size());

		objDetection.Display(imgDisplay, imgStixel);

		char chTime[100];
		sprintf(chTime, "Detection : %.3f ms", dtime);
		putText(imgStixel, chTime, Point(5, 15), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0), 2);

		imshow("imgDisplay", imgDisplay);
		imshow("Stixel", imgStixel);

		char key = waitKey(nDelay);
		if (key == 27)
			break;
		else if (key >= '1' && key <= '4') // resize를 조정하기 위해서.
			nSize = (double)(key - '0');
		else if (key == ' ') // play 하고 pause 를 toggle
			nDelay = 1 - nDelay;
		else if (key == ']') // pause하고, 다음 프레임으로 이동
		{
			nDelay = 0;
			(*objLoader)++;
		}
		else if (key == '[') // pause하고, 이전 프레임으로 이동
		{
			nDelay = 0;
			(*objLoader)--;
		}

		if (nDelay != 0) // 만약 play 상태일 때 맨 마지막 프레임에 도달했으면 pause상태로 변경
			(*objLoader)++ ? nDelay = 1 : nDelay = 0;
	}
	return 0;
}