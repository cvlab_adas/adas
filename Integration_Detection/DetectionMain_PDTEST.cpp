#include <iostream>
#include "../include/DefStruct.h"

#include "../LibPedestrian/IntegrationPD.h"
#ifdef _DEBUG
#pragma comment(lib, "../Debug/LibPedestrian.lib")
#else
#pragma comment(lib, "../Release/LibPedestrian.lib")
#endif

using namespace std;

int main()
{
	cout << "hello detection world!" << endl;
	Object_t objObstacle;

	//////////////////////////////////////////////////////////////////////////
	bool flgOK = false;
	CIntegrationPD objPD(flgOK);
	if (!flgOK)
	{
		printf("CIntegrationPD's ctor error %d\n", __LINE__);
		return -1;
	}

	vector<Object_t> IfWeHaveObject_t;
	IfWeHaveObject_t.push_back(objObstacle);

	Mat IfWeHaveAnImage = Mat::zeros(640, 360, CV_8UC3);
	objPD.RunOnce(IfWeHaveAnImage, IfWeHaveObject_t);
	

	//////////////////////////////////////////////////////////////////////////


	return 0;
}
