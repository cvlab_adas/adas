#include "VisionBrain.h"

CVisionBrain::CVisionBrain()
{

}
	
int CVisionBrain::Brain(vector<Object_t>& vecobjPedCandidate, vector<Object_t>& vecobjCarCandidate, SLanes objLane, double dVeloKph)
{
	if (vecobjCarCandidate.size() != vecobjPedCandidate.size()){
		cout << "candidate vector size error!" << endl;
		return -1;
	}

	m_vecobjObstacle.clear();
	m_vecobjPedCandidate = vecobjPedCandidate;
	m_vecobjCarCandidate = vecobjCarCandidate;

	DecideClass();
	CalcRisk(dVeloKph);

	return 0;
}

int CVisionBrain::DecideClass()
{
	for (int i = 0; i < m_vecobjPedCandidate.size(); i++)
	{
		if (m_vecobjCarCandidate[i].nClass == Else && m_vecobjPedCandidate[i].nClass == Else){
			m_vecobjObstacle.push_back(m_vecobjCarCandidate[i]);
			continue;
		}
		else if (m_vecobjPedCandidate[i].dClassScore > m_vecobjCarCandidate[i].dClassScore){
			m_vecobjObstacle.push_back(m_vecobjPedCandidate[i]);
		}
		else if (m_vecobjPedCandidate[i].dClassScore < m_vecobjCarCandidate[i].dClassScore){
			m_vecobjObstacle.push_back(m_vecobjCarCandidate[i]);
		}
		else if (m_vecobjCarCandidate[i].dClassScore == 0 && m_vecobjPedCandidate[i].dClassScore == 0){
			m_vecobjCarCandidate[i].nClass = Else;
			m_vecobjObstacle.push_back(m_vecobjCarCandidate[i]);
		}
		// this 'else' part means each class score is same.
		else
		{
			cout << "Ped and Car score is same. I decide this object into Ped." << endl;
			// but It will add the process using geometry information
			m_vecobjObstacle.push_back(m_vecobjPedCandidate[i]);
		}
	}

	return 0;
}
int CVisionBrain::CalcRisk(double dVeloKph)
{
	for (int i = 0; i < m_vecobjObstacle.size(); i++)
	{
		double dWeight = 1.;
		if (m_vecobjObstacle[i].nClass == Ped || m_vecobjObstacle[i].nClass == Car) dWeight = 2.;
		m_vecobjObstacle[i].dCollisionRisk = dWeight*dVeloKph/m_vecobjObstacle[i].dZ;
	}

	return 0;
}