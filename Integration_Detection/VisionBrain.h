/**
@file VisionBrain.h
@date 2016/05/25
@author ���°�(tkwoo@inha.edu) Inha Univ.
@brief 
*/
#pragma once

#include <iostream>
#include "../include/DefStruct.h"

using namespace std;

class CVisionBrain{
private:
	// input
	vector<Object_t> m_vecobjPedCandidate;
	vector<Object_t> m_vecobjCarCandidate;
	SLanes m_objLane;
	double m_dVelocityKph;

	// fucntion
	int DecideClass();
	int CalcRisk(double dVeloKph);

public:
	// output
	vector<Object_t> m_vecobjObstacle;

	// function
	CVisionBrain();
	int Brain(vector<Object_t>& vecobjPedCandidate, vector<Object_t>& vecobjCarCandidate, SLanes objLane, double dVeloKph);

};