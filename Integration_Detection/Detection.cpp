/**
@file Detection.h
@date 2016/05/25
@author 우태강(tkwoo@inha.edu) Inha Univ.
@brief
*/
#include "Detection.h"

using namespace cv;

CDetection::CDetection(StereoCamParam_t& objStereoParam, bool& flgOK)
	:objStereoVision(objStereoParam),
	objPD(flgOK), 
	objVD(flgOK),
	objLD(objStereoParam.objCamParam.m_sizeSrc.width, 
	objStereoParam.objCamParam.m_sizeSrc.height, 
	- objStereoParam.objCamParam.m_dPitchDeg, 
	objStereoParam.objCamParam.m_dYawDeg,
	objStereoParam.objCamParam.m_dFocalLength,
	objStereoParam.objCamParam.m_dFocalLength,
	objStereoParam.objCamParam.m_dOx,
	objStereoParam.objCamParam.m_dOy,
	1000 * objStereoParam.objCamParam.m_dCameraHeight
	)
{
	m_objStereoParam = objStereoParam;
	
	if (!flgOK)
	{
		printf("ctor error %d\n", __LINE__);
		return;
	}
	objLD.setRoiIpmConfig();
}
int CDetection::DetectObstacle(Mat& imgLeft, Mat& imgRight, double dVeloKph){
	vecobjObjectResult.clear();
	m_objLanes.vecLanes.clear();
	m_objLanes.vecLanesLeft.clear();
	m_objLanes.vecLanesRight.clear();

	//[Stereo] hypothesis detection
	objStereoVision.Objectness(imgLeft, imgRight);
	m_imgFreeSpace = objStereoVision.m_imgGround;

	//[Lane] lane detection
 	objLD.Detection(imgLeft, m_imgFreeSpace);
	objLD.GetLaneResult(m_objLanes.vecLanes);

	

	//[Recognition] ped, car detection
	vector<Object_t> vecobjPedHypothesis = objStereoVision.m_vecobjBB;
	vector<Object_t> vecobjCarHypothesis = objStereoVision.m_vecobjBB;
	objVD.RunOnce(imgLeft, vecobjCarHypothesis, m_objLanes);
	objPD.RunOnce(imgLeft, vecobjPedHypothesis);
	
	//[Brain] brain
	objVisionBrain.Brain(vecobjPedHypothesis, vecobjCarHypothesis, SLanes(), dVeloKph);

	vecobjObjectResult = objVisionBrain.m_vecobjObstacle;

	return 0;
}
void CDetection::Display(Mat& imgDisplay, Mat& imgStixelDisplay){
	//objStereoVision.Display(imgDisplay, imgStixelDisplay);

	Mat imgColorTemp = imgDisplay.clone();
	
	//if (imgStixelDisplay.channels() == 3) cv::cvtColor(imgStixelDisplay, imgStixelDisplay, CV_BGR2GRAY);

	objStereoVision.DrawGround(imgColorTemp, m_imgFreeSpace);
	line(imgColorTemp, Point(0, m_objStereoParam.objCamParam.m_nVanishingY), Point(imgColorTemp.cols, m_objStereoParam.objCamParam.m_nVanishingY), Scalar(255, 255, 255), 3);
	DrawLane(imgColorTemp);
	objStereoVision.DrawStixel(imgColorTemp, objStereoVision.m_vecobjStixels);
	
	//cv::cvtColor(imgStixelDisplay, imgStixelDisplay, CV_GRAY2BGR);
	addWeighted(imgStixelDisplay, 0.4, imgColorTemp, 0.6, 0., imgStixelDisplay);

	for (int i = vecobjObjectResult.size() - 1; i >= 0; i--)
	{
		if (vecobjObjectResult[i].nClass == Ped){
			cv::putText(imgStixelDisplay, "Ped", vecobjObjectResult[i].rectBB.tl() + Point(1, 10), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0, 0, 255), 1);
			rectangle(imgStixelDisplay, vecobjObjectResult[i].rectBB, Scalar(0, 0, 255*vecobjObjectResult[i].dCollisionRisk), 2, 8);
		}
		else if (vecobjObjectResult[i].nClass == Car){
			cv::putText(imgStixelDisplay, "Car", vecobjObjectResult[i].rectBB.tl() + Point(1, 10), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0, 255, 0), 1);
			rectangle(imgStixelDisplay, vecobjObjectResult[i].rectBB, Scalar(0, 255 * vecobjObjectResult[i].dCollisionRisk, 0), 2, 8);
		}
		else if (vecobjObjectResult[i].nClass == Else){
			cv::putText(imgStixelDisplay, "Else", vecobjObjectResult[i].rectBB.tl() + Point(1, 10), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(255, 255, 255), 1);
			rectangle(imgStixelDisplay, vecobjObjectResult[i].rectBB, Scalar::all(255 * vecobjObjectResult[i].dCollisionRisk), 2, 8);
		}
		else
			rectangle(imgStixelDisplay, vecobjObjectResult[i].rectBB, Scalar::all(255), 2, 8);
		
		char chRisk[50];
		sprintf(chRisk, "CR:%.3lf", vecobjObjectResult[i].dCollisionRisk);
		cv::putText(imgStixelDisplay, chRisk, vecobjObjectResult[i].rectBB.tl() + Point(1, 20), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(255, 255, 255), 1);

		char temp[20];
		sprintf_s(temp, sizeof(temp), "%.2fm", vecobjObjectResult[i].dZ);
		cv::putText(imgStixelDisplay, temp, vecobjObjectResult[i].rectBB.br() - Point(vecobjObjectResult[i].rectBB.width, 0), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0, 0, 255), 2);
	}

	for (int i = vecobjObjectResult.size() - 1; i >= 0; i--)
	{
		if (vecobjObjectResult[i].nClass == Ped){
			cv::putText(imgDisplay, "Ped", vecobjObjectResult[i].rectBB.tl() + Point(1, 10), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0, 0, 255), 1);
			rectangle(imgDisplay, vecobjObjectResult[i].rectBB, Scalar(0, 0, 255 * vecobjObjectResult[i].dCollisionRisk), 2, 8);
		}
		else if (vecobjObjectResult[i].nClass == Car){
			cv::putText(imgDisplay, "Car", vecobjObjectResult[i].rectBB.tl() + Point(1, 10), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0, 255, 0), 1);
			rectangle(imgDisplay, vecobjObjectResult[i].rectBB, Scalar(0, 255 * vecobjObjectResult[i].dCollisionRisk, 0), 2, 8);
		}
		else if (vecobjObjectResult[i].nClass == Else){
			cv::putText(imgDisplay, "Else", vecobjObjectResult[i].rectBB.tl() + Point(1, 10), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(255, 255, 255), 1);
			rectangle(imgDisplay, vecobjObjectResult[i].rectBB, Scalar::all(255 * vecobjObjectResult[i].dCollisionRisk), 2, 8);
		}
		else
			rectangle(imgDisplay, vecobjObjectResult[i].rectBB, Scalar::all(255), 2, 8);

		char chRisk[50];
		sprintf(chRisk, "CR:%.3lf", vecobjObjectResult[i].dCollisionRisk);
		cv::putText(imgDisplay, chRisk, vecobjObjectResult[i].rectBB.tl() + Point(1, 20), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(255, 255, 255), 1);

		char temp[20];
		sprintf_s(temp, sizeof(temp), "%.2fm", vecobjObjectResult[i].dZ);
		cv::putText(imgDisplay, temp, vecobjObjectResult[i].rectBB.br() - Point(vecobjObjectResult[i].rectBB.width, 0), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0, 0, 255), 2);
	}
}

void CDetection::DrawLane(Mat& imgDisplay){

	if (m_objLanes.vecLanes.size() != 0){
		//cout << "size : " << m_objLanes.vecLanes.size() << endl;
		m_objLanes.vecLanesLeft.push_back(m_objLanes.vecLanes[0]);
		m_objLanes.vecLanesRight.push_back(m_objLanes.vecLanes[1]);
	}
	else if (m_objLanes.vecLanes.size() == 0 && m_objLanes.vecLanesLeft.size() == 0 && m_objLanes.vecLanesRight.size() == 0){
		return;
	}

	line(imgDisplay, m_objLanes.vecLanesLeft[0].ptUvStartLine, m_objLanes.vecLanesLeft[0].ptUvEndLine, Scalar(255, 255, 255), 6);
	line(imgDisplay, m_objLanes.vecLanesRight[0].ptUvStartLine, m_objLanes.vecLanesRight[0].ptUvEndLine, Scalar(255, 255, 255), 6);

	return;
	
	// 곡선 지원을 위한 코드 현재 실행되지 않음
	Mat matCoeffLeftLane = m_objLanes.GetCurveCoeff(m_objLanes.vecLanesLeft);
	Mat matCoeffRightLane = m_objLanes.GetCurveCoeff(m_objLanes.vecLanesRight);

	for (int i = m_imgLeftInput.rows; i < m_objStereoParam.objCamParam.m_nVanishingY; i--){
		float solvLeft_x = matCoeffLeftLane.at<float>(2)*pow(i, 2) + matCoeffLeftLane.at<float>(1)*i + matCoeffLeftLane.at<float>(0);
		float solvRight_x = matCoeffRightLane.at<float>(2)*pow(i, 2) + matCoeffRightLane.at<float>(1)*i + matCoeffRightLane.at<float>(0);
		circle(imgDisplay, Point((int)solvLeft_x, i), 1, Scalar(0, 255, 255), 1, 8, 0);
		circle(imgDisplay, Point((int)solvRight_x, i), 1, Scalar(0, 255, 255), 1, 8, 0);
	}
	imshow("img", imgDisplay);
}