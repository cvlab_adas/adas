#include "Detection.h"

int main()
{
	Mat imgLeftInput, imgRightInput;
	StereoCamParam_t objParam = CStereoVisionForADAS::InitStereoParam(KITTI);
	bool flgPedOK = false;

	CDetection objDetection(objParam, flgPedOK);

	while (1){
		int64 t = getTickCount();
		imgLeftInput = imread("0000000111_left.png", 1);
		imgRightInput = imread("0000000111_right.png", 1);

		Mat imgStixel = imgLeftInput.clone();
		Mat imgDisplay = imgLeftInput.clone();

		imshow("original", imgLeftInput);

		objDetection.DetectObstacle(imgLeftInput, imgRightInput, 10.);



		t = getTickCount() - t;
		double dtime = t * 1000 / getTickFrequency();
		printf("Detection Time elapsed: %.3fms, [# of Stixels : %4d]\n", dtime, objDetection.objStereoVision.m_vecobjStixels.size());

		objDetection.Display(imgDisplay, imgStixel);

		char chTime[100];
		sprintf(chTime, "Detection : %.3f ms", dtime);
		putText(imgStixel, chTime, Point(5, 15), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0), 2);

		imshow("imgDisplay", imgDisplay);
		imshow("Stixel", imgStixel);

		char key = waitKey(1);
		if (key == 27)
			break;
	}
	return 0;
}