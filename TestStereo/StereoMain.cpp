#include <stdio.h>

#include "../LibStereo/StereoVisionForADAS.h"
#ifdef _DEBUG
#pragma comment(lib, "../Debug/LibStereo.lib")
#else
#pragma comment(lib, "../Release/LibStereo.lib")
#endif

#include "../UtilDatasetLoader/DatasetLoader.h"
#ifdef _DEBUG
#pragma comment(lib, "../Debug/UtilDatasetLoader.lib")
#else
#pragma comment(lib, "../Release/UtilDatasetLoader.lib")
#endif

int main()
{
	//
	// Config.xml 파일을 불러와서 CDatasetLoader를 동적할당 함
	// Config.xml 파일에 쓰여있는 내용으로 KITTI를 사용할건지
	// CVLAB 을 사용할건지 자동으로 결정됨.
	//
	CDatasetCreator dc;
	//if (!dc.LoadConfigFile("Config_daimler.xml"))
	if (!dc.LoadConfigFile("Config_kitti0091.xml"))
		return -1;
	CDatasetLoader* objLoader = dc.Create();

	//
	// SDataFormat는 스테레오 영상과 기타 센서값이 포함되어 있는 구조체
	//
	SDataFormat sData;
	Mat imgDisplay[2];
	double nSize = 2;
	int nDelay = 0;
	double dtime = 0;//time var

	// stixel variable
	Mat imgLeftInput, imgRightInput;
	//Mat imgDisplay, imgDisparity, imgLeftGT;

	//StereoCamParam_t objParam = CStereoVisionForADAS::InitStereoParam(Daimler);
	StereoCamParam_t objParam = CStereoVisionForADAS::InitStereoParam(KITTI);
	CStereoVisionForADAS objStereoVision(objParam);

	Mat imgStixel;

	int cntframe = 0;
	while (1)
	{
		cntframe++;
		int64 t = getTickCount();

		//
		// DB loader가 가리키고 있는 프레임에서 센서값을 추출해 온다.
		//
		*objLoader >> sData;
		// objLoader->m_nVidChannel 에는 스테레오인지 아닌지에 대한 값이 들어있고
		// 이걸 이용해서 sData.img[i] 로 호출하게 되면 로드한 영상에 접근할 수 있다
		// resize는 따라하지 않아도 괜찮음
		// KITTI같은 스테레오의 경우 [0]은 왼쪽 [1]은 오른쪽 영상이다
		//
		imgLeftInput = sData.img[0];
		imgRightInput = sData.img[1];
		imshow("original", imgLeftInput);

		objStereoVision.Objectness(sData.img[0], sData.img[1]);
		
		t = getTickCount() - t;
		dtime = t * 1000 / getTickFrequency();
		printf("SV Time elapsed: %.3fms, [# of Stixels : %4d]\n", dtime, objStereoVision.m_vecobjStixels.size());

		
		Mat imgStixel = sData.img[0].clone();
		Mat imgDisplay = sData.img[0].clone();

		///////////////// another interface for recognition module //////////////
		//float dist;
		//uchar disp = 48;
		//objStereoVision.Disp16ToDepth(disp, dist);
		//cout << "dist : " << dist << endl;

		//Mat arect;
		//Rect rect(100, 200, 50, 50);
		//
		//objStereoVision.RectToDisp(rect, arect);
		////Scalar a = mean(arect);
		//uchar a = arect.at<uchar>(25, 25);
		////cout << a << endl;
		//printf("%u\n", a);
		////cout << (float)arect.at<uchar>(150, 250)/16. << endl;
		//
		///*cout << a[0] << endl;
		//double dt = a[0];*/
		//objStereoVision.Disp16ToDepth((uchar)(a), dist);
		//cout << "hi" << dist << endl;
		////rectangle(imgStixel, rect, Scalar(255, 255, 0), 4);
		//imshow("ddd", arect);
		//

		objStereoVision.Display(imgDisplay, imgStixel);

		char chTime[100];
		sprintf(chTime, "SV : %.3f ms", dtime);
		putText(imgStixel, chTime, Point(5, 15), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0), 2);
		
		imshow("imgLeft", imgDisplay);
		imshow("Stixel", imgStixel);

		for (int i = 0; i < objLoader->m_nVidChannel; i++)
		{
			/*resize(sData.img[i], imgDisplay[i], Size(0, 0), 0.5*nSize, 0.5*nSize);
			char szTemp[30];
			sprintf(szTemp, "imgDisplay%d", i);*/


			/*objStixelEstimation.Display(imgDisplay[0], imgStixel);
			imshow("stixel", imgStixel);
			imshow(szTemp, imgDisplay[i]);*/
		}
		char key = waitKey(nDelay);
		if (key == 27) // ESC 는 종료
			break;
		else if (key >= '1' && key <= '4') // resize를 조정하기 위해서.
			nSize = (double)(key - '0');
		else if (key == ' ') // play 하고 pause 를 toggle
			nDelay = 1 - nDelay;
		else if (key == ']') // pause하고, 다음 프레임으로 이동
		{
			nDelay = 0;
			(*objLoader)++;
		}
		else if (key == '[') // pause하고, 이전 프레임으로 이동
		{
			nDelay = 0;
			(*objLoader)--;
		}

		if (nDelay != 0) // 만약 play 상태일 때 맨 마지막 프레임에 도달했으면 pause상태로 변경
			(*objLoader)++ ? nDelay = 1 : nDelay = 0;
	}

	return 0;
}
