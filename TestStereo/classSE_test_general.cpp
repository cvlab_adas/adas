/**
	@file classSE_test_general.cpp
	@date 2015/11/5
	@author 우태강(tkwoo@vision.inha.ac.kr) Inha Univ.
	@brief CStixelEstimation test를 위한 driver file
*/

#include "../LibStereo/StereoVIsionForADAS.h"
#ifdef _DEBUG
#pragma comment(lib, "../Debug/LibStereo.lib")
#else
#pragma comment(lib, "../Release/LibStereo.lib")
#endif

using namespace std;
using namespace cv;

int main()
{
	StereoCamParam_t objParam = CStereoVisionForADAS::InitStereoParam(Daimler);
	CStereoVisionForADAS objStereoVision(objParam);
	while (1){

		Mat imgLeft = imread("Left_923730u.pgm", 0);
		Mat imgRight = imread("Right_923730u.pgm", 0);

		Mat imgStixel = imgLeft.clone();
		Mat imgDisplay = imgLeft.clone();

		int64 t = getTickCount();
		objStereoVision.Objectness(imgLeft, imgRight);
		printf("Time elapsed: %fms\n", (getTickCount() - t) * 1000 / getTickFrequency());

		objStereoVision.Display(imgDisplay, imgStixel);

		imshow("imgLeft", imgDisplay);
		imshow("Stixel", imgStixel);

		if (waitKey(1) == 27) break;
		//break;
	}
	/*
	#if CV_MAJOR_VERSION==3
	if (CV_VERSION == "3.1.0")
	{
	cout << CV_VERSION << endl;
	}
	#endif*/
	waitKey();


	return 0;
}

