#include <stdio.h>

#include "../UtilDatasetLoader/DatasetLoader.h"
#ifdef _DEBUG
#pragma  comment(lib, "../Debug/UtilDatasetLoader.lib")
#else
#pragma comment(lib,  "../Release/UtilDatasetLoader.lib")
#endif

#define CutIn 0

#if CutIn
#include "../LibVehicle/CRForestDetector_CutIn.h"
#else
#include "../LibVehicle/CRForestDetector.h"
#endif
#ifdef _DEBUG
#pragma comment(lib, "../Debug/LibVehicle.lib")
#else
#pragma comment(lib, "../Release/LibVehicle.lib")
#endif

int main()
{
	printf("This is Vehicle detection project..\n");

	CDatasetCreator dc;
	if (!dc.LoadConfigFile("Config.xml"))
		return -1;
	CDatasetLoader* objLoader = dc.Create();

	SDataFormat sData;
	Mat imgDisplay[2];
	double nSize = 2;
	int nDelay = 0;
	//-------------------------------------------------------------
#if RECORD
	VideoCapture m_CapVideo;

	//////output video open
	//VideoWriter oVideoWriter;
	const string NAME = "video/0084.avi";   // Form the new name with container
	int ex = static_cast<int>(m_CapVideo.get(CV_CAP_PROP_FOURCC));     // Get Codec Type- Int form

	Size S = Size((int)m_CapVideo.get(CV_CAP_PROP_FRAME_WIDTH),    // Acquire input size
		(int)m_CapVideo.get(CV_CAP_PROP_FRAME_HEIGHT));

	VideoWriter outputVideo;                                        // Open the output
	outputVideo.open(NAME, ex, 20, S, true);
	//outputVideo.open(NAME, CV_FOURCC('W', 'M', 'V', '2'), 30, S, true);
	if (!outputVideo.isOpened())
	{
		cout << "Could not open the output video for write: " << endl;
		exit(-1);
	}
#endif
	//-------------------------------------------------------------
	Mat imgInputVideo, imgOutputVideo;

	//string result_sha = "training";

	// crConfig 객체 생성하면서 환경설정 파일 로드 (config.txt)
#if CutIn
	CRConfig crConfig("../LibVehicle/config_cutin.txt");
#else 
	CRConfig crConfig("../LibVehicle/config_kitti2.txt");
#endif

	// crForest 객체 생성하면서 tree 파일들 로드
	CRForest crForest(crConfig);
	CKalman cObjKalman;

#if CutIn
	//CRForestDetector crDetect(crConfig, crForest, cObjKalman);
	CRForestDetectorCI crDetectCI(crConfig, crForest, cObjKalman);
#else
	CRForestDetector crDetect(crConfig, crForest, cObjKalman);
#endif

#if EVALUATE
	// load GT
	crDetect.LoadKITTIGT("../LibVehicle/KITTI_train_obj.txt"); 
	int nIdx = 0;

#endif

#if TimeCheck
	double dAverageTime = 0;
#endif

	nDelay = 1;

	while (1){

		Mat imgInputVideo, imgOutputVideo;

#if TimeCheck
		double dTimeBegin = cvGetTickCount();
#endif

		*objLoader >> sData;
		imgInputVideo = sData.img[0].clone();
		imgOutputVideo = sData.img[0].clone();

#if CutIn
		//// HanYang DB
		//Rect rectLeft = Rect(0, 0, imgInputVideo.cols / 2, imgInputVideo.rows);
		//imgInputVideo = imgInputVideo(rectLeft);
		//imgOutputVideo = imgInputVideo(rectLeft);

		// 차량 검출 시작 1차( 입력: 영상 )
		crDetectCI.DetectVehicle(imgInputVideo);
		// discriminative Houg map 그리기

		// 차량 검출 결과 출력
		crDetectCI.ShowVehicles(imgOutputVideo);

#else
		// 차량 검출 시작 1차( 입력: 영상 )
		crDetect.DetectVehicle(imgInputVideo);

		// 차량 검출 결과 출력
		crDetect.ShowVehicles(imgOutputVideo);

#if RECORD
		if (crDetect.nFrameIdx == 382)
			break;
		outputVideo.write(imgOutputVideo);
#endif

#if EVALUATE 
		crDetect.DrawKITTIGT(nIdx, imgOutputVideo);
		crDetect.EvaluatePerformance(crDetect.vec_rectCandidates, nIdx, crDetect.outDetection);
		//imshow("result",imgOutputVideo);
		nIdx++;
#endif
#endif
		
#if TimeCheck
		double dTimeEnd = cvGetTickCount();
		double dTestTime = (double)(0.001 * (dTimeEnd - dTimeBegin) / cvGetTickFrequency());
		printf("[time]: %3.2f msec\n", dTestTime);
		dAverageTime += dTestTime;
#endif

#if Debug
		imshow("result", imgOutputVideo);
		// 버퍼 메모리 제거
#if Cutin
		crDetectCI.ClearMemory();
#else
		crDetect.ClearMemory();
#endif
#endif
		char key = waitKey(nDelay);
		if (key == 27) // ESC 는 종료
			break;

#if EVALUATE 
#else 
		else if (key >= '1' && key <= '4') // resize를 조정하기 위해서.
			nSize = (double)(key - '0');
		else if (key == ' ') // play 하고 pause 를 toggle
			nDelay = 1 - nDelay;
		else if (key == ']') // pause하고, 다음 프레임으로 이동
		{
			nDelay = 0;
			(*objLoader)++;
		}
		else if (key == '[') // pause하고, 이전 프레임으로 이동
		{
			nDelay = 0;
			(*objLoader)--;
		}

		if (nDelay != 0) // 만약 play 상태일 때 맨 마지막 프레임에 도달했으면 pause상태로 변경
			(*objLoader)++ ? nDelay = 1 : nDelay = 0;
#endif
	}
#if RECORD
	outputVideo.release();
#endif

#if TimeCheck
#if CutIn
	dAverageTime = dAverageTime / (double)(crDetectCI.nFrameIdx + 1);
#else
	dAverageTime = dAverageTime / (double)(crDetect.nFrameIdx + 1);
#endif
	
#endif
#if EVALUATE
	crDetect.PrintEvaluateResult(crDetect.outDetection);
#endif

	return 0;
}