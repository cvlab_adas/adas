#pragma once

#include "opencv2/opencv.hpp"

using namespace std;
using namespace cv;

class CKF_Tracker
{
public:
	CKF_Tracker();
	~CKF_Tracker();

	struct STrackInfo
	{
		unsigned int nID;
		unsigned int cntAge;
		unsigned int cntTotalVisible;
		unsigned int cntConsecutiveInvisible;
		Rect_<float> rectBB;
		Point2f ptCentroid;
		KalmanFilter objKF;
	};

	void Track(const vector<Rect_<int> > &rectDetectBB);
	void DrawResult(Mat &imgDisplay);

private:
	void PredictNewLocationOfTracks();
	void detectionToTrackAssignment(const vector<Rect_<int> > &rectDetectBB);
	void updateAssignedTracks(const vector<Rect_<int> > &rectDetectBB);
	void updateUnassignedTracks();
	void deleteLostTracks();
	void createNewTracks(const vector<Rect_<int> > &rectDetectBB);
	void configureKalmanFilter(KalmanFilter &KF, string strModelType, Point2f centroid);
	vector<STrackInfo> m_vecTracks;
	vector<pair<int, int> > m_vecAssignments;
	vector<int> m_vecUnassignedTracks;
	vector<int> m_vecUnassignedDetections;
	unsigned int m_nextID;

	double m_dist_thres;

	int m_dimState;
	Mat m_matTransition; // A
	Mat m_matMeasurement; // H
	Mat m_matProcessNoiseCov; // Q
	Mat m_matMeasureNoiseCov; // R
};

