#include "KF_Tracker.h"
#include "HungarianAlg.h"

CKF_Tracker::CKF_Tracker()
{
	m_dist_thres = 60.0;

	m_nextID = 0;

	m_dimState = 4;

}

CKF_Tracker::~CKF_Tracker()
{
}

void CKF_Tracker::Track(const vector<Rect_<int> > &rectDetectBB)
{
	PredictNewLocationOfTracks();

	detectionToTrackAssignment(rectDetectBB);

	updateAssignedTracks(rectDetectBB);

	updateUnassignedTracks();

	deleteLostTracks();

	createNewTracks(rectDetectBB);
}

void CKF_Tracker::DrawResult(Mat &imgDisplay)
{
	int minVisibleCount = 8;

	for (int i = 0; i < (int)m_vecTracks.size(); i++)
	{
		if (m_vecTracks[i].cntTotalVisible <= minVisibleCount)
			continue;

		rectangle(imgDisplay, m_vecTracks[i].rectBB, CV_RGB(255, 255, 0));

		char szTemp[100];
		sprintf(szTemp, "ID:%d", m_vecTracks[i].nID);
		if (m_vecTracks[i].cntConsecutiveInvisible > 0)
			sprintf(szTemp, "%s Predict", szTemp);

		putText(imgDisplay, szTemp, Point2f(m_vecTracks[i].rectBB.x, m_vecTracks[i].rectBB.y), CV_FONT_HERSHEY_COMPLEX, 1, CV_RGB(255, 255, 0));
	}
}

void CKF_Tracker::PredictNewLocationOfTracks()
{
	for (auto track : m_vecTracks)
	{
		Rect_<float> &r = track.rectBB;

		track.objKF.predict();

		track.ptCentroid.x = track.objKF.statePre.at<float>(0);
		track.ptCentroid.y = track.objKF.statePre.at<float>(m_dimState / 2);

		r.x = track.ptCentroid.x - r.width / 2.f;
		r.y = track.ptCentroid.y - r.height / 2.f;
	}
}

void CKF_Tracker::detectionToTrackAssignment(const vector<Rect_<int> > &rectDetectBB)
{
	int N = (int)m_vecTracks.size();
	int M = (int)rectDetectBB.size();

	vector<vector<double> > Cost(N, vector<double>(M));

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++)
		{
			Point2f cent(rectDetectBB[j].x + rectDetectBB[j].width / 2.f, rectDetectBB[j].y + rectDetectBB[j].height / 2.f);
			Point2f diff = (m_vecTracks[i].ptCentroid - cent);
			double dist = norm(diff);
			Cost[i][j] = dist;
		}
	}

	vector<int> assignment;
	if (N != 0)
	{
		AssignmentProblemSolver APS;
		APS.Solve(Cost, assignment, AssignmentProblemSolver::optimal);
	}
	
	m_vecAssignments.clear();
	m_vecUnassignedTracks.clear();
	m_vecUnassignedDetections.clear();

	for (int i = 0; i < (int)assignment.size(); i++)
	{
		if (assignment[i] != -1)
		{
			if (Cost[i][assignment[i]] <= m_dist_thres)
				m_vecAssignments.push_back(pair<int, int>(i, assignment[i]));
			else
				m_vecUnassignedTracks.push_back(i);
		}
	}

	for (int i = 0; i < (int)rectDetectBB.size(); i++)
	{
		auto it = find(assignment.begin(), assignment.end(), i);
		if (it == assignment.end())
			m_vecUnassignedDetections.push_back(i);
	}
}

void CKF_Tracker::updateAssignedTracks(const vector<Rect_<int> > &rectDetectBB)
{
	for (int i = 0; i < (int)m_vecAssignments.size(); i++)
	{
		int idxT = m_vecAssignments[i].first;
		int idxD = m_vecAssignments[i].second;

		Mat meas = Mat::zeros(2, 1, CV_32FC1);
		meas.at<float>(0) = rectDetectBB[idxD].x + rectDetectBB[idxD].width / 2.f;
		meas.at<float>(1) = rectDetectBB[idxD].y + rectDetectBB[idxD].height / 2.f;

		m_vecTracks[idxT].objKF.correct(meas);

		m_vecTracks[idxT].rectBB = rectDetectBB[idxD];

		m_vecTracks[idxT].cntAge++;
		m_vecTracks[idxT].cntTotalVisible++;
		m_vecTracks[idxT].cntConsecutiveInvisible = 0;
	}
}

void CKF_Tracker::updateUnassignedTracks()
{
	for (int i = 0; i < (int)m_vecUnassignedTracks.size(); i++)
	{
		int idx = m_vecUnassignedTracks[i];

		m_vecTracks[idx].cntAge++;
		m_vecTracks[idx].cntConsecutiveInvisible++;
	}
}

void CKF_Tracker::deleteLostTracks()
{
	if ((int)m_vecTracks.size() == 0)
		return;

	unsigned int invisibleForTooLong = 5;
	unsigned int ageThreshold = 8;

	for (int i = 0; i < (int)m_vecTracks.size(); i++)
	{
		float visibility = (float)m_vecTracks[i].cntTotalVisible / (float)m_vecTracks[i].cntAge;

		if ((m_vecTracks[i].cntAge < ageThreshold && visibility < 0.6f) || m_vecTracks[i].cntConsecutiveInvisible >= invisibleForTooLong)
		{
			m_vecTracks.erase(m_vecTracks.begin() + i);
			i--;
		}
	}
}

void CKF_Tracker::createNewTracks(const vector<Rect_<int> > &rectDetectBB)
{
	for (int i = 0; i < (int)m_vecUnassignedDetections.size(); i++)
	{
		int j = m_vecUnassignedDetections[i];

		STrackInfo ti;

		ti.ptCentroid = Point2f(rectDetectBB[j].x + rectDetectBB[j].width / 2.f, rectDetectBB[j].y + rectDetectBB[j].height / 2.f);

		ti.rectBB = rectDetectBB[j];

		configureKalmanFilter(ti.objKF, "ConstantVelocity", ti.ptCentroid);

		ti.cntAge = 1;
		ti.cntTotalVisible = 1;
		ti.cntConsecutiveInvisible = 0;
		ti.nID = m_nextID++;

		m_vecTracks.push_back(ti);
	}
}

void CKF_Tracker::configureKalmanFilter(KalmanFilter &KF, string strModelType, Point2f centroid)
{
	int nSubDim;
	if (strModelType == "ConstantAcceleration")
		nSubDim = 3;
	else
		nSubDim = 2;

	Mat tempA = Mat::eye(nSubDim, nSubDim, CV_32FC1);
	for (int i = 0; i < nSubDim - 1; i++) tempA.at<float>(i, i + 1) = 1;

	Mat tempH = Mat::zeros(1, nSubDim, CV_32FC1);
	tempH.at<float>(0, 0) = 1;

	float Q[2] = { 100, 25 };
	Mat tempQ(m_dimState, 1, CV_32FC1);
	for (int i = 0; i < m_dimState; i++)
		tempQ.at<float>(i) = Q[i % 2];

	float R = 100;
	Mat tempR(2, 1, CV_32FC1, R);

	float cov[2] = { 200, 50 };
	Mat tempCov(m_dimState, 1, CV_32FC1);
	for (int i = 0; i < m_dimState; i++)
		tempCov.at<float>(i) = cov[i % 2];

	// KF
	KF.init(m_dimState, 2);

	tempA.copyTo(KF.transitionMatrix(Range(0, nSubDim), Range(0, nSubDim)));
	tempA.copyTo(KF.transitionMatrix(Range(nSubDim, m_dimState), Range(nSubDim, m_dimState)));

	tempH.copyTo(KF.measurementMatrix(Range(0, 1), Range(0, nSubDim)));
	tempH.copyTo(KF.measurementMatrix(Range(1, 2), Range(nSubDim, m_dimState)));

	KF.processNoiseCov = Mat::diag(tempQ);
	KF.measurementNoiseCov = Mat::diag(tempR);

	KF.errorCovPost = Mat::diag(tempCov);

	KF.statePost.at<float>(0) = centroid.x;
	KF.statePost.at<float>(m_dimState / 2) = centroid.y;
}