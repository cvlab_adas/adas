#define _CRT_SECURE_NO_WARNINGS

#include "../UtilTimeChecker/TimeChecker.h"
#include "../UtilDatasetLoader/DatasetLoader.h"
#ifdef _DEBUG
#pragma comment(lib, "../Debug/UtilDatasetLoader.lib")
#else
#pragma comment(lib, "../Release/UtilDatasetLoader.lib")
#endif

#include "../LibPedestrian/PedestrianACF2.h"
#ifdef _DEBUG
#pragma comment(lib, "../Debug/LibPedestrian.lib")
#else
#pragma comment(lib, "../Release/LibPedestrian.lib")
#endif

#include "KF_Tracker.h"

int main()
{
	CDatasetCreator dc;
	if (!dc.LoadConfigFile("Config.xml")) return -1;
	CDatasetLoader* objLoader = dc.Create();
	if (!objLoader) return -1;

	SDataFormat sData;
	Mat imgInput;
	Mat imgDisplay;
	double nSize = 2;
	int nDelay = 1;

	CTimeChecker tc;
	CTimeChecker tc2;

	CPedestrianACF2 objPD;
	if (!objPD.LoadClassifier("../LibPedestrian/ACF_Caltech+.txt")) return -1;

	int cnt = 0;

	CKF_Tracker tracker;

	while (1)
	{
		*objLoader >> sData;

		//imgInput = sData.img[0];
		resize(sData.img[0], imgInput, Size(0, 0), 0.5*nSize, 0.5*nSize);

		imgDisplay = imgInput.clone();

		tc.tic();
		objPD.Detect(imgInput);
		tc.toc();
		objPD.DrawResult(imgDisplay, CV_RGB(255, 0, 0));

		putText(imgDisplay, "C++ " + tc.to_string(2) + "ms", Point(10, 60), FONT_HERSHEY_COMPLEX, 0.9, CV_RGB(255, 0, 255), 2);
		imshow("detection", imgDisplay);

		vector<Rect_<int> > rectResult;
		objPD.GetResultRects(rectResult);
		tc2.tic();
		tracker.Track(rectResult);
		tc2.toc();
		tracker.DrawResult(imgDisplay);
		putText(imgDisplay, "track " + tc2.to_string(2) + "ms", Point(10, 90), FONT_HERSHEY_COMPLEX, 0.9, CV_RGB(255, 0, 255), 2);
		imshow("tracking", imgDisplay);

		char key = waitKey(nDelay);
		if (key == 27)
			break;
		else if (key >= '1' && key <= '4')
			nSize = (double)(key - '0');
		else if (key == ' ')
			nDelay = 1 - nDelay;
		else if (key == ']')
		{
			nDelay = 0;
			(*objLoader)++;
		}
		else if (key == '[')
		{
			nDelay = 0;
			(*objLoader)--;
		}
		else if (key == 8) // backspace 는 첫 프레임으로
		{
			objLoader->GoToSpecificFrame(0);
			continue;
		}

		if (nDelay != 0)
			(*objLoader)++ ? nDelay = 1 : nDelay = 0;
	}

	printf("average time = %sms\n", tc.avg.to_string().c_str());

	return 0;
}