#define _CRT_SECURE_NO_WARNINGS

#include "../UtilTimeChecker/TimeChecker.h"
#include "../UtilDatasetLoader/DatasetLoader.h"
#include "../LibPedestrian/PedestrianACF2.h"
#include "../LibTrackingByDetection/MultiKalmanFilter.h"
#ifdef _DEBUG
#pragma comment(lib, "../Debug/UtilDatasetLoader.lib")
#pragma comment(lib, "../Debug/LibPedestrian.lib")
#pragma comment(lib, "../Debug/LibTrackingByDetection.lib")
#else
#pragma comment(lib, "../Release/UtilDatasetLoader.lib")
#pragma comment(lib, "../Release/LibPedestrian.lib")
#pragma comment(lib, "../Release/LibTrackingByDetection.lib")
#endif



double boxoverlap(Rect_<double> a, Rect_<double> b, int32_t criterion = -1)
{

	// overlap is invalid in the beginning
	double o = -1;

	// get overlapping area
	double x1 = max(a.tl().x, b.tl().x);
	double y1 = max(a.tl().y, b.tl().y);
	double x2 = min(a.br().x, b.br().x);
	double y2 = min(a.br().y, b.br().y);

	// compute width and height of overlapping area
	double w = x2 - x1;
	double h = y2 - y1;

	// set invalid entries to 0 overlap
	if (w <= 0 || h <= 0)
		return 0;

	// get overlapping areas
	double inter = w*h;
	double a_area = a.area();
	double b_area = b.area();

	// intersection over union overlap depending on users choice
	if (criterion == -1)     // union
		o = inter / (a_area + b_area - inter);
	else if (criterion == 0) // bbox_a
		o = inter / a_area;
	else if (criterion == 1) // bbox_b
		o = inter / b_area;

	// overlap
	return o;
}


int main()
{
	CDatasetCreator dc;
	if (!dc.LoadConfigFile("Config.xml")) return -1;
	CDatasetLoader* objLoader = dc.Create();
	if (!objLoader) return -1;

	SDataFormat sData;
	Mat imgInput;
	Mat imgDisplay;
	Mat imgDisplayTrack;
	double nSize = 1;
	int nDelay = 1;

	CTimeChecker tc;

	CPedestrianACF2 objPD;
	if (!objPD.LoadClassifier("../LibPedestrian/ACF_Caltech+.txt")) return -1;

	int cntFrame = 0;

	//FILE *fp = fopen("../DB/KITTI/2011_09_26_drive_0091_sync/GT_2011_09_26_drive_0091_sync.txt", "rt");
	int cntTP = 0;
	int cntFP = 0;
	int cntFN = 0;
	int cntGT = 0;

	CMultiKalmanFilter tracker;

	//FILE *fpGPC = fopen("GPC.txt", "wt");


	while (1)
	{
		*objLoader >> sData;

		//imgInput = sData.img[0];
		resize(sData.img[0], imgInput, Size(0, 0), 0.5*nSize, 0.5*nSize);

		imgDisplay = imgInput(Rect(0, 80, imgInput.cols, imgInput.rows - 80)).clone();
		imgDisplayTrack = imgDisplay.clone();

		tc.tic();
		objPD.Detect(imgDisplay);
		tc.toc();

		vector<Rect_<int> > vecrectDetect;
		objPD.GetResultRects(vecrectDetect);

// 		for (int i = 0; i < (int)vecrectDetect.size(); i++)
// 		{
// 			double x = (double)(vecrectDetect[i].y + vecrectDetect[i].height);
// 			double y = (double)vecrectDetect[i].height;
// 			if (abs((1.0294 * x - 87.152) - y) >= (x - 100)/2-5 )
// 			{
// 				// false
// 				vecrectDetect.erase(vecrectDetect.begin() + i);
// 				i--;
// 			}
// 		}

		for (int i = 0; i < (int)vecrectDetect.size(); i++)
			rectangle(imgDisplay, vecrectDetect[i], CV_RGB(0, 255, 255), 2);


		tracker.Track(vecrectDetect);
		tracker.DrawResult(imgDisplayTrack);

		//////////////////////////////////////////////////////////////////////////
		//GT drawing start
// 		vector<Rect_<int> > vecrectGTMissing;
// 
// 		int nFrame = 0;
// 		int nObj = 0;
// 		fscanf(fp, "%d %d", &nFrame, &nObj);
// 		for (int i = 0; i < nObj; i++)
// 		{
// 			Rect_<float> rect;
// 			char szObjType[30];
// 			char szObjOcclu[30];
// 			fscanf(fp, "%f %f %f %f %s %s", &rect.x, &rect.y, &rect.width, &rect.height, szObjType, szObjOcclu);
// 
// 			rect.x *= (float)(0.5*nSize);
// 			rect.y *= (float)(0.5*nSize);
// 			rect.width *= (float)(0.5*nSize);
// 			rect.height *= (float)(0.5*nSize);
// 
// 			Scalar myColor;
// 			if (!strcmp(szObjOcclu, "non"))
// 				myColor = CV_RGB(0, 255, 0);
// 			else if (!strcmp(szObjOcclu, "partly"))
// 				myColor = CV_RGB(255, 255, 0);
// 			else
// 				myColor = CV_RGB(255, 0, 0);
// 
// 			bool isPedestrian = false;
// 			if (!strcmp(szObjType, "Pedestrian"))
// 				isPedestrian = true;
// 
// 			bool isNonOccluded = false;
// 			if (!strcmp(szObjOcclu, "non"))
// 				isNonOccluded = true;
// 
// 			bool isTruncated = false;
// 			if (rect.x + rect.width >= imgDisplay.size().width || rect.y + rect.height >= imgDisplay.size().height || rect.x < 0 || rect.y < 0)
// 				isTruncated = true;
// 
// 			bool isTooSmall = false;
// 			if (rect.width < 20.5f || rect.height < 50.f)
// 				isTooSmall = true;
// 
// 			if (isPedestrian && (rect.y + rect.height < imgDisplay.size().height))
// 			{
// 				rectangle(imgDisplay, rect, myColor, 1);
// 				//fprintf(fpGPC, "%f\t%f\n", rect.y + rect.height, rect.height);
// 			}
// 
// 			bool existingTP = false;
// 			for (int j = 0; j < (int)vecrectDetect.size(); j++)
// 			{
// 				if (boxoverlap(rect, vecrectDetect[j]) >= 0.5) // gt와 detection의 bbox가 50% 이상 겹쳤을 때
// 				{
// 					if (isPedestrian && isNonOccluded && !isTruncated && !isTooSmall)
// 					{
// 						cntTP++;
// 						cntGT++;
// 						existingTP = true;
// 					}
// 
// 					vecrectDetect.erase(vecrectDetect.begin() + j); // detection list에서 제거를 해야 다음 검사 때 FP에 카운팅 되지 않음
// 					j--;
// 				}
// 			}
// 
// 			if (isPedestrian && isNonOccluded && !isTruncated && !isTooSmall && !existingTP)
// 			{
// 				vecrectGTMissing.push_back(rect);
// 				cntGT++;
// 			}
// 		}
// 
// 		cntFP += (int)vecrectDetect.size();
// 		cntFN += (int)vecrectGTMissing.size();
		//GT drawing end
		//////////////////////////////////////////////////////////////////////////

		//objPD.DrawResult(imgDisplay, CV_RGB(0, 255, 255));
		

		putText(imgDisplay, "C++ " + tc.to_string(2) + "ms", Point(10, 60), FONT_HERSHEY_COMPLEX, 0.9, CV_RGB(255, 0, 255), 2);
		imshow("detection", imgDisplay);
		imshow("Tracking", imgDisplayTrack);

		cntFrame++;
		//if (cntFrame > 339) break;

		char key = waitKey(nDelay);
		if (key == 27)
			break;
		else if (key >= '1' && key <= '4')
			nSize = (double)(key - '0');
		else if (key == ' ')
			nDelay = 1 - nDelay;
		else if (key == ']')
		{
			nDelay = 0;
			(*objLoader)++;
		}
		else if (key == '[')
		{
			nDelay = 0;
			(*objLoader)--;
		}
		else if (key == 8) // backspace 는 첫 프레임으로
		{
			objLoader->GoToSpecificFrame(0);
			continue;
		}

		if (nDelay != 0)
			(*objLoader)++ ? nDelay = 1 : nDelay = 0;
	}

	cout << imgDisplay.size() << endl;

// 	printf("average time = %sms\n", tc.avg.to_string().c_str());
// 	printf("cntGT %d\n", cntGT);
// 	printf("cntTP %d\n", cntTP);
// 	printf("cntFN %d\n", cntFN);
// 	printf("cntFP %d\n", cntFP);
// 	fclose(fp);

	//fclose(fpGPC);

	return 0;
}