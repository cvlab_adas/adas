#define _CRT_SECURE_NO_WARNINGS

#include "../UtilDatasetLoader/DatasetLoader.h"
#include "../UtilTimeChecker/TimeChecker.h"
#ifdef _DEBUG
#pragma comment(lib, "../Debug/UtilDatasetLoader.lib")
#else
#pragma comment(lib, "../Release/UtilDatasetLoader.lib")
#endif

#include "../LibPedestrian/PedestrianACF.h"
#include "../LibPedestrian/PedestrianACF2.h"
#include "../LibPedestrian/PedestrianCascade.h"
#ifdef _DEBUG
#pragma comment(lib, "../Debug/LibPedestrian.lib")
#else
#pragma comment(lib, "../Release/LibPedestrian.lib")
#endif

int main()
{
	CDatasetCreator dc;
	if (!dc.LoadConfigFile("Config.xml")) return -1;
	CDatasetLoader* objLoader = dc.Create();
	if (!objLoader) return -1;

	SDataFormat sData;
	Mat imgInput;
	Mat imgDisplay;
	double nSize = 2;
	int nDelay = 0;

	CPedestrianDetector* objPD[4];
	objPD[0] = new CPedestrianACF();
	if (!objPD[0]->LoadClassifier("../LibPedestrian/ACF_Caltech+.txt")) return -1;
	objPD[1] = new CPedestrianACF2();
	if (!objPD[1]->LoadClassifier("../LibPedestrian/ACF_Caltech+.txt")) return -1;
	objPD[2] = new CPedestrianCascade();
	if (!objPD[2]->LoadClassifier("../LibPedestrian/haarCascade_fullbody(ocv_default).xml")) return -1;
	objPD[3] = new CPedestrianCascade();
	if (!objPD[3]->LoadClassifier("../LibPedestrian/hogCascade_pedestrians(ocv_default).xml")) return -1;

	Scalar colors[4] = { CV_RGB(255, 255, 0), CV_RGB(255, 0, 0), CV_RGB(0, 255, 255), CV_RGB(0, 0, 255) };

	vector<string> vecstrType = { "ACF0", "ACF", "HAAR", "HOG" };
	vector<CTimeChecker<msec> > tc(4);

	int cnt = 0;

	while (1)
	{
		*objLoader >> sData;

		resize(sData.img[0], imgInput, Size(0, 0), 0.5*nSize, 0.5*nSize);
		imgDisplay = imgInput.clone();

		for (int i = 1; i <= 3; i+=2)
		{
			tc[i].tic();
			objPD[i]->Detect(imgInput);
			tc[i].toc();
			objPD[i]->DrawResult(imgDisplay, colors[i]);

			putText(imgDisplay, vecstrType[i] + ": " + tc[i].to_string(2) + "ms", Point(10, i * 30 + 30), FONT_HERSHEY_COMPLEX, 0.8, colors[i], 2);

			//sprintf(szTemp, "ACF_Caltech+_C++_%06d.jpg", cnt++);
			//imwrite(szTemp, imgDisplay[i]);
		}

		char szTemp[30];
		sprintf(szTemp, "imgDisplay");
		imshow(szTemp, imgDisplay);
		
		char key = waitKey(nDelay);
		if (key == 27)
			break;
		else if (key >= '1' && key <= '4')
			nSize = (double)(key - '0');
		else if (key == ' ')
			nDelay = 1 - nDelay;
		else if (key == ']')
		{
			nDelay = 0;
			(*objLoader)++;
		}
		else if (key == '[')
		{
			nDelay = 0;
			(*objLoader)--;
		}
		else if (key == 8) // backspace 는 첫 프레임으로
		{
			objLoader->GoToSpecificFrame(0);
			continue;
		}

		if (nDelay != 0)
			(*objLoader)++ ? nDelay = 1 : nDelay = 0;
	}

	delete objPD[0];
	delete objPD[1];
	delete objPD[2];
	delete objPD[3];

	return 0;
}