#define _CRT_SECURE_NO_WARNINGS

#include "../UtilTimeChecker/TimeChecker.h"
#include "../UtilVideoLoader/VideoPlayer.h"
#ifdef _DEBUG
#pragma comment(lib, "../Debug/UtilVideoLoader.lib")
#else
#pragma comment(lib, "../Release/UtilVideoLoader.lib")
#endif

#include "../LibPedestrian/PedestrianACF2.h"
#ifdef _DEBUG
#pragma comment(lib, "../Debug/LibPedestrian.lib")
#else
#pragma comment(lib, "../Release/LibPedestrian.lib")
#endif

// 작업순서1 : PedestrianKITTIObject.cpp 파일 실행 --> 000000.txt 파일이 나옴 --> console application 프로젝트의 sha128 폴더 내로 이동
// 작업순서2 : ConsoleApplication 프로젝트 실행(evaluation.cpp) --> 입력인자 잘 조정해야함 --> pedestrian_detection.txt 나옴, test_c.txt 나옴
// 작업순서3 : matlab에서 pedestrian_detection.txt 로 auc를 구할 수 있고, test_c.txt 로 tp,fp,fn 을 구할 수 있음

int main()
{
	CVideoPlayer* objLoader = new CVideoPlayer;
	if (!objLoader->LoadDBFile(CVideoPlayer::DB_IMAGE, "D:\\DBDBDBDBDB\\KITTI_Object\\training\\image_2")) return -1;

	Mat imgInput;
	Mat imgDisplay;
	double nSize = 2;
	int nDelay = 1;

	CTimeChecker_<msec> tc;

	CPedestrianACF2 objPD;
	if (!objPD.LoadClassifier("../LibPedestrian/ACF_Caltech+.txt")) return -1;

	int cnt = 0;
	double sumtime = 0;

	FILE *fp;

	while (1)
	{
		*objLoader >> imgInput;

		//resize(imgInput, imgDisplay, Size(0, 0), 0.5*nSize, 0.5*nSize);
		imgDisplay = imgInput.clone();

		tc.tic();
		objPD.Detect(imgInput);
		tc.toc();
		objPD.DrawResult(imgDisplay, CV_RGB(255, 0, 0));

		//putText(imgDisplay, "ACF " + tc.to_string(2) + "ms", Point(10, 60), FONT_HERSHEY_COMPLEX, 0.9, CV_RGB(255, 0, 255), 2);
		imshow("detection", imgDisplay);

		char szFileName[100];
		sprintf(szFileName, "%06d.txt", cnt++);
		fp = fopen(szFileName, "w");
		vector<Rect_<int> > vecrect; vector<float> vecscore;
		objPD.GetResultRects(vecrect);
		objPD.GetResultScore(vecscore);
		for (int i = 0; i < (int)vecrect.size(); i++)
		{
			fprintf(fp, "Pedestrian -1.0 -1.0 -1.0 %.2lf %.2lf %.2lf %.2lf -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 %.2lf\n", (double)vecrect[i].x, (double)vecrect[i].y, (double)(vecrect[i].x + vecrect[i].width), (double)(vecrect[i].y + vecrect[i].height), vecscore[i]);
		}
		fclose(fp);

		//char szTemp[100];
		//sprintf(szTemp, "ACF_Caltech+_C++_%06d.jpg", cnt++);
		//imwrite(szTemp, imgDisplay);

		if (cnt == 7481) break;

		char key = waitKey(nDelay);
		if (key == 27)
			break;
		else if (key >= '1' && key <= '4')
			nSize = (double)(key - '0');
		else if (key == ' ')
			nDelay = 1 - nDelay;
		else if (key == ']')
		{
			nDelay = 0;
			(*objLoader)++;
		}
		else if (key == '[')
		{
			nDelay = 0;
			(*objLoader)--;
		}

		if (nDelay != 0)
			(*objLoader)++ ? nDelay = 1 : nDelay = 0;
	}

	printf("avg time = %lf\n", tc.avg.to_<double>());

	delete objLoader;

	return 0;
}