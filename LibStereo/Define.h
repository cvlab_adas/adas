/**
	@file Define.h
	@date 2016/03/01
	@author 우태강(tkwoo@vision.inha.ac.kr) Inha Univ.
	@brief 전체 파라미터 통일을 위한 Struct 및 변수 정의 file
*/

#pragma once
//#include "opencv2\opencv.hpp"
#include "opencv2/opencv.hpp"
#define PI 3.141592

enum { Daimler, KITTI, HICAM, CityScape };
enum { STEREO_BM = 0 }; //, STEREO_SGBM = 1
enum { GRAY, COLOR };

struct CameraParam_t
{
	double m_dPitchDeg;	///< unit : degree
	double m_dFocalLength; ///< unit : pixels
	double m_dCameraHeight; ///< cam height from ground
	double m_dFOVvDeg; ///< vertical FOV
	double m_dFOVhDeg; ///< horizontal FOV
	int m_nVanishingY; ///< vanishing line location
	cv::Size m_sizeSrc;
	CameraParam_t(){
		m_dPitchDeg = 0.;
		m_dFocalLength = 0.;
		m_dCameraHeight = 0.;
		m_dFOVvDeg = 0.;
		m_dFOVhDeg = 0.;
		m_sizeSrc = cv::Size(0, 0);
	}
};
struct StereoCamParam_t
{
	int m_nNumberOfDisp; ///< number of disparity.
	int m_nWindowSize; ///< window size. It must be odd number.
	double m_dBaseLine; ///< baseline, unit : meters1
	double m_dMaxDist; ///< Maximum distance value, unit : meters
	CameraParam_t objCamParam;
	StereoCamParam_t(){
		m_nNumberOfDisp = 80;
		m_nWindowSize = 9;
		m_dBaseLine = 0.;
		m_dMaxDist = 50.0;
	}
};

