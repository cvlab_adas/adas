#define _CRT_SECURE_NO_WARNINGS

#include "DatasetLoader.h"

#ifdef _DEBUG
#define LOG_OUT(fmt) printf( "[%s:%d] %s\n",__FUNCTION__,__LINE__,fmt)
#else
#define LOG_OUT(fmt)
#endif


CImgSeqLoader::CImgSeqLoader(string strPathFile, bool &flg)
{
	flg = LoadDataset(strPathFile);
}

bool CImgSeqLoader::LoadDataset(string strPathBase)
{
	bool retVal = false;
	if (m_flgLoaded)
	{
		LOG_OUT("dataset is already loaded");
		retVal = false;
	}
	else if (strPathBase.size() == 0)
	{
		LOG_OUT("wrong input argument");
		return false;
	}
	else
	{
		retVal = true;
		
		m_nVidChannel = 1;

		for (int i = 0; i < m_nVidChannel; i++)
		{
			string strPath = strPathBase;

			m_flgLoaded = true;

			m_objPlayer[i] = new CVideoPlayer;
			if (!m_objPlayer[i]->LoadDBFile(CVideoPlayer::DB_IMAGE, strPath))
				retVal = false;
		}
	}

	return retVal;
}