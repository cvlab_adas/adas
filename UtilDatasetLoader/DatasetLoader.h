#pragma once

#include "../UtilVideoLoader/VideoPlayer.h"
#ifdef _DEBUG
#pragma comment(lib, "../Debug/UtilVideoLoader.lib")
#else
#pragma comment(lib, "../Release/UtilVideoLoader.lib")
#endif

struct SDataFormat
{
	Mat img[2];
};

class CDatasetLoader
{
public:
	CDatasetLoader();
	virtual ~CDatasetLoader();

	bool operator++(int);
	bool operator--(int);
	void operator>>(SDataFormat& data);

	bool GoToSpecificFrame(int nSpecFrame);

	int m_nVidChannel;

protected:
	void Initialization();
	void Finalization();
	string ExtractFolderAbsPath(string strFolderPath) const;

	CVideoPlayer* m_objPlayer[2];
	bool m_flgLoaded;

	virtual bool LoadDataset(string strPathOrFile) = 0;

};

class CKITTILoader : public CDatasetLoader
{
public:
	CKITTILoader(string strPathBase, bool &flg);
	virtual ~CKITTILoader() {};

private:
	virtual bool LoadDataset(string strPathOrFile);
};

class CCVLABLoader : public CDatasetLoader
{
public:
	CCVLABLoader(string strPathBase, bool &flg);
	virtual ~CCVLABLoader() {};

private:
	virtual bool LoadDataset(string strPathOrFile);
};

class CDAIMLERLoader : public CDatasetLoader
{
public:
	CDAIMLERLoader(string strPathBase, bool &flg);
	virtual ~CDAIMLERLoader() {};

private:
	virtual bool LoadDataset(string strPathOrFile);
};

class CImgSeqLoader : public CDatasetLoader
{
public:
	CImgSeqLoader(string strPathBase, bool &flg);
	virtual ~CImgSeqLoader() {};

private:
	virtual bool LoadDataset(string strPathOrFile);
};


class CDatasetCreator
{
public:
	CDatasetCreator() : m_idxDataset(-1)
	{
		m_vecstrListDataset.push_back("KITTI");
		m_vecstrListDataset.push_back("CVLAB");
		m_vecstrListDataset.push_back("DAIMLER");
		m_vecstrListDataset.push_back("IMGSEQ");
		m_objLoader = NULL;
	}

	~CDatasetCreator()
	{
		if (m_objLoader)
			delete m_objLoader;
	}

	CDatasetLoader* Create()
	{
		bool bCreated;
		if (m_idxDataset == 0)
			m_objLoader = new CKITTILoader(m_strDBPath, bCreated);
		else if (m_idxDataset == 1)
			m_objLoader = new CCVLABLoader(m_strDBPath, bCreated);
		else if (m_idxDataset == 2)
			m_objLoader = new CDAIMLERLoader(m_strDBPath, bCreated);
		else if (m_idxDataset == 3)
			m_objLoader = new CImgSeqLoader(m_strDBPath, bCreated);
		else
			m_objLoader = NULL;

		if (!bCreated)
		{
			delete m_objLoader;
			m_objLoader = NULL;
		}

		return m_objLoader;
	}

	bool LoadConfigFile(string strCfgFile = "Config.xml");
	
private:
	vector<string> m_vecstrListDataset;
	string m_strDBPath;
	int m_idxDataset;

	CDatasetLoader* m_objLoader;
};
