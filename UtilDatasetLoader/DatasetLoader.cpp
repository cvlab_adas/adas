#include "DatasetLoader.h"

#ifdef _DEBUG
#define LOG_OUT(fmt) printf( "[%s:%d] %s\n",__FUNCTION__,__LINE__,fmt)
#else
#define LOG_OUT(fmt)
#endif

CDatasetLoader::CDatasetLoader() : m_flgLoaded(false)
{
	Initialization();
}

CDatasetLoader::~CDatasetLoader()
{
	Finalization();
}

void CDatasetLoader::Initialization()
{
	Finalization();

	m_nVidChannel = 0;

	for (int i = 0; i < 2; i++)
		m_objPlayer[i] = NULL;
}

void CDatasetLoader::Finalization()
{
	if (m_flgLoaded)
		for (int i = 0; i < 2; i++)
			if (m_objPlayer[i] != NULL)
			{
				delete m_objPlayer[i];
				m_objPlayer[i] = NULL;
			}

	m_flgLoaded = false;
}

bool CDatasetLoader::operator++(int)
{
	bool retval = true;
	for (int i = 0; i < m_nVidChannel; i++)
		retval = retval & (*m_objPlayer[i])++;
	return retval;
}

bool CDatasetLoader::operator--(int)
{
	bool retval = true;
	for (int i = 0; i < m_nVidChannel; i++)
		retval = retval & (*m_objPlayer[i])--;
	return retval;
}

void CDatasetLoader::operator>>(SDataFormat& data)
{
	for (int i = 0; i < m_nVidChannel; i++)
		*m_objPlayer[i] >> data.img[i];
}

bool CDatasetLoader::GoToSpecificFrame(int nSpecFrame)
{
	bool retval = true;
	for (int i = 0; i < m_nVidChannel; i++)
		retval = retval & m_objPlayer[i]->GoToSpecificFrame(nSpecFrame);
	return retval;
}

string CDatasetLoader::ExtractFolderAbsPath(string strFolderPath) const
{
	if (strFolderPath.size())
		if (strFolderPath[strFolderPath.size() - 1] != '/')
			strFolderPath.push_back('/');
	char szAbsPath[_MAX_PATH];
#ifdef WIN32
	_fullpath(szAbsPath, strFolderPath.c_str(), _MAX_PATH);
#else //WIN32
	realpath(strFolderPath.c_str(), szAbsPath);
	sprintf(szAbsPath, "%s%c", szAbsPath, '/');
#endif //WIN32
	return szAbsPath;
}

bool CDatasetCreator::LoadConfigFile(string strCfgFile)
{
	FileStorage fs(strCfgFile, FileStorage::READ);

	if (!fs.isOpened())
	{
		LOG_OUT("There is no config file..");
		return false;
	}

	string strDBName, strDBPath;
	fs["DB_Info"]["Name"] >> strDBName;
	fs["DB_Info"]["Path"] >> strDBPath;

	for (int i = 0; i < (int)m_vecstrListDataset.size(); i++)
	{
		if (strDBName == m_vecstrListDataset[i])
		{
			m_idxDataset = i;
			break;
		}
	}

	if (m_idxDataset == -1)
		return false;

	m_strDBPath = strDBPath;

	return true;
}