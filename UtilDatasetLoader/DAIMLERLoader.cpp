#define _CRT_SECURE_NO_WARNINGS

#include "DatasetLoader.h"

#ifdef _DEBUG
#define LOG_OUT(fmt) printf( "[%s:%d] %s\n",__FUNCTION__,__LINE__,fmt)
#else
#define LOG_OUT(fmt)
#endif


CDAIMLERLoader::CDAIMLERLoader(string strPathBase, bool &flg)
{
	flg = LoadDataset(strPathBase);
}

bool CDAIMLERLoader::LoadDataset(string strPathBase)
{
	bool retVal = false;
	if (m_flgLoaded)
	{
		LOG_OUT("dataset is already loaded");
		retVal = false;
	}
	else if (strPathBase.size() == 0)
	{
		LOG_OUT("wrong input argument");
		return false;
	}
	else
	{
		retVal = true;
		if (strPathBase[strPathBase.size() - 1] != '/')
			strPathBase.push_back('/');

		m_nVidChannel = 2;

		for (int i = 0; i < m_nVidChannel; i++)
		{
			char szTemp[100];
			sprintf(szTemp, "TestData_c%d_part1/TestData/c%d/", i, i);
			string strPath = strPathBase + szTemp;

			m_flgLoaded = true;

			m_objPlayer[i] = new CVideoPlayer;
			if (!m_objPlayer[i]->LoadDBFile(CVideoPlayer::DB_IMAGE, strPath))
				retVal = false;
		}
	}

	return retVal;
}