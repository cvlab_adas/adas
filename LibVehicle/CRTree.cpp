/* 
// Author: Juergen Gall, BIWI, ETH Zurich
// Email: gall@vision.ee.ethz.ch
*/

#include "CRTree.h"
#include <fstream>
#include <opencv2/opencv.hpp> //<../opencv249/include/opencv/highgui.h>
#include <algorithm>

using namespace std;

/////////////////////// Constructors /////////////////////////////

// Read tree from file
CRTree::CRTree(const char* filename) {
	cout << "Load Tree " << filename << endl;

	int dummy;

	ifstream in(filename);
	if(in.is_open()) {
		// allocate memory for tree table
		in >> max_depth;
		num_nodes = (int)pow(2.0,int(max_depth+1))-1;
		// num_nodes x 7 matrix as vector
		treetable = new int[num_nodes * 7];
		int* ptT = &treetable[0];
		
		// allocate memory for leafs
		in >> num_leaf;
		leaf = new LeafNode[num_leaf];

		// number of center points per patch 
		in >> num_cp;

		// read tree nodes
		for(unsigned int n=0; n<num_nodes; ++n) {
			in >> dummy; in >> dummy;
			for(unsigned int i=0; i<7; ++i, ++ptT) {
				in >> *ptT;
			}
		}

		// read tree leafs
		LeafNode* ptLN = &leaf[0];
		for(unsigned int l=0; l<num_leaf; ++l, ++ptLN) {
			in >> dummy;
			in >> ptLN->pfg;
			
			// number of positive patches
			in >> dummy;
			ptLN->vCenter.resize(dummy);
			for(int i=0; i<dummy; ++i) {
				ptLN->vCenter[i].resize(num_cp);
				for(unsigned int k=0; k<num_cp; ++k) {
					in >> ptLN->vCenter[i][k].x;
					in >> ptLN->vCenter[i][k].y;
				}
			}
		}

	} else {
		cerr << "Could not read tree: " << filename << endl;
	}

	in.close();

}
