#include "CRForestDetector.h"

CRForestDetector::CRForestDetector(const CRConfig& crCF)
	: strImfiles(crCF.strImfiles), vec_fScale(crCF.vec_fScale), nPatchWidth(crCF.nPatchWidth),
	nPatchHeight(crCF.nPatchHeight), szTrain(crCF.szTrain)
{
	//Initializer();	
	// 비디오 시작 플래그 설정
	bStartOfVideo = true;
}

CRForestDetector::CRForestDetector(const CRConfig& crCF, CRForest& cRF, CKalman& cKM)
	: strImfiles(crCF.strImfiles), vec_fScale(crCF.vec_fScale), nPatchWidth(crCF.nPatchWidth), 
	nPatchHeight(crCF.nPatchHeight), szTrain(crCF.szTrain), 
	crForest(&cRF),
	cKalman(&cKM)
{
	//Initializer();	
	// 비디오 시작 플래그 설정
	bStartOfVideo = true;
}

void CRForestDetector::GenerateHoughMapInGlobalROI(const vector<Mat>& vec_imgFeature, Mat& imgHough)
{
	Rect rectPatch;
	vector<double> vec_dSum;
	vec_dSum.resize(vec_imgFeature.size());

	//reset the output image
	imgHough.setTo(Scalar::all(0));

	//// for backprojection
	//vector<vector<Point2i>> vec_GroupOffsetDerive;
	//vector<Point2i> vec_OffsetDerive;

	//vector<vector<vector<Point2i>>> vec_vecOffsetDerive;

	//vec_vecOffsetDerive.resize(vec_imgFeature[0].rows);
	//for (int j = 0; j < vec_vecOffsetDerive.size(); j++){
	//	vec_vecOffsetDerive[j].resize(vec_imgFeature[0].cols);
	//}

	float weight = 0;
	nStrideX = 6;
	nStrideY = 6;
	//get pointers to feature channels
	int stepImg;
	uchar** ptFch = new uchar*[vec_imgFeature.size()];
	uchar** ptFch_row = new uchar*[vec_imgFeature.size()];

	for (unsigned int c = 0; c < vec_imgFeature.size(); c++)
		ptFch[c] = vec_imgFeature[c].data;
	stepImg = vec_imgFeature[0].step / sizeof(ptFch[0][0]);

	//get pointer to output image
	int stepDet;
	float* ptDet;
	ptDet = (float*)imgHough.data;
	stepDet = imgHough.step / sizeof(ptDet[0]);

	int xoffset = nPatchWidth / 2;	//width = 12 --> xoffset = 6
	int yoffset = nPatchHeight / 2;

	int x, y, cx, cy; // x,y top left; cx,cy center of patch
	cy = yoffset;
	if (vec_imgFeature[0].rows > nPatchHeight && vec_imgFeature[0].cols > nPatchWidth){
		for (y = 0; y < vec_imgFeature[0].rows - nPatchHeight; y += nStrideY, cy += nStrideY) {
			// Get start of row
			for (unsigned int c = 0; c < vec_imgFeature.size(); ++c)
				ptFch_row[c] = &ptFch[c][0];
			cx = xoffset;

			for (x = 0; x < vec_imgFeature[0].cols - nPatchWidth; x += nStrideX, cx += nStrideX) {
				// cx, cy 는 패치의 중심좌표
				rectPatch = Rect(x, y, nPatchWidth, nPatchHeight);

				// regression for a single patch
				vector<const LeafNode*> result;
				crForest->regression(result, ptFch_row, stepImg);


				//vote for all leafs of trees
				for (vector<const LeafNode*>::const_iterator itL = result.begin(); itL != result.end(); itL++){
					// iterator for leaf nodes
					if ((*itL)->pfg > 0.5){

						float w = (*itL)->pfg / (float)((*itL)->vCenter.size() * result.size());
						//cout << "weight: "<<w << endl;

						int offsetIdx = 0;
#if DISCRIMINATIVE_VOTING_1st
						float fBestOffsetVal = 9999999;
						int nBestOffsetIdx = 0;
#endif						 
						//vote for all points stored in the leaf node
						for (vector<vector<CvPoint>>::const_iterator it = (*itL)->vCenter.begin(); it != (*itL)->vCenter.end(); it++, offsetIdx++){

#if DISCRIMINATIVE_VOTING_1st
							int nPx = (*it)[0].x + szTrain.width / 2;
							int nPy = (*it)[0].y + szTrain.height / 2;
							Rect rectPatchInTrainImage = Rect(nPx - nPatchWidth / 2, nPy - nPatchHeight / 2, nPatchWidth, nPatchHeight);
							float fMeanSum;
							float fStdevSum;
							Mat imgPowSum;

							for (int f = 0; f < vec_imgFeature.size(); f++){
								Mat imgTrainPatchMeanFeature = vec_imgMeanFeature[f](rectPatchInTrainImage);
								Mat imgTrainPatchStdevFeature = vec_imgStdevFeature[f](rectPatchInTrainImage);

								Mat imgTestPatchMeanFeature = vec_imgFeature[f](rectPatch);
								imgTestPatchMeanFeature.convertTo(imgTestPatchMeanFeature, CV_32FC1);
								imgTrainPatchMeanFeature.convertTo(imgTrainPatchMeanFeature, CV_32FC1);

								Mat imgTestPatchStdevFeature = abs(imgTestPatchMeanFeature - imgTrainPatchMeanFeature);
							/*	for (int hh = 0; hh < imgTestPatchStdevFeature.rows;hh++){
									for (int ww = 0; ww < imgTestPatchStdevFeature.cols;ww++){
										imgTestPatchStdevFeature.at<float>(hh, ww) = abs(imgTestPatchStdevFeature.at<float>(hh, ww));
									}
								}*/

								//multiply(imgTestPatchStdevFeature, imgTestPatchStdevFeature, imgPowSum);
								//Mat imgStdevDiff = abs(imgTestPatchStdevFeature - imgTrainPatchStdevFeature);

								//vec_dSum[f] = sum(imgStdevDiff)[0];
								vec_dSum[f] = sum(imgTestPatchStdevFeature)[0];
								fMeanSum = +vec_dSum[f];
								fStdevSum = +pow(vec_dSum[f], 2);
							}

							fMeanSum = fMeanSum / (float)vec_dSum.size();
							fStdevSum = sqrt(fStdevSum / (float)vec_dSum.size() - pow(fMeanSum, 2));
							// 여기서부터 다시 시작
							if (fStdevSum < fBestOffsetVal){
								fBestOffsetVal = fStdevSum;
								nBestOffsetIdx = offsetIdx;
							}
#else
							// 차량 중심 추정 좌푠
							int x = cx - (*it)[0].x;
							int y = cy - (*it)[0].y;
							if (y >= 0 && y < imgHough.rows && x >= 0 && x < imgHough.cols){
								*(ptDet + x + y*stepDet) += w*nStrideX*nStrideY;	//check
								//vec_OffsetDerive.push_back(Point2i(cx,cy));
							}
#endif
						}
						//vec_vecOffsetDerive[y][x] = vec_OffsetDerive;
				
#if DISCRIMINATIVE_VOTING_1st
						// 차량 중심 추정 좌푠
						int x = cx - (*itL)->vCenter[nBestOffsetIdx][0].x;
						int y = cy - (*itL)->vCenter[nBestOffsetIdx][0].y;
						if (y >= 0 && y < imgHough.rows && x >= 0 && x < imgHough.cols){
							*(ptDet + x + y*stepDet) += w*nStrideX*nStrideY*(*itL)->vCenter.size();	//check
						}
#endif
					}//end if
				}

				for (unsigned int c = 0; c < vec_imgFeature.size(); c++){
					//++ptFch_row[c];
					ptFch_row[c] += nStrideX;
				}
			}//end for x loop

			//increase pointer y
			for (unsigned int c = 0; c < vec_imgFeature.size(); c++)
				ptFch[c] += stepImg*nStrideY;
		}//end for y loop
	}

	//vec_HoughOffsetDerive.push_back(vec_vecOffsetDerive);

	GaussianBlur(imgHough, imgHough, Size(7, 7), 2.5);

	//vec_vecOffsetDerive.clear();
}

void CRForestDetector::setVanishingPoint(int nVanishingX, int nVanishingY)
{
	ptVanishing.x = nVanishingX;
	ptVanishing.y = nVanishingY;
}

void CRForestDetector::Initializer()
{
	// 소실점 설정
	//setVanishingPoint(640, 360);
	//KITTI data vanishing point
	setVanishingPoint(0.5*imgSrc.cols, 0.45*imgSrc.rows);

	// 프레임 인덱스 설정
	nFrameIdx = 0;

	// 검출 모드 설정
	bIsGlobalROI = global_roi;
	//bIsGlobalROI = local_roi;
	
	// 초기 전역적 ROI 설정
	if (bIsGlobalROI == global_roi){
		InitializeROI();
		InitializeScale();
	}
	// 트랙커 정보 설정
	InitTrackerInfo();

	//// ouput 
	InitializeOutput(outDetection);

	// 백프로젝션 벡터 집합
	vec_HoughOffsetDerive.resize(5);

	//vec_imgMeanFeature.resize(5);
	//vec_imgStdevFeature.resize(5);
}

void CRForestDetector::InitializeROI()
{
	// 전역 ROI 사용하는 경우 ROI가 거리에 따라 n개로 생성됨 (ex) n=4)
	// ROI 생성 시 최소 검출 위치와 소실점을 기준으로 삼각형을 만듦

	// 최소 검출 위치 설정
	// INHA DB
	//vec_nRoiTlY.push_back(450);
	// KITTI DB
	int nFirstROIWidth = imgSrc.cols*0.7;
	int nFirstROIBottom = imgSrc.rows*0.9;
	vec_nRoiTlY.push_back(nFirstROIBottom);

	vec_szROI.resize(vec_fScale.size());
	vec_ptROI.resize(vec_fScale.size());

	// 최소 검출 위치의 ROI의 너비 설정 ( == > 삼각형의 밑변 설정) 
	vec_szROI[0].width = nFirstROIWidth;

	int nTerm;
	vector<int> vecTempY;

	// 소실점과 가장 가까운 ROI의 밑변( = 삼각형 밑변)을 기준으로 ROI들의 y 위치 계산
	nTerm = (vec_nRoiTlY[0] - ptVanishing.y) / (float)vec_fScale.size();

	for (int i = 0; i < vec_fScale.size(); i++){
		vec_szROI[i].height = nFirstROIBottom - ptVanishing.y - nTerm*i;
		vec_ptROI[i].y = imgSrc.rows*0.35;
		//vec_ptROI[i].y = ptVanishing.y;
	}

	// 소실점과 가장 가까운 ROI의 밑변( = 삼각형 밑변)을 기준으로 ROI들의 너비 계산
	for (int i = 1; i < vec_fScale.size(); i++){
		vec_szROI[i].width = nFirstROIWidth*((vec_fScale.size() - i) / (float)vec_fScale.size());
	}

	// ROI 저장
	for (int i = 0; i < vec_fScale.size(); i++){
		vec_ptROI[i].x = (imgSrc.cols - vec_szROI[i].width) / 2;
		vec_rectROI.push_back(Rect(vec_ptROI[i], vec_szROI[i]));
	}

	int nHighY = vec_ptROI[0].y;
	for (int i = 0; i < vec_fScale.size(); i++){
		if (vec_ptROI[i].y < nHighY) nHighY = vec_ptROI[i].y;
	}

	// feature를 추출하기 위한 최소 영역 계산
	rectFeatureExtractRegion.width = nFirstROIWidth + 2;
	rectFeatureExtractRegion.x = (imgSrc.cols - rectFeatureExtractRegion.width) / 2;
	rectFeatureExtractRegion.y = nHighY;
	rectFeatureExtractRegion.height = vec_nRoiTlY[0] - nHighY + 1 + 2;

	// Just for KITTI
	rectTrackROI = rectFeatureExtractRegion;

}


void CRForestDetector::InitializeROIBasedOnDist(){


}

void CRForestDetector::InitializeScale()
{
	for (int i = 0; i < vec_fScale.size(); i++){
		vec_fScale[i] = (szTrain.height / (float)vec_rectROI[i].height)*1.2;
	}
}




void CRForestDetector::CropROIFromFeature(Mat& imgFeature, vector<Mat>& vec_imgFeature)
{
	Mat mRoi;
	Mat mTempInput;
	Rect rectTemp;
	Size sizeRoi;
	vector<Mat> CropedRois;

	//// 첫번째 ROI에 대해 crop 
	mTempInput = imgFeature.clone();
	/*sizeRoi = Size((int)(mTempInput.cols*vec_fScale[0]), (int)(mTempInput.rows*vec_fScale[0]));
	resize(mTempInput, mTempInput, sizeRoi);
	vec_imgFeature[0]=mTempInput;*/

	for (int i = 0; i < vec_fScale.size(); i++){
		// Feature로부터 ROI crop
		rectTemp = Rect(vec_rectROI[i].x - rectFeatureExtractRegion.x, vec_rectROI[i].y - rectFeatureExtractRegion.y, vec_rectROI[i].width, vec_rectROI[i].height);
		mRoi = mTempInput(rectTemp);

		// ROI 내 차량 사이즈가 학습이미지 사이즈와 유사해지도록 이미지를 리사이즈 시킴
		//hekim 160217
		//float fResizeRatio = szTrain.height / (float)vec_rectROI[i].height;
		//sizeRoi = Size((int)(mRoi.cols*fResizeRatio*1.2), (int)(mRoi.rows*fResizeRatio*1.2));
		sizeRoi = Size((int)(mRoi.cols*vec_fScale[i]), (int)(mRoi.rows*vec_fScale[i]));
		//hekim 160217
	
		resize(mRoi, mRoi, sizeRoi);
		vec_imgFeature[i] = mRoi;
	}
}

void CRForestDetector::GenerateHoughMapInPyramid()
{

	if (imgSrc.channels() == 1) {
		std::cerr << "Gray color images are not supported." << std::endl;
	}
	else {
		Mat imgTemp;
		Mat imgTemp2;

		vector<Mat>  vImgTemp;
		vector<vector<Mat>> vec_storeROI;
		vector<Mat> vecTemp;

		vec_storeROI.resize(vec_fScale.size());

		Mat mFirstROI = imgSrc(rectFeatureExtractRegion);

		// ROI로부터 feature 추출 - 각각 벡터(vImg)에 저장
		extractFeatureChannels(mFirstROI, vec_imgFeature);

		vImgTemp.resize(vec_fScale.size());
		unsigned int num_scale, num_feature;

		for (num_scale = 0; num_scale < vec_fScale.size(); num_scale++){
			vec_storeROI[num_scale].resize(vec_imgFeature.size());
		}

		for (num_feature = 0; num_feature < vec_imgFeature.size(); num_feature++){
			//하나의 feature로부터 ROI 영역에 해당하는 이미지들 잘라서 벡터로 저장
			CropROIFromFeature(vec_imgFeature[num_feature], vImgTemp);

			if (num_feature == 0){
				for (int i = 0; i < vec_fScale.size(); i++){
					imgTemp = Mat(vImgTemp[i].rows, vImgTemp[i].cols, CV_32FC1);
					vec_imgGlobalHough.push_back(imgTemp);
				}
			}

			for (num_scale = 0; num_scale < vec_fScale.size(); num_scale++){
				vec_storeROI[num_scale][num_feature] = vImgTemp[num_scale];
			}
		}

		for (num_scale = 0; num_scale < vec_fScale.size(); ++num_scale){
			for (num_feature = 0; num_feature < vec_imgFeature.size(); ++num_feature){

				imgTemp2 = vec_storeROI.at(num_scale).at(num_feature);
				vecTemp.push_back(imgTemp2);
			}

			// feauture 이미지들로부터 허프맵 생성
			GenerateHoughMapInGlobalROI(vecTemp, vec_imgGlobalHough[num_scale]);
			vecTemp.clear();
		}		
	}
}

void CRForestDetector::extractFeatureChannels(Mat& img, vector<Mat>& vImg) {	
	
	Mat imgLAB;
	vector<Mat> temp;

	Mat mGray = Mat(img.rows, img.cols, CV_8UC1);

	vImg.resize(5);
	for (unsigned int c = 0; c < vImg.size(); c++)
		vImg[c] = Mat(img.rows, img.cols, CV_8UC1);

	// intensity 이미지 저장 <-- edge feature
	cvtColor(img, vImg[0], CV_RGB2GRAY);

	// Temporary images for computing I_x, I_y (Avoid overflow for cvSobel)
	Mat I_x, I_y;
	// |I_x|, |I_y|
	
	// x 방향으로 1차 미분한 이미지, y 방향으로 1차 미분한 이미지 <-- edge feature
	Sobel(vImg[0], I_x, CV_16S, 1, 0, 3);
	Sobel(vImg[0], I_y, CV_16S, 0, 1, 3);

	convertScaleAbs(I_x*0.25, vImg[1]);
	convertScaleAbs(I_y*0.25, vImg[2]);

	// L, a, b feature 추출 <-- color feature
	cvtColor(img, imgLAB, CV_RGB2Lab);
	split(imgLAB, temp);
	vImg[0] = temp[0].clone();		// L (intensity)
	vImg[3] = temp[1].clone();		// A
	vImg[4] = temp[2].clone();		// B
	//vImg[5] = MinFilter2(vImg[0], 3, 3);
	//vImg[6] = MaxFilter2(vImg[0], 3, 3);

}


void CRForestDetector::GenerateHoughMapInLocalROI(Rect& rectFeatureRg)
{
	vector<vector<Mat>> vec_rectFeatureROI;
	float fRatio = 0;
	Size size_Resize;
	vector<Mat> vec_mCopyFeature;
	vector<Rect> vec_rectLocalROICopy;
	
	Mat imgFeatureRegion = imgSrc(rectFeatureRg);
	extractFeatureChannels(imgFeatureRegion, vec_imgFeature);

	vec_sLocalHoughMap.resize(vec_rectLocalROIs.size());
	vec_rectLocalROICopy.resize(vec_rectLocalROIs.size());

	//crop ROIs and store
	for (int i = 0; i < vec_rectLocalROICopy.size(); i++){
		vector<Mat> vecTtemp;

		vec_rectLocalROICopy[i].x = vec_rectLocalROIs[i].tl().x - rectFeatureRg.tl().x;
		vec_rectLocalROICopy[i].y = vec_rectLocalROIs[i].tl().y - rectFeatureRg.tl().y;
		vec_rectLocalROICopy[i].width = vec_rectLocalROIs[i].width;
		vec_rectLocalROICopy[i].height = vec_rectLocalROIs[i].height;

		for (int j = 0; j < vec_imgFeature.size(); j++){
			Mat imgCropFeature = vec_imgFeature[j](vec_rectLocalROICopy[i]);
			vecTtemp.push_back(imgCropFeature);
		}
		vec_rectFeatureROI.push_back(vecTtemp);

	}

	//resize feature ROIs
	for (int i = 0; i < vec_rectFeatureROI.size(); i++){
		fRatio = 1 / (vec_rectLocalROIs[i].width / (float)szTrain.width);
		//cout << "szTrain " << szTrain.width << " " << szTrain.height << endl;
		//cout << "vec_rectLocalROIs " << vec_rectLocalROIs[i].width << " " << vec_rectLocalROIs[i].height << endl;
		//cout << "fRatio " << fRatio << endl;
		vec_sLocalHoughMap[i].scale = fRatio;
		size_Resize = Size(vec_rectLocalROIs[i].width * (float)(fRatio), vec_rectLocalROIs[i].height * (float)(fRatio));
		for (int j = 0; j < vec_rectFeatureROI[i].size(); j++){
			//cout << "size_Resize " << size_Resize.width << " " << size_Resize.height << endl;
			resize(vec_rectFeatureROI[i][j], vec_rectFeatureROI[i][j], size_Resize);
		}
	}

	//generate blank images for hough map
	//generate HoughMap
	for (int j = 0; j < vec_rectFeatureROI.size(); j++){
		vec_sLocalHoughMap[j].mROI = Mat(vec_rectFeatureROI[j][0].cols, vec_rectFeatureROI[j][0].rows, CV_32FC1);
		GenerateHoughMapInGlobalROI(vec_rectFeatureROI[j], vec_sLocalHoughMap[j].mROI);
	}
	imshow("hough local 1", vec_sLocalHoughMap[0].mROI);
	imshow("hough local 2", vec_sLocalHoughMap[1].mROI);
}


void CRForestDetector::CalculateFeatureExtractRegion()
{
	int nMinXX = 9999, nMinYY = 9999, nMaxXX = 0, nMaxYY = 0;
	Rect rectForFeature;
		
	vec_sLocalROIs.resize(vec_rectLocalROIs.size());

	for (int i = 0; i < vec_rectLocalROIs.size(); i++){
		if (vec_rectLocalROIs[i].tl().x < nMinXX) nMinXX = vec_rectLocalROIs[i].tl().x - 2;
		if (vec_rectLocalROIs[i].tl().y < nMinYY) nMinYY = vec_rectLocalROIs[i].tl().y - 2;
		if (vec_rectLocalROIs[i].br().x > nMaxXX) nMaxXX = vec_rectLocalROIs[i].br().x + 2;
		if (vec_rectLocalROIs[i].br().y > nMaxYY) nMaxYY = vec_rectLocalROIs[i].br().y + 2;
	}
	if (vec_rectLocalROIs.size() != 0){
		if (nMinXX < 0) nMinXX = 0;
		if (nMinYY < 0) nMinYY = 0;
		if (nMaxXX > IMAGE_WIDTH) nMaxXX = IMAGE_WIDTH-1;
		if (nMaxYY > IMAGE_HEIGHT) nMaxYY = IMAGE_HEIGHT - 1;

		rectForFeature = Rect(nMinXX, nMinYY, nMaxXX - nMinXX, nMaxYY - nMinYY);
	}
	else{
		rectForFeature = Rect(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT);
	}

	//cout << "rectForFeature " << rectForFeature.x << " " << rectForFeature.y << " " << rectForFeature.width << " " << rectForFeature.height<< endl;
	// 지역적 ROI로부터 허프맵 계산
	GenerateHoughMapInLocalROI(rectForFeature);

}


void CRForestDetector::LocalizeGlobalVehicleCenter(Point pt, vector<Point>& ptMaxInROIs, vector<int>& vec_includedROIIdx)
{
	int nIncludeROI = 0;
	Point nTempPt;
	vec_includedROIIdx.resize(0);

	// peak 포인트가 몇번째 ROI에 존재하는지 확인
	for (int i = 0; i < vec_fScale.size(); i++){
		if (pt.x >vec_rectROI[i].x&& pt.x <vec_rectROI[i].br().x-1
			&& pt.y >vec_rectROI[i].y&& pt.y < vec_rectROI[i].br().y-1 ){
			vec_includedROIIdx.push_back(i);
		}
	}

	//각 Hough Map에 해당하는 point 위치 계산
	for (int i = 0; i < vec_includedROIIdx.size(); i++){
		int nROIIdx = vec_includedROIIdx[i];
		nTempPt = (pt - vec_rectROI[nROIIdx].tl())*vec_fScale[nROIIdx];
		ptMaxInROIs.push_back(nTempPt);
	}
}

void CRForestDetector::CalculateProperBoxSize(vector<Point>& ptMaxInROIs, Rect& rectIPBox, vector<int>& vec_includedROIIdx)
{
	Mat mTempHough;
	float fSizeIntensity = 0;
	float fMax = 0;
	float fOptimalScale = 0;
	int nOptScaleNum = 0;
	int nY = 0, nX = 0;
	Point ptVCenter;

	// 한점에 대해 어느 ROI에서 가장 높은 확률 누적값을 가지는지 확인
	for (int i = 0; i < ptMaxInROIs.size(); i++){
		int nROIIdx = vec_includedROIIdx[i];
		mTempHough = vec_imgGlobalHough[nROIIdx];
		if (ptMaxInROIs[i].x>0 && ptMaxInROIs[i].y>0 && ptMaxInROIs[i].y < vec_imgGlobalHough[nROIIdx].rows , ptMaxInROIs[i].x < vec_imgGlobalHough[nROIIdx].cols ){
			fSizeIntensity = mTempHough.at<float>(ptMaxInROIs[i].y, ptMaxInROIs[i].x);

			// Peak point에서 각기 다른 허프맵에서의 intensity를 계산 후 최대값 비교 
			if (fSizeIntensity > fMax){
				fOptimalScale = vec_fScale[nROIIdx];
				fMax = fSizeIntensity;
				nOptScaleNum = nROIIdx;
			}
		}
	}

	// peak 점에서의 검출 사이즈 계산
	if (ptMaxInROIs.size() != 0){
		if (nOptScaleNum < vec_imgGlobalHough.size() - 1){
			ptVCenter = ptMaxInROIs[nOptScaleNum];

			Point ptMiddleTL = (vec_rectROI[nOptScaleNum].tl() + vec_rectROI[nOptScaleNum + 1].tl());
			Point ptMiddleBR = (vec_rectROI[nOptScaleNum].br() + vec_rectROI[nOptScaleNum + 1].br());
			ptMiddleTL.x /= 2;
			ptMiddleTL.y /= 2;
			ptMiddleBR.x /= 2;
			ptMiddleBR.y /= 2;

			nX = ptMaxInROIs[nOptScaleNum].y;
			nY = ptMaxInROIs[nOptScaleNum].y;

			if (ptVCenter.x > ptMiddleTL.x && ptVCenter.x<ptMiddleBR.x && ptVCenter.y>ptMiddleTL.y && ptVCenter.y < ptMiddleBR.y){
				rectIPBox.width = szTrain.width / (fOptimalScale + vec_fScale[nOptScaleNum + 1]);
				rectIPBox.height = szTrain.height / (fOptimalScale + vec_fScale[nOptScaleNum + 1]);
			}
			else{
				rectIPBox.width = szTrain.width / (float)fOptimalScale;
				rectIPBox.height = szTrain.height / (float)fOptimalScale;
			}
		}
		else{
			rectIPBox.width = szTrain.width / (float)fOptimalScale;
			rectIPBox.height = szTrain.height / (float)fOptimalScale;
		}

	}
	else{
	}
}

void CRForestDetector::GetClusters(vector<Rect>& vecDetected, vector <vector< Rect >> &vecGroup)
{
	int nDiffX = 0, nDiffY = 0;
	double dDiffDist = 0, dComp = 0;
	vector<Rect> vecTempCluster;
	int i, j;
	bool bNewCluster = true;

	for (i = 0; i < vecDetected.size(); i++){
		vecTempCluster.push_back(vecDetected[i]);

		// 박스들에 대해 거리 비교
		for (j = i + 1; j < vecDetected.size(); j++){
			nDiffX = vecDetected[i].x - vecDetected[j].x;
			nDiffY = vecDetected[i].y - vecDetected[j].y;
			dDiffDist = sqrt(pow(nDiffX, 2) + pow(nDiffY, 2));
			dComp = sqrt(pow((vecDetected[i].width + vecDetected[j].width) / 2, 2) + pow((vecDetected[i].height + vecDetected[j].height) / 2, 2))*0.1;
			
			// 거리가 특정값 이내이면 한 군집으로 인식
			if (dDiffDist < dComp){
				vecTempCluster.push_back(vecDetected[j]);
				vecDetected.erase(vecDetected.begin() + j);
			}
		}
		for (int k = 0; k < vecGroup.size(); k++){
			for (int x = 0; x < vecGroup[k].size(); x++){
				nDiffX = vecGroup[k][x].x - vecTempCluster[0].x;
				nDiffY = vecGroup[k][x].y - vecTempCluster[0].y;
				dDiffDist = sqrt(pow(nDiffX, 2) + pow(nDiffY, 2));
				dComp = sqrt(pow((vecGroup[k][0].width + vecTempCluster[0].width) / 2, 2) + pow((vecGroup[k][0].height + vecTempCluster[0].height) / 2, 2))*0.1;
				if (dDiffDist < dComp) {
					bNewCluster = false;
					break;
				}
			}
			if (!bNewCluster){
				int w = 0;
				if (dDiffDist == 0) w = 1;
				for (; w < vecTempCluster.size(); w++){
					vecGroup[k].push_back(vecTempCluster[w]);
				}
				vecTempCluster.clear();
				break;
			}
		}
		if (bNewCluster){
			vecGroup.push_back(vecTempCluster);
			vecTempCluster.clear();
		}
	}
}

void CRForestDetector::ClusterRect(vector<Rect>& vecClustered)
{
	Rect rectOverlap;
	float fOverlapRate;
	double dXDiff, dYDiff, dDiff, dComp;

	// 오버랩 비율 및 박스들 사이 거리 계산 후 군집화 수행
	for (int i = 0; i < vecClustered.size(); i++){
		for (int j = i + 1; j < vecClustered.size(); j++){
			rectOverlap = (vecClustered[i]) & (vecClustered[j]);
			fOverlapRate = rectOverlap.area() * 2 / (float)(vecClustered[i].area() + vecClustered[j].area() - rectOverlap.area());

			//point distance
			dXDiff = vecClustered[i].x - vecClustered[j].x;
			dYDiff = vecClustered[i].y - vecClustered[j].y;
			dDiff = sqrt(pow(dXDiff, 2) + pow(dYDiff, 2));
			dComp = sqrt(pow((vecClustered[i].width + vecClustered[j].width) / 2, 2) + pow((vecClustered[i].height + vecClustered[j].height) / 2, 2));

			if (fOverlapRate > 0.2&& dDiff <= dComp){
				vecClustered[i] = vecClustered[j];
				vecClustered.erase(vecClustered.begin() + j);
				j--;
			}
		}

	}
}

void CRForestDetector::DetectVehicle(Mat &imgVideoInput)
{
#if Debug 
#else
	// 버퍼 메모리 제거
	ClearMemory();
#endif

	//printf("imgVideoInput  %d %d\n", imgVideoInput.cols, imgVideoInput.rows);
	
	imgVideoInput.copyTo(imgSrc);
	if (bStartOfVideo==true){
		Initializer();
		// 트랙킹 영역 지정
		imgTrackingRegion = Mat::zeros(imgSrc.size(), CV_8UC1);
		imgTrackingRegion(rectTrackROI).setTo(Scalar::all(255));
		bStartOfVideo = false;
	}
	
	//bIsGlobalROI에 따라 지역적 혹은 전역적 ROI에서 차량 검출 수행
	switch (bIsGlobalROI) {
	case global_roi:
		// detect in GlobalROI
		DetectInFullROI();
		break;
	
	case local_roi:
		// detect in LocalROI
		DetectInLocalROI();
		break;
		
	default:
		break;
	}

#if EVALUATE
#else
	KalmanMultiTracker(High, MultiKF, nFrameIdx);
#endif
	ClusterRect(vec_rectCandidates);
	
	SetVehicleScore(vec_rectCandidates, vec_fCandidates);


	nFrameIdx++;
}

// 차량 Score 저장
void CRForestDetector::SetVehicleScore(vector<Rect>& vec_rectCandi, vector<float>& vec_fCandi){
	float fScaleRatio = 0, fScaleSize = 0;
	Mat imgHough, imgHoughCrop;
	float fScore = 0;
	bool bCheckScale = false;
	Point ptVCenter;
	Rect rectROI;

	Mat mMean, mStddev;
	sMeanStddev vecMeanStdDev;
	double dMean, dStddevValue;
	double dMax, dMin = 0;

	vec_fCandi.clear();
	vec_fCandi.resize(vec_rectCandi.size());

	for (int i = 0; i < vec_rectCandi.size(); i++){
		ptVCenter = Point((vec_rectCandi[i].x + vec_rectCandi[i].width / 2), (vec_rectCandi[i].y + vec_rectCandi[i].height / 2));

		fScaleRatio = szTrain.width/(float)vec_rectCandi[i].width;
		fScaleRatio = round(fScaleRatio * pow(float(10), 1) + 0.5f) / pow(float(10), 1);

		//printf("fScaleRatio %f\n", fScaleRatio);
		for (int j = 0; j < vec_fScale.size(); j++){
			fScaleSize = round(vec_fScale[j] * pow(float(10), 1) + 0.5f) / pow(float(10), 1);
			//printf("fScaleSize %f\n", fScaleSize);
			//printf("ScaleRatio - fScaleSize %f\n", fScaleRatio - fScaleSize);

			if ((fScaleRatio - fScaleSize) == 0){
				imgHough = vec_imgGlobalHough[j].clone();
				meanStdDev(imgHough, mMean, mStddev);
				dMean = mMean.at<double>(0, 0);
				dStddevValue = mStddev.at<double>(0, 0);
				minMaxLoc(imgHough, &dMin, &dMax);

				rectROI = vec_rectROI[j];
				bCheckScale = true;
				break;
			}
		}

		if (bCheckScale==true){
			ptVCenter = ptVCenter - rectROI.tl();
			if (ptVCenter.x <= 0) ptVCenter.x = 1;
			if (ptVCenter.x >= imgHough.cols) ptVCenter.x = imgHough.cols-1;
			if (ptVCenter.y <= 0) ptVCenter.y = 1;
			if (ptVCenter.y >= imgHough.rows) ptVCenter.y = imgHough.rows-1;
			
			fScore = (dMax - dMean)/dStddevValue;
			//printf("fScore %f\n", fScore);

			/*fScore = imgHough.at<float>(ptVCenter.y, ptVCenter.x);
			printf("fScore %f\n", fScore);
			fScore = (fScore - dMin) / abs(dMax - dMin);
			printf("dMax %f\n", dMax);
			printf("dMin  %f\n", dMin);*/

			vec_fCandi[i] = fScore;
		}
		bCheckScale = false;
		

	}
}


void CRForestDetector::DetectInFullROI()
{
	// 허프맵 생성
	GenerateHoughMapInPyramid();

	//imshow("hough1", vec_imgGlobalHough[0]);

	// 허프맵의 peak들로부터 자동적으로 임계값 계산
	FindAutomaticThreshold();

	// 전역적 ROI에서 차량 후보 검출
	FindVehicleCandidate();

}

void CRForestDetector::DetectInLocalROI()
{
	vector<Rect> vec_rectInput;	//input

	CalculateFeatureExtractRegion();
	//GenerateHoughMapInLocalROI();

	//test
	//imshow("hough1", vec_sLocalHoughMap[0].mROI);
	//imshow("hough1", vec_sLocalHoughMap[1].mROI);

	// 지역적 ROI로부터 차량 중심점 위치계산 
	LocalizeLocalVehicleCenter();
}

void CRForestDetector::FindAutomaticThreshold()
{
	Mat mMean, mStddev;
	sMeanStddev vecMeanStdDev;
	double dMean, dStddevValue;
	double dMaxStddev = 0;

	// 각각의 ROI로부터 허프맵 계산 후 intensity 값들의 mean, standard deviation을 계산한여 threshold 계산
	for (int i = 0; i < vec_imgGlobalHough.size(); i++){
		meanStdDev(vec_imgGlobalHough[i], mMean, mStddev);
		dMean = mMean.at<double>(0, 0);
		dStddevValue = mStddev.at<double>(0, 0);
		
		// 4개의 허프맵의 standard deviation 중에서 가장 높은 값을 사용한다.  
		if (dStddevValue > dMaxStddev){
			dMaxStddev = dMean;
			vecMeanStdDev.dMean = dMean;
			vecMeanStdDev.dStddev = dStddevValue;
		}
	}
	fGlobalThreshold = (float)(vecMeanStdDev.dMean + vecMeanStdDev.dStddev*ALPHA);	
	//fGlobalThreshold = 0.5;
}

void CRForestDetector::FindAutomaticThreshold2()
{
	Mat mMean, mStddev;
	sMeanStddev vecMeanStdDev;
	double dMean, dStddevValue;
	double dMaxStddev = 0;
	Mat imgHough;

	// 각각의 ROI로부터 허프맵 계산 후 intensity 값들의 mean, standard deviation을 계산한여 threshold 계산
	for (int i = 0; i < vec_imgGlobalHough.size(); i++){
		//imgHough = vec_imgGlobalHough[i];
		//imgHough = vec_imgGlobalHough[i].clone();

		meanStdDev(vec_imgGlobalHough[i], mMean, mStddev);
		dMean = mMean.at<double>(0, 0);
		dStddevValue = mStddev.at<double>(0, 0);

		// 4개의 허프맵의 standard deviation 중에서 가장 높은 값을 사용한다.  
		if (dStddevValue > dMaxStddev){
			dMaxStddev = dMean;
			vecMeanStdDev.dMean = dMean;
			vecMeanStdDev.dStddev = dStddevValue;
		}
	}
	fGlobalThreshold = (float)(vecMeanStdDev.dMean + vecMeanStdDev.dStddev*ALPHA);
	//fGlobalThreshold = 0.5;
}

void CRForestDetector::FindVehicleCandidate()
{
	vector<Point>  vec_gloabalPts;
	vector<Rect> vec_detected;
	
	// 허프맵의로부터 peak 값 계산
	FindPeaksInHough(vec_gloabalPts);

	//cout << "pt size " << vec_gloabalPts.size() << endl;

	// 차량 사이즈 계산
	CalculateVehicleSize(vec_gloabalPts, vec_detected);

	// 차량 후보 군집화
	GroupVehicleCandidates(vec_detected);

}

void CRForestDetector::FindPeaksInHough(vector<Point> &vec_gloabalPoints)
{
	vector<sPointInfo> vecPoint;
	Mat imgROI, vImgHough;
	Point ptTemp;

	//detect the vehicles using Hough map
	for (int num_scale = 0; num_scale < vec_fScale.size(); num_scale++){
		vImgHough = vec_imgGlobalHough[num_scale].clone();
		//vec_imgGlobalHough[num_scale].copyTo(vImgHough);

		//// to show hough map in ROIs -- can be erased
		//imgROI = imgSrc(vec_rectROI[num_scale]);
		//Size mROISize = Size(vImgHough.cols, vImgHough.rows);
		//resize(imgROI, imgROI, mROISize);
		////

		// 허프맵의 peak들을 추출
		FindMaximum(vImgHough, fGlobalThreshold, vec_fScale[num_scale], vecPoint);
		//FindMaximum2(vImgHough, vec_fScale[num_scale], vecPoint);
		for (int i = 0; i < vecPoint.size(); i++){
			ptTemp = vec_rectROI[num_scale].tl() + Point(vecPoint[i].nX, vecPoint[i].nY);
			vec_gloabalPoints.push_back(ptTemp);
			// 1차 검출된 peak 점들을 영상에 출력
			//circle(imgSrc, ptTemp, 3, Scalar(255, 0, 0), 2);	//	check
			//imshow("imgSrc", imgSrc);
		}
		vecPoint.clear();
	}
}

void CRForestDetector::CalculateVehicleSize(vector<Point> &vec_gloabalPoints, vector<Rect> &vec_Detected)
{
	vector<Point> vecMaxPt;
	Rect rectDetect;
	vector<int> vecIncludedROIIdx;

	for (int i = 0; i < vec_gloabalPoints.size(); i++){
#if Debug
		circle(imgSrc, vec_gloabalPoints[i], 3, Scalar(255, 0, 255), 3);	
#endif
		// 전역적 ROI로부터 차량 중심점 위치계산 
		LocalizeGlobalVehicleCenter(vec_gloabalPoints[i], vecMaxPt, vecIncludedROIIdx);
		// peak의 intensity에 따른 영상 내 차량 사이즈 계산 
		CalculateProperBoxSize(vecMaxPt, rectDetect, vecIncludedROIIdx);
		
		rectDetect.x = vec_gloabalPoints[i].x - rectDetect.width / 2;
		rectDetect.y = vec_gloabalPoints[i].y - rectDetect.height / 2;
		vec_Detected.push_back(rectDetect);
		vecMaxPt.clear();
	}
	//cout << "vec_gloabalPoints " << vec_gloabalPoints.size() << endl;
	//cout << "vec_Detected " << vec_Detected.size() << endl;
}

void CRForestDetector::GroupVehicleCandidates(vector<Rect> &vec_rectDetected)
{
	vector <vector< Rect >> vecGroup;

	GetClusters(vec_rectDetected, vecGroup);
	for (int i = 0; i < vecGroup.size(); i++){
		if (vecGroup[i].size() > 1){
			groupRectangles(vecGroup[i], 1, 0.2);
		}
	}
	for (int i = 0; i < vecGroup.size(); i++){
		if (vecGroup[i].size() != 0){
			vec_rectCandidates.push_back(vecGroup[i][0]);
		}
	}

	// 사각형 군집화
	ClusterRect(vec_rectCandidates);
	
}


void CRForestDetector::SetLocalROIs(vector<Rect>& vec_rectROIs)
{
	vec_rectLocalROIs = vec_rectROIs;
}

void CRForestDetector::LocalizeLocalVehicleCenter()
{
	Rect rectTemp;
	double dLocalMax = 0;
	Point ptLocalMax;
	float fLocalScale = 0;
	Size szOriginal;
	Point ptRectPosition;

	// 지역적 ROI의 허프맵에서 가장 높은 점을 추출
	for (int lr = 0; lr < vec_sLocalHoughMap.size(); lr++){
		minMaxLoc(vec_sLocalHoughMap[lr].mROI, 0, &dLocalMax, 0, &ptLocalMax);
		if (dLocalMax < LOCAL_THRESHOLD){
			break;
		}
		
		//이미지 내에서의 peak 점 및 박스 위치 계산
		fLocalScale = vec_sLocalHoughMap[lr].scale;

		szOriginal.width = szTrain.width / fLocalScale;
		szOriginal.height = szTrain.height / fLocalScale;

		ptRectPosition.x = ptLocalMax.x - szOriginal.width / 2;
		ptRectPosition.y = ptLocalMax.y - szOriginal.height / 2;

		ptRectPosition += vec_rectLocalROIs[lr].tl();

		rectTemp = Rect(ptRectPosition, szOriginal);
		vec_rectCandidates.push_back(rectTemp);
	}
}

void CRForestDetector::ShowVehicles(Mat& imgDisp)
{

	for (int i = 0; i < vec_rectCandidates.size(); i++){
		rectangle(imgDisp, vec_rectCandidates[i], CV_RGB(255, 0, 0), 2);
#if Debug
		//rectangle(imgSrc, vec_rectCandidates[i], CV_RGB(255, 0, 0), 2);
#endif
	}

#if Debug	
	for (int i = 0; i < vec_rectROI.size(); i++){
		rectangle(imgDisp, vec_rectROI[i], CV_RGB(255, 255, 255), 1);

		//rectangle(imgSrc, vec_rectROI[i], CV_RGB(255, 255, 255), 1);
	}
#endif

	//rectangle(imgDisp, rectFeatureExtractRegion, CV_RGB(255, 255, 255), 1);

#if Debug
	for (int i = 0; i < vec_imgGlobalHough.size(); i++){
		string strHough = "hough";
		char strIdx[5]; 
		sprintf(strIdx, "%d", i);

		strHough += strIdx;

		imshow(strHough, vec_imgGlobalHough[i]);
	}
#endif
}

void CRForestDetector::ClearMemory()
{
	vec_imgGlobalHough.clear();
	vec_imgFeature.clear();
	vec_rectCandidates.clear();
	vec_sLocalHoughMap.clear();
	vec_HoughOffsetDerive.clear();
}
//
//void CRForestDetector::ReleaseViedo()
//{
//	m_CapVideo.release();
//}

CRForestDetector::~CRForestDetector()
{
}

void CRForestDetector::getVehicleCandidates(vector<Rect>& vec_rectCandidates)
{
	vec_rectCandidates = vec_rectCandidates;
}

void CRForestDetector::LoadMeanNStdevImage()
{
	for (int i = 0; i < 5; i++){
		string strMean = "../LIbVehicle/files/imgMean/posMeanImg";
		string strStdev = "../LIbVehicle/files/imgStdev/posStdevImg";
		string strFormat = ".jpg";

		char strIdx[10];
		sprintf(strIdx, "%d", i);
		strMean += strIdx;
		strMean += strFormat.c_str();
		strStdev += strIdx;
		strStdev += strFormat.c_str();

		for (int i = 0; i < 5; i++){
			vec_imgMeanFeature[i] = Mat(szTrain.height, szTrain.width, CV_32FC1);
			vec_imgStdevFeature[i] = Mat(szTrain.height, szTrain.width, CV_32FC1);
		}

		vec_imgMeanFeature [i] = imread(strMean.c_str(), CV_LOAD_IMAGE_GRAYSCALE);
		if (!vec_imgMeanFeature[i].data){
			cout << "could not open " << strMean << endl;
		}
		vec_imgStdevFeature[i] = imread(strStdev.c_str(), CV_LOAD_IMAGE_GRAYSCALE);
		if (!vec_imgStdevFeature[i].data){
			cout << "could not open " << strStdev << endl;
		}
	}

}

void CRForestDetector::DrawDiscriminativeHoughMap(vector<Mat>& vec_imgDiscriminativeHM)
{

}

void CRForestDetector::InitTrackerInfo()
{
	fScaleDist = 0.5;
	nCntCandidate = 3; // 후보 ROI 검증 횟수, 검증되면 tracking 시작 3
	nCntBefore = 4; // 못찾은 횟수가 연속으로 n프레임 이상이면 ROI 제거 4
	nFrameCandiate = 3; // 못찾은 횟수가 연속으로 n프레임 이상이면 후보 ROI 제거 3
	nAngleThresh = 5;
	
}

void CRForestDetector::KalmanMultiTracker(Track_t & Set, vector<StructKalman>& MultiKF, int& nCntFrame)
{
	Point pCurrent;
	//int nPushCnt = 0;

	//vecBefore: 여러번 검출된 candidate
	//vecBefore가 존재하는 경우
	//4.
	for (int i = 0; i < Set.vecBefore.size(); i++)
	{
		pCurrent.x = imgSrc.cols;
		pCurrent.y = imgSrc.rows;
		int num = -1;

		float fDistXbefore = ptVanishing.x - (Set.vecBefore[i].x + Set.vecBefore[i].width / 2);
		float fDistYbefore = ptVanishing.y - (Set.vecBefore[i].y + Set.vecBefore[i].height / 2);
		float fDistBefore = fDistXbefore*fDistXbefore + fDistYbefore*fDistYbefore;
		fDistBefore = sqrt(fDistBefore);
		float fAngleBefore = fAngle(fDistXbefore, fDistYbefore);


		//kalmant tracking 결과인 MultiKF와 vecRectTracking 비교 후 filtering
		for (int j = 0; j < vec_rectCandidates.size(); j++)
		{
			float fDistXCandi = ptVanishing.x - (vec_rectCandidates[j].x + vec_rectCandidates[j].width / 2);
			float fDistYCandi = ptVanishing.y - (vec_rectCandidates[j].y + vec_rectCandidates[j].height / 2);
			float fDistCandi = fDistXCandi*fDistXCandi + fDistYCandi*fDistYCandi;
			fDistCandi = sqrt(fDistCandi);
			float fAngleCandi = fAngle(fDistXCandi, fDistYCandi);

			// kalman tracking 결과와 1차 검출 사이 거리
			float dist = abs(vec_rectCandidates[j].x + (vec_rectCandidates[j].width) / 2 - MultiKF[i].ptEstimate.x)*abs(vec_rectCandidates[j].x + (vec_rectCandidates[j].width) / 2 - MultiKF[i].ptEstimate.x)
				+ abs(vec_rectCandidates[j].y + (vec_rectCandidates[j].height) / 2 - MultiKF[i].ptEstimate.y)*abs(vec_rectCandidates[j].y + (vec_rectCandidates[j].height) / 2 - MultiKF[i].ptEstimate.y);
			dist = sqrt(dist);

			float pCurrnetXY = pCurrent.x*pCurrent.x + pCurrent.y*pCurrent.y;
			pCurrnetXY = sqrt(pCurrnetXY);

			//1차 검출과 소실점 사이 거리가 kalman tracking과 소실점사이 거리보다 작은 경우 
			//candidate과 vecRectTracking 사이 dx, dy를 pCurrent에 저장
			if (((dist < pCurrnetXY && dist < fScaleDist *Set.vecBefore[i].width&&abs(vec_rectCandidates[j].width - Set.vecBefore[i].width)<Set.vecBefore[i].width*0.3) /*&& (abs(fAngleBefore - fAngleCandi) < ANGLE_THRESH)*/) /*&& vecRectTracking[j].area() >= Set.vecBefore[i].area()*0.8 && vecRectTracking[j].y + vecRectTracking[j].height / 2 - (Set.vecBefore[i].y + (Set.vecBefore[i].height) / 2) <= 1*/)
			{
				pCurrent.x = abs(vec_rectCandidates[j].x + (vec_rectCandidates[j].width) / 2 - MultiKF[i].ptEstimate.x);
				pCurrent.y = abs(vec_rectCandidates[j].y + (vec_rectCandidates[j].height) / 2 - MultiKF[i].ptEstimate.y);
				num = j;
			}
		}

		//이전 프레임에서 추적이 존재하는 경우
		//1차 검출과 tracking 결과의 거리 차이가 조건을 만족하지 않는 경우 
		if (num == -1 && MultiKF.size() > 0)
		{
			MultiKF[i].bOK = false;
			MultiKF[i].ptCenter.x = MultiKF[i].matPrediction.at<float>(0);
			MultiKF[i].ptCenter.y = MultiKF[i].matPrediction.at<float>(1);
			MultiKF[i].speedX = MultiKF[i].matPrediction.at<float>(2);
			MultiKF[i].speedY = MultiKF[i].matPrediction.at<float>(3);
			MultiKF[i].width = MultiKF[i].matPrediction.at<float>(4);
			MultiKF[i].height = MultiKF[i].matPrediction.at<float>(5);

			//vecBefore의 값을 tracking 결과로 대체 (좌표)
			/*	Set.vecBefore[i].x = MultiKF[i].ptPredict.x - (Set.vecBefore[i].width) / 2;
			Set.vecBefore[i].y = MultiKF[i].ptPredict.y - (Set.vecBefore[i].height) / 2;
			*/	////hekim added
			/*	Set.vecBefore[i].width = (MultiKF[i].nEstimateWidth + (Set.vecBefore[i].width)) / 2;
			Set.vecBefore[i].height = (MultiKF[i].nEstimateHeight + (Set.vecBefore[i].height)) / 2;*/
			/*	Set.vecBefore[i].width = MultiKF[i].sizePredict.width;
			Set.vecBefore[i].height = MultiKF[i].sizePredict.height;*/

			Set.vecBefore[i].x = MultiKF[i].ptPredict.x - (Set.vecBefore[i].width) / 2;
			Set.vecBefore[i].y = MultiKF[i].ptPredict.y - (Set.vecBefore[i].height) / 2;

			/*Set.vecBefore[i].width = MultiKF[i].nEstimateWidth;
			Set.vecBefore[i].height = MultiKF[i].nEstimateHeight;*/

			//


		}
		else if (Set.vecBefore.size() != 0)
		{

			//num==-1 이어서 에러남 ㅎㅎㅎㅎ

			//Kalman parameter 갱신
			MultiKF[i].bOK = true;
			MultiKF[i].speedX = vec_rectCandidates[num].x + (vec_rectCandidates[num].width) / 2 - (Set.vecBefore[i].x + (Set.vecBefore[i].width) / 2);
			MultiKF[i].speedY = vec_rectCandidates[num].y + (vec_rectCandidates[num].height) / 2 - (Set.vecBefore[i].y + (Set.vecBefore[i].height) / 2);

			Set.vecBefore[i] = vec_rectCandidates[num];
			vec_rectCandidates.erase(vec_rectCandidates.begin() + num);
			MultiKF[i].ptCenter.x = Set.vecBefore[i].x + (Set.vecBefore[i].width) / 2;
			MultiKF[i].ptCenter.y = Set.vecBefore[i].y + (Set.vecBefore[i].height) / 2;
			MultiKF[i].width = Set.vecBefore[i].width;
			MultiKF[i].height = Set.vecBefore[i].height;

			//MultiKF[i].width = MultiKF[i].sizePredict.width;
			//MultiKF[i].height = MultiKF[i].sizePredict.height;

			Set.vecCount[i] = nCntFrame; //vecCount 갱신

		}
		bool bcheck = false;

		//	Tracking box 검증 
		// 반복 candidate과 tracking 결과 필터링
		if (Set.vecBefore[i].y < 0 || Set.vecBefore[i].x < 0 || Set.vecBefore[i].y + Set.vecBefore[i].height >= imgSrc.rows || Set.vecBefore[i].x + Set.vecBefore[i].width >= imgSrc.cols/* || abs(MultiKF[i].ptCenter.x - Set.vecBefore[i].x)> fStandardWidth * TRACKING_MARGIN_RATIO*//* && fDistYCandi>0*/)
		{
			Set.vecBefore.erase(Set.vecBefore.begin() + i);
			MultiKF.erase(MultiKF.begin() + i);
			Set.vecCount.erase(Set.vecCount.begin() + i);
			i--;
			bcheck = true;
		}

		if (bcheck == false && (/*imgROImask.at<uchar>((Set.vecBefore[i].y < 0 ? 0 : Set.vecBefore[i].y), (Set.vecBefore[i].x < 0 ? 0 : Set.vecBefore[i].x)) == 0 ||*/ imgTrackingRegion.at<uchar>((Set.vecBefore[i].y + Set.vecBefore[i].height < imgSrc.rows ? Set.vecBefore[i].y + Set.vecBefore[i].height : imgSrc.rows - 1), (Set.vecBefore[i].x + Set.vecBefore[i].width < imgSrc.cols ? Set.vecBefore[i].x + Set.vecBefore[i].width : imgSrc.cols - 1)) == 0 || (nCntFrame - Set.vecCount[i])>nCntBefore) && Set.vecBefore.size() != 0 && MultiKF.size() != 0){
			Set.vecBefore.erase(Set.vecBefore.begin() + i);
			MultiKF.erase(MultiKF.begin() + i);
			Set.vecCount.erase(Set.vecCount.begin() + i);
			i--;
		}
	}
	///// Kalman filtering 시작 //////////////x
	//3. 
	for (int i = 0; i < Set.vecCandidate.size(); i++)
	{
		pCurrent.x = imgSrc.cols;
		pCurrent.y = imgSrc.rows;
		///////////
		int num = -1;
		//vecRectTracking: 새로 갱신된 1차 검출들
		for (int j = 0; j < vec_rectCandidates.size(); j++)
		{
			//Candidate과 vecRectTracking 사이 거리 계산 (dist)
			float dist = abs(vec_rectCandidates[j].x + (vec_rectCandidates[j].width) / 2 - (Set.vecCandidate[i].x + (Set.vecCandidate[i].width) / 2))*abs(vec_rectCandidates[j].x + (vec_rectCandidates[j].width) / 2 - (Set.vecCandidate[i].x + (Set.vecCandidate[i].width) / 2))
				+ abs(vec_rectCandidates[j].y + (vec_rectCandidates[j].height) / 2 - (Set.vecCandidate[i].y + (Set.vecCandidate[i].height) / 2))*abs(vec_rectCandidates[j].y + (vec_rectCandidates[j].height) / 2 - (Set.vecCandidate[i].y + (Set.vecCandidate[i].height) / 2));
			dist = sqrt(dist);
			// 초기 pCurrentXY는 이미지 대각선 길이
			//
			float pCurrnetXY = pCurrent.x*pCurrent.x + pCurrent.y*pCurrent.y;
			pCurrnetXY = sqrt(pCurrnetXY);
			//candidate과 vecRectTracking 사이 거리가 pCurrentXY보다 작다면
			//거리가 candidate의 너비*비례상수 값보다 작은 경우
			if (dist < pCurrnetXY && dist < fScaleDist * 2 * Set.vecCandidate[i].width)
			{
				float fDistXCandi = ptVanishing.x - (vec_rectCandidates[j].x + vec_rectCandidates[j].width / 2);
				float fDistYCandi = ptVanishing.y - (vec_rectCandidates[j].y + vec_rectCandidates[j].height / 2);
				float fDistCandi = fDistXCandi*fDistXCandi + fDistYCandi*fDistYCandi;
				fDistCandi = sqrt(fDistCandi);
				float fAngleCandi = fAngle(fDistXCandi, fDistYCandi);
				/*	cout << "fStandardWidth : " << fStandardWidth << endl;
				cout << "dx : " <<abs( vecRectTracking[j].x - Set.vecCandidate[i].x )<< endl;*/

				/*	cout << "1차후보-소실점 거리	" << Set.vecRtheta[i].x << endl;
				cout << "2차후보-소실점 거리	" << fDistCandi << endl;

				cout << "1차후보-소실점 각도	" << Set.vecRtheta[i].y << endl;
				cout << "2차후보-소실점 각도	" << fAngleCandi << "	차	" << Set.vecRtheta[i].y - fAngleCandi << endl;
				*/

				//candidate과 소실점 사이 거리가 vecRectTracking과 소실점사이 거리보다 작은 경우 
				//candidate과 소실점 사이 각도와 vecRectTracking과 소실점사이 각도의 차이가 임계값보자 작은 경우
				//candidate과 vecRectTracking 사이 dx, dy를 pCurrent에 저장
				if (/*Set.vecRtheta[i].x < fDistCandi &&*/ abs(Set.vecRtheta[i].y - fAngleCandi) < nAngleThresh/*&&abs(vecRectTracking[j].x - Set.vecCandidate[i].x)<fStandardWidth * TRACKING_MARGIN_RATIO*/)
				{
					pCurrent.x = abs(vec_rectCandidates[j].x + (vec_rectCandidates[j].width) / 2 - (Set.vecCandidate[i].x + (Set.vecCandidate[i].width) / 2));
					pCurrent.y = abs(vec_rectCandidates[j].y + (vec_rectCandidates[j].height) / 2 - (Set.vecCandidate[i].y + (Set.vecCandidate[i].height) / 2));
					num = j;	//새로 갱신된 1차 검출의 index 저장 
				}
			}
		}
		int speedtempX = 0;
		int speedtempY = 0;
		//위 조건을 만족하는 candidate이 있는 경우
		if (num > -1 && vec_rectCandidates.size() > 0)
		{
			Set.vecCountPush[i].x++;			// 찾으면 +1
			Set.vecCountPush[i].y = nCntFrame;	// 찾았을때의 frame 갱신
			speedtempX = vec_rectCandidates[num].x - Set.vecCandidate[i].x;	//한프레임사이 x축에서 이동거리 
			speedtempY = vec_rectCandidates[num].y - Set.vecCandidate[i].y;	//한프레임사이 y축에서 이동거리
			Set.vecCandidate[i] = vec_rectCandidates[num];
			//////////
			//Candidate의 dist 값과 angle값을 매칭 1차 검출의 정보로 갱신
			float fDistXCandi = ptVanishing.x - (vec_rectCandidates[num].x + vec_rectCandidates[num].width / 2);
			float fDistYCandi = ptVanishing.y - (vec_rectCandidates[num].y + vec_rectCandidates[num].height / 2);
			float fDistCandi = fDistXCandi*fDistXCandi + fDistYCandi*fDistYCandi;
			fDistCandi = sqrt(fDistCandi);
			float fAngleCandi = fAngle(fDistXCandi, fDistYCandi);
			Set.vecRtheta[i] = Point2f(fDistCandi, fAngleCandi);
			//////
			//	cout << i<<" R thetha : " << Set.vecRtheta[i]<<endl;
			vec_rectCandidates.erase(vec_rectCandidates.begin() + num);
		}
		// cntCandidate넘게 찾으면 새로운 target으로 인식 (candidate이 여러 프레임에서 검출된 경우)
		if (Set.vecCountPush[i].x >= nCntCandidate)
		{
			////
			//vecBefore : 최종 tracking target
			//tracking 관련 MultiKF 저장
			Set.vecBefore.push_back(Set.vecCandidate[i]);	//Candidate를 vecBefore에 저장
			/*nPushCnt++;*/
			StructKalman temKalman;
			temKalman.speedX = speedtempX;
			temKalman.speedY = speedtempY;
			cKalman->kalmanTrackingStart(temKalman, Set.vecCandidate[i]);
			MultiKF.push_back(temKalman);
			Set.vecCount.push_back(nCntFrame);

			Set.vecCandidate.erase(Set.vecCandidate.begin() + i);
			Set.vecCountPush.erase(Set.vecCountPush.begin() + i);
			Set.vecRtheta.erase(Set.vecRtheta.begin() + i);
			i--;
		}
	}

	//1.
	//vecRectTracking : 1차 검출 
	//1차 검출 box들을 candidate 선정

	for (int i = 0; i < vec_rectCandidates.size(); i++)
	{
		//Set.vecRtheta.push_back(

		float fDistXCandi = ptVanishing.x - (vec_rectCandidates[i].x + vec_rectCandidates[i].width / 2);
		float fDistYCandi = ptVanishing.y - (vec_rectCandidates[i].y + vec_rectCandidates[i].height / 2);
		float fDistCandi = fDistXCandi*fDistXCandi + fDistYCandi*fDistYCandi;
		fDistCandi = sqrt(fDistCandi);

		float fAngleCandi = fAngle(fDistXCandi, fDistYCandi);
		Set.vecRtheta.push_back(Point2f(fDistCandi, fAngleCandi));	//candidate의 각도 정보 저장
		Set.vecCandidate.push_back(vec_rectCandidates[i]);	//candidiate 벡터에 저장
		Set.vecCountPush.push_back(Point(0, nCntFrame));	//frame 정보 저장
	}
	//2. 
	//frameCandidate이상 연속으로 못찾으면 제거
	//cntframe: 현재 frame 번호
	//candidiate들의 frame 번호와 비교하면서 차이가 frameCandidate이상이면 candidate의 정보를 제거한다
	for (int i = 0; i < Set.vecCountPush.size(); i++)
	{

		if (nCntFrame - Set.vecCountPush[i].y >= nFrameCandiate)
		{
			Set.vecCandidate.erase(Set.vecCandidate.begin() + i);
			Set.vecCountPush.erase(Set.vecCountPush.begin() + i);
			Set.vecRtheta.erase(Set.vecRtheta.begin() + i);
			i--;
		}
	}

	/// Kalman filtering 시작 //////////////
	for (unsigned int i = 0; i<Set.vecBefore.size(); i++)
	{
		//	cout << "vecBefore size : "<<Set.vecBefore.size();
		bool bfinish = cKalman->DoKalmanFiltering(MultiKF[i], Set.vecBefore[i], imgTrackingRegion);
		//kalman filter에 의해 예측된 사각형이 화면(roi) 넘어가는 경우 제거
		if (bfinish == false)
		{
			Set.vecBefore.erase(Set.vecBefore.begin() + i);
			MultiKF.erase(MultiKF.begin() + i);
			Set.vecCount.erase(Set.vecCount.begin() + i);
			i--;
			bfinish = true;
		}
	}

	/*cout << "vecBefore Push " << nPushCnt << endl;
	cout << "vecBefore Size " << Set.vecBefLoadTestDataore.size() << endl;*/
	vec_rectCandidates = High.vecBefore;
}

void CRForestDetector::DetectForEval(Mat &imgInput, float& fThreshold, sOutput &sDetection)
{
	ClearMemory();

	imgSrc = imgInput.clone();
	GenerateHoughMapForEval();

	if (CheckTrueForEval(fThreshold)){
		sDetection.TP++;
	}
	else sDetection.FP++;

}

void CRForestDetector::GenerateHoughMapForEval()
{
	if (imgSrc.channels() == 1) {
		std::cerr << "Gray color images are not supported." << std::endl;
	}
	else {
		Mat imgTemp;
		Mat imgTemp2;

		vector<Mat>  vImgTemp;
		vector<vector<Mat>> vec_storeROI;
		vector<Mat> vecTemp;

		vec_storeROI.resize(vec_fScale.size());

		Mat imgCopy = imgSrc.clone();

		// ROI로부터 feature 추출 - 각각 벡터(vImg)에 저장
		extractFeatureChannels(imgCopy, vec_imgFeature);
		vecTemp.resize(vec_imgFeature.size());

		// 학습 이미지로 리사이즈
		Size szTrainData = Size(szTrain.width, szTrain.height);
		for (int i = 0; i < vec_imgFeature.size(); i++){
			resize(vec_imgFeature[i], vecTemp[i], szTrainData);
		}

		imgTemp = Mat(szTrainData.height, szTrainData.width, CV_32FC1);

		// feauture 이미지들로부터 허프맵 생성
		GenerateHoughMapInGlobalROI(vecTemp, imgTemp);
		vec_imgGlobalHough.push_back(imgTemp);

	}
}

bool CRForestDetector::CheckTrueForEval(float &fThreshold)
{
	double dMax = 0;
	Mat imgCopyHough = vec_imgGlobalHough[0].clone();
	
	minMaxLoc(imgCopyHough, 0, &dMax);
	//cout << "Max " << dMax <<" threshold "<<fThreshold<< endl;

	if (dMax > fThreshold) return true;
	else return false;
}

void CRForestDetector::GetResultRects(vector<Rect>& vec_rectBB){
	vec_rectBB.clear();
	//printf("vec_rectCandidates %d\n", vec_rectCandidates.size());

	vec_rectBB.assign(vec_rectCandidates.begin(), vec_rectCandidates.end());
}
void CRForestDetector::GetResultScores(vector<float>& vec_fObjScore){
	vec_fObjScore.clear();
	//printf("vec_fCandidates %d\n", vec_fCandidates.size());
	vec_fObjScore.assign(vec_fCandidates.begin(), vec_fCandidates.end());
}


