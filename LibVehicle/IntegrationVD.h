#pragma once

#include <stdint.h>
#include "../include/DefStruct.h"
#include "CRForestDetector.h"

using namespace cv;

class CIntegrationVD{
public:
	//CIntegrationVD(/*CRForestDetector *crDT*/);
	CIntegrationVD(bool &flgReady);
	~CIntegrationVD();

	void RunOnce(Mat &imgInput, vector<Object_t> &vec_objVD, SLanes &sLane);

private:
	CRForestDetector *m_objVD;
	double boxoverlap(Rect_<double> a, Rect_<double> b, int32_t criterion=0);

};