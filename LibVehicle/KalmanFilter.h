#ifndef KALMANFILTER_H
#define KALMANFILTER_H

#include <opencv2/opencv.hpp>
#include <iostream>
using namespace std;
using namespace cv;

struct StructKalman{

	KalmanFilter KF;

	Mat_<float> state; /* (x, y, Vx, Vy) */
	Mat processNoise;
	Mat_<float> smeasurement;
	Scalar rgb;
	int failcase;
	int succcase;
	Point ptEstimate;
	//int ptEstimate.x;
	//int ptEstimate.y;
	Mat matPrediction;
	Point ptCenter;
	//int ptCenter.x;
	//int ptCenter.y;
	bool bOK;

	int width;
	int height;
	int speedX;
	int speedY;
	Point ptPredict;
	Size sizePredict;

	StructKalman()
	{
		KalmanFilter temKF(8, 6, 0);	//상태8, 측정6
		Mat_<float> temState(8, 1);	//상태값(??)
		Mat temProcessNoise(8, 1, CV_32F);	//노이즈 상태값4
		Mat_<float> temMeasurement(6, 1);	//측정6
		temKF.statePost.at<float>(2) = 0;
		temKF.statePost.at<float>(3) = 0;
		temKF.statePost.at<float>(4) = 0;
		temKF.statePost.at<float>(5) = 0;
		temKF.statePost.at<float>(6) = 0;
		temKF.statePost.at<float>(7) = 0;
		temKF.statePre.at<float>(2) = 0;
		temKF.statePre.at<float>(3) = 0;
		temKF.statePre.at<float>(4) = 0;
		temKF.statePre.at<float>(5) = 0;
		temKF.statePost.at<float>(6) = 0;
		temKF.statePost.at<float>(7) = 0;
		//kalmansetting(temKF,temMeasurement);
		temMeasurement.at<float>(2) = 0;
		temMeasurement.at<float>(3) = 0;
		KF = temKF;
		state = temState;
		processNoise = temProcessNoise;
		smeasurement = temMeasurement;

		speedX = 0;
		speedY = -3;
	}

};


class CKalman{
	//friend class CRForestDetector;

private:
public:
	CKalman();
	~CKalman();
		
	void DrawCross(Mat img, Point center, Scalar color, int d);
	void SetKalmanParameter(KalmanFilter& KF, Mat_<float>& measurement);
	int DoKalmanFiltering(StructKalman& MultiKF, Rect& rec, Mat& imgROImask);
	void kalmanTrackingStart(StructKalman& MultiKF, Rect& recStart);
};

#endif

