#include "CRForestDetector_CutIn.h"

CRForestDetectorCI::CRForestDetectorCI(const CRConfig& crCF, /*const*/ CRForest& cRF, CKalman& cKM)
	: CRForestDetector(crCF, cRF, cKM)
{
	//InitializerCI();
}

void CRForestDetectorCI::InitializeROICI()
{
	// HanYang DB
	Size szROI = Size(0.3*imgSrc.cols, 0.25*imgSrc.rows);
	//Point ptROI = Point(0.6*imgSrc.cols, 0.4*imgSrc.rows);
	Point ptROI = Point(0.7*imgSrc.cols, 0.4*imgSrc.rows);
	Rect rectROI, rectFeatureROI, rectTrackingROI;

	for (int i = 0; i < 2; i++){
		rectROI = Rect(ptROI, szROI);
		vec_rectROI.push_back(rectROI);
		//ptROI = Point(2, 250);
		//ptROI = Point(0.1*imgSrc.cols, 0.4*imgSrc.rows);
		ptROI = Point(0, 0.4*imgSrc.rows);

		rectFeatureROI = vec_rectROI[i];
		rectTrackingROI = vec_rectROI[i];
		vec_rectFeatureExtractRegion.push_back(rectFeatureROI);
		vec_rectTrackROI.push_back(rectTrackingROI);

	}
}

void CRForestDetectorCI::InitializerCI()
{
	// 소실점 설정
	//setVanishingPoint(640, 360);
	//KITTI data vanishing point
	setVanishingPoint(0.5*imgSrc.cols, 0.5*imgSrc.rows);

	// 프레임 인덱스 설정
	nFrameIdx = 0;
	// 비디오 시작 플래그 설정
	bStartOfVideo = true;

	// 초기 전역적 ROI 설정
	InitializeROICI();

	// 트랙커 정보 설정
	CRForestDetector::InitTrackerInfo();

}

void CRForestDetectorCI::DetectVehicle(Mat& imgVideoInput)
{
	ClearMemory();

	imgVideoInput.copyTo(imgSrc);

	if (bStartOfVideo == true){
		InitializerCI();
		// 트랙킹 영역 지정
		imgTrackingRegion = Mat::zeros(imgSrc.size(), CV_8UC1);
		imgTrackingRegion(rectTrackROI).setTo(Scalar::all(255));
		bStartOfVideo = false;
	}

	DetectInCutInROI();
	//CRForestDetector::KalmanMultiTracker(High, MultiKF, nFrameIdx);
	//CRForestDetector::ClusterRect(vec_rectCandidates);

	nFrameIdx++;
}

void CRForestDetectorCI::DetectInCutInROI()
{
	// 허프맵 생성
	GenerateHoughMapPyramidCI();

	// 허프맵의 peak들로부터 자동적으로 임계값 계산
	CRForestDetector::FindAutomaticThreshold();

	// 전역적 ROI에서 차량 후보 검출
	FindVehicleCandidateCI();
}

void CRForestDetectorCI::LocalizeCutInVehicleCenter()
{

}

void CRForestDetectorCI::GenerateHoughMapPyramidCI()
{
	if (imgSrc.channels() == 1) {
		std::cerr << "Gray color images are not supported." << std::endl;
	}
	else{
		Mat imgTemp;
		Mat imgTemp2;

		vector<Mat>  vImgTemp;
		vector<vector<Mat>> vec_storeROI;
		vector<Mat> vecTemp;

		vec_storeROI.resize(vec_fScale.size());

		for (int r = 0; r < vec_rectROI.size(); r++){
			Mat mFirstROI = imgSrc(vec_rectFeatureExtractRegion[r]);
			// ROI로부터 feature 추출 - 각각 벡터(vImg)에 저장
			CRForestDetector::extractFeatureChannels(mFirstROI, vec_imgFeature);
			//
			vImgTemp.resize(vec_imgFeature.size());
			int num_scale, num_feature;

			for (num_scale = 0; num_scale < vec_fScale.size(); num_scale++){
				Size szImgFeature = Size(vec_imgFeature[0].cols*vec_fScale[num_scale], vec_imgFeature[0].rows*vec_fScale[num_scale]);
				for (num_feature = 0; num_feature < vec_imgFeature.size(); num_feature++){
					resize(vec_imgFeature[num_feature], vImgTemp[num_feature], szImgFeature);
					// 수정
				}
				imgTemp = Mat(szImgFeature.height, szImgFeature.width, CV_32FC1);
				vec_imgGlobalHough.push_back(imgTemp);

				CRForestDetector::GenerateHoughMapInGlobalROI(vImgTemp, vec_imgGlobalHough.back());
				//vImgTemp.clear();
			}
		}
	}

}

void CRForestDetectorCI::FindVehicleCandidateCI()
{
	vector<Point>  vec_gloabalPts;
	vector<Rect> vec_detected;

	FindPeaksInHoughCI(vec_gloabalPts);

	// 차량 사이즈 계산
	CalculateVehicleSizeCI(vec_gloabalPts, vec_detected);

	// 차량 후보 군집화
	CRForestDetector::GroupVehicleCandidates(vec_detected);
}

void CRForestDetectorCI::FindPeaksInHoughCI(vector<Point> &vec_gloabalPoints)
{
	vector<sPointInfo> vecPoint;
	Mat imgROI, vImgHough;
	Point ptTemp;

	for (int r = 0; r < vec_rectROI.size(); r++){
		//detect the vehicles using Hough map
		for (int num_scale = 0; num_scale < vec_fScale.size(); num_scale++){
			//vec_imgGlobalHough[num_scale+r*vec_fScale.size()].copyTo(vImgHough);
			vImgHough = vec_imgGlobalHough[num_scale + r*vec_fScale.size()].clone();

			//// to show hough map in ROIs -- can be erased
			//imgROI = imgSrc(vec_rectROI[num_scale]);
			//Size mROISize = Size(vImgHough.cols, vImgHough.rows);
			//resize(imgROI, imgROI, mROISize);
			////

			// 허프맵의 peak들을 추출
			FindMaximum(vImgHough, fGlobalThreshold, vec_fScale[num_scale], vecPoint);
			for (int i = 0; i < vecPoint.size(); i++){
				ptTemp = vec_rectROI[r].tl() + Point(vecPoint[i].nX/* / (float)vec_fScale[num_scale]*/, vecPoint[i].nY /*/ (float)vec_fScale[num_scale]*/);
				vec_gloabalPoints.push_back(ptTemp);
				// 1차 검출된 peak 점들을 영상에 출력
				circle(imgSrc, ptTemp, 3, Scalar(255, 0, 0), 3);	//	check
			}
			vecPoint.clear();
		}
	}
}

void CRForestDetectorCI::CalculateVehicleSizeCI(vector<Point> &vec_gloabalPoints, vector<Rect> &vec_Detected)
{
	vector<Point> vecMaxPt;
	Rect rectDetect;

	for (int i = 0; i < vec_gloabalPoints.size(); i++){
		//circle(imgSrc, vec_gloabalPoints[i], 3, Scalar(255, 0, 255), 3);	

		// 전역적 ROI로부터 차량 중심점 위치계산 
		LocalizeGlobalVehicleCenterCI(vec_gloabalPoints[i], vecMaxPt);
		// peak의 intensity에 따른 영상 내 차량 사이즈 계산 
		CalculateProperBoxSizeCI(vecMaxPt, rectDetect);

		rectDetect.x = vec_gloabalPoints[i].x - rectDetect.width / 2;
		rectDetect.y = vec_gloabalPoints[i].y - rectDetect.height / 2;
		vec_Detected.push_back(rectDetect);
		vecMaxPt.clear();
	}
}

void CRForestDetectorCI::CalculateProperBoxSizeCI(vector<Point>& ptMaxInROIs, Rect& rectIPBox)
{
	Mat mTempHough;
	float fSizeIntensity = 0;
	float fMax = 0;
	float fOptimalScale = 0;
	int nOptScaleNum = 0;
	int nY = 0, nX = 0;
	Point ptVCenter;

	// 한점에 대해 어느 ROI에서 가장 높은 확률 누적값을 가지는지 확인
	for (int i = 0; i < ptMaxInROIs.size(); i++){
		mTempHough = vec_imgGlobalHough[i];
		if (ptMaxInROIs[i].y < vec_imgGlobalHough[i].rows - 1, ptMaxInROIs[i].x < vec_imgGlobalHough[i].cols - 1){
			fSizeIntensity = mTempHough.at<float>(ptMaxInROIs[i].y, ptMaxInROIs[i].x);

			// Peak point에서 각기 다른 허프맵에서의 intensity를 계산 후 최대값 비교 
			if (fSizeIntensity > fMax){
				fOptimalScale = vec_fScale[i];
				fMax = fSizeIntensity;
				nOptScaleNum = i;
			}
		}
		rectIPBox.width = szTrain.width / (float)(fOptimalScale*2);
		rectIPBox.height = szTrain.height / (float)(fOptimalScale*2);
	
	}
	/*
	// peak 점에서의 검출 사이즈 계산
	if (ptMaxInROIs.size() != 0){
		
		if (nOptScaleNum < vec_imgGlobalHough.size() - 1){
			ptVCenter = ptMaxInROIs[nOptScaleNum];

			Point ptMiddleTL = (vec_rectROI[nOptScaleNum].tl() + vec_rectROI[nOptScaleNum + 1].tl());
			Point ptMiddleBR = (vec_rectROI[nOptScaleNum].br() + vec_rectROI[nOptScaleNum + 1].br());
			ptMiddleTL.x /= 2;
			ptMiddleTL.y /= 2;
			ptMiddleBR.x /= 2;
			ptMiddleBR.y /= 2;

			nX = ptMaxInROIs[nOptScaleNum].y;
			nY = ptMaxInROIs[nOptScaleNum].y;

			if (ptVCenter.x > ptMiddleTL.x && ptVCenter.x<ptMiddleBR.x && ptVCenter.y>ptMiddleTL.y && ptVCenter.y < ptMiddleBR.y){
				rectIPBox.width = szTrain.width / ((fOptimalScale + vec_fScale[nOptScaleNum + 1]) / (float)2);
				rectIPBox.height = szTrain.height / ((fOptimalScale + vec_fScale[nOptScaleNum + 1]) / (float)2);
			}
			else{
				rectIPBox.width = szTrain.width / (float)fOptimalScale;
				rectIPBox.height = szTrain.height / (float)fOptimalScale;
			}
		}
		else{
			rectIPBox.width = szTrain.width / (float)fOptimalScale;
			rectIPBox.height = szTrain.height / (float)fOptimalScale;
		}
	}
	*/
}

void CRForestDetectorCI::LocalizeGlobalVehicleCenterCI(Point pt, vector<Point>& ptMaxInROIs)
{
	Point nTempPt;

	//각 Hough Map에 해당하는 point 위치 계산
	//for (int r = 0; r < vec_rectROI.size();r++){
		for (int i = 0; i < vec_fScale.size(); i++){
			if (pt.x < imgSrc.cols/2)
				nTempPt = (pt - vec_rectROI[1].tl())*vec_fScale[i];
			else
				nTempPt = (pt - vec_rectROI[0].tl())*vec_fScale[i];

			ptMaxInROIs.push_back(nTempPt);
		}
	//}
}

void CRForestDetectorCI::ShowVehicleCI(Mat& imgDisp)
{
	CRForestDetector::ShowVehicles(imgDisp);
}

void CRForestDetectorCI::ClearMemoryCI()
{
	vec_imgGlobalHough.clear();
	vec_imgFeature.clear();
	vec_rectCandidates.clear();
	vec_sLocalHoughMap.clear();
}

CRForestDetectorCI::~CRForestDetectorCI()
{

}

