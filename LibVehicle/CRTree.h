/* 
// Author: Juergen Gall, BIWI, ETH Zurich
// Email: gall@vision.ee.ethz.ch
*/

#pragma once
#include "opencv/cxcore.h"
#include "opencv/cv.h"

#include <iostream>
#include <fstream>

#define _copysign copysign

using namespace std;
using namespace cv;

// Structure for the leafs
struct LeafNode {
	// Vectors from object center to training patches
	vector<vector<CvPoint> > vCenter;
	// Probability of foreground
	float pfg;

	// Constructors
	LeafNode() {}

	// IO functions
	void show(int delay, int width, int height);
	void print() const {
		cout << "Leaf " << vCenter.size() << " " << pfg << endl;
	}

};

class CRTree {
public:
	// Constructors
	CRTree(const char* filename);
	CRTree(int min_s, int max_d, int cp, CvRNG* pRNG) : min_samples(min_s), max_depth(max_d), num_leaf(0), num_cp(cp), cvRNG(pRNG) {
		num_nodes = (int)pow(2.0,int(max_depth+1))-1;		//max_depth (tree�� normal node ��) = 15 --> 32767	==> �ϳ��� Ʈ���� �� ��� ����
		// num_nodes x 7 matrix as vector
		treetable = new int[num_nodes * 7];
		for(unsigned int i=0; i<num_nodes * 7; ++i) treetable[i] = 0;
		// allocate memory for leafs
		leaf = new LeafNode[(int)pow(2.0,int(max_depth))];
	}
	~CRTree() {delete[] leaf; delete[] treetable;}

	// Set/Get functions
	unsigned int GetDepth() const {return max_depth;}
	unsigned int GetNumCenter() const {return num_cp;}

	// Regression
	const LeafNode* regression(uchar** ptFCh, int stepImg) const;

private: 

	// Data structure

	// tree table
	// 2^(max_depth+1)-1 x 7 matrix as vector
	// column: leafindex x1 y1 x2 y2 channel thres
	// if node is not a leaf, leaf=-1
	int* treetable;

	// stop growing when number of patches is less than min_samples
	unsigned int min_samples;

	// depth of the tree: 0-max_depth
	unsigned int max_depth;

	// number of nodes: 2^(max_depth+1)-1
	unsigned int num_nodes;

	// number of leafs
	unsigned int num_leaf;

	// number of center points per patch
	unsigned int num_cp;

	//leafs as vector
	LeafNode* leaf;

	CvRNG *cvRNG;
};

inline const LeafNode* CRTree::regression(uchar** ptFCh, int stepImg) const {
	// pointer to current node
	const int* pnode = &treetable[0];	//Start with tree #0
	int node = 0;

	// Go through tree until one arrives at a leaf, i.e. pnode[0]>=0)
	while(pnode[0]==-1) {
		// binary test 0 - left, 1 - right
		// Note that x, y are changed since the patches are given as matrix and not as image 
		// p1 - p2 < t -> left is equal to (p1 - p2 >= t) == false
		
		// pointer to channel
		uchar* ptC = ptFCh[pnode[5]];
		// get pixel values 
		int p1 = *(ptC+pnode[1]+pnode[2]*stepImg);
		int p2 = *(ptC+pnode[3]+pnode[4]*stepImg);
		// test
		bool test = ( p1 - p2 ) >= pnode[6];

		// next node: 2*node_id + 1 + test
		// increment node/pointer by node_id + 1 + test
		int incr = node+1+test;
		node += incr;
		pnode += incr*7;
	}

	// return leaf
	return &leaf[pnode[0]];
}
