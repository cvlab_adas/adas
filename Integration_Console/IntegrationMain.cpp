#define _CRT_SECURE_NO_WARNINGS

#include "../UtilDatasetLoader/DatasetLoader.h"
#include "../UtilTimeChecker/TimeChecker.h"

#include "../LibLane/CMultiROILaneDetection.h"
#include "../LibStereo/StixelEstimation.h"
#include "../LibVehicle/CRForestDetector_CutIn.h"
#include "../LibPedestrian/PedestrianACF2.h"

#ifdef _DEBUG
#pragma comment(lib, "../Debug/UtilDatasetLoader.lib")
#pragma comment(lib, "../Debug/LibLane.lib")
#pragma comment(lib, "../Debug/LibStereo.lib")
#pragma comment(lib, "../Debug/LibVehicle.lib")
#pragma comment(lib, "../Debug/LibPedestrian.lib")
#else
#pragma comment(lib, "../Release/UtilDatasetLoader.lib")
#pragma comment(lib, "../Release/LibLane.lib")
#pragma comment(lib, "../Release/LibStereo.lib")
#pragma comment(lib, "../Release/LibVehicle.lib")
#pragma comment(lib, "../Release/LibPedestrian.lib")
#endif

int main()
{
	CDatasetCreator dc;
	if (!dc.LoadConfigFile("Config.xml"))
		return -1;
	CDatasetLoader* objLoader = dc.Create();
	if (!objLoader) return -1;

	////////// Lane ////////////////////////////////////////////////////////////////
	CMultiROILaneDetection objLD(1242, 375, 3.4, 3.6, 984.2439, 980.8141, 690.0, 233.196, 1730.0);
	objLD.setRoiIpmConfig();
	vector<SLane> lanes; //검출된 차선을 받아오기 위한 변수.
	vector<Rect> rectResults; //검출된 차선으로 기반으로 ROI를 받아오기 위한 변수.

	////////// Stereo ////////////////////////////////////////////////////////////////
	CStixelEstimation objStixelEstimation;
	objStixelEstimation.m_flgDisplay = true;
	objStixelEstimation.m_flgColor = false;
	objStixelEstimation.SetParam(CStixelEstimation::KITTI);
	int nError = objStixelEstimation.SetStixelWidth(1);
	if (nError == -1){
		printf("Error!!\n");
		return -1;
	}

	////////// Vehicle ////////////////////////////////////////////////////////////////
	CRConfig crConfig("../LibVehicle/config.txt");
	CRForest crForest(crConfig);
	CKalman cObjKalman;
	CRForestDetector crDetect(crConfig, crForest, cObjKalman);

	////////// Pedstrian ////////////////////////////////////////////////////////////////
	CPedestrianACF2 objPD;
	//CPedestrianCascade objPD;
	if (!objPD.LoadClassifier("../LibPedestrian/ACF_Caltech+.txt")) return -1;

	////////// Time Checker ////////////////////////////////////////////////////////////////
	vector<string> vecstrType = { "LD", "ST", "VD", "PD" };
	vector<CTimeChecker<msec> > tc(4);

	SDataFormat sData;
	Mat imgInput[2];
	Mat imgDisp;
	Mat imgDispStixel;

	bool flgOnOff[4] = { true, true, true, true };

	double nSize = 2;
	int nDelay = 0;

	while (1)
	{
		*objLoader >> sData;

		for (int i = 0; i < objLoader->m_nVidChannel; i++)
			imgInput[i] = sData.img[i];

		imgDisp = imgInput[0].clone(); // left image
		imgDispStixel = imgInput[0].clone(); // left image

		// Lane
		if (flgOnOff[0])
		{
			tc[0].tic();
			objLD.Detection(imgInput[0]);
			tc[0].toc();
			objLD.DisplayResult(lanes, imgDisp);
		}

		// Stereo
		if (flgOnOff[1])
		{
			tc[1].tic();
			objStixelEstimation.StixelEstimation(imgInput[0], imgInput[1], true);
			tc[1].toc();
			objStixelEstimation.Display(imgDisp, imgDispStixel);
		}
		
		// Vehicle
		if (flgOnOff[2])
		{
			tc[2].tic();
			crDetect.DetectVehicle(imgInput[0]);
			tc[2].toc();
			crDetect.ShowVehicles(imgDisp);
		}

		// Pedestrian
		if (flgOnOff[3])
		{
			tc[3].tic();
			objPD.Detect(imgInput[0]);
			tc[3].toc();
			objPD.DrawResult(imgDisp);
		}

		double sum = 0;
		for (int i = 0; i < (int)vecstrType.size(); i++)
		{
			if (!flgOnOff[i]) continue;
			sum += tc[i].to<double>();
			putText(imgDisp, vecstrType[i] + ": " + tc[i].to_string(2) + "ms", Point(10, i * 30 + 30), FONT_HERSHEY_COMPLEX, 0.8, CV_RGB(255, 0, 255), 2);
		}
		char szTemp[100]; sprintf(szTemp, "Total: %.2lfms", sum);
		putText(imgDisp, szTemp, Point(10, 150), FONT_HERSHEY_COMPLEX, 0.8, CV_RGB(255, 100, 255), 2);

		imshow("result", imgDisp);
		imshow("Stixel", imgDispStixel);


		objLD.ReleaseDetection(); // 이건 ywlee가 해결해야함


		char key = waitKey(nDelay);
		if (key == 27) // ESC 는 종료
			break;
		//		else if (key >= '1' && key <= '4') // resize를 조정하기 위해서.
		//			nSize = (double)(key - '0');
		else if (key == 'l' || key == 'L')
			flgOnOff[0] = !flgOnOff[0];
		else if (key == 's' || key == 'S')
			flgOnOff[1] = !flgOnOff[1];
		else if (key == 'v' || key == 'V')
			flgOnOff[2] = !flgOnOff[2];
		else if (key == 'p' || key == 'P')
			flgOnOff[3] = !flgOnOff[3];
		else if (key == ' ') // play 하고 pause 를 toggle
			nDelay = 1 - nDelay;
		else if (key == ']') // pause하고, 다음 프레임으로 이동
		{
			nDelay = 0;
			(*objLoader)++;
		}
		else if (key == '[') // pause하고, 이전 프레임으로 이동
		{
			nDelay = 0;
			(*objLoader)--;
		}
		else if (key == 8) // backspace 는 첫 프레임으로
		{
			objLoader->GoToSpecificFrame(0);
			continue;
		}

		if (nDelay != 0) // 만약 play 상태일 때 맨 마지막 프레임에 도달했으면 pause상태로 변경
			(*objLoader)++ ? nDelay = 1 : nDelay = 0;
	}

	return 0;
}